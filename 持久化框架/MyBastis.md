# MyBastis

1.MyBastis是什么？

- ​	是个后端框架框架
- 是一个封装JDBC的持久化框架

2.有毛用？

- 封装了JDBC
- JDBC多个步骤是完全一致的，应该被复用。（数据源+apache DbUtil）
- 还要写SQL,SQL语句固定在代码。
- 结果集转化为对象仍然不方便。
- 开发Dao的工作量偏大。
- 让完全不懂数据库的开发者，以卖你想对象的方式操作数据库。

3.MyBastis怎么用？

- ​	引jar包
- 配置
  - 配置文件
  - 映射文件
- 常用类和接口
  - sqlSession 
    - 完全一面向对象方式封装了CRUD
    - 

4.mybatis映射的细节



​	![image-20211126083251096](https://gitee.com/junyoung2012/blogimg/raw/master/image-20211126083251096.png)

#与$的区别？

- ​	都可以咋mybatis中绑定参数
- $是利用拼接字符串，#是利用？后set参数
- #默认使用prepareStatment,$默认的Statment
- sql注入漏洞。

inser

- ​	useGeneratedKeys
- keyProperty

selectKey

​	MyBatis 有另外一种方法来处理数据库不支持自动生成类型，或者可能 JDBC 驱动不支 持自动生成主键时的主键生成问题。

- ​	order

select

- ​	resultMap
- statementType
- resultSetType

resultMap

- id 
- result 
- association 包含对象（多对一）
  - property
  - resultMap 也可以配置好的对象
- constructor  强制使用某个构造方法，装填结果对象
- collection （处理一对多）
- fetchType="lazy" （延迟加载，懒加载）
  - 对于并不是必须使用数据，延迟加载，有利于提高效率
- discriminator 用来表达继承关系

5.动态SQL

- ​	if

![image-20211126095820359](https://gitee.com/junyoung2012/blogimg/raw/master/image-20211126095820359.png)

- ```sql
  <select id=”findActiveBlogLike”
  parameterType=”Blog” resultType=”Blog”>
  SELECT * FROM BLOG WHERE 1=1  
  <if test=”title != null”>
   and title like #{title}
  </if>
  <if test=”author != null and author.name != null”>
  AND title like #{author.name}
  </if>
  </select>
  ```

- 

- choose(when,otherwise)

- ```sql
  <select id=”findActiveBlogLike”
  parameterType=”Blog” resultType=”Blog”>
  SELECT * FROM BLOG WHERE state = „ACTIVE‟
  <choose>
  <when test=”title != null”>
  AND title like #{title}
  </when>
  <when test=”author != null and author.name != null”>
  AND title like #{author.name}
  </when>
  <otherwise>
  AND featured = 1
  </otherwise>
  </choose>
  </select>
  ```

- 

- trim(where,set)

- ```sql
  <where>
  <if test=”state != null”>
  state = #{state}
  </if>
  <if test=”title != null”>
  AND title like #{title}
  </if>
  <if test=”author != null and author.name != null”>
  AND title like #{author.name}
  </if>
  </where>
  
  ```

- ```sql
  <trim  prefix="WHERE" prefixOverrides="AND |OR ">
  <if test=”state != null”>
  and state = #{state}
  </if>
  <if test=”title != null”>
  AND title like #{title}
  </if>
  <if test=”author != null and author.name != null”>
  AND title like #{author.name}
  </if>
  </where>
  
  ```

- ```sql
  <update id="updateAuthorIfNecessary"
  parameterType="domain.blog.Author">
  update Author
  <set>
  <if test="username != null">username=#{username},</if>
  <if test="password != null">password=#{password},</if>
  <if test="email != null">email=#{email},</if>
  <if test="bio != null">bio=#{bio}</if>
  </set>
  where id=#{id}
  </update>
  
  ```

- 

- 

- foreach

6.Mybatis与Spring的结合

- ​	不用实现Dao
- 定义接口
- 定义和接口像对应的映射文件
- 只需要配置好Spring,Spring就可以得到sqlSession,sqlSesionFactory,并且自动的产生Dao的实现类,把这个实现类的载入Spring容器。
- 引包

7.缓存

- ​	MyBatis 也有一级缓存和二级缓存.

- 一级缓存也叫本地缓存，MyBatis 的一级缓存是在会话（SqlSession）层面进行缓存的。MyBatis 的一级缓存是默认开启的，不需要任何的配置。

- 把对象缓存在同一SqlSession里，如果我们取查同一id对象，不重新读数据库

- 二级缓存是用来解决一级缓存不能跨会话共享的问题的，范围是namespace 级别的，可以被多个SqlSession 共享（只要是同一个接口里面的相同方法，都可以共享）

- ```
  常用的缓存淘汰策略有以下
  
  先进先出算法（FIFO）
  Least Frequently Used（LFU）
  淘汰一定时期内被访问次数最少的页面，以次数作为参考
  Least Recently Used（LRU）
  淘汰最长时间未被使用的页面，以时间作为参考
  
  ```

- 

8日志系统

- ``` 
  ALL < DEBUG < INFO <WARN < ERROR < FATAL < OFF
  Log4j建议只使用四个级别，优先级从高到低分别是ERROR、WARN、INFO、DEBUG。通过在这里定义的级别，可以控制到应用程序中相应级别的日志信息的开关。比如在这里定义了INFO级别，则应用程序中所有DEBUG级别的日志信息将不被打印出来
  
  ```

- ```
  org.apache.log4j.ConsoleAppender（控制台）
  org.apache.log4j.FileAppender（文件）
  org.apache.log4j.DailyRollingFileAppender（每天产生一个日志文件）
  org.apache.log4j.RollingFileAppender（文件大小到达指定尺寸的时候产生一个新的文件）
  org.apache.log4j.WriterAppender（将日志信息以流格式发送到任意指定的地方）
  ```

- Log4j提供的layout有以下几种：

  org.apache.log4j.HTMLLayout（以HTML表格形式布局），
  org.apache.log4j.PatternLayout（可以灵活地指定布局模式），
  org.apache.log4j.SimpleLayout（包含日志信息的级别和信息字符串），
  org.apache.log4j.TTCCLayout（包含日志产生的时间、线程、类别等等信息）

- 常用的日志

  - Log4j

  - SLF4J（ Simple Logging Facade for Java）简单日志门面：门面模式

  - Commons Logging

  - Logback 

    
