# Vue 3 之研究

1.vue是什么？

- ​	Vue (读音 /vjuː/，类似于 **view**)
- 是前端MVC(M-V-VM view model)框架
  - Angula JS
  - React
  - VUE
- <img src="D:\J1704\frontEnd\images\4afbfbedab64034f29596c8ba6c379310b551da2" alt="å°¤é¨æºª" style="zoom:33%;" />

尤雨溪，[前端](https://baike.baidu.com/item/前端/5956545)框架[Vue.js](https://baike.baidu.com/item/Vue.js/19884851)的作者，[HTML5](https://baike.baidu.com/item/HTML5/4234903)版Clear的打造人，独立开源开发者。曾就职于Google Creative Labs和Meteor Development Group。由于工作中大量接触开源的[JavaScript](https://baike.baidu.com/item/JavaScript/321142)项目，最后自己也走上了开源之路，现全职开发和维护Vue.js

尤雨溪毕业于上海[复旦附中](https://baike.baidu.com/item/复旦附中/6563044)，在美国完成大学学业，本科毕业于Colgate University，后在Parsons设计学院获得Design & Technology[艺术硕士](https://baike.baidu.com/item/艺术硕士/6203783)学位，任职于纽约Google Creative Lab。

尤雨溪大学专业并非是[计算机专业](https://baike.baidu.com/item/计算机专业/10586245)，在大学期间他学习专业是室内艺术和艺术史，后来读了[美术设计](https://baike.baidu.com/item/美术设计/3496001)和技术的硕士，正是在读硕士期间，他偶然接触到了JavaScript ，从此被这门[编程语言](https://baike.baidu.com/item/编程语言/9845131)深深吸引，开启了自己的[前端](https://baike.baidu.com/item/前端/5956545)生涯。 [3] 

2014年2月，开发了一个前端开发库Vue.js。Vue.js 是构建 Web 界面的 [JavaScript](https://baike.baidu.com/item/JavaScript) 框架，是一个通过简洁的API提供高效的数据绑定和灵活的组件系统。

2.Vue的特色

- ​	符合MVC结构
- 它可以双向绑定
  - Vue是怎么做到双向绑定的？
    - 界面引起数据变化 通过监听元素事件
    - 数据引起界面刷新
      - vue3 是通过 proxy 利用aop增加了属性set功能，刷新view
      - vue Object.defineProperty 重新定义原来的属性，来添加界面刷新功能。

# vue中v-if和v-show的区别

 \1. v-show 只是简单的控制元素的 display 属性，而 v-if 才是条件渲染（条件为真，元素将会被渲染，条件为假，元素会被销毁）；

  \2. v-show 有更高的首次渲染开销，而 v-if 的首次渲染开销要小的多；

  \3. v-if 有更高的切换开销，v-show 切换开销小；

  \4. v-if 有配套的 v-else-if 和 v-else，而 v-show 没有

  \5. v-if 可以搭配 template 使用，而 v-show 不行



## 如何解决跨域问题？

**跨域**，指的是浏览器不能执行其他网站的脚本。它是由浏览器的同源策略造成的，是浏览器施加的安全限制。

所谓同源是指，**域名，协议，端口**均相同，只要有一个不同，就是跨域。

跨域会阻止什么操作？

浏览器是从两个方面去做这个同源策略的，一是针对接口的请求，二是针对Dom的查询

1.阻止接口请求比较好理解，比如用ajax从http://192.168.100.150:8020/实验/jsonp.html页面向http://192.168.100.150:8081/zhxZone/webmana/dict/jsonp发起请求，由于两个url端口不同，所以属于跨域，在console打印台会报No 'Access-Control-Allow-Origin' header is present on the requested resource

![img](D:\J1704\frontEnd\images\20181207092232976.png)

值得说的是虽然浏览器禁止用户对请求返回数据的显示和操作，但浏览器确实是去请求了，如果服务器没有做限制的话会返回数据的，在调试模式的network中可以看到返回状态为200，且可看到返回数据
**2.阻止dom获取和操作**

关于iframe中对象的获取方式请看：js iframe获取documen中的对象为空问题_lianzhang861的博客-CSDN博客_获取iframe的document

比如a页面中嵌入了iframe，src为不同源的b页面，则在a中无法操作b中的dom，也没有办法改变b中dom中的css样式。

而如果ab是同源的话是可以获取并操作的。



跨域的解决方案

- ​	document.domain + iframe    (只有在主域相同的时候才能使用该方法)

- 动态创建script这个没什么好说的，因为script标签不受同源策略的限制

- location.hash + iframe

- 原理是利用location.hash来进行传值。

  假设域名a.com下的文件cs1.html要和cnblogs.com域名下的cs2.html传递信息。
  1) cs1.html首先创建自动创建一个隐藏的iframe，iframe的src指向cnblogs.com域名下的cs2.html页面
  2) cs2.html响应请求后再将通过修改cs1.html的hash值来传递数据
  3) 同时在cs1.html上加一个定时器，隔一段时间来判断location.hash的值有没有变化，一旦有变化则获取获取hash值
  注：由于两个页面不在同一个域下IE、Chrome不允许修改parent.location.hash的值，所以要借助于a.com域名下的一个代理iframe

  

  - CORS

  CORS背后的思想，就是使用自定义的HTTP头部让浏览器与服务器进行沟通，从而决定请求或响应是应该成功，还是应该失败。
  
  
  
  JSONP包含两部分：回调函数和数据。只能get
  
  回调函数是当响应到来时要放在当前页面被调用的函数。
  
  数据就是传入回调函数中的json数据，也就是回调函数的参数了。
  
  - 1）安全问题(请求代码中可能存在安全隐患)
  
    2）要确定jsonp请求是否失败并不容易
  
  
  
  - web sockets是一种浏览器的API，它的目标是在一个单独的持久连接上提供全双工、双向通信。(同源策略对web sockets不适用)
  
    web sockets原理：在JS创建了web socket之后，会有一个HTTP请求发送到浏览器以发起连接。取得服务器响应后，建立的连接会使用HTTP升级从HTTP协议交换为web sockt协议。
  
    只有在支持web socket协议的服务器上才能正常工作。
  
- springBoot解决跨域

  - ```java
    import org.springframework.context.annotation.Bean;  
    import org.springframework.context.annotation.Configuration;  
    import org.springframework.web.cors.CorsConfiguration;  
    import org.springframework.web.cors.UrlBasedCorsConfigurationSource;  
    import org.springframework.web.filter.CorsFilter;  
      
    @Configuration  
    public class CorsConfig {  
        private CorsConfiguration buildConfig() {  
            CorsConfiguration corsConfiguration = new CorsConfiguration();  
            corsConfiguration.addAllowedOrigin("*"); // 1允许任何域名使用
            corsConfiguration.addAllowedHeader("*"); // 2允许任何头
            corsConfiguration.addAllowedMethod("*"); // 3允许任何方法（post、get等） 
            return corsConfiguration;  
        }  
      
        @Bean  
        public CorsFilter corsFilter() {  
            UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();  
            source.registerCorsConfiguration("/**", buildConfig()); // 4  
            return new CorsFilter(source);  
        }  
    }
    ```

  - 

  - ```java
    import javax.servlet.*;  
    import javax.servlet.http.HttpServletResponse;  
    import java.io.IOException;  
       
    @Component  
    public class CorsFilter implements Filter {  
      
        final static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(CorsFilter.class);  
     
        public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {  
            HttpServletResponse response = (HttpServletResponse) res;  
            response.setHeader("Access-Control-Allow-Origin", "*");  
            response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");  
            response.setHeader("Access-Control-Max-Age", "3600");  
            response.setHeader("Access-Control-Allow-Headers", "x-requested-with");  
            System.out.println("*********************************过滤器被使用**************************");  
            chain.doFilter(req, res);  
        }  
        public void init(FilterConfig filterConfig) {}  
        public void destroy() {}  
    }
    ```

- VueRoute

  - ### 路由模式解析

    - hash 使用URL的hash值来作为路由。支持所有浏览器。
    - History: 以来HTML5 History API 和服务器配置。参考官网中HTML5 History模式
    - Abstract： 支持所有javascript运行模式。如果发现没有浏览器的API，路由会自动强制进入这个模式。

  - ### 两种模式的区别

    - hash模式背后的原理是`onhashchange`事件，可以在`window`对象上监听这个事件：

    - 更关键的一点是，因为hash发生变化的url都会被浏览器记录下来，从而你会发现浏览器的前进后退都可以用了，同时点击后退时，页面字体颜色也会发生变化。这样一来，尽管浏览器没有请求服务器，但是页面状态和url一一关联起来，后来人们给它起了一个霸气的名字叫**前端路由**，成为了单页应用标配。

    - 随着history api的到来，前端路由开始进化了，前面的**hashchange，你只能改变#后面的url片段，而history api则给了前端完全的自由**

    - 通过history api，我们丢掉了丑陋的#，但是它也有个毛病：

      　　不怕前进，不怕后退，就怕**刷新**，**f5**，（如果后端没有准备的话），因为刷新是实实在在地去请求服务器的。

VueRoute的使用步骤

- ​	安装依赖
  
- `npm install vue-router -S`
  
-   　创建路由规则

  - 

- 　绑定vue-router到vue实例

  - 修改main.js文件（全局性操作）

    - 引入route/index.js **router**

    - ```
      createApp(App).use(router).mount('#app')
      ```

  - 首先定义一个vue

  - 定义routeLink

  - ```
    <router-link to="/user">User</router-link>
    ```

  - 在route/index.js routes数组种添加，路由规则

- 嵌套路由

- 命名路由

- 导航守卫（在客户端进行权限验证）

  - beforeach




# vue的UI框架

- Element UI
- iView
- Mint UI
- BootstrapVue

ui框架的特点

- 浏览器兼容
- 响应式编程
- 网格布局
- 丰富的组件



https://www.cnblogs.com/joe235/p/12055463.html



vueshop的开发

​	1.首先cli 创建了vue的项目

- ​		vueRoute支持

2.添加，element ui的支持

​	npm 安装

推荐使用 npm 的方式安装，它能更好地和 [webpack](https://webpack.js.org/) 打包工具配合使用。

```shell
npm i element-ui -S
```

在 main.js 中写入以下内容：

```javascript
import Vue from 'vue';
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import App from './App.vue';

Vue.use(ElementUI);

new Vue({
  el: '#app',
  render: h => h(App)
});
```

3.引入全局css,和字体图标

```
import './assets/public.css'
```



4.引入axios ajax库

```
npm install axios
```

```
import axios from 'axios'

// 绑定ajax到$http
Vue.prototype.$http = axios
```



html5的Web Storage对象

​	和cookie相似，区别是它是为了更大容量存储设计的，cookie的大小是受限的，并且每次请求一个新的页面的时候cookie都会被发送过去。而web Storage仅仅是为了在本地“存储”数据而生

sessionStorage、localStorage、cookie都是在浏览器端存储的数据，其中sessionStorage的概念很特别，引入了一个“浏览器窗口”的概念，sessionStorage是在同源的同窗口中，始终存在的数据，也就是说只要这个浏览器窗口没有关闭，即使刷新页面或进入同源另一个页面，数据仍然存在，关闭窗口后，sessionStorage就会被销毁，同时“独立”打开的不同窗口，即使是同一页面，sessionStorage对象也是不同的

拥有setItem,getItem,removeItem,clear等方法

localStorage生命周期是永久，这意味着除非用户显示在浏览器提供的UI上清除localStorage信息，否则这些信息将永远存在

sessionStorage生命周期为当前窗口或标签页，一旦窗口或标签页被永久关闭了，那么所有通过sessionStorage存储的数据也就被清空了。

不同浏览器无法共享localStorage或sessionStorage中的信息。相同浏览器的不同页面间可以共享相同的 localStorage



