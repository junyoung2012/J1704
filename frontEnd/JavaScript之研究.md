JavaScript之研究

1.什么是JavaScript?

- 是一种开发语言，当前最红的语言。
- 能在客户端运行，也可以在服务端运行，在嵌入式环境也能运行。
- 为什么这样红???

2.js的helloword

​	js是什么样的语言？

​			

- ​	js是javaScript的简称
- ​	js是一个解释性语言 
- ​	js也是1995产生
- js和java没有直接的关系
- js netscap（网景）开发

​					解释器：

浏览器是它的重要解释器

node.js也是一个重要的解释器

- 是JDK
- 又是应用服务器
- npm 相当于maven,前端依赖管理工具

在node.js下运行

在浏览器运行

- js代码无法直接在浏览器种运行
- 必须嵌入到html代码中（脚本语言）
- 在浏览器中运行js的4种方式

js是一个解释性语言？

js是一个解释性语言

解释2中运行环境下的解释器

- V8引擎是一个`JavaScript引擎`实现，谷歌对其进行了`开源`。
- V8`使用C++开发`，在运行JavaScript之前，V8将其**编译**成原生**机器码**`（IA-32, x86-64, ARM, or MIPS CPUs），并且使用了如`内联缓存`（inline caching）等方法来`提高性能。
- 有了这些功能，JavaScript程序在V8引擎下的运行速度媲美二进制程序。

3.js基本语法

- 标识符

  - js和java基本相同的。

  - Javascript 的保留关键字不可以用作变量、标签或者函数名。有些保留关键字是作为 Javascript 以后扩展使用。

    | abstract | arguments    | boolean    | break      | byte         |
    | -------- | ------------ | ---------- | ---------- | ------------ |
    | case     | catch        | char       | class*     | const        |
    | continue | **debugger** | default    | **delete** | do           |
    | double   | else         | enum*      | **eval**   | export*      |
    | extends* | **false**    | final      | finally    | float        |
    | for      | **function** | goto       | if         | implements   |
    | import*  | in           | instanceof | int        | interface    |
    | let      | long         | native     | new        | null         |
    | package  | private      | protected  | public     | return       |
    | short    | static       | super*     | switch     | synchronized |
    | this     | throw        | throws     | transient  | true         |
    | try      | typeof       | var        | void       | volatile     |
    | while    | with         | **yield**  |            |              |

    \* 标记的关键字是 ECMAScript5 中新添加的。

- 变量、常量

  - 数据类型
    - js没有数据类型？错误的
    - js是弱类型语言
      - 一个变量时可以被赋值位多种数据类型
      - 数据类型可以默认转换
  - js中的变量
    - // js的变量必须先定义再使用
    - 变量定义的3中方式
      - var 
      - let
      - 赋值操作
      - let和var的区别？
        - let（const）是块变量
        - var的局部变量

- 常用运算

  - ```
    == 与 ===的区别？
    ```

  - 

4.js的结构化编程

- ​	3个结构
  - 顺序结构
    - js的回车符也是换行符
  - 分支结构
  - 重复结构
    - break和continue
- 函数
  - 函数
    - 定义
    - 调用
  - 类
  - 闭包
- 数组

5.js面向对象

- 定义类
  - 自定义class
    - 
  - js语言内置类
  - DOM(文档对象模型)
  - BOM
- 创建对象
- 封装
- 继承
- 多态

6.语言的常用类

- Object
- Math
- Date
- Array
- String
- Regexp
- Number
- Function

7.DOM

​	文档对象模型

​			时把HTML视作一棵文档树，

​			文档树上上的树，分支，叶子，属性，都是Node(节点)

​			就是把整个的HTML(XML)读入到内存当作一个树形结构来进行处理

- document 文档
- element 元素
- attrbute 属性
- Text 文本

8.BOM(浏览器对象模型)

​	![image-20211115105839613](https://gitee.com/junyoung2012/blogimg/raw/master/image-20211115105839613.png)

​	

9.Ajax

​	什么是ajax?（异步的js 和 xml技术）

- 前端技术
- 局部提交，局部刷新
- 利用xmlHttpRequest和innerHtml开创了新的时代

ajax用在什么地方？

- ​	验证用户
- 自动提示
- 聊天
- 投票显示
- 站内信提示

原生态ajax的原理

- ​	6个步骤。
- ​	异步和同步区别

10.ES5 

- 严格模式

  - ECMAscript 5添加了第二种运行模式

  - 这种模式使得Javascript在更严格的条件下运行。

  - **进入标志**

    - ```javascript
      "use strict";
      ```

  - 语法和行为改变

    - **全局变量显式声明**
    - **静态绑定**
      - 禁止使用with语句
      - 创设eval作用域
    - **增强的安全措施**
      - 禁止this关键字指向全局对象  this的值为undefined
      - 禁止在函数内部遍历调用栈
    - 禁止删除变量
    - **显式报错**
    - 对禁止扩展的对象添加新属性，会报错。
    - 严格模式下，删除一个不可删除的属性，会报错。

  -  **重名错误**

    - 对象不能有重名的属性
    - 函数不能有重名的参数
    - 禁止八进制表示法     10,0B1001,0x1E,010

  - **arguments对象的限制**

    - 不允许对arguments赋值
    - arguments不再追踪参数的变化
    - 禁止使用arguments.callee

  - **函数必须声明在顶层**

    - 不允许在非函数的代码块内声明函数。

  - 增加了保留字

    - implements, interface, let, package, private, protected, public, static, yield。

    

- 自启动函数

  - 自己定义的函数，js环境启动后能够直接被调用。
  - 为什么要使用自启动函数？
    - 给变量一个特定的作用空间
    - 消除了js全局变量作用域不确定的危险。

- **闭包**

  - 什么是闭包
    - js中的数据结构
    - 一个返回类型是函数的函数。
    - 限制一个作用域
    - 存储的变量的声明范围大于作用范围。

11.Fetch的研究（ajax的新实现方式）

12.ES6的新特性