前端框架-jQuery

1.什么是jQuery?

- ​	是一种前端框架
- DOM封装框架-对DOM操作简化的框架

2.为什么要使用jQuery ?

- 浏览器兼容
- 强大的选择器
- 简化了DOM操作
- 封装了ajax
- 丰富的生态圈，丰富的第三方插件
- 丰富的动画效果

3.常用jQuery操作

- HTML 元素选取
  HTML 元素操作
  CSS 操作
  HTML 事件函数
  JavaScript 特效和动画
  HTML DOM 遍历和修改
  AJAX

4.jQuery的helloWorld

5.jQuery的选择器

- 基本选择器
  - id选择器
  - class选择器
  - 标签选择器
  - 选择器组
- 层级选择器
  - 后代选择器
  - 子选择器
  - next选择器
  - next all 选择器
- 过滤性选择器
  - 基本
  - 内容
  - 可见性
  - 属性
  - 子元素
  - 表单属性
- 表单选择器
  - ：input
  - :button

6.DOM操作

- ​	dom对象转jquery对象 $(dom对象)
- jQuery对象转DOM
  - dom数组each
  - dom数组，$("li").get(0)
  - $("li")[0]
- 获取对象在jQuery数组的位置
  - index([selector|element])
- text/htmlval//css
- css操作
  - //css
  - class
- 特殊属性
  - height/width.position/offset/scrollTop
- 属性
  - prop/attr