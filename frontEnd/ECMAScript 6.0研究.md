# ECMAScript 6.0研究

ES6， 全称 ECMAScript 6.0 ，是 JavaScript 的下一个版本标准，**2015**.06 发版。

ES6的新特性

- let和Const

  - 块级作用域

  - ```javascript
    {
      let a = 0;
      var b = 1;
    }
    a  // ReferenceError: a is not defined
    b  // 1
    ```

  - 不能重复声明

  - ```javascript
    let a = 1;
    let a = 2;// Identifier 'a' has already been declared
    var b = 3;
    var b = 4;
    a  
    b  // 4
    ```

  - 不存在变量提升

  - ```javascript
    console.log(a);  //ReferenceError: a is not defined
    let a = "apple";
     
    console.log(b);  //undefined
    var b = "banana";
    ```

- 解构赋值

- ## Symbol

- MAP and SET

  - Maps 和 Objects 的区别
    一个 Object 的键只能是字符串或者 Symbols，但一个 Map 的键可以是任意值。
    Map 中的键值是有序的（FIFO 原则），而添加到对象中的键则不是。
    Map 的键值对个数可以从 size 属性获取，而 Object 的键值对个数只能手动计算。
    Object 都有自己的原型，原型链上的键名有可能和你自己在对象上的设置的键名产生冲突。

- for in遍历的是数组的索引（即键名），
  而for of遍历的是数组元素值。

- for in的一些**缺陷**:

  1. 索引是字符串型的数字，因而不能直接进行集合运算
  2. 遍历顺序可能不是实际的内部顺序
  3. for in会遍历数组所有的可枚举属性，包括原型。例如的原型方法method和name属性

- **for of 不遍历method和name,适合用来遍历数组**

  - **for of不支持普通对象**

  - **for of 遍历数组**

    **for in 遍历对象**

## Set 对象

Set 对象允许你存储任何类型的唯一值，无论是原始值或者是对象引用。用来消除重复。

- ​	反射：
  ​		创建新对象
  - 改变、访问对象的值
  - 动态删除方法
  - 动态调用方法

## 字符串

- ​	**includes()**：

- **startsWith()**

- **endsWith()**

- repeat

- **padStart**

- **padEnd**

- ### 模板字符串


## ES6 对象

- ​	属性简写
- 方法的简写
- ... 对象合并

## ES6 数组

- ​	... 复制数组
- Array.of() 参数转数组
- fill

## 函数

- ​	参数默认值
- 可变参数
- **箭头函数**
  - 简化书写方式
  - this 定义时的环境

## ES6 Class 类

- class关键字
  - constructor
  - static修饰类方法
- get/set
- extends
  - super

## ES6 模块

## Promise 

​	Promise 是一个对象，从它可以获取异步操作的消息

- ​	让js有了多线程的执行能力
- Promise异步执行并且根据结果执行回调函数的能力





![img](https://gitee.com/junyoung2012/blogimg/raw/master/upload-images.jianshu.io&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg)

执行栈选择最先进入队列的宏任务（一般都是script），执行其同步代码直至结束；
检查是否存在微任务，有则会执行至微任务队列为空；
如果宿主为浏览器，可能会渲染页面；
开始下一轮tick(事件循环)，执行宏任务中的异步代码（setTimeout等回调）。

 Promise 链式编程最好保持扁平化，不要嵌套 Promise。

有了 Promise 对象，就可以将异步操作以同步操作的流程表达出来，避免了层层嵌套的回调函数。此外，Promise 对象提供统一的接口，使得控制异步操作更加容易。





## fetch

js中也有官方新的ajax接口

```
fetch('http://example.com/movies.json')
  .then(response => response.json())
  .then(data => console.log(data));
```

```javascript
// Example POST method implementation:
async function postData(url = '', data = {}) {
  // Default options are marked with *
  const response = await fetch(url, {
    method: 'POST', // *GET, POST, PUT, DELETE, etc.
    mode: 'cors', // no-cors, *cors, same-origin
    cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
    credentials: 'same-origin', // include, *same-origin, omit
    headers: {
      'Content-Type': 'application/json'
      // 'Content-Type': 'application/x-www-form-urlencoded',
    },
    redirect: 'follow', // manual, *follow, error
    referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
    body: JSON.stringify(data) // body data type must match "Content-Type" header
  });
  return response.json(); // parses JSON response into native JavaScript objects
}

postData('https://example.com/answer', { answer: 42 })
  .then(data => {
    console.log(data); // JSON data parsed by `data.json()` call
  });
```

### [上传 JSON 数据](https://developer.mozilla.org/zh-CN/docs/Web/API/Fetch_API/Using_Fetch#上传_json_数据)

使用 [`fetch()`](https://developer.mozilla.org/zh-CN/docs/Web/API/fetch) POST JSON数据

```
const data = { username: 'example' };

fetch('https://example.com/profile', {
  method: 'POST', // or 'PUT'
  headers: {
    'Content-Type': 'application/json',
  },
  body: JSON.stringify(data),
})
.then(response => response.json())
.then(data => {
  console.log('Success:', data);
})
.catch((error) => {
  console.error('Error:', error);
});
```

Copy to Clipboard

### [上传文件](https://developer.mozilla.org/zh-CN/docs/Web/API/Fetch_API/Using_Fetch#上传文件)

可以通过 HTML `<input type="file" />` 元素，[`FormData()`](https://developer.mozilla.org/zh-CN/docs/Web/API/FormData/FormData) 和 [`fetch()`](https://developer.mozilla.org/zh-CN/docs/Web/API/fetch) 上传文件。

```
const formData = new FormData();
const fileField = document.querySelector('input[type="file"]');

formData.append('username', 'abc123');
formData.append('avatar', fileField.files[0]);

fetch('https://example.com/profile/avatar', {
  method: 'PUT',
  body: formData
})
.then(response => response.json())
.then(result => {
  console.log('Success:', result);
})
.catch(error => {
  console.error('Error:', error);
});
```

Copy to Clipboard

### [上传多个文件](https://developer.mozilla.org/zh-CN/docs/Web/API/Fetch_API/Using_Fetch#上传多个文件)

可以通过 HTML `<input type="file" multiple />` 元素，[`FormData()`](https://developer.mozilla.org/zh-CN/docs/Web/API/FormData/FormData) 和 [`fetch()`](https://developer.mozilla.org/zh-CN/docs/Web/API/fetch) 上传文件。

```
const formData = new FormData();
const photos = document.querySelector('input[type="file"][multiple]');

formData.append('title', 'My Vegas Vacation');
for (let i = 0; i < photos.files.length; i++) {
  formData.append(`photos_${i}`, photos.files[i]);
}

fetch('https://example.com/posts', {
  method: 'POST',
  body: formData,
})
.then(response => response.json())
.then(result => {
  console.log('Success:', result);
})
.catch(error => {
  console.error('Error:', error);
});
```

Copy to Clipboard

### [逐行处理文本文件](https://developer.mozilla.org/zh-CN/docs/Web/API/Fetch_API/Using_Fetch#逐行处理文本文件)

从响应中读取的分块不是按行分割的，并且是 `Uint8Array` 数组类型（不是字符串类型）。如果你想通过 `fetch()` 获取一个文本文件并逐行处理它，那需要自行处理这些复杂情况。以下示例展示了一种创建行迭代器来处理的方法（简单起见，假设文本是 UTF-8 编码的，且不处理 `fetch()` 的错误）。

```
async function* makeTextFileLineIterator(fileURL) {
  const utf8Decoder = new TextDecoder('utf-8');
  const response = await fetch(fileURL);
  const reader = response.body.getReader();
  let { value: chunk, done: readerDone } = await reader.read();
  chunk = chunk ? utf8Decoder.decode(chunk) : '';

  const re = /\n|\r|\r\n/gm;
  let startIndex = 0;
  let result;

  for (;;) {
    let result = re.exec(chunk);
    if (!result) {
      if (readerDone) {
        break;
      }
      let remainder = chunk.substr(startIndex);
      ({ value: chunk, done: readerDone } = await reader.read());
      chunk = remainder + (chunk ? utf8Decoder.decode(chunk) : '');
      startIndex = re.lastIndex = 0;
      continue;
    }
    yield chunk.substring(startIndex, result.index);
    startIndex = re.lastIndex;
  }
  if (startIndex < chunk.length) {
    // last line didn't end in a newline char
    yield chunk.substr(startIndex);
  }
}

async function run() {
  for await (let line of makeTextFileLineIterator(urlOfFile)) {
    processLine(line);
  }
}

run();
```

Copy to Clipboard

### [检测请求是否成功](https://developer.mozilla.org/zh-CN/docs/Web/API/Fetch_API/Using_Fetch#检测请求是否成功)

如果遇到网络故障或服务端的 CORS 配置错误时，[`fetch()`](https://developer.mozilla.org/zh-CN/docs/Web/API/fetch) promise 将会 reject，带上一个 [`TypeError`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/TypeError) 对象。虽然这个情况经常是遇到了权限问题或类似问题——比如 404 不是一个网络故障。想要精确的判断 `fetch()` 是否成功，需要包含 promise resolved 的情况，此时再判断 [`Response.ok`](https://developer.mozilla.org/zh-CN/docs/Web/API/Response/ok) 是否为 true。类似以下代码：

```
fetch('flowers.jpg')
  .then(response => {
    if (!response.ok) {
      throw new Error('Network response was not OK');
    }
    return response.blob();
  })
  .then(myBlob => {
    myImage.src = URL.createObjectURL(myBlob);
  })
  .catch(error => {
    console.error('There has been a problem with your fetch operation:', error);
  });
```

Copy to Clipboard

### [自定义请求对象](https://developer.mozilla.org/zh-CN/docs/Web/API/Fetch_API/Using_Fetch#自定义请求对象)

除了传给 `fetch()` 一个资源的地址，你还可以通过使用 [`Request()`](https://developer.mozilla.org/zh-CN/docs/Web/API/Request/Request) 构造函数来创建一个 request 对象，然后再作为参数传给 `fetch()`：

```
const myHeaders = new Headers();

const myRequest = new Request('flowers.jpg', {
  method: 'GET',
  headers: myHeaders,
  mode: 'cors',
  cache: 'default',
});

fetch(myRequest)
  .then(response => response.blob())
  .then(myBlob => {
    myImage.src = URL.createObjectURL(myBlob);
  });
```

Copy to Clipboard

`Request()` 和 `fetch()` 接受同样的参数。你甚至可以传入一个已存在的 request 对象来创造一个拷贝：

```
const anotherRequest = new Request(myRequest, myInit);
```

Copy to Clipboard

这个很有用，因为 request 和 response bodies 只能被使用一次（译者注：这里的意思是因为设计成了 stream 的方式，所以它们只能被读取一次）。创建一个拷贝就可以再次使用 request/response 了，当然也可以使用不同的 `init` 参数。创建拷贝必须在读取 body 之前进行，而且读取拷贝的 body 也会将原始请求的 body 标记为已读。

**备注：** [`clone()`](https://developer.mozilla.org/zh-CN/docs/Web/API/Request/clone) 方法也可以用于创建一个拷贝。它和上述方法一样，如果 request 或 response 的 body 已经被读取过，那么将执行失败。区别在于， `clone()` 出的 body 被读取不会导致原 body 被标记为已读取。

## [Headers](https://developer.mozilla.org/zh-CN/docs/Web/API/Fetch_API/Using_Fetch#headers)

使用 [`Headers`](https://developer.mozilla.org/zh-CN/docs/Web/API/Headers) 的接口，你可以通过 [`Headers()`](https://developer.mozilla.org/zh-CN/docs/Web/API/Headers/Headers) 构造函数来创建一个你自己的 headers 对象。一个 headers 对象是一个简单的多键值对：

```
const content = 'Hello World';
const myHeaders = new Headers();
myHeaders.append('Content-Type', 'text/plain');
myHeaders.append('Content-Length', content.length.toString());
myHeaders.append('X-Custom-Header', 'ProcessThisImmediately');
```

Copy to Clipboard

也可以传入一个多维数组或者对象字面量：

```
const myHeaders = new Headers({
  'Content-Type': 'text/plain',
  'Content-Length': content.length.toString(),
  'X-Custom-Header': 'ProcessThisImmediately'
});
```

Copy to Clipboard

它的内容可以被获取：

```
console.log(myHeaders.has('Content-Type')); // true
console.log(myHeaders.has('Set-Cookie')); // false
myHeaders.set('Content-Type', 'text/html');
myHeaders.append('X-Custom-Header', 'AnotherValue');

console.log(myHeaders.get('Content-Length')); // 11
console.log(myHeaders.get('X-Custom-Header')); // ['ProcessThisImmediately', 'AnotherValue']

myHeaders.delete('X-Custom-Header');
console.log(myHeaders.get('X-Custom-Header')); // null
```

Copy to Clipboard

虽然一些操作只能在 [`ServiceWorkers`](https://developer.mozilla.org/zh-CN/docs/Web/API/Service_Worker_API) 中使用，但是它提供了更方便的操作 Headers 的 API。

如果使用了一个不合法的 HTTP Header 属性名，那么 Headers 的方法通常都抛出 TypeError 异常。如果不小心写入了一个不可写的属性（[见下方](https://developer.mozilla.org/zh-CN/docs/Web/API/Fetch_API/Using_Fetch#guard)），也会抛出一个 TypeError 异常。除此以外的情况，失败了并不抛出异常。例如：

```
const myResponse = Response.error();
try {
  myResponse.headers.set('Origin', 'http://mybank.com');
} catch (e) {
  console.log('Cannot pretend to be a bank!');
}
```

Copy to Clipboard

最好在在使用之前检查内容类型 `content-type` 是否正确，比如：

```
fetch(myRequest)
  .then(response => {
     const contentType = response.headers.get('content-type');
     if (!contentType || !contentType.includes('application/json')) {
       throw new TypeError("Oops, we haven't got JSON!");
     }
     return response.json();
  })
  .then(data => {
      /* process your data further */
  })
  .catch(error => console.error(error));
```

Copy to Clipboard

### [Guard](https://developer.mozilla.org/zh-CN/docs/Web/API/Fetch_API/Using_Fetch#guard)

由于 Headers 可以在 request 中被发送或者在 response 中被接收，并且规定了哪些参数是可写的，Headers 对象有一个特殊的 guard 属性。这个属性没有暴露给 Web，但是它影响到哪些内容可以在 Headers 对象中被操作。

可能的值如下：

- `none`：默认的。
- `request`：从 request 中获得的 headers（[`Request.headers`](https://developer.mozilla.org/zh-CN/docs/Web/API/Request/headers)）只读。
- `request-no-cors`：从不同域（[`Request.mode`](https://developer.mozilla.org/zh-CN/docs/Web/API/Request/mode) `no-cors`）的 request 中获得的 headers 只读。
- `response`：从 response 中获得的 headers（[`Response.headers`](https://developer.mozilla.org/zh-CN/docs/Web/API/Response/headers)）只读。
- `immutable`：在 ServiceWorkers 中最常用的，所有的 headers 都只读。

**备注：** 你不可以添加或者修改一个 guard 属性是 `request` 的 Request Header 的 `Content-Length` 属性。同样地，插入 `Set-Cookie` 属性到一个 response header 是不允许的，因此，Service Worker 中，不能给合成的 Response 设置 cookie。

## [Response 对象](https://developer.mozilla.org/zh-CN/docs/Web/API/Fetch_API/Using_Fetch#response_对象)

如上所述，[`Response`](https://developer.mozilla.org/zh-CN/docs/Web/API/Response) 实例是在 `fetch()` 处理完 promise 之后返回的。

你会用到的最常见的 response 属性有：

- [`Response.status`](https://developer.mozilla.org/zh-CN/docs/Web/API/Response/status) — 整数（默认值为 200）为response的状态码。
- [`Response.statusText`](https://developer.mozilla.org/zh-CN/docs/Web/API/Response/statusText) — 字符串（默认值为 ""），该值与 HTTP 状态码消息对应。 注意：HTTP/2 [不支持](https://fetch.spec.whatwg.org/#concept-response-status-message)状态消息
- [`Response.ok`](https://developer.mozilla.org/zh-CN/docs/Web/API/Response/ok) — 如上所示，该属性是来检查 response 的状态是否在 200 - 299（包括200 和 299）这个范围内。该属性返回一个布尔值。

它的实例也可用通过 JavaScript 来创建，但只有在 [`ServiceWorkers`](https://developer.mozilla.org/zh-CN/docs/Web/API/Service_Worker_API) 中使用 [`respondWith()`](https://developer.mozilla.org/zh-CN/docs/Web/API/FetchEvent/respondWith) 方法并提供了一个自定义的 response 来接受 request 时才真正有用：

```
const myBody = new Blob();

addEventListener('fetch', event => {
  // ServiceWorker intercepting a fetch
  event.respondWith(
    new Response(myBody, {
      headers: { 'Content-Type': 'text/plain' }
    })
  );
});
```

Copy to Clipboard

[`Response()`](https://developer.mozilla.org/zh-CN/docs/Web/API/Response/Response) 构造方法接受两个可选参数—— response 的 body 和一个初始化对象（与[`Request()`](https://developer.mozilla.org/zh-CN/docs/Web/API/Request/Request) 所接受的 init 参数类似）。

**备注：** 静态方法 [`error()`](https://developer.mozilla.org/zh-CN/docs/Web/API/Response/error) 只是返回了错误的 response。与此类似地，[`redirect()`](https://developer.mozilla.org/zh-CN/docs/Web/API/Response/redirect) 只是返回了一个可以重定向至某 URL 的 response。这些也只与 Service Worker 有关。

## [Body](https://developer.mozilla.org/zh-CN/docs/Web/API/Fetch_API/Using_Fetch#body)

不管是请求还是响应都能够包含 body 对象。body 也可以是以下任意类型的实例。

- [`ArrayBuffer`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/ArrayBuffer)
- [`ArrayBufferView`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/TypedArray) (Uint8Array等)
- [`Blob`](https://developer.mozilla.org/zh-CN/docs/Web/API/Blob)/File
- string
- [`URLSearchParams`](https://developer.mozilla.org/zh-CN/docs/Web/API/URLSearchParams)
- [`FormData`](https://developer.mozilla.org/zh-CN/docs/Web/API/FormData)

Body 类定义了以下方法（这些方法都被 [`Request`](https://developer.mozilla.org/zh-CN/docs/Web/API/Request) 和 [`Response`](https://developer.mozilla.org/zh-CN/docs/Web/API/Response)所实现）以获取 body 内容。这些方法都会返回一个被解析后的 Promise 对象和数据。

- [`Request.arrayBuffer()` (en-US)](https://developer.mozilla.org/en-US/docs/Web/API/Request/arrayBuffer) / [`Response.arrayBuffer()`](https://developer.mozilla.org/zh-CN/docs/Web/API/Response/arrayBuffer)
- [`Request.blob()` (en-US)](https://developer.mozilla.org/en-US/docs/Web/API/Request/blob) / [`Response.blob()`](https://developer.mozilla.org/zh-CN/docs/Web/API/Response/blob)
- [`Request.formData()` (en-US)](https://developer.mozilla.org/en-US/docs/Web/API/Request/formData) / [`Response.formData()`](https://developer.mozilla.org/zh-CN/docs/Web/API/Response/formData)
- [`Request.json()` (en-US)](https://developer.mozilla.org/en-US/docs/Web/API/Request/json) / [`Response.json()`](https://developer.mozilla.org/zh-CN/docs/Web/API/Response/json)
- [`Request.text()` (en-US)](https://developer.mozilla.org/en-US/docs/Web/API/Request/text) / [`Response.text()`](https://developer.mozilla.org/zh-CN/docs/Web/API/Response/text)

相比于XHR，这些方法让非文本化数据的使用更加简单。

请求体可以由传入 body 参数来进行设置：

```
const form = new FormData(document.getElementById('login-form'));
fetch('/login', {
  method: 'POST',
  body: form
});
```

Copy to Clipboard

request 和 response（包括 `fetch()` 方法）都会试着自动设置 `Content-Type`。如果没有设置 `Content-Type` 值，发送的请求也会自动设值。

## [特性检测](https://developer.mozilla.org/zh-CN/docs/Web/API/Fetch_API/Using_Fetch#特性检测)

Fetch API 的支持情况，可以通过检测 [`Headers`](https://developer.mozilla.org/zh-CN/docs/Web/API/Headers), [`Request`](https://developer.mozilla.org/zh-CN/docs/Web/API/Request), [`Response`](https://developer.mozilla.org/zh-CN/docs/Web/API/Response) 或 [`fetch()`](https://developer.mozilla.org/zh-CN/docs/Web/API/fetch) 是否在 [`Window`](https://developer.mozilla.org/zh-CN/docs/Web/API/Window) 或 [`Worker`](https://developer.mozilla.org/zh-CN/docs/Web/API/Worker) 域中来判断。例如：

```
if (window.fetch) {
  // run my fetch request here
} else {
  // do something with XMLHttpRequest?
}
```

Copy to Clipboard

## [Polyfill](https://developer.mozilla.org/zh-CN/docs/Web/API/Fetch_API/Using_Fetch#polyfill)

如果要在不支持的浏览器中使用 Fetch，可以使用 [Fetch Polyfill](https://github.com/github/fetch)。

## [规范](https://developer.mozilla.org/zh-CN/docs/Web/API/Fetch_API/Using_Fetch#规范)

| 详细说明                                | 状态            | 注释               |
| :-------------------------------------- | :-------------- | :----------------- |
| [Fetch](https://fetch.spec.whatwg.org/) | Living Standard | Initial definition |

## [浏览器兼容性](https://developer.mozilla.org/zh-CN/docs/Web/API/Fetch_API/Using_Fetch#浏览器兼容性)

[Report problems with this compatibility data on GitHub](https://github.com/mdn/browser-compat-data/issues/new?body= %23%23%23%23+What+information+was+incorrect%2C+unhelpful%2C+or+incomplete%3F %23%23%23%23+What+did+you+expect+to+see%3F %23%23%23%23+Did+you+test+this%3F+If+so%2C+how%3F   MDN+page+report+details<%2Fsummary> *+Query%3A+`api.fetch` *+MDN+URL%3A+https%3A%2F%2Fdeveloper.mozilla.org%2Fzh-CN%2Fdocs%2FWeb%2FAPI%2FFetch_API%2FUsing_Fetch *+Report+started%3A+2021-12-07T03%3A44%3A32.734Z <%2Fdetails>&title=api.fetch+-+)

|                                         |    desktop     |     mobile     |           server           |                   |                                             |                  |                 |                |                     |                                             |                  |                  |                             |
| :-------------------------------------- | :------------: | :------------: | :------------------------: | :---------------: | :-----------------------------------------: | :--------------: | :-------------: | :------------: | :-----------------: | :-----------------------------------------: | :--------------: | :--------------: | --------------------------- |
|                                         |     Chrome     |      Edge      |          Firefox           | Internet Explorer |                    Opera                    |      Safari      | WebView Android | Chrome Android | Firefox for Android |                Opera Android                |  Safari on iOS   | Samsung Internet | Deno                        |
| `fetch`                                 | Full support42 | Full support14 |       Full support39       |   No supportNo    |               Full support29                | Full support10.1 | Full support42  | Full support42 |   Full support39    |               Full support29                | Full support10.3 | Full support4.0  | Full support1.0footnoteOpen |
| Support for blob: and data:Experimental | Full support48 | Full support79 |       Full support39       |   No supportNo    | Compatibility unknown; please update this.? | Full support10.1 | Full support43  | Full support48 |   Full support39    | Compatibility unknown; please update this.? | Full support10.3 | Full support5.0  | Full support1.9             |
| referrerPolicy                          | Full support52 | Full support79 |       Full support52       |   No supportNo    |               Full support39                | Full support11.1 | Full support52  | Full support52 |   Full support52    |               Full support41                |   No supportNo   | Full support6.0  | No supportNo                |
| signalExperimental                      | Full support66 | Full support16 |       Full support57       |   No supportNo    |               Full support53                | Full support11.1 | Full support66  | Full support66 |   Full support57    |               Full support47                | Full support11.3 | Full support9.0  | Full support1.11            |
| Streaming response bodyExperimental     | Full support43 | Full support14 | Full support64disabledOpen |   No supportNo    |               Full support29                | Full support10.1 | Full support43  | Full support43 |    No supportNo     |                No supportNo                 | Full support10.3 | Full support4.0  | Full support1.0             |
| Available in workers                    | Full support42 | Full support14 |       Full support39       |   No supportNo    |               Full support29                | Full support10.1 | Full support42  | Full support42 |   Full support39    |               Full support29                | Full support10.3 | Full support4.0  | Full supportYes             |

