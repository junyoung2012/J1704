linux初学教程

1.如何搭建linux系统

- ​	虚拟机
  - vmware 14
    - CPU 虚拟化
    - 随后安装操作系统
    - 网络设置
      - 没有网络
      - 直连主机
      - 桥接（相当于和主机同一子网）
      - nat转换
- 物理机
  - 驱动问题
- 云服务器
  - 阿里云
  - 腾讯云
  - 免费

2.如何选择操作系统

- 内核
  - 分发（发行版本）
  - ![image-20211112190740247](https://gitee.com/junyoung2012/blogimg/raw/master/image-20211112190740247.png)

3.买了云服务器

- ​	控制台
  - 管理
    - 可以选操作系统
      - centOS 7以上
    - 就是网络防火墙
      - SSH 远程控制
      - ftp
      - http
      - https
      - mysql 3306
    - 可以重启

4.远程连接

- ​	putty
- **SecureCRT**
- vnc view(图形界面)

5.配置网络

- 查看linux版本

- ``` 
  uname -a ",可显示电脑以及操作系统的相关信息
  cat /proc/version ch
  "cat /proc/version",说明正在运行的内核版本
  cat /etc/redhat-release  查看分发版本
  cat /etc/issue 查看分发版本
  ```

- 配置网络

  - 首先，通过执行命令ip addr，找到需要配置的网卡名。

    ![CentOS7 配置网络](https://gitee.com/junyoung2012/blogimg/raw/master/739bc049610f8b56c156f9dd9ce951e10ff8d3eb.jpg)

    临时配个IP地址

    ``` 
    ifconfig eth0 192.168.56.111 netmask 255.255.255.0 
    ```

    永久配置IP地址

    ``` 
     vim /etc/sysconfig/network-scripts/ifcfg-eth0
     
    ```

    ``` 
    
    ```

    ![image-20211112193837399](https://gitee.com/junyoung2012/blogimg/raw/master/image-20211112193837399.png)

    

修改后重启一下网络服务即可（无报错就是正常重启）

systemctl restart network

然后执行ip addr 确认IP配置

执行ping www.baidu.com 确认DNS解析正常



云服务器

​	公网 IP

  私网 IP(主要用于服务器集群)

![image-20211112194415798](https://gitee.com/junyoung2012/blogimg/raw/master/image-20211112194415798.png)

关闭linux主机防火墙

​	永久关闭

``` 
systemctl status firewalld.service

查看防火墙状态
输入命令：systemctl disable firewalld.service，禁止防火墙服务器
```

关闭seLinux

```
永久关闭selinux方法

 vim /etc/sysconfig/selinux
 
 找到行

SELINUX=enforcing

替换为

SELINUX=disabled
```





使用客户端连接linux

​	![image-20211112191926770](https://gitee.com/junyoung2012/blogimg/raw/master/image-20211112191926770.png)

​	

5.linux的启动过程

- 内核的引导。
- 运行 init。
  系统初始化。
  建立终端 。
  用户登录系统。

Linux系统有7个运行级别(runlevel)：

- 运行级别0：系统停机状态，系统默认运行级别不能设为0，否则不能正常启动
- 运行级别1：单用户工作状态，root权限，用于系统维护，禁止远程登陆
- 运行级别2：多用户状态(没有NFS)
- 运行级别3：完全的多用户状态(有NFS)，登陆后进入控制台命令行模式
- 运行级别4：系统未使用，保留
- 运行级别5：X11控制台，登陆后进入图形GUI模式
- 运行级别6：系统正常关闭并重启，默认运行级别不能设为6，否则不能正常启动

至于在每个运行级中将运行哪些守护进程，用户可以通过chkconfig或setup中的"System Services"来自行设定。



## Linux 关机

```
shutdown –h now 立马关机
halt 关闭系统
reboot 就是重启，等同于 shutdown –r now
```

​	 **登录时生效的环境变量配置文件**
在 [Linux](https://so.csdn.net/so/search?from=pc_blog_highlight&q=Linux) 系统登录时主要生效的环境变量配置文件有以下五个：

-  **/etc/profile** 全局配置（jdk在这配置）
-  /etc/profile.d/*.sh
- ~/.bash_profile
- **~/.bashrc**
-  /etc/bashrc

6.安装JDK

​	有没有JDK?

​		如何卸载

​	centOS安装软件的3中方式

- 交叉编译

- rpm 卸载软件

  - ### Linux之——rpm命令常用选项

    - ### 安装rpm包

    - *# rpm -ivh ***.rpm　　#其中i表示安装，v表示显示安装过程，h表示显示进度*

    - ### 查询软件包

      ``` 
      # rpm -q PACKAGE_NAME
      # rpm -qp ***.rpm 获取当前目录下的rpm包相关信息
      # rpm -qa | less 列出所有已安装的软件包
      # rpm -qf /usr/sbin/httpd 查看某个文件属于哪个软件包，可以是普通文件或可执行文件，跟文件的绝对路径
      # rpm -qi PACKAGE_NAME 列出已安装的这个包的标准详细信息
      # rpm -ql PACKAGE_NAME 列出rpm包的文件内容
      # rpm -q –scripts kernel | less 列出已安装rpm包自带的安装前和安装后脚本
      # rpm -qa –queryformat ‘Package %{NAME} was build on %{BUILDHOST}\n’ |less queryformat强大的查询
      # rpm –querytags | less 可以列出queryformat可以使用的所有变量从而组合成更强大的查询
      
      ```

      

- yum
  - 换yum源
  - 下载jdk
    - wget 命令，
  - 上传文件
    - rz命令上传命令
  - 解压文件
    - tar
  - 配置环境变量

文件目录操作命令

​		根目录下，有那些目录？

![img](https://gitee.com/junyoung2012/blogimg/raw/master/d0c50-linux2bfile2bsystem2bhierarchy.jpg)

**/bin**：
bin 是 Binaries (二进制文件) 的缩写, 这个目录存放着最经常使用的命令。

**/etc：**
etc 是 Etcetera(等等) 的缩写,这个目录用来存放所有的系统管理所需要的配置文件和子目录。



**/home**：
用户的主目录，在 Linux 中，每个用户都有一个自己的目录，一般该目录名是以用户的账号命名的，如上图中的 alice、bob 和 eve。



**/dev ：**
dev 是 Device(设备) 的缩写, 该目录下存放的是 Linux 的外部设备，在 Linux 中访问设备的方式和访问文件的方式是相同的。

**/mnt**：
系统提供该目录是为了让用户临时挂载别的文件系统的，我们可以将光驱挂载在 /mnt/ 上，然后进入该目录就可以查看光驱里的内容了。



**/opt**：
opt 是 optional(可选) 的缩写，这是给主机额外安装软件所摆放的目录。比如你安装一个ORACLE数据库则就可以放到这个目录下。默认是空的。



**/usr**：
 usr 是 unix shared resources(共享资源) 的缩写，这是一个非常重要的目录，用户的很多应用程序和文件都放在这个目录下，类似于 windows 下的 program files 目录。



**/var**：
var 是 variable(变量) 的缩写，这个目录中存放着在不断扩充着的东西，我们习惯将那些经常被修改的目录放在这个目录下。包括各种日志文件。





磁盘管理

查找

```  
whereis ,find ,grep、 which、 whereis、 locate
```



### linux 上传文件 rz命令 提示command not found 解决方法

``` 
# yum -y install lrzsz
```

解压**tar命令**

使用 tar --help命令能够阅读tar命令的详细用法解析。
示例
tar -cf archive.tar foo bar # 从文件 foo 和 bar 创建归档文件archive.tar。
tar -tvf archive.tar # 详细列举归档文件 archive.tar中的所有文件。
tar -xf archive.tar # 展开归档文件 archive.tar中的所有文件。
选项
1、 主要选项:
-A, --catenate, --concatenate 追加 tar 文件至归档
-c, --create 创建一个新归档
-d, --diff, --compare 找出归档和文件系统的差异
–delete 从归档(非磁带！)中删除
-r, --append 追加文件至归档结尾
-t, --list 列出归档内容
–test-label 测试归档卷标并退出
-u, --update 仅追加比归档中副本更新的文件
-x, --extract, --get 从归档中解出文件
2、压缩选项
-a, --auto-compress 使用归档后缀名来决定压缩程序
-I, --use-compress-program=PROG ，通过 PROG 过滤(必须是能接受 -d 选项的程序)
-j, --bzip2 通过 bzip2 压缩归档
-J, --xz 通过 xz 压缩归档
-z, --gzip, --gunzip, --ungzip 通过 gzip 压缩归档
-Z, --compress, --uncompress 通过 compress 压缩归档
3、辅助选项
-v, --verbose 详细地列出处理的文件
-f, --file=ARCHIVE 使用归档文件或 ARCHIVE 设备，该选项后必须接*打包的.tar
-----------------------------------




用户操作命令

文件权限设置

文本编辑工具vi,vim

​	![img](D:\J1704\frontEnd\images\vim-vi-workmodel.png)



/usr/jdk1.8.0_231

其它文本操作命令

​		grep

​		tac/cat 

​		more/less

​		head/tail 

​	如何读取日志文件？

- ​		很长
- 新产生在最后
- tail 、grep

​	如何复制，情况日志文件？

​	如何在vim中显示行号？

​	如何vi复制

​			yy 复制游标所在的那一行(常用)

​		p P 黏贴

删除dd



​	如何在vi查找？

​		/查找的词

​	n向下

N向上

​	

卸载openJDK

``` rpm -qa|grep jdk
rpm -qa|grep jdk

```

remove后面的参数是上面得到的结果.noarch结尾的包

```
yum -y remove copy-jdk-configs-3.3-10.el7_5.noarch
```

7.安装Tomcat

- ​	上传文件

- 解压文件到目录

- 修改目录名称

- 启动Tomcat

- 测试Tomcat,是否正常

  - wget http://localhost:8080

- 配置Tomcat自启动

- ``` 
  CATALINA_HOME=/opt/tomcat9
  
  CATALINA_BASE=/opt/tomcat9
  
  PATH=$PATH:$CATALINA_BASE/bin
  
  export PATH CATALINA_BASE CATALINA_HOME
  ```

- ```
  /etc/rc.d/rc.local 
  
  /opt/tomcat9/bin/startup.sh
  ```

- ```
  /usr/lib/systemd/system
  
  [Unit]
  
  Description=Tomcat
  
  After=syslog.target network.target remote-fs.target nss-lookup.target
  
    
  
  [Service]
  
  Type=oneshot
  
  ExecStart=/opt/tomcat9/bin/startup.sh
  
  ExecStop=/opt/tomcat9/bin/shutdown.sh
  
  ExecReload=/bin/kill -s HUP $MAINPID
  
  RemainAfterExit=yes
  
   
  
  [Install]
  
  WantedBy=multi-user.target
  ```

- ```
  systemctl enable tomcat9.service
  ```

- ``` 
  设置vi /opt/tomcat/bin/setclasspath.sh  
  
  export JAVA_HOME=/opt/java/jdk1.8.0_191
  export JRE_HOME=/opt/java/jdk1.8.0_191/jre
  ```

- ``` 
  systemctl start tomcat9.service    启动tomcat
  
  systemctl stop tomcat9.service    关闭tomcat   
  
  systemctl restart tomcat9.service    重启tomcat 
  ```

- 

8.安装mysql

- ​	检查是否安装过mysql

- ``` 
  rpm -pa | grep mysql
  
  rpm -pa | grep mariadb
  可能的显示结果如下：
  mariadb-libs-5.5.56-2.el7.x86_64  
  卸载数据库
  rpm -e mariadb-libs-5.5.56-2.el7.x86_64 #删除上面的程序
  ```

- https://dev.mysql.com/downloads/mysql/

- ![image-20211118191844461](https://gitee.com/junyoung2012/blogimg/raw/master/image-20211118191844461.png)

- mkdir mysq

- rpm -ivh mysql-community-common-8.0.27-1.el7.x86_64.rpm --nodeps --force

  ```
  [root@localhost mysql]#  rpm -ivh mysql-community-common-8.0.27-1.el7.x86_64.rpm --nodeps --force                       
  warning: mysql-community-common-8.0.27-1.el7.x86_64.rpm: Header V3 DSA/SHA256 Signature, key ID 5072e1f5: NOKEY
  Preparing...                          ################################# [100%]
  Updating / installing...
     1:mysql-community-common-8.0.27-1.e################################# [100%]
  [root@localhost mysql]#  rpm -ivh mysql-community-libs-8.0.27-1.el7.x86_64.rpm --nodeps --force                              
  warning: mysql-community-libs-8.0.27-1.el7.x86_64.rpm: Header V3 DSA/SHA256 Signature, key ID 5072e1f5: NOKEY
  Preparing...                          ################################# [100%]
  Updating / installing...
     1:mysql-community-libs-8.0.27-1.el7################################# [100%]
  [root@localhost mysql]#  rpm -ivh mysql-community-client-8.0.27-1.el7.x86_64.rpm --nodeps --force                            
  warning: mysql-community-client-8.0.27-1.el7.x86_64.rpm: Header V3 DSA/SHA256 Signature, key ID 5072e1f5: NOKEY
  Preparing...                          ################################# [100%]
  Updating / installing...
     1:mysql-community-client-8.0.27-1.e################################# [100%]
  [root@localhost mysql]#  rpm -ivh mysql-community-server-8.0.27-1.el7.x86_64.rpm --nodeps --force                              
  warning: mysql-community-server-8.0.27-1.el7.x86_64.rpm: Header V3 DSA/SHA256 Signature, key ID 5072e1f5: NOKEY
  Preparing...                          ################################# [100%]
  Updating / installing...
     1:mysql-community-server-8.0.27-1.e################################# [100%]
  [root@localhost mysql]# rpm -qa | grep mysql
  mysql-community-server-8.0.27-1.el7.x86_64
  mysql-community-common-8.0.27-1.el7.x86_64
  mysql-community-client-8.0.27-1.el7.x86_64
  mysql-community-libs-8.0.27-1.el7.x86_64
  [root@localhost mysql]# 
  
  ```

  ```
  [root@localhost mysql]# mysqld --initialize;
  [root@localhost mysql]# ll /var/lib/mysql
  total 176568
  -rw-r----- 1 root root       56 Nov 19 03:39 auto.cnf
  -rw------- 1 root root     1680 Nov 19 03:40 ca-key.pem
  -rw-r--r-- 1 root root     1112 Nov 19 03:40 ca.pem
  -rw-r--r-- 1 root root     1112 Nov 19 03:40 client-cert.pem
  -rw------- 1 root root     1676 Nov 19 03:40 client-key.pem
  -rw-r----- 1 root root   196608 Nov 19 03:40 #ib_16384_0.dblwr
  -rw-r----- 1 root root  8585216 Nov 19 03:39 #ib_16384_1.dblwr
  -rw-r----- 1 root root     5502 Nov 19 03:40 ib_buffer_pool
  -rw-r----- 1 root root 12582912 Nov 19 03:40 ibdata1
  -rw-r----- 1 root root 50331648 Nov 19 03:40 ib_logfile0
  -rw-r----- 1 root root 50331648 Nov 19 03:39 ib_logfile1
  drwxr-x--- 2 root root        6 Nov 19 03:40 #innodb_temp
  drwxr-x--- 2 root root      143 Nov 19 03:40 mysql
  -rw-r----- 1 root root 25165824 Nov 19 03:40 mysql.ibd
  drwxr-x--- 2 root root     8192 Nov 19 03:40 performance_schema
  -rw------- 1 root root     1676 Nov 19 03:40 private_key.pem
  -rw-r--r-- 1 root root      452 Nov 19 03:40 public_key.pem
  -rw-r--r-- 1 root root     1112 Nov 19 03:40 server-cert.pem
  -rw------- 1 root root     1680 Nov 19 03:40 server-key.pem
  drwxr-x--- 2 root root       28 Nov 19 03:40 sys
  -rw-r----- 1 root root 16777216 Nov 19 03:40 undo_001
  -rw-r----- 1 root root 16777216 Nov 19 03:40 undo_002
  [root@localhost mysql]# chown mysql:mysql /var/lib/mysql -R;
  [root@localhost mysql]# systemctl start mysqld.service;
  [root@localhost mysql]# systemctl status mysqld.service;
  ● mysqld.service - MySQL Server
     Loaded: loaded (/usr/lib/systemd/system/mysqld.service; enabled; vendor preset: disabled)
     Active: active (running) since Fri 2021-11-19 03:42:06 EST; 14s ago
       Docs: man:mysqld(8)
             http://dev.mysql.com/doc/refman/en/using-systemd.html
    Process: 17689 ExecStartPre=/usr/bin/mysqld_pre_systemd (code=exited, status=0/SUCCESS)
   Main PID: 17714 (mysqld)
     Status: "Server is operational"
     CGroup: /system.slice/mysqld.service
             └─17714 /usr/sbin/mysqld
  
  Nov 19 03:42:04 localhost.localdomain systemd[1]: Starting MySQL Server...
  Nov 19 03:42:06 localhost.localdomain systemd[1]: Started MySQL Server.
  [root@localhost mysql]# systemctl enable mysqld;
  [root@localhost mysql]# 
  ```

  

9.简单的shell

9.oracle

10.nginx

11.docker

​		