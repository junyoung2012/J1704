console.log(this)
this.a = 20;
var test = {
	a: 40,
	init: function() {
		function go() {
			console.log(this.a);
		}
		go.prototype.a = 50;
		return go;
	}
}
var p = test.init();
p();
new(test.init())();
