// js的继承
//js的封装
function Student(){
	var _name;
	this.setName=function(name){
		_name=name;
	}
	this.getName=function(){
		return _name;
	}
}

function BoyStudent(){
	var _power;
	this.setPower=function(power){
		_power=power;
	}
	this.getPower=function(){
		return _power;
	}
	
}


var stu=new Student();
stu.setName("李四");
BoyStudent.prototype=stu;
var boy=new BoyStudent();
boy.setPower(100);
// boy.setName()
// console.log(boy.getName());

console.log(boy.prototype);
console.log(boy.__proto__===BoyStudent.prototype);
console.log(BoyStudent.prototype);//Student对象
console.log(Student.prototype);//Student {}
console.log(Student.__proto__.__proto__.__proto__);//Function


// js的继承是一种动态扩展，是在运行中扩展了某个对象
// 原型是对象。js是基于原型继承的
// BoyStudent类->Student对象->Function对象-Object对象->null



