// js的Array（list，statck,dequeue）
// 长度可变
// 任意类型
var array1=[];
var array2=new Array(3);
var array3=new Array(3,4,6,8,9);
console.log(array2.length);
console.log(array2[0]);
array2[5]="abc";
console.log(array2[5]);

array2[6]=12;
console.log(array2[6]);

console.log(array2.length);

console.log(array3)

// concat Joins two arrays and returns a new array.  
var array4=array1.concat(array3);
console.log(array1);
console.log(array4);
// join Joins all elements of an array into a string. 
console.log(array3.join("-"))
// 栈
// pop Removes the last element from an array and returns that element. 
// push Adds one or more elements to the end of an array and returns that last element added. 
array3.push(88);
console.log(array3)
console.log(array3.pop())
console.log(array3)
// reverse Transposes the elements of an array: the first array element becomes the last and the last becomes the first. 
// array3.reverse()

// shift Removes the first element from an array and returns that element 
// 双向队列
// array3.shift();
// unshift Adds one or more elements to the front of an array and returns the new length of the array. 
// array3.unshift(999);
// slice Extracts a section of an array and returns a new array. 
console.log(array3.slice(1,3))
console.log(array3.slice(1,99))
console.log(array3.slice(1))
console.log(array3.slice(1,-1))
// splice Adds and/or removes elements from an array. 
// 替换与删除
array3.splice(1,2,"aaa","bbb")
// sort Sorts the elements of an array. 
// 排序
// toString Returns a string representing the specified object. 
console.log(array3)