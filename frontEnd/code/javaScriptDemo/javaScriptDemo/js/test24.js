// js的function

// call() 和 apply() 之间的区别
// 不同之处是：

// call() 方法分别接受参数。

// apply() 方法接受数组形式的参数。

// 2者第一个参数，都是方法调用时,的this
// 如果要使用数组而不是参数列表，则 apply() 方法非常方便。

function test1(){
	
}

var test2=function(){
	
}

var test3=(a,b) =>{ return a+b};

//使用函数对象，定义函数
var test4=new Function("x","y","return x*y");
//console.log(test4(3,7))

var test5=function(){
	console.log(this)
	console.log(arguments)
}
// console.log(test5.call(this,5,8));
console.log(test5.apply(this,[5,8]));

// 求取数组的最大值
var array=[3,4,5,6];
console.log(Math.max.apply(null,array));

//Boolean
// The initial value of the Boolean object. 
// The value is converted to a boolean value,
// 	if necessary. 
// 	If value is omitted or is 0, null, false, or the empty string (""), 
// 	the object has an initial value of false.
// 	 All other values, including the string "false",
// 	  create an object with an initial value of true.

var num1=new Number("123");;
var num1=new Number("123a");// NaN
var num1=new Number("three");// NaN
var num1=new Number("3e3");// 3000
console.log(num1);