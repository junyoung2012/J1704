// js的重载
function Student(){
	var _name,_age;
	// this.set=function(name){
	// 	_name=name;
	// }
	// this.set=function(age){
	// 	_age=age;
	// }
	// this.set=function(p){
	// 	if(typeof(p)==="string"){
	// 		_name=p;
	// 	}else if(typeof(p)==="number"){
	// 		_age=p;
	// 	}
	// }
	// 利用分支结构，判别不同的参数情况实现了多种业务逻辑，从而实现了重载
	this.set=function(){
		for(let i=0;i<arguments.length;i++){
			// console.log(arguments[i]);
			if(typeof(arguments[i])==="string"){
				_name=arguments[i];
			}else if(typeof(arguments[i])==="number"){
				_age=arguments[i];
			}
		}

	}
	this.toString=function(){
		return _name+"年龄："+_age;
	}
}



var stu=new Student();
// stu.set("李四");
// stu.set(14);
stu.set("李四",22);
stu.set(22,"李四");
console.log(stu.toString())
