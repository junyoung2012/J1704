// js的变量的作用范围
console.log(x);
var x=88;
console.log(x);
function test1(){
	//单var置顶
	var a=1,
		b=2,
		c=3;
	
	// var a=b=c=1;
	console.log(x);
	y=23;//这是个全局变量（隐式全局变量）
	for(let i=1;i<=5;i++){
		console.log(i)
	}
	console.log(i);
}
test1();
console.log("y="+y);
// console.log(i)
console.log("b="+b);