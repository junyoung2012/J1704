// js的定义类
function Student(_name){
	this.name=_name;
}

var student=new Student("张三");//被new才是类，没有new就是函数
console.log(student.name);

var student1=Student("李四");
console.log(student1);
//console.log(student1.name);
console.log(this.name);

