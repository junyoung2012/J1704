// js的Date
var d1=new Date();
console.log(d1);
console.log(typeof d1);//object

console.log(d1.constructor);

// new Date()
// new Date("month day, year hours:minutes:seconds")
// new Date(yr_num, mo_num, day_num)
// new Date(yr_num, mo_num, day_num, hr_num, min_num, sec_num) 

var d2=new Date(2012,11,30);
console.log(d2)

var d3=new Date(2020,2,0);
console.log(d3)


// 方法概览
// getDate 日. 
// getDay 星期几. 
// getHours 时. 
// getMinutes 分. 
// getMonth 月. 
// getSeconds 秒. 
// getTime . Returns the number of milliseconds in a date string since January 1, 1970, 00:00:00, local time. 
var d4=new Date(1970,0,1);
console.log(d4)

console.log(d4.getTime()-d4.getTimezoneOffset()*60*1000)
// getTimezoneOffset 分钟为单位的时区偏离值 
console.log(d4.getTimezoneOffset())

// getYear 年

// parse 
var d5=Date.parse("Aug 9, 1995");//返回的时据 January 1, 1970, 00:00:00 毫秒数
d4.setTime(d5);
console.log(d4)

// setDate Sets the day of the month for a specified date. 
// setHours Sets the hours for a specified date. 
// setMinutes Sets the minutes for a specified date. 
// setMonth Sets the month for a specified date. 
// setSeconds Sets the seconds for a specified date. 
// setTime Sets the value of a Date object. 
// setYear Sets the year for a specified date. 

// toGMTString Converts a date to a string, using the Internet GMT conventions. 
// toLocaleString Converts a date to a string, using the current locale's conventions. 
// UTC Returns the number of milliseconds in a Date object since January 1, 1970, 00:00:00, Universal Coordinated Time (GMT). 

Math.round8=function(num,scale){
	num*=Math.pow(10,scale);
	if(num-Math.floor(num)>=0.8){
		return Math.ceil(num/Math.pow(10,scale));
	}else{
		return Math.floor(num/Math.pow(10,scale));
	}
}

console.log(Math.round8(3.18,1)===3.2);
function round8(){
	
}

