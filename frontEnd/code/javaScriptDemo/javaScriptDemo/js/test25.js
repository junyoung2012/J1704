// js的Json
console.log(JSON)

var person = {
	firstName: 'Bill',
	lastName: "Gates",
	toStr: function() {

	}
};
console.log(JSON.stringify(person))

var text = '{ "employees" : [' +
	'{ "firstName":"Bill" , "lastName":"Gates" },' +
	'{ "firstName":"Steve" , "lastName":"Jobs" },' +
	'{ "firstName":"Alan" , "lastName":"Turing" } ]}';
	
var json=JSON.parse(text);
console.log(json)
