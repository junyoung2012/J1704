// js的不同变量范围
function Student(_name){
	var age=10;//（1）private
	this.name=_name;//（2）public
	for(let i=1;i<5;i++){//块变量
		
	}
	//js类里只有属性，你可以认为所谓方法就是，类型为function的变量
	this.show=function(){//函数里面还能定义内部函数
		console.log(age);
	}
}
Student.classProp="这是一个类属性";//(3)类属性：只能通过才能访问
var stu=new Student("张三");
console.log(stu.name);
console.log(stu.age);//js访问任何对象的属性，如果没有改属性，不会报异常，会显示为undefined
stu.show();
console.log(stu.classProp);
console.log(Student.classProp);//类属性
Student.prototype.prop2="这是一个原型属性，所有的对象是共享";//（4）原型属性，原型是一个对象 
console.log(stu.prop2);
stu.prop2="改变";
var stu2=new Student("李四");
console.log(stu2.prop2);
console.log(Student.prop2);

stu2.prop3="对象属性";//(5)对象属性

//js的面向对象，就是围绕着原型和原型链进行

/*
	使用js定义一个类
	需要定义私有变量，公开变量，公开方法，原型方法，类变量，对象变量。
*/




