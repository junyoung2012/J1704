// js的String
var s1="hello";
var s2=new String("1234567890");
console.log(s1);
console.log(s2);
console.log(s2.length);


// 方法概览
// anchor Creates an HTML anchor that is used as a hypertext target. 
console.log(s2.anchor("a1"));
// big Causes a string to be displayed in a big font as if it were in a BIG tag. 
// blink Causes a string to blink as if it were in a BLINK tag. 
// fixed Causes a string to be displayed in fixed-pitch font as if it were in a TT tag. 
// fontcolor Causes a string to be displayed in the specified color as if it were in a <FONT COLOR=color> tag. 
// fontsize Causes a string to be displayed in the specified font size as if it were in a <FONT SIZE=size> tag. 
// italics Causes a string to be italic, as if it were in an I tag. 
// link Creates an HTML hypertext link that requests another URL. 
// small Causes a string to be displayed in a small font, as if it were in a SMALL tag. 
// strike Causes a string to be displayed as struck-out text, as if it were in a STRIKE tag. 
// sup Causes a string to be displayed as a superscript, as if it were in a SUP tag. 
// sub Causes a string to be displayed as a subscript, as if it were in a SUB tag. 


// charAt Returns the character at the specified index. 
	console.log(s2.charAt(2))
// charCodeAt Returns a number indicating the ISO-Latin-1 codeset value of the character at the given index.  
// concat Combines the text of two strings and returns a new string. 
console.log(s2.concat("a","a","a"));
// fromCharCode Returns a string from the specified sequence of numbers that areISO-Latin-1 codeset values. 
// indexOf Returns the index within the calling String object of the first occurrence of the specified value. 
// lastIndexOf Returns the index within the calling String object of the last occurrence of the specified value.  
// match Used to match a regular expression against a string. 
// replace Used to find a match between a regular expression and a string, and to replace the matched substring with a new substring. 
	var s3="abc123abc1234";
	console.log(s3.replace("abc","*"))
	// console.log(s3.replaceAll("abc","*"))  作业：给String类加replaceAll
// search Executes the search for a match between a regular expression and a specified string. 
// 截取
// slice Extracts a section of a string and returns a new string.  
// substr Returns the characters in a string beginning at the specified location through the specified number of characters. 
// substring Returns the characters in a string between two indexes into the string. 
	console.log(s2.substring(1,-1))//begin,end
	console.log(s2.substr(2,3))//begin.length
	console.log(s2.slice(2,-1))//begin.end
	
// split Splits a String object into an array of strings by separating the string into substrings. 
	console.log(s2.split("").reverse().join(""))
// toLowerCase Returns the calling string value converted to lowercase. 
// toUpperCase Returns the calling string value converted to uppercase. 
var s4="   abc  "
console.log(s4.trim())





