// js的函数
function test1(){
	
}
// 匿名函数
var test2=function(){
	console.log("这是一个匿名函数")
}

// test2();

function test3(a,b){
	console.log(a);
	console.log(b);
}

// test3("aaa","bbb")
// test3(222,333)
//test3(222)//形参和实参不一致

function compute(a,b,option){
	switch(option){
		case "+":
			return a+b;
		case "-":
			return a-b;
	}
	
}

// console.log(compute(3,5,"+"))
// console.log(compute(3,5,"-"))

function compute(a,b,option){
	switch(option){
		case "+":
			return a+b;
		case "-":
			return a-b;
	}
	
}

function add(a,b){
	return a+b;
}

function compute1(a,b,fn){
	return fn(a,b);
}

console.log(compute1(3,5,add))
//策略模式（把匿名函数作为参数进行传递）
console.log(compute1(3,5,function(x,y){
	return x-y;
}))

console.log(compute1(3,5,(x,y)=>x*y))