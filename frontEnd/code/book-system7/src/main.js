import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import './assets/style/base.css'
import './assets/style/style.css'
import http from './utils/http.js'
var app=createApp(App);
app.config.globalProperties.$http=http;
app.use(router).mount('#app')
