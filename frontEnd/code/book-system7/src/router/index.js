import { createRouter, createWebHashHistory } from 'vue-router'
import Home from '../views/Home.vue'

const routes = [
  {
    path: '/',
    redirect:"/main"
  },
  {
    path: '/main',
    name: 'Main',
    component: () =>import('../views/Main.vue'),
	children:[
		{
		  path: 'empInput',
		  component: () =>import('../views/EmpInput.vue')
		},
		{
		  path: 'empList',
		  component: () =>import('../views/EmpList.vue')
		},
	]
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
