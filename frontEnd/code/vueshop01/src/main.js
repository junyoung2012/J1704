import Vue from 'vue'
import App from './App.vue'
import router from './router'
// element-ui支持
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import './assets/public.css'
import axios from 'axios'

Vue.config.productionTip = false
Vue.use(ElementUI);
// 配置请求的根路径
axios.defaults.baseURL = 'http://localhost:80/v1/'
// axios如果也有拦截器，就可以挂在令牌进行验证
// 请求拦截
axios.interceptors.request.use((request => {
	// 为请求头对象，添加token验证的Authorization字段
	request.headers.Authorization = window.sessionStorage.getItem('token')
	// 最后必须return config
	return request
}));
// 响应拦截
axios.interceptors.response.use((res => {
	if(res.data.meta.status!=200){
		//清除登录状态
		window.sessionStorage.removeItem('token');
	}
	return res;
}));

// 绑定ajax到$http
Vue.prototype.$http = axios

new Vue({
	router: router,
	render: h => h(App)
}).$mount('#app')
