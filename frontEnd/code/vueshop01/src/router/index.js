import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect:"/login"
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('../views/Login.vue')
  },
  {
    path: '/home',
    name: 'Home',
    component:Home,
	children:[
		{
			path: 'welcome',
			component: () => import('../views/Welcome.vue')
		},
		{
			path: 'users',
			component: () => import('../views/Users.vue')
		},
	]
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  }
]

const router = new VueRouter({
  routes
})
// vueRoute导航守卫（拦截器或过滤器）
router.beforeEach((to, from, next) => {
	// 如果用户访问的登录页，直接放行
	if (to.path === '/login')
			return next();
	const tokenStr = window.sessionStorage.getItem('token');
	// 没有token 强制跳转到登录页
	if (!tokenStr) {
		// 您还没有登录,请首先登录
		Vue.prototype.$message.error('您还没有登录,请首先登录')
		return next('/login')
	}
	next();//其它一律放行
});
export default router
