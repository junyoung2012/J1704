$(function(){
	var id=$.cookie('skin_id');
	if(id!=null){
		$("#cssfile").attr("href","styles/skin/"+id+".css")
		$("#skin li").removeClass("selected")
			.eq(id.slice(-1)).addClass("selected");
	}
	$("#skin li").click(function(){
		$(this).addClass("selected")
			.siblings().removeClass("selected");
		$("#cssfile").attr("href","styles/skin/"+this.id+".css")
		 $.cookie('skin_id', this.id);
	})
})