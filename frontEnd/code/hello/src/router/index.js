import {
	createRouter,
	createWebHashHistory,
	createWebHistory
} from 'vue-router'
import Home from '../views/Home.vue'

const routes = [{
		path: '/',
		name: 'Home',
		component: Home
	},
	{
		path: '/about',
		name: 'About',
		// route level code-splitting
		// this generates a separate chunk (about.[hash].js) for this route
		// which is lazy-loaded when the route is visited.
		component: () => import( /* webpackChunkName: "about" */ '../views/About.vue')
	},
	/*新增user路径，配置了动态的id*/
	{
		path: "/user/:id",
		name:"username",
		component: () => import('../views/user.vue')
	},
	{
		path: "/main",
		component: () => import('../views/Main.vue'),
		children:[
			{
				path: "emp",
				component: () => import('../views/Emp.vue')
			},
			{
				path: "dept",
				component: () => import('../views/Dept.vue')
			},
		]
	},
]

const router = createRouter({
	history: createWebHashHistory(),
	// history: createWebHistory(),
	routes
})
// 拦截器
router.beforeEach((to, from, next) => {
  console.log(to);
  console.log(from);
  if(to.fullPath== "/about"){
	  alert(222);
	  next("/main");
	  return;
  }
  next();
})
export default router
