# Redis的研究

1.什么是redis;

- ​	Redis 是完全开源的，是一个高性能的 **key-value** 数据库。(键值数据库)不就是一个可以持久化的map集合。

2.特点

Redis支持数据的持久化，可以将内存中的数据保存在磁盘中，重启的时候可以再次加载进行使用。
Redis不仅仅支持简单的key-value类型的数据，同时还提供list，set，zset，hash等数据结构的存储。
Redis支持数据的备份，即master-slave模式的数据备份。可以多个实例创建集群。

性能极高 – Redis能读的速度是110000次/s,写的速度是81000次/s 。

丰富的数据类型 – Redis支持二进制案例的 Strings, Lists, Hashes, Sets 及 Ordered Sets 数据类型操作。

原子 – Redis的所有操作都是原子性的，意思就是要么成功执行要么失败完全不执行。单个操作是原子性的。多个操作也支持事务，即原子性，通过MULTI和EXEC指令包起来

丰富的特性 – Redis还支持 **publish/subscribe**, 通知, key 过期等等特性。redis就是个简单的消息中间件



2.启动

```
redis-server.exe
```

3.使用

​	```

```
redis-cli
```

4.支持的数据类型

Redis支持五种数据类型：string（字符串），hash（哈希），list（列表），set（集合）及zset(sorted set：有序集合)。



Springboot +redis+Token解决分布式应用登录权限言则会那个的问题

5.图像化客户端

RedisDesktopManager

1.引包

```
<!-- redis -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-data-redis</artifactId>
        </dependency>
```

2.配置

```yml
  redis:
    database: 0
    host: 127.0.0.1
    port: 6379
    timeout: 2000
    lettuce:
      pool:
        max-active: 8
        max-wait: -1
        max-idle: 8
        min-idle: 0

```

3.RedisConfig

4.得到了RedisTemplate 模板类

​	5.TokenUtil存入到redis

6.在验证拦截器。吧令牌格式验证玩不腻后

7可以直接通过redis验证用户信息。



​	![image-20211218102858444](https://gitee.com/junyoung2012/blogimg/raw/master/image-20211218102858444.png)