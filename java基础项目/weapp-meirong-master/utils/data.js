var req = require('reqUtils')
/**
 * banner数据
 */
function getBannerData(that) {
    req.get('/TestServlet', {
        "name":"杨",
        "age":20
    }, 1)
    .then(res => {
      // console.log(res.data)
      that.setData({
        banner_url:res.data
      })
    })
}

function getIndexNavData(that){
    req.get('/IndexNavData.json', {
      }, 1)
      .then(res => {
        // console.log(res.data)
        that.setData({
            navTopItems:res.data
        })
      })
}

/*
 * 首页 对应 标签 数据项
 */
function getIndexNavSectionData(that) {
    req.get('/IndexNavSectionData.json', {
    }, 1)
    .then(res => {
      that.setData({
        navSectionItems:res.data,
        list: res.data
      })
    })
}
/**
 * 技师 数据
 * */
function getSkilledData(that) {
    req.get('/SkilledData.json', {
    }, 1)
    .then(res => {
      that.setData({
        navSectionItems:res.data,
        list: res.data
      })
    })
}

/**
 * 选择器 数据
 */
function getPickerData() {
    var arr = [{
            name: '张三',
            phone: '13812314563',
            province: '北京',
            city: '北京',
            addr: '朝阳区望京悠乐汇A座8011'
        },
        {
            name: '李四',
            phone: '13812314563',
            province: '北京市',
            city: '北京市',
            addr: '朝阳区望京悠乐汇A座4020'
        }
    ]
    return arr
}
/**
 * 查询 地址
 * */
var user_data = userData()

function searchAddrFromAddrs(addrid) {
    var result
    for (let i = 0; i < user_data.addrs.length; i++) {
        var addr = user_data.addrs[i]
        console.log(addr)
        if (addr.addrid == addrid) {
            result = addr
        }
    }
    return result || {}
}
/**
 * 用户数据
 * */
function userData() {
    var arr = {
        uid: '1',
        nickname: '山炮',
        name: '张三',
        phone: '13833337998',
        avatar: '../../images/avatar.png',
        addrs: [{
                addrid: '1',
                name: '张三',
                phone: '13812314563',
                province: '北京',
                city: '直辖市',
                addr: '朝阳区望京悠乐汇A座8011'
            },
            {
                addrid: '2',
                name: '李四',
                phone: '13812314563',
                province: '北京',
                city: '直辖市',
                addr: '朝阳区望京悠乐汇A座4020'
            }
        ]
    }
    return arr
}
/**
 * 省
 * */
function provinceData() {
    var arr = [
        // {pid:1,pzip:'110000',pname:'北京市'},
        // {pid:2,pzip:'120000',pname:'天津市'}
        '请选择省份', '福建省'
    ]
    return arr
}
/**
 * 市
 * */
function cityData() {
    var arr = [
        // {cid:1,czip:'110100',cname:'市辖区',pzip:'110000'},
        // {cid:2,czip:'120100',cname:'市辖区',pzip:'120000'}
        '请选择城市', '福州市', '厦门市', '宁德市'
    ]
    return arr
}
/*
 * 对外暴露接口
 */
module.exports = {
    getBannerData: getBannerData,
    getIndexNavData: getIndexNavData,
    getIndexNavSectionData: getIndexNavSectionData,
    getPickerData: getPickerData,
    getSkilledData: getSkilledData,
    userData: userData,
    provinceData: provinceData,
    cityData: cityData,
    searchAddrFromAddrs: searchAddrFromAddrs

}