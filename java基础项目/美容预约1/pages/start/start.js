//获取应用实例
var app = getApp();
Page({
  data: {
    banners:["https://dcdn.it120.cc/2020/01/07/c3183558-55e8-4589-9ea7-81670004941a.jpg"
    ,"https://dcdn.it120.cc/2020/01/07/b7471e4f-dc0d-4245-afb7-b64fbd9bb29a.jpg"
    ,"/images/1635509248101.jpg"],
    swiperMaxNumber: 3,
    swiperCurrent: 0
  },
  onLoad:function(){
    const _this = this
  },
  onShow:function(){
    
  },
  swiperchange: function (e) {
    console.log(e.detail.current)
    this.setData({
      swiperCurrent: e.detail.current
    })
  },
  goToIndex: function (e) {
    wx.switchTab({
        url: '/pages/index/index',
    });
  },
  imgClick(){
    if (this.data.swiperCurrent + 1 != this.data.swiperMaxNumber) {
      wx.showToast({
        title: '左滑进入',
        icon: 'none',
      })
    }
  }
});