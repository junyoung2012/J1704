var app = getApp()
var mydata = require('../../utils/data')
var util = require('../../utils/util')
var req = require('../../utils/reqUtils.js')

Page( {
  data: {
    addrArray:util.replacePhone(mydata.userData().addrs,true),
    addrIndex:0,
    date : new Date().getFullYear()+"-"+(new Date().getMonth()+1)+"-"+new Date().getDate(),
    time : '12:00',
    bookToastHidden:true,
    message:'',
    bookId:''
  },
  onLoad: function (options) {
   this.setData({
    aid:options.aid
   })
  },
  closeHide:function(e){
    this.setData({
      ismask: 'none'
    });
  },
  // 地址选择
  bindAddrPickerChange : function(e){
    console.log('Addrpicker发送选择改变，携带值为', e.detail.value)
    this.setData({
      addrIndex: e.detail.value
    })
  },
  bindToastTap:function(){
    var that=this;
    console.log({
      serviceId:that.data.aid,
      date:that.data.date+" "+that.data.time,
      message:that.data.message,
      nickName:app.globalData.userInfo
    });
    req.post('/BookServiceAction', {
      serviceId:that.data.aid,
      date:that.data.date+" "+that.data.time,
      message:that.data.message,
      nickName:app.globalData.userInfo.nickName
    }, 1)
    .then(res => {
      console.log(res)
      if(res.data>0){
        that.setData({
          bookToastHidden:false,
          bookId:res.data
      })
      }
    })
  },
  hideToast:function(){
    this.setData({
          bookToastHidden:true
      })
  },
  // 日期选择
  bindDateChange: function(e){
    console.log('date picker发送选择改变，携带值为', e.detail.value)
    this.setData({
          date: e.detail.value
    })
  },
  // 时间选择
  bindTimeChange: function(e){
    console.log('time picker发送选择改变，携带值为', e.detail.value)
    this.setData({
          time: e.detail.value
    })
  }
  
})