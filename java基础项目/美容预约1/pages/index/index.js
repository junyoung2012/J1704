//index.js
//获取应用实例
var app = getApp()
var fileData = require('../../utils/data.js')
var req = require('../../utils/reqUtils.js')

Page({
  // 页面初始数据
  data: {
      colors:['red','orange','yellow','green','purple'],
      // banner 初始化
      banner_url:fileData.getBannerData() ,
      indicatorDots: true,
      navTopItems:fileData.getIndexNavData(),
      vertical: false,
      autoplay: true,
      interval: 3000,
      duration: 1000,
      navSectionItems: fileData.getIndexNavSectionData()[0],
      curNavId: 1,
      curIndex: 0,
      index:0
  },
   
  onLoad:function(){
    //从服务端加载导航栏
    req.get('/GetNavTopItems', {}, 1)
    .then(res => {
      this.setData({
        navTopItems:res.data
      })
    })
   //从服务端加载服务数据
    req.get('/GetNavSectionData', {
      typeId:1
    }, 1)
    .then(res => {
      this.setData({
        list:res.data
      })
    });
  },
  //标签切换
  switchTab: function(e) {
      let id = e.currentTarget.dataset.id;
      let index = parseInt(e.currentTarget.dataset.index)
      req.get('/GetNavSectionData', {
        typeId:id
      }, 1)
      .then(res => {
        this.setData({
          list:res.data
        })
      });
       this.setData({
        curNavId: id,
        curIndex: index,
      })
  },
  // 跳转至详情页
  navigateDetail: function(e){
    wx.navigateTo({
      url:'../detail/detail?artype=' + e.currentTarget.dataset.aid
    })
  },
  // 加载更多
  loadMore: function (e) {
    console.log('加载更多')
    var curid = this.data.curIndex
    if (this.data.navSectionItems[curid].length === 0) return
    var that = this
    that.data.navSectionItems[curid] = that.data.navSectionItems[curid].concat(that.data.navSectionItems[curid])
    that.setData({
      list: that.data.navSectionItems,
    }) 
  },
  // book
  bookTap: function(e){
    wx.navigateTo({
      url:'../book/book?aid='+e.currentTarget.dataset.aid
    })
  }
})
