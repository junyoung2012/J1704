# java基础项目

## 项目目的

1. 综合应用以前的知识
2. 回顾以下学习的基本技能
3. 在项目中得到初步的设计能力
4. 建立团队的开发意识

## 项目的基本结构

​	![image-20211025083829143](https://gitee.com/junyoung2012/blogimg/raw/master/image-20211025083829143.png)

1. 广告轮播

   ![image-20211025084018514](https://gitee.com/junyoung2012/blogimg/raw/master/image-20211025084018514.png)

2. 频道

   ![image-20211025084225845](https://gitee.com/junyoung2012/blogimg/raw/master/image-20211025084225845.png)

3. 首页数据

   ![image-20211025084357941](https://gitee.com/junyoung2012/blogimg/raw/master/image-20211025084357941.png)

4. 管理数据

   ![image-20211025084514166](https://gitee.com/junyoung2012/blogimg/raw/master/image-20211025084514166.png)

5. 用户数据

   显示

6. 预约

   我的预约

   ![image-20211025084824729](https://gitee.com/junyoung2012/blogimg/raw/master/image-20211025084824729.png)

## 完成的任务

1. ​	提交程序
   1. 数据库，程序
   2. 图片，数据（一组一份）
   3. 前端微信小程序
2. 数据库设计说明书（一组一份）
3. 搭建测试环境（一组一份）
4. 每个要总结（一组一份）

整个要求

1. 每个人协同完成项目开发
2. 要求小组所有的成员，开发成果要一致。



2021.10.25

1. 确立开发的主题
2. 分组
3. 搜集资源
4. 设计数据库



## 微信小程序的分析



- 技师距离
- 预约流程
- 我的预约
