JDBC的研究

# JDBC的历史

## 	ODBC(open database connection )

## 	JDBC (java database connection)

### api 连接数据库时代

### JDBC->ODBC->数据库

### 纯java实现的JDBC

- jdbc就是java发布操作数据库的接口
- java发布接口，数据库厂家提供实现
- 数据库厂家提供的是java类。基本都是一jar提供。被称作**数据库驱动**



# JDBC 操作过程

![image-20211021093354349](https://gitee.com/junyoung2012/blogimg/raw/master/image-20211021093354349.png)

1. 引入驱动jar
2. 加载驱动
3. 得到connection
4. 建立statement
5. 执行
6. 处理结果
7. 关闭资源

# JDBC的常用类和接口

 DriverManager

- getConnection
- setLoginTimeout(int seconds) 

## 接口 Connection

- 得到陈述对象
  - Statement   
  - PreparedStatement  预编译陈述
  - CallableStatement 调用存储过程
- 处理事务
  - setAutoCommit(boolean autoCommit)  设置事务自动提交 ，设置为false就手动打开事务
  - commit() 
  - rollback() 
  - rollback(Savepoint savepoint) 
  - setSavepoint(String name) 
  - setTransactionIsolation(int level)  设置隔离级别

Statement

类型

- resultSetType 

  - ​	ResultSet.TYPE_FORWARD_ONLY 前向结果集（效率最高）
  - ​	ResultSet.TYPE_SCROLL_INSENSITIVE  滚动不敏感 
  - ​	ResultSet.TYPE_SCROLL_SENSITIVE   滚动敏感 

  

- resultSetConcurrency（并发性）

  - ResultSet.CONCUR_READ_ONLY 
  -  ResultSet.CONCUR_UPDATABLE

  

- resultSetHoldability （可保存性）

  - ResultSet.HOLD_CURSORS_OVER_COMMIT 
  -  ResultSet.CLOSE_CURSORS_AT_COMMIT 

常用操作

​	执行

- int executeUpdate(String sql)    **执行增删改**
  - (1) 对于 SQL 数据操作语言 (DML) 语句，返回行计数 
  - (2) 对于什么都不返回的 SQL 语句，返回 0 
- ResultSet executeQuery(String sql) **执行select查询**
- boolean execute(String sql) **能做增删改**，也能查询，true和false不代表成功失败**
- executeBatch 批量执行sql

绑定参数

​	SetXXX

~~Statement~~与prepareedStatement的区别？

- 预编译的效率高
- 用？作为占位符代替参数，可读性好
- 安全高，防止**SQL注入漏洞**

ResultSet

- ​	移动
- getXXX
- getMetaData()  得到元数据

​	

## 連接池

​	连接池必须要实现DataSource接口，实现getConnection() 

连接池是创建和管理一个数据库连接的缓冲池的技术，这些连接准备好被任何需要它们的[线程](https://baike.baidu.com/item/线程/103101)使用。

T1=连接创建时间

T2=使用时间

T3=是销毁时间

T1+T3和T2差不多，必须使用池化技术管理连接。

作用：

1. **减少连接创建时间**
2. **简化的编程模式**
3. **受控的资源使用**

java常用的连接池（数据源）

- druid 阿里 德鲁伊
- DBCP
- C3P0 

数据库连接池的主要操作如下：

1. ​	建立数据库连接池对象（服务器启动）。
2. 按照事先指定的参数创建初始数量的数据库连接（即：空闲连接数）
3. 对于一个数据库访问请求，直接从连接池中得到一个连接。如果[数据库连接池](https://baike.baidu.com/item/数据库连接池)对象中没有空闲的连接，且连接数没有达到最大（即：最大活跃连接数），创建一个新的数据库连接。
4. 存取数据库，释放连接
5. 超时释放连接
6. 关闭数据库，释放所有数据库连接（此时的关闭数据库连接，并非真正关闭，而是将其放入空闲队列中。如实际空闲连接数大于初始空闲连接数则释放连接）
7. 释放数据库连接池对象（服务器停止、维护期间，释放数据库连接池对象，并释放所有连接）。





