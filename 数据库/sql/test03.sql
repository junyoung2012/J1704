/*函数*/
--单行函数
  --字符函数
  --dual系统提供的测试表
    select 'abcdef' from dual;
    select ascii('abcdef') from dual;--97
    select ascii('a') from dual;
    
    select chr(97) from dual;--a
    
    select concat('ab','cd') from dual;--abcd
     select concat('ab','cd','ef') from dual;--错
     select 'ab'||'cd' from dual;--abcd
     select 'ab'+'cd' from dual;--错
     select 3+2 from dual;
     select '3'+'2' from dual;--自动转换
     select '3'+'2'+10 from dual;--自动转换
     
     select instr('abcdef','abc') from dual;--1
     select instr('abcdef','bbc') from dual;--0 
     select instr('abcdefabc','abc',2) from dual; 
    select instr('abcdefabc','abc',1,2) from dual; 
    
    select length('123456') from dual;
    
    select lpad('123',5,0) from dual;
    select lpad('123456',5,0) from dual;--截取，填充
    select rpad('123',5,0) from dual;
    select rpad('123456',5,0) from dual;
 
    select ltrim('   123456   ') from dual;
    
    select ltrim('123456','12') from dual;
    select ltrim('21232456','312') from dual;
    select trim('   21232456  ') from dual;
    
    
     select replace('1231245612','12') from dual;
    select replace('1231245612','12','*') from dual;
    
     select substr('12345678',2) from dual;
     select substr('12345678',2,3) from dual;
     select substr('12345678',-2) from dual;--反向截取
  
    
  --数学函数
     select floor(-3.14) from dual;-- -4
     select ceil(-3.14) from dual;-- -3
     select round(-3.14) from dual;-- -3
     select round(-3.14,1) from dual;-- -3.1
      select mod(8,5) from dual;-- 3
      select power(8,2) from dual;-- 64
      
  --日期函数
  select CURRENT_DATE from dual;--2021/10/19 11:36:46
  select SYSDATE from dual;--2021/10/19 11:37:18
  select CURRENT_TIMESTAMP(9) from dual;--19-10月-21 11.37.47.833000000 上午 +08:00
  select SYSTIMESTAMP(9) from dual;--19-10月-21 11.38.25.140000000 上午 +08:00
  
   select SYSDATE+1 from dual;--天
  select date'2021-10-19'- date'2021-10-16' from dual;
  select SYSDATE- date'2021-10-16' from dual;
   select months_between(date'2021-6-19', date'2021-10-16') from dual;
   select ADD_MONTHS(date'2021-6-19',3) from dual;
   
   select last_day(date'2021-10-16') from dual;
   select last_day(date'2020-02-16')-3 from dual;
   
   select next_day(sysdate,2) from dual;--下周一
   select next_day(sysdate,'星期一') from dual;--下周一
   
   select ROUND(sysdate,'year') from dual;
   select ROUND(sysdate,'month') from dual;
   
   select extract(day from sysdate) from dual;
   select extract(year from sysdate) from dual;
  
  --转换函数
  select to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') from dual;
   select to_char(sysdate,'dd')+10 from dual;
   
   select to_date('2021-10-19 11:51:44','yyyy-mm-dd hh24:mi:ss') from dual;
   select to_char(1234567.879,'$000,000,000.00') from dual;
  select to_number('$001,234,567.88','$000,000,000.00') from dual;
 
  --其它函数
  
  select nvl(null,2) from dual;
  select empno,nvl(comm,0) from emp;
    select empno,nvl(comm,'a') from emp;--错
    --三目运算
   select nvl2(1,2,3) from dual; 
   
   --多选一结构
   select decode(5,1,'甲',2,'乙','爱咋咋') from dual;
    
    
--窗口函数
--自定义函数
