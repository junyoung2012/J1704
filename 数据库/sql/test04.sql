1.选择部门30中的所有员工.
select * from emp
     where deptno=30
2.列出所有办事员(CLERK)的姓名，编号和部门编号.
select * from emp

select * from emp
       where job='CLERK'
       
select ename,empno,deptno from emp
       where job='CLERK'
       


3.找出补贴高于薪金的员工.

select * from emp

select ename,sal,comm from emp

select ename,sal,comm from emp
       where comm>sal
       
select * from emp
       where comm>sal



4.找出补贴高于薪金的60%的员工.
select * from emp

select ename,sal*0.6,comm from emp

select ename,sal*0.6,comm from emp
       where comm>sal*0.6

select * from emp
       where comm>sal*0.6



5.找出部门10中所有经理(MANAGER)和部门20中所有办事员(CLERK)的详细资料.
select * from emp

select * from emp
       where deptno=10 and job='MANAGER'
       
select * from emp
       where deptno=20 and job='CLERK'
       
select * from emp
       where deptno=10 and job='MANAGER' 
             or deptno=20 and job='CLERK'
             
6.找出部门10中所有经理(MANAGER),部门20中所有办事员(CLERK),
既不是经理又不是办事员但其薪金大于或等于2000的所有员工的详细资料.

7.找出收取补贴的员工的不同工作.
comm is not null 
comm > 0 


select * from emp
       where comm > 0 
剔重

8.找出不收取补贴或收取的补贴低于100的员工.

9.找出各月倒数第3天受雇的所有员工.
select * from emp

select hiredate from emp

select hiredate,last_day(hiredate) from emp

select hiredate,last_day(hiredate),last_day(hiredate)-2 from emp

select * from emp
       where hiredate=last_day(hiredate)-2

10.找出早于12年前受雇的员工.  

select * from emp

select hiredate from emp
select hiredate,add_months(hiredate,12*12),sysdate from emp

11.以首字母大写的方式显示所有员工的姓名.
select ename,initcap(ename) from emp

12.显示正好为5个字符的员工的姓名.



13.显示不带有"R"的员工的姓名.
select * from emp
       where ename like '%R%'


14.显示所有员工姓名的前三个字符.

substr(1,len)

15.显示所有员工的姓名,用a替换所有"A"
replace

16.显示满10年服务年限的员工的姓名和受雇日期.

select sysdate-365.25*10 from dual;

17.显示员工的详细资料,按姓名排序.

18.显示员工的姓名和受雇日期,根据其服务年限,将最老的员工排在最前面.
select ename,hiredate from emp
       order by hiredate desc


19.显示所有员工的姓名、工作和薪金,按工作的降序排序,若工作相同则按薪金排序.



20.显示所有员工的姓名、加入公司的年份和月份,按受雇日期所在月排序,若月份相同则将最早年份的员工排在最前面.

select ename,hiredate,to_char(hiredate,'yyyy') 年份,to_char(hiredate,'mm') 月份 from emp
       order by 月份,年份
       
21.显示在一个月为30天的情况所有员工的日薪金,忽略余数.

22.找出在(任何年份的)2月受聘的所有员工。


23.对于每个员工,显示其加入公司的天数.
select sysdate-hiredate from emp

24.显示姓名字段的任何位置包含"A"的所有员工的姓名.
select ename from emp
select ename,instr(ename,'A') from emp


25.以年月日的方式显示所有员工的服务年限.

40年5月20天

select ename，hiredate,floor(months_between(sysdate,hiredate)/12) from emp

select ename，hiredate,mod(floor(months_between(sysdate,hiredate)/12),12) from emp

select ename，hiredate,
       floor(sysdate-add_months(hiredate,floor(months_between(sysdate,hiredate)))) from emp
       
40.列出从事同一种工作但属于不同部门的员工的一种组合
select E1.ename,E2.Ename from emp E1
       inner join emp E2 on E1.job=E2.job and E1.deptno!=E2.deptno
       
41.列出所有部门的详细信息和部门人数

select * from emp
select * from dept

select * from dept
       left join emp on  dept.deptno=emp.deptno
              
select  dept.deptno,dname,loc,count(1) from dept
       left join emp on  dept.deptno=emp.deptno
       group by  dept.deptno,dname,loc
        
        
       
42.列出各种工作的最低工资

45.显示各部门员工薪金最高的前2名
--相关子查询，求出比我工资高的人数
  人数<2            
SELECT
	* 
FROM
	emp e 
WHERE
	( SELECT COUNT(1) FROM emp WHERE sal > e.sal AND deptno = e.deptno ) < 2 
ORDER BY
	e.deptno,
	e.sal DESC ;       
       
 SELECT
	e.* ,( SELECT COUNT(1) FROM emp 
      WHERE sal > e.sal AND deptno = e.deptno )
FROM
	emp e 
  
查询所有雇员总数和获得奖金的员工数。

 SELECT case  when comm > 0 then  1 else 0 end case from emp;
 SELECT count(1) 雇员总数,sum(case  when comm > 0 then  1 else 0 end) 获得奖金的员工数  from emp;
    
          
 查询20号部门的员工姓名、工资及其级别。
 
 SELECT * from emp;
 SELECT grade, 
        losal, 
        hisal
        from salgrade
 
    SELECT * from emp E
           left join salgrade S on E.Sal between S.Losal and S.Hisal
           
           
