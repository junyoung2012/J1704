select translate('1234','123456','ABCDEFG') FROM DUAL;
/*    层次查询

*/
select empno,ename,mgr from emp
       start with mgr is null
       connect by prior empno=mgr
       
select  empno,ename,sys_connect_by_path(ename,'/') from emp
       start with mgr is null
       connect by prior empno=mgr
       

/*
MERGE INTO [target-table] A USING [source-table sql] B ON([conditional expression] and [...]...)
WHEN MATCHED THEN
	[UPDATE sql]
WHEN NOT MATCHED THEN
	[INSERT sql]
  */
  truncate table Otable --截断表（DDL）,无法回滚，没有日志
  delete from Otable -- DML语句，有日志，可以回滚
  
--drop  table Otable ;
create table Otable(
       name varchar2(30) primary key,
       city varchar2(30)
);
--drop  table Ntable;
create table Ntable(
       name varchar2(30) primary key,
       city varchar2(30)
);

insert into Otable values('张三','西安');
insert into Otable values('李四','兰州');

insert into Ntable values('张三','咸阳');
insert into Ntable values('王五','天水');
commit


select * from Otable;
select * from Ntable;




     
update (select O.NAME,O.CITY oc,N.City nc from Otable O
       inner join Ntable N on O.NAME=N.Name)
       set oc=nc
       
--相关子查询       
update Otable O
        set O.City=(select city from Ntable N where O.Name=N.NAME) 
--合并        
MERGE INTO Otable O
       USING  Ntable N on (O.Name=N.NAME)  
       WHEN MATCHED THEN
            update set O.City=N.City
       WHEN NOT MATCHED THEN
            insert values(N.NAME,N.City)
             
select empno,ename,sal,rank() over(order by sal) from emp;            
select empno,ename,sal,avg(sal) over() from emp; 
select empno,ename,sal,job,avg(sal) over(partition by job ) from emp;              
select empno,ename,sal,ntile(3) over(order by sal) from emp;            
select empno,ename,sal,dense_rank() over(order by sal) from emp;            



create table t_student(stuid number(10,2) primary key,name varchar2(100), age number(3,0), sex char(1));

create table t_course(cid number(10,2) primary key, name varchar2(100), teacherid number(10, 2));
create table t_teacher(tid number(10,2) primary key, tname varchar2(100));

alter table t_course
add constraint tcourse_fk foreign key (teacherid) references t_teacher(tid);

create table t_sc(stuid number(10, 2), cid number(10,2), score number(10,2));
alter table t_sc 
add constraint t_sc_pk primary key (stuid, cid);
alter table t_sc
add constraint t_sc_stuid_fk foreign key (stuid) references t_student(stuid);
alter table t_sc
add constraint t_sc_cid_fk foreign key (cid) references t_course(cid);



5、查询没学过“叶平”老师课的同学的学号、姓名；
select * from t_teacher;
select * from t_student;
select * from t_sc;
select * from t_course;


select S.STUID,S.Name from t_sc SC
       left join t_student S on Sc.Stuid=S.STUID
       Left join t_course C on SC.Cid=C.cid
       left join t_teacher T on C.Teacherid=T.TID
       where tname='叶平'
       
select S.STUID,S.Name from t_sc SC
       left join t_student S on Sc.Stuid=S.STUID
       Left join t_course C on SC.Cid=C.cid
       
select S.STUID,S.Name from t_student S
where STUID not in       
      (select Stuid from t_sc SC       
         Left join t_course C on SC.Cid=C.cid
         left join t_teacher T on C.Teacherid=T.TID and tname='叶平')

       



