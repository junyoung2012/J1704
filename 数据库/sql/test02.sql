--创建表空间（类似于My SQL中的数据库）

--创建用户（生产系统中，一个服务器只用一个Oracle数据库，
-- 一个数据库实例只能连接一个数据库，多个软件系统工作在同一服务器，一定要公用数据库
--为了区分模式对象，就需要依据软件系统，建立不同的用户
--表空间和用户是多对多关系）

--授权
 -- resource   模式对象所有者（开发权限）
 --connect   数据进行增删改查（生产权限）
 
 --重新登录
 
        --scott用户的示例数据库
        
--sql语句  结构化查询语句 sql-89 sql-92
/*
         DDL 数据定义语言 （增删改数据库模式对象）
         DML 数据操纵语言  （数据进行增删改查）
         DCL 数据控制语言    （控制数据库对象的）
*/

      
--DML 数据操纵语言
      --查
         -- 1.投影（对字段的操作）
            -- 1.1 选取全部字段
            select * from emp;
            -- 1.2 选取部分字段
            select empno from emp;
            select empno,ename from emp;
            -- 1.3 选取部分字段，并且调整顺序
            select ename,empno from emp;
            -- 1.4 可以使用函数，或者计算，产生表达式
            select substr(ename,1,2) from emp;
            --null做任何运算结果都是null
            select ename,sal,comm,sal+nvl(comm,0) from emp;
            -- 1.5 别名
            select ename,sal,comm,sal+nvl(comm,0) as 收入 from emp;
            select ename,sal,comm,sal+nvl(comm,0) as "收 入" from emp;
            select ename,sal,comm,sal+nvl(comm,0)  "收 入" from emp E;
            select ename,sal,comm,sal+nvl(comm,0)  "收 入" from emp as E;--不行，表的别名是没有as
            
            --1.6 常数列 
            select ename,1 常数列,sysdate,'abcd'  from emp;
            
            --1.7 剔重
            --1.8 case 分支
            select distinct job from emp;--只按投影列表（选择的字段列表）来剔重
            select dist job from emp;-- 不行
            
            select 
            case deptno
              when 10 then 'A部门'
              when 20 then 'B部门'
              else
                   '其它部门'
            end case
            from emp;
            
            select 
            case 
              when sal between 0 and 2000 then '低工资'
              when sal between 2001 and 5000 then '高工资'
              else '其它'
            end case
            from emp;
            
            
         -- 2.选择
            -- 在结果集中选择出部分记录行
            
            select * from emp
                   where ;
           --关系运算 =,!=,<>,>,<,>=,<=,in,not in,is null ,is not null,between ...and ,like
           -- 逻辑运算 not and or
           -- 逻辑谓词 some any all exists
           
           = 是单的
           between ...and 是闭区间
           is null 是判别null的正确方式，不能用=null
           like 
           
           select * from emp
                   where comm=null ;--错
          select * from emp
                   where comm is null ;
           select * from emp
                   where ename like 'A';--错
           select * from emp
                   where ename like '%A%';
           select * from emp
                   where ename like 'A____';
             
            select * from emp
                   where deptno=10 or  deptno=20   
            select * from emp
                   where (deptno=10) or  (deptno=20)
                   
             
            select * from emp
                   where sal>some (select sal from emp where deptno=10) --任意一个    
            select * from emp
                   where sal>any (select sal from emp where deptno=10) --任意一个  
                           
            select * from emp
                   where sal>all (select sal from emp where deptno=10)   --全部        
                   
            select * from emp E1
                   where exists(select 1 from emp E2 where E1.Deptno=E2.deptno and E2.Deptno=10 )
            -- exists 按记录行匹配，不经过投影，所以效率高
            
                  
       -- 3.排序
        --结果集不排序，默认认为是无序的，是按照聚集索引，内容排列顺序
          select ename,job from emp
                 order by empno, job asc,ename desc --可以多个关键字排序 ，用逗号分开
                 --每个关键字是可以，单独设置升序(asc)降序(desc)
                 --可以使用没有投影的字段排序
                 --一般不在子查询中排序，只在结果集最后排序
          
       -- 4.分组
            --group by
            --having
            --分组的2个用处
                    --1.使用聚合函数  max,min,sum,count,avg
                    --2.剔重
                    
              select count(1) from emp --把所有记录视作一组
              
              --分组的投影字段，分组条件列，或者是聚合列
              select deptno,min(ename),count(1) from emp 
                    where  deptno>0 --where 先执行，能把结果集限制的更小，处理的数据更少，效率高
                    group by deptno
                    having count(1)>0 --having 分组条件列，或者是聚合列,在分组后执行
                                   -- having 一般只用于限制聚合列
             --（精确的）消除重复       
             select job,min(empno) from  emp  
                    group by  job   
                           
        -- 5.连接
         
            select * from emp
            select * from dept
            
            select * from emp,dept 
            --笛卡尔积
                   行数=2表之积，列数=2表之和
            --等联（内等联结）
                   行数=取决于连接条件，列数=2表之和
            select * from emp,dept
                   where emp.deptno=dept.deptno
                   
            select * from emp
                   inner join dept on emp.deptno=dept.deptno --推荐       
           select * from emp
                   inner join dept on emp.deptno!=dept.deptno       
           select * from emp
                    join dept on emp.deptno=dept.deptno       
                    
           --左联（左联结）
             行数=取决于连接条件,保证左表记录返回，列数=2表之和
           select * from emp
                   left outer join dept on emp.deptno=dept.deptno --推荐  
                   
            select * from dept
                   left outer join emp on emp.deptno=dept.deptno    
                    order by  emp.deptno 
            select * from dept
                   left join emp on emp.deptno=dept.deptno --推荐    
                    order by  emp.deptno 
                    
            select * from emp,dept
                   where emp.deptno=dept.deptno(+) --左联的古典写法       
                    
            左联与等连的区别？
            都可以连接2表
            连接字段
            如何引用表与被引用表，数据符合引用完整性的，等连和左联没区别
            数据不是那么规范，不符合引用完整性，左联更安全，总能保证左表的所有记录会被加载。
            ！！！编号转名称，是连接的最主要场景
             --右联（就是左联的对称）      
              select * from emp
                   right outer join dept on emp.deptno=dept.deptno                    
               select * from emp
                   right  join dept on emp.deptno=dept.deptno
              select * from emp,dept
                   where emp.deptno(+)=dept.deptno --右联的古典写法    
                    
                             
             --全连接(左联+右联)
             --保证左表，右表记录都被加载
             select * from emp
                    full outer  join dept on emp.deptno=dept.deptno  
                    
             --自然连接(按2表相同字段名的列，等连)
            select * from emp
                    Natural join dept                 
        -- 6.子查询  
            --1.单行子查询
            select * from emp
                   where deptno=(
                     select deptno from Dept
                            where loc='DALLAS');
             --2.多行子查询
             select * from emp
                   where deptno in(
                     select deptno from Dept
                            where deptno>10);
            --3.多列子查询                            
            select * from   emp
                   where (ename,job)=( select ename,job from emp where empno=7369) 
                   
                   update   emp
                     set sal=0
                     where (ename,job)=( select ename,job from emp where empno=7369)             
            --4.标量子查询
              select ename,sal,5000-sal 差额 from   emp
              
              select ename,sal,(select max(sal) from emp)-sal 差额 from emp
            --5.相关子查询
            select ename,sal,deptno,
                   (select max(sal) from emp E1 where Emp.Deptno=E1.Deptno)-sal 差额 from emp
            
            --6.DDL子查询(复制表与数据)
            create table EmpInDept10
            as
            select empno,ename,sal from emp
                   where deptno=10
                   
           select * from EmpInDept10        
                                                           
        -- 7.联合
            -- 并集
            select * from emp
                   where deptno=10
            union --带剔重
            select * from emp
                      
            select * from emp
                   where deptno=10
            union all --不带剔重
            select * from emp
            --不需要字段一致，只要数据类型一致,字段数量一致
            select empno,ename from emp
                   where deptno=10
            union 
            select  empno,job from emp
                  
           --交集
           select * from emp
                   where deptno=10
            INTERSECT 
            select * from emp
            --差集
            select * from emp      
            minus 
            select * from emp
                   where deptno=10
            
      --增
          insert into 表名（字段列表） 
                 values(值列表)
          insert into Emp(empno,ename) 
                 values(1,'张无忌',2); --值过多
         insert into Emp(empno,ename) 
                 values(1); --没有足够的值
           insert into Emp(empno,ename) 
                 values(1,'张无忌子'); --长度字节，会异常，不能插入
          --数值类型，没有界定符 
          --字符串 单引号，双引号不行
          --日期类型 date'yyyy-mm-dd'  to_date('','掩码') 
          
          select * from empindept10    
          delete from empindept10 
          alter table empindept10 add bithday date not null;
          --批量插入
          insert into empindept10(empno,ename,sal,bithday)
                 select E.empno,E.Ename,sal,date'1970-01-01' from Emp E
                
      --删
                 --在生产中，强制先查后删（安全）
                 select count(1) from empindept10
                        where empno=7369
                        
                 select * from empindept10
                        where  empno=7369
                        
                 --数据备份
                 create table empindept10_his20211020
                 as
                        select * from empindept10
                        where  empno=7369
                        
                 delete * from empindept10
                        where  empno=7369      
      --改
      
            select * from empindept10
                        where  empno=7369
                        
             update empindept10
                    set
                        sal=sal*1.1,
                        ename='赵敏'
                     where  empno=7369   
                     
     --分页
     --rownum 是系统提供的特殊字段，用来表达结果集的序号（伪列）
     select * from emp
            where rownum<5
            
      select * from emp
            where rownum between 5 and 10
            
      select * from emp
            where rownum=1
            
            
        select * from (select rownum rn,E.* from emp E order by ename)    
                where rn between 5 and 10
               
        
           select * from (select rownum rn,E.* from  (select * from emp E order by ename) E)    
                where rn between 5 and 10
                         
                               
