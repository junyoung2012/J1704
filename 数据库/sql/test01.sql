/*
create tableSpace TB_TEST2
  datafile 'D:\oraclexe\app\oracle\oradata\XE\TB_TEST2.dbf'
  size 10M
  autoextend on next 1m;
  
create user test_user2
    identified by 123456;
    
grant resource,connect to test_user2;
*/

--创建表
--字段 约束
/*数据类型
     数字
                       muber (1-38)
     字符串
                       char(1-2000) 不写默认1
                       varchar
                       varchar2
     日期
          date 精确到秒
          timestamp 精确到纳秒
     */
--drop table Student;
     
create table Student(
       id number(7,2),
       name varchar(2001),
       dob timestamp
       
);

insert into Student values(12345);
insert into Student values(123456);--不行

insert into Student values(12345.56);
insert into Student values(123456.56);--不行
insert into Student values(23456.567);--行

insert into Student(name) values('a');--行
insert into Student(name) values('aa');--行

insert into Student(dob) values(sysdate);--行
/**
       主键
       非null
       唯一
       默认
       检查
       外键
*/
create table Grade(
       id number(9) primary key ,
       name varchar2(2001)  not null
);
--drop table Student;
create table Student(
       id number(7,2) ,
       grade_id references Grade(id),
       name varchar2(2001) unique not null,
       dob timestamp default sysdate,
       sex int check ( sex in (0,1)),
       primary key(id)
       
);

select * from Student;
