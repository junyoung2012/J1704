oracle的研究

1.什么是信息，数据，数据库管理系统（DBMS）,数据库？

2.DBMS是对数据进行管理的，网络软件的服务端。

3.DMMS+管理的数据=数据库

4.oracle的历史

- 文件型数据库

- 层状数据库

- 关系性数据库管理系统

  - 二维表存储数据
  - 表之间有外键关系
  - 符合实体关系模型

- Oracle分为

  - 历史版本 8i,9i,10g,11grid,12c
  - 功能版本 
    - 企业版
    - 标准版
    - Express快捷版
      - 安装包只有300M多 ，有1.5G多
      - 免费进行开发、部署和分发
      - XE 将最多存储 4GB 的用户数据
      - 最多使用 1GB 内存，并在主机上使用一个 CPU
      - 只能建立1个数据库

  数据库使用者角色

  - 数据开发人员
  - DBA 数据库管理人员
  - BI(商业分析人员)

5.Oracle xe数据库的安装

- ​	![image-20211018095345580](https://gitee.com/junyoung2012/blogimg/raw/master/image-20211018095345580.png)
  - 不要安装在中文路径（D:\oraclexe）
  - 在本子上记录密码

6.启动oracle

- ​		![image-20211018095742886](https://gitee.com/junyoung2012/blogimg/raw/master/image-20211018095742886.png)
- ![image-20211018100640833](https://gitee.com/junyoung2012/blogimg/raw/master/image-20211018100640833.png)

查看数据库是否正常启动

- ​	使用SQl Plus连接数据库管理系统
  - 默认2个用户
    - sys 系统中权限最高的用户 不能使用norma身份登录
    - system 系统中权限最高的用户 它可以使用norma身份登录
  - 用户登录oracle有3个常用角色
    - sysdba 数据管理者
    - sysoper 数据库操作者
    - normal 普通身份登录
- 登录
  - ![image-20211018101624153](https://gitee.com/junyoung2012/blogimg/raw/master/image-20211018101624153.png)

![image-20211018101853881](https://gitee.com/junyoung2012/blogimg/raw/master/image-20211018101853881.png)

查看监听状态

![image-20211018102248329](https://gitee.com/junyoung2012/blogimg/raw/master/image-20211018102248329.png)

![image-20211018103138736](https://gitee.com/junyoung2012/blogimg/raw/master/image-20211018103138736.png)

![image-20211018105006938](https://gitee.com/junyoung2012/blogimg/raw/master/image-20211018105006938.png)



​	

sqlplus的使用

- ​	sqlplus命令
  - 登录
  - 退出
  - 连接
  - 断开连接  disconn
  - 
- sql语句
- PL Sql语句 （过程化SQL语句）

创建表空间

``` sql
 create tableSpace TB_TEST2
    datafile 'D:\oraclexe\app\oracle\oradata\XE\TB_TEST2.dbf'
    size 10M
    autoextend on next 1m;
```

创建用户

``` 
create user test_user2
    identified by 123456
    --default tableSpace TB_TEST2;
```

授权

 ``` sql
 grant resource,connect to test_user2;
 ```

- ​	resource 用户角色表示的是 对象的所有者 增删改模式对象
- ​    connect用户角色表示的是 连接数据库，对对象进行CRUD 的权限



安装PL SQLDelvoper

PLD的使用

1. ​	SQL窗口
2. 命令窗口  执行sqlPlus
3. 程序窗口 定义各种模式对象

varchar与varchar2的区别？

- varchar是标准sql里面的， varchar2是oracle提供的独有的数据类型。

- 但是oracle准备在下一版本，不支持varchar类型。

- varchar对于汉字占两个字节，对于数字，英文字符是一个字节，占的内存小，varchar2具体要看数据库使用的字符集

- varchar对空串不处理，varchar2将空串当做null来处理。

  

SQL的执行顺序

``` 
1.from 
2.on 
3.where
4.group by 
5.hving
6.select (rownumber)
7.order by
```

``` 
(7)     SELECT 
(8)     DISTINCT <select_list>
(1)     FROM <left_table>
(3)     <join_type> JOIN <right_table>
(2)     ON <join_condition>
(4)     WHERE <where_condition>
(5)     GROUP BY <group_by_list>
(6)     HAVING <having_condition>
(9)     ORDER BY <order_by_condition>
(10)    LIMIT <limit_number>
```

1.truncate table 

2.marg
