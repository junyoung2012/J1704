--变量名 【CONSTANT】 数据类型 【nor null】 :=值表达式

--------------------------------
declare
      v_1 varchar2(20):='global v1';
      v_2 number(4,-3):=1234;
      v_3 boolean :=true;
begin
      dbms_output.put_line(v_1);
      declare
          v_1 varchar2(20):='sub v1';
      begin
          dbms_output.put_line(v_1);
          dbms_output.put_line(v_2);
          --dbms_output.put_line(TO_CHAR(v_3));
      end;
end;

--------------------------------
declare
      v_ename emp.ename %type;
      v_sal emp.sal %type;
      v_sal_tax  v_sal%type;
      c_tax_rate number(3,2):=0.03;
      
      
begin
      select ename,sal into v_ename,v_sal from emp
             where empno =&eno;
             v_sal_tax:=v_sal*c_tax_rate;
      dbms_output.put_line('雇员名：'||v_ename);
      dbms_output.put_line('工资：'||v_sal);
      dbms_output.put_line('所得税：'||v_sal_tax);
end;

-----------------------------------------------------------
declare
      v_emp emp%rowtype ;
      
      
begin
      select * into v_emp from emp
             where empno =&eno;
             
      dbms_output.put_line('雇员名：'||v_emp.eanme);
      dbms_output.put_line('工资：'||v_emp.sal);
end;
------------------------------------------------------------

declare
      type emp_recode_type is record(
           name emp.ename%type,
           salary emp.sal%type,
           job  emp.job%type
      );
      
      emp_recode emp_recode_type;
begin
      select name,salary,job into emp_recode from emp
             where empno =&eno;
             
      dbms_output.put_line('雇员名：'||emp_recode.eanme);
      dbms_output.put_line('工资：'||emp_recode.sal);
end;
------------------------------------------------------------

declare
      type emp_table_type is table of emp.ename%type
           index by binary_integer;
   
      
      emp_table emp_table_type;
begin
      --select ename into emp_table(-1)  from emp
            -- where empno =&eno;
      emp_table(0):='jack';
      emp_table(1):='luck';        
      dbms_output.put_line('emp_table(0)'||emp_table(0));
      dbms_output.put_line('emp_table(1)'||emp_table(1));
     
end;
------------------------------------------------------------

--条件控制

declare
      v_sal number(7,2) ;
begin
      select sal into v_sal from emp
             where ename='SCOTT';
      dbms_output.put_line('SCOTT工资：'||v_sal);
     
      if v_sal<1000 then
          dbms_output.put_line('SCOTT工资低于1000');
      else
          if  v_sal<2000 then
             dbms_output.put_line('SCOTT工资在1000到2000之间');
          end if;
      end if;
            
end;

------------------------------------------------------------
declare 
      v_grade char :='a';
begin
      case v_grade
           when 'a' then
                dbms_output.put_line('优秀');
           when 'b' then
                 dbms_output.put_line('良好');
           when 'c' then
                dbms_output.put_line('及格');
      end case;
end;

-------------------------------------------------------------
--循环控制
declare
      v_i number :=1;
      v_s number:=0;
begin
      loop
          exit when v_i>100;
          v_s:=v_i+v_s;
          v_i:=v_i+1;
      end loop;
      dbms_output.put_line(v_s);
end;
-----------------------------------------------------------------
declare
      v_i number :=1;
      v_s number:=0;
begin
      while v_i<=100 loop
          v_s:=v_i+v_s;
          v_i:=v_i+1;
      end loop;
      dbms_output.put_line(v_s);
end;
-----------------------------------------------------------------
declare
      v_s number:=0;
begin
      for v_i in 1..100 loop
          v_s:=v_i+v_s;
      end loop;
      dbms_output.put_line(v_s);
end;
-----------------------------------------------------------------
select * from user_errors
--异常处理
--预定义异常
declare
       v_name emp.ename%type;
begin
       select ename into v_name from emp where empno=&eno;
       dbms_output.put_line('雇员名：'||v_name);
exception
       when no_data_found then
            dbms_output.put_line('雇员不存在：'||v_name);

end;


-----------------------------------------------------------------
--非预定义异常
declare 
            e_null_error exception;
            pragma exception_init(e_null_error,-1400);
begin
            insert into dept values(null,'demo1','bj1');
exception
            when e_null_error then
               dbms_output.put_line('不能向部门编号列插入null');  
end;
-----------------------------------------------------------------

declare
               v_sqlcode number;
               v_sqlerrm varchar2(2048);
begin
               insert into dept values()
exception
	
end;
-----------------------------------------------------------------
--自定义异常
declare
               v_sal number;
               e_sal_error exception;
begin
               select sal into v_sal from emp where empno=&empno;
               if v_sal>=2500 then
                  dbms_output.put_line('该雇员工资'||v_sal);
                  raise e_sal_error;
               end if;
exception
               when e_sal_error then
                    dbms_output.put_line('该雇员工资高于2500');
end;
-----------------------------------------------------------------
create or replace procedure query_temp
       (
          v_no in emp.empno%type,
          v_name out emp.ename%type,
          v_sal out emp.sal%type
          
       )
is 
       e_sal_exception exception;
begin
       select ename,sal into  v_name,v_sal from emp where empno=v_no;
       if v_sal>=2500 then
           dbms_output.put_line('该雇员工资'||v_sal);
           raise e_sal_exception;
       end if;
exception
        when no_data_found then
              dbms_output.put_line('没有改雇员');
        when e_sal_exception then
              dbms_output.put_line('该雇员工资高于2500');

end query_temp;

select * from user_errors;

declare
  a1 emp.ename%type;
  a2 emp.sal%type;
begin
  query_temp(v_name=>a1,v_sal=>a2,v_no=>5678);
   dbms_output.put_line(a1);
   dbms_output.put_line(a2);
end;


declare
  a1 emp.ename%type;
  a2 emp.sal%type;
begin
  query_temp(7788,a1,a2);
   dbms_output.put_line(a1);
   dbms_output.put_line(a2);
end;
-----------------------------------------------------------------
create or replace  function get_sal_by_deptno
       (
          v_dept_no in emp.deptno%type,
          v_emp_cnt out number
       )
       return number
 is
       v_sum number(10,2);
 begin
       select sum(sal),count(*) into v_sum,v_emp_cnt from emp where deptno=v_dept_no;
       return v_sum;
 end get_sal_by_deptno;
 -----------------------------------------------------------------------
 declare 
  -- Local variables here
  v_a1 emp.deptno%type;
  v_a2 number;
  v_sum number(10,2);
  
begin
  -- Test statements here
  v_sum:=get_sal_by_deptno(10,v_a2);
  dbms_output.put_line(v_a2||':'||v_sum);
end;
-----------------------------------------------------------------
create or replace procedure query_comm_if_null
       (v_no in emp.empno%type)
 is
       v_comm emp.comm%type;
 begin
       select comm into v_comm from emp where empno=v_no;
       if v_comm is null or v_comm=0 then
          raise_application_error(-20001,'该雇员无补助');
       end if;
 exception
       when no_data_found then
            dbms_output.put_line('没有该雇员');
 end query_comm_if_null;
 
 
 declare 
  e_comm_null exception;
  pragma exception_init(e_comm_null,-20001);
begin
  dbms_output.put_line('7788');
  query_comm_if_null(7788);
exception
  when e_comm_null  then
       dbms_output.put_line('提醒');
end;
-----------------------------------------------------------------
declare
  type deptRecord is record(
       deptno dept.deptno%type,
       dname dept.dname%type,
       loc dept.loc%type
  );
  v_deptRecord deptRecord;
  v_dept_name dept.dname%type;
  v_dept_no dept.deptno%type;
  v_loc dept.loc%type;
  cursor c1
         is
            select dname,loc from dept where deptno<=10;
          
   cursor c2(v_dept_no number)
         is 
            select dname,loc from dept where deptno<=v_dept_no;
           
   cursor c3(v_dept_no number)
            return deptRecord
          is
                select deptno,dname,loc from dept where deptno<=v_dept_no;
                 
   cursor c4(v_dept_no number)
            return deptRecord
          is
                select deptno,dname,loc from dept where deptno<=v_dept_no;
   v_dept_rec c4%rowtype;  
           
begin
   open c1;
   loop
        fetch c1 into v_dept_name,v_loc ;
        if c1%found then
           dbms_output.put_line('c1:'||v_dept_name||'-'||v_loc );
        else
           dbms_output.put_line('c1:记录集处理完毕');
           exit;
        end if;
   end loop;
   
   open c2(20);
   loop
     fetch c2 into v_dept_name,v_loc ;
     exit when c2%notfound;
     dbms_output.put_line('c2:'||v_dept_name||'-'||v_loc );
    end loop;

  open c3(v_dept_no=>30);
  loop
       fetch c3 into v_deptRecord;
       exit when c3%notfound;
        dbms_output.put_line('c3:'||v_deptRecord.dname||'-'||v_deptRecord.loc );
  end loop;
  
  open c4(v_dept_no=>30);
  loop
       fetch c4 into v_dept_rec;
       exit when c4%notfound;
        dbms_output.put_line('c4:'||v_dept_rec.dname||'-'||v_dept_rec.loc );
  end loop;
  close c1;
  close c2;
  close c3;
  close c4;
 
  
  for v_dept_rec in c4(v_dept_no=>30) loop
      dbms_output.put_line('c4:'||v_dept_rec.dname||'-'||v_dept_rec.loc );
  end loop;
  
end;

select * from user_errors;
-----------------------------------------------------------------

select * from emp where empno='7839';

select * from emp order by empno;


----------------------------------------------------------------------
create or replace package my_pkg
       is
       pragma serially_reusable;
       v_deptrec dept%rowtype;
       v_sqlcode number;
       v_sqlerrm varchar2(2048);
       function add_dept(
                v_deptno number,
                v_deptName varchar2,
                v_deptLoc varchar2
       )return number;
       function remove_dept(
                v_deptno number
       )return number;
       procedure query_dept(v_deptno number);
       procedure read_dept;
end my_pkg;

---------------------------------------------------------------------
create or replace package body  my_pkg
       is
       pragma serially_reusable;
       --私有变量
       v_flag number;
       --私有游标
       cursor mycursor is
              select deptno,dname from dept;
       --私有函数
       function check_dept(v_deptno number)
                return number
       is
       begin
                select count(*) into v_flag from dept 
                       where deptno=v_deptno;
                if v_flag>0 then
                   v_flag:=1;
                end if;
                return v_flag;
       end check_dept;
                
      
       --公有函数
       function add_dept(
                v_deptno number,
                v_deptName varchar2,
                v_deptLoc varchar2
       )
                return number
       is
       begin
                if check_dept(v_deptno)=0 then
                   insert into dept values(v_deptno,v_deptName,v_deptLoc);
                   return 1;
                else
                   return 0;
                end if;
       exception
       
                when others then
                     v_sqlcode:=sqlcode;
                     v_sqlerrm:=sqlerrm;
       end add_dept;
       
       function remove_dept(
                v_deptno number
       )
                return number
       is
       begin
                if check_dept(v_deptno)=0 then
                   delete from dept where deptno=v_deptno;
                   return 1;
                else
                   return 0;
                end if;
       exception
       
                when others then
                     v_sqlcode:=sqlcode;
                     v_sqlerrm:=sqlerrm;
       end remove_dept; 
       --公有过程        ;
       procedure query_dept(v_deptno number)
       is
       begin
                if check_dept(v_deptno)=0 then
                   select * into v_deptrec from dept 
                       where deptno=v_deptno;
   
                end if;
       exception
       
                when others then
                     v_sqlcode:=sqlcode;
                     v_sqlerrm:=sqlerrm;
       end query_dept; 
       
       procedure read_dept
       is
                 v_deptno number;
                 v_dname varchar2(14);
                 c_mycursor mycursor%rowtype;  
       begin
      
                 for c_mycursor in mycursor loop
                     v_deptno:=c_mycursor.deptno;
                     v_dname:=c_mycursor.dname;
                     dbms_output.put_line(v_deptno||' '||v_dname);
                 end loop;
               
               -- null ;
       end read_dept;
begin
       v_sqlcode :=null;
       v_sqlerrm :='初始化文本消息';       
end my_pkg;
--select * from user_errors
begin
  my_pkg.read_dept;
end ;
--------------------------------
create or replace trigger  tr_emp_time
before --触发时机
insert or update or delete--触发事件
on emp--触发对象
begin
   if (to_char(sysdate,'day') in('星期六','星期日')) or
      (to_char(sysdate,'hh24') not between 8 and 18) then
      raise_application_error(-20001,'不是上班时间，不能改');
   end if;
end;
----------------------------------------------------
--行级触发器
create or replace  trigger tr_emp_sal_comm
       before update of sal,comm or delete
       on emp
       for each row
       when (old.job='SALESMAN')
begin
       case
            when updating('sal') then
                 if :new.sal<:old.sal then
                    raise_application_error(-20001,'销售人员工资只能升不能降');
                 end if;
            when updating('comm') then
                 if :new.comm<:old.comm then
                    raise_application_error(-20002,'销售人员补助只能升不能降');
                 end if;
            when deleting then
                 raise_application_error(-20002,'销售人员记录不能删除');
      end case;
end;     

--------------
update emp  set sal=1400 where empno='7844';
-------------------------------------------------------------------




create or replace view view_dept_emp
as 
       select  a.deptno,a.dname,b.empno,b.ename
       from dept a,emp b
       where a.deptno=b.deptno;
-----------------

create or replace trigger tr_i_o_dept_emp
instead of insert
on view_dept_emp
for each row
declare
    v_count number;
begin
    select count(*) into v_count  from dept where deptno=:new.deptno;
    if v_count=0 then
       insert into dept(deptno,dname) values(:new.deptno,:new.dname);
    end if;
     select count(*) into v_count  from emp where empno=:new.empno;
    if v_count=0 then
       insert into emp(empno,ename,deptno) values(:new.empno,:new.ename,:new.deptno);
    end if;
end;


-----------------------
insert into view_dept_emp values(11,'d111','222','e222');
----------------------------------------------------------------------------




