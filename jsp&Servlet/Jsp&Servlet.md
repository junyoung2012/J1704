# Jsp&Servlet

1.网页部署成为网站

- ​	什么是HTTP协议？

  - 是网络的一个应用层协议
  - http 超文本传输协议一定是基于请求响应过程，发起请求，产生响应，基于TCP协议上
  - 一个请求，一次响应，有求必应
  - ![image-20211101084602700](https://gitee.com/junyoung2012/blogimg/raw/master/image-20211101084602700.png)

  2.![image-20211101084832663](https://gitee.com/junyoung2012/blogimg/raw/master/image-20211101084832663.png)

![image-20211101085039929](https://gitee.com/junyoung2012/blogimg/raw/master/image-20211101085039929.png)

HTTP的客户端

- ​		浏览器
  - IE /EDGE
  - chrome
  - safari
  - fireFox
  - Opel
- java URLConnection
- HTTpClient
- OkHTTP
- wget/curl

HTTP的服务端

- ​	网络应用的服务端，能启动监听服务，等待连接

- 进行网络通讯

- 接收请求，发出响应

- （javaEE应用）应用服务器

  - Tomcat 开源占有率最高
  - Jetty   内嵌应用服务器（内部测试）
  - Ngenix （反向代理，负载冗余）
  - Apache(处理静态应用，PHP 最好伙伴) LAMP=linux apache mysql php
  - JBoss(功能强大，EJB容器)
  - GlassFish(玻璃鱼，SUN开发的)
  - WebLogic(商用，EJB支持，服务器集群的支持)
  - WebShare（商用，EJB支持，服务器集群的支持）

- HTTP的请求方法

  - GET与POST的区别

  - Accpet

    - 告诉服务端,客户端接收什么类型的响应

  - Referer

    - 表示这是请求是从哪个URL进来的,比如想在网上购物,但是不知道选择哪家电商平台,你就去问度娘,说哪家电商的东西便宜啊,然后一堆东西弹出在你面前,第一给就是某宝,当你从这里进入某宝的时候,这个请求报文的Referer就是www.baidu.com

    Cache-Control

    - 对缓存进行控制,如一个请求希望响应的内容在客户端缓存一年,或不被缓可以通过这个报文头设置

    Accept-Encoding

    - 这个属性是用来告诉服务器能接受什么编码格式,包括字符编码,压缩形式(一般都是压缩形式)
      - 例如:Accept-Encoding:gzip, deflate(这两种都是压缩格式)

    Host

    - 指定要请求的资源所在的主机和端口

    **User-Agent** 作用：告诉服务器，客户端使用的操作系统、浏览器版本和名称

    

  - ![image-20211101091304816](D:\J1704\jsp&Servlet\images\image-20211101091304816.png)

HTTP的响应

​	![image-20211101092257148](https://gitee.com/junyoung2012/blogimg/raw/master/image-20211101092257148.png)

![image-20211101092404434](https://gitee.com/junyoung2012/blogimg/raw/master/image-20211101092404434.png)

MIME （多用途互联网邮件扩展类型）

常见的媒体格式类型如下：

- **text/html** ： HTML格式
- **text/plain** ：纯文本格式
- text/xml ： XML格式
- image/gif ：gif图片格式
- **image/jpeg** ：jpg图片格式   验证码
- image/png：png图片格式

以application开头的媒体格式类型：

- application/xhtml+xml ：XHTML格式

- application/xml： XML数据格式

- application/atom+xml ：Atom XML聚合格式

- **application/json**： JSON数据格式

- application/pdf：pdf格式

- application/msword ： Word文档格式

- **application/vnd.ms-*excel***     excel文档格式

- **application/octet-stream** ： 二进制流数据（如常见的文件下载）

- application/x-www-form-urlencoded ： <form encType="">中默认的encType，form表单数据被编码为key/value格式发送到服务器（表单默认的提交数据的格式）

  

另外一种常见的媒体格式是上传文件之时使用的：

- **multipart/form-data** ： 需要在表单中进行文件上传时，就需要使用该格式
- 

2.Tomcat应用服务器的安装，启动和部署

- ​	安装
  - Tomcat是一个java程序，**必须使用JDK**
  - 解压就可以使用
  - 需要配置一个环境变量 JAVA_HOME （不带bin）
- 启动
  - 允许Tomcat目录下bin/startup.bat
  - ![image-20211101093604772](https://gitee.com/junyoung2012/blogimg/raw/master/image-20211101093604772.png)



![image-20211101093727841](https://gitee.com/junyoung2012/blogimg/raw/master/image-20211101093727841.png)

Tomcat的目录结构

- ​	bin目录

  - 主要是用来存放tomcat的命令，主要有两大类，一类是以.sh结尾的（linux命令），另一类是以.bat结尾的（windows命令）。
  - **startup** 用来启动tomcat 
  - shutdown 用来关闭tomcat 

- conf目录

  - 主要是用来存放tomcat的一些配置文件。

    - server.xml可以设置端口号、设置域名或IP、默认加载的项目、请求编码 

      web.xml可以设置tomcat支持的文件类型 

      context.xml可以用来配置数据源之类的 

      tomcat-users.xml用来配置管理tomcat的用户与权限 

![image-20211101095335802](D:\J1704\jsp&Servlet\images\image-20211101095335802.png)



- 一个主机上可以安装多个Tomcat

  - 就是应为它们会使用不同的端口

- 一**个Tomcat也可以启动多个实例**

  

**协议，IP,端口，有一个不一样，就是2个网站（域）**



把应用程序部署到Tomcat

- 自动部署

- Server.xml部署

  - ![image-20211101100330324](https://gitee.com/junyoung2012/blogimg/raw/master/image-20211101100330324.png)

- web.xml

  **在D:\test\apache-tomcat-8.5.43\conf\Catalina\localhost：添加testA.xml**

  　　**![img](D:\J1704\jsp&Servlet\images\1733080-20190827163012891-669986898.png)**

  　　添加如下内容：

  ```
  <?xml version="1.0" encoding="UTF-8"?>
  <Context docBase="D:\test\dubbo-admin-2.5.10" reloadable="false" />
  ```

  　　docBase还是指向WEB工程的绝对路径。

  此时启动tomcat，浏览器输入：localhost:8080/testA，说明部署成功！（**访问路径为此XML的名称**）

3.HTML和JSP有什么区别？

- 都可以产生网页
- 里面都可以有HTML,CSS,JS
- ​	html可以直接倍浏览器打开，jsp不行
- jsp可以java代码（脚本代码）
- jsp为什么要嵌入java代码？
  - 不就是利用Java代码生成HTML代码
- 动态网页技术：动态（代码根据情况，临时现场产生的）产生网页的技术
  - C++
  - ASP(vb,ASP.net(C#))
  - PHP
  - JSP
  - python
  - go

4.IDEA 

- ​	JET Brein公司出品的JavaEE工具
- ![image-20211101103043167](https://gitee.com/junyoung2012/blogimg/raw/master/image-20211101103043167.png)
- 社区版，专业版

5.maven的使用

- ​	什么是maven ?
  - maven是一个工具
- maven的作用？
  - 依赖管理（jar包数据库）
  - 构建工具（编译构建项目）
  - 项目管理工具（搭建，管理，测试，部署项目）
- 怎么用？
  - 下载（java程序）![image-20211101103645141](https://gitee.com/junyoung2012/blogimg/raw/master/image-20211101103645141.png)
  - 解压
  - 配置
    - maven自己配置（setting.xml）
      - ![image-20211101103814739](https://gitee.com/junyoung2012/blogimg/raw/master/image-20211101103814739.png)
      - ![image-20211101103925564](https://gitee.com/junyoung2012/blogimg/raw/master/image-20211101103925564.png)
- 检查版本
  - ![image-20211101104154067](https://gitee.com/junyoung2012/blogimg/raw/master/image-20211101104154067.png)
- mvn system插件
  - ![image-20211101104442174](https://gitee.com/junyoung2012/blogimg/raw/master/image-20211101104442174.png)
- GAV
  - 组织（公司或组织）
  - 软件的编号
  - 版本
- ![image-20211101104728405](https://gitee.com/junyoung2012/blogimg/raw/master/image-20211101104728405.png)

![image-20211101105129709](https://gitee.com/junyoung2012/blogimg/raw/master/image-20211101105129709.png)

![image-20211101105310672](https://gitee.com/junyoung2012/blogimg/raw/master/image-20211101105310672.png)

maven项目的目录结构

src/main/java目录核心代码部分。
src/main/resources  配置资源部分。
src/test/java  测试代码部分。
src/test/resources  测试资源文件。
src/main/webapp 网页页面资源，js，css，图片等。

![image-20211101111713342](https://gitee.com/junyoung2012/blogimg/raw/master/image-20211101111713342.png)

以下的文档介绍是Maven希望的目录结构，并且也是目录创建工程是采用的目录结构。Maven推荐大家尽可能的遵守这样的目录结构。

| **src/main/java**  | **Application/Library sources** |
| ------------------ | ------------------------------- |
| src/main/resources | Application/Library resources   |
| src/main/filters   | Resource filter files           |
| src/main/assembly  | Assembly descriptors            |
| src/main/config    | Configuration files             |
| src/main/webapps   | Web application sources         |
| src/test/java      | Test sources                    |
| src/test/resources | Test resources                  |
| src/test/filters   | Test resource filter files      |
| src/site           | Site                            |
| LICENSE.txt        | Project's license               |
| README.txt         | Project's readme                |



## maven为什么要定义这么多目录？

​	**约定大于配置**，maven在构建项目过程中会根据不同文件夹的性质，做出不同的操作



JSP的内部元素

1. 编译元素（jsp如何编译java class）

   1. page 页面

      pageEncoding JSP字符集

      contentype 响应的数据类型

   2. include 包含(编译时包含)

   3. taglib 标签库

2. 模板元素

   1. HTML
   2. CSS
   3. JS

3. ~~脚本元素~~

   1. 普通脚本元素
   2. 声明式脚本
   3. 表达式脚本

4. 注释元素

   1. html注释
   2. jsp注释 （节省流量）

5. ~~动作指令~~

   1. 动作指令就是使用jsp前缀标签，代替java代码
   2. 为什么要有动作指令？
      1. jsp作为模板技术，就是用来显示数据的
      2. 数据和表示进行分离
      3. 它被**JSTL,EL**表达式替代
      4. jsp:include（运行时包含）
   
6. 其它的模板技术

   1. freeMark
   2. themeLeaf
   3. velocity

   



5.什么是Servlet?

- ​	什么是Servlet？

  - Servlet服务端脚本技术，它是javaEE技术的一个组成。

    - 什么是Java EE?

      - Java EE，Java 平台企业版（Java Platform Enterprise Edition）

      - javaEE13项技术

        

        　　JAVAEE平台由一整套服务（Services）、应用程序接口（APIs）和协议构成，它对开发基于Web的多层应用提供了功能支持，下面对JAVAEE中的13种技术规范进行简单的描述(限于篇幅，这里只进行简单的描述)：

          　　1、**JDBC**(Java Database Connectivity) 　　JDBC API为访问不同的数据库提供了一种统一的途径，象ODBC一样，JDBC对开发者屏蔽了一些细节问题，另外，JDCB对数据库的访问也具有平台无关性。

          　　2、JNDI(Java Name and Directory Interface) 　　JNDI API被用于执行名字和目录服务。它提供了一致的模型来存取和操作企业级的资源如DNS和LDAP，本地文件系统，或应用服务器中的对象。

          　　3、EJB(Enterprise JavaBean) 　　JAVAEE技术之所以赢得媒体广泛重视的原因之一就是EJB。它们提供了一个框架来开发和实施分布式商务逻辑，由此很显著地简化了具有可伸缩性和高度复杂的企业级应用的开发。EJB规范定义了EJB组件在何时如何与它们的容器进行交互作用。容器负责提供公用的服务，例如目录服务、事务管理、安全性、资源缓冲池以及容错性。但这里值得注意的是，EJB并不是实现JAVAEE的唯一途径。正是由于JAVAEE的开放性，使得有的厂商能够以一种和EJB平行的方式来达到同样的目的。

          　　4、RMI(Remote Method Invoke) 　　正如其名字所表示的那样，RMI协议调用远程对象上方法。它使用了序列化方式在客户端和服务器端传递数据。RMI是一种被EJB使用的更底层的协议。

          　　5、Java IDL/CORBA 　　在Java IDL的支持下，开发人员可以将Java和CORBA集成在一起。他们可以创建Java对象并使之可在CORBA ORB中展开, 或者他们还可以创建Java类并作为和其它ORB一起展开的CORBA对象的客户。后一种方法提供了另外一种途径，通过它Java可以被用于将你的新的应用和旧的系统相集成。

          　　6、**JSP(Java Server Pages)** 　　JSP页面由HTML代码和嵌入其中的Java代码所组成。服务器在页面被客户端所请求以后对这些Java代码进行处理，然后将生成的HTML页面返回给客户端的浏览器。

          　　7、**Java Servlet** 　　Servlet是一种小型的Java程序，它扩展了Web服务器的功能。作为一种服务器端的应用，当被请求时开始执行，这和CGI Perl脚本很相似。Servlet提供的功能大多与JSP类似，不过实现的方式不同。JSP通常是大多数HTML代码中嵌入少量的Java代码，而servlets全部由Java写成并且生成HTML。

          　　8、**XML**(Extensible Markup Language) 　　XML是一种可以用来定义其它标记语言的语言。它被用来在不同的商务过程中共享数据。 XML的发展和Java是相互独立的，但是，它和Java具有的相同目标正是平台独立性。通过将Java和XML的组合，您可以得到一个完美的具有平台独立性的解决方案。

          　　9、JMS(Java Message Service) 　　JMS是用于和面向消息的中间件相互通信的应用程序接口(API)。它既支持点对点的域，有支持发布/订阅(publish/subscribe)类型的域，并且提供对下列类型的支持：经认可的消息传递,事务型消息的传递，一致性消息和具有持久性的订阅者支持。JMS还提供了另 一种方式来对您的应用与旧的后台系统相集成。

          　　10、JTA(Java Transaction Architecture) 　　JTA定义了一种标准的API，应用系统由此可以访问各种事务监控。

          　　11、JTS(Java Transaction Service) 　　JTS是CORBA OTS事务监控的基本的实现。JTS规定了事务管理器的实现方式。该事务管理器是在高层支持Java Transaction API (JTA)规范，并且在较底层实现OMG OTS specification的Java映像。JTS事务管理器为应用服务器、资源管理器、独立的应用以及通信资源管理器提供了事务服务。

          　　12、**JavaMail** 　　JavaMail是用于存取邮件服务器的API，它提供了一套邮件服务器的抽象类。不仅支持SMTP服务器，也支持IMAP服务器。

          　　13、JAF(JavaBeans Activation Framework) 　　JavaMail利用JAF来处理MIME编码的邮件附件。MIME的字节流可以被转换成Java对象，或者转换自Java对象。大多数应用都可以不需要直接使用JAF

  7.如何创建Servlet?

  1. 特殊的类
     1. 实现Servlet接口
     2. 继承genericlServlet
     3. 继承HttpServlet(常用)
  2. 配置
     1. xml
        1. servlet
        2. servlet-maping
     2. 注解配置（Servlet 3.0）

  8 Servlet 生命周期

  - ​	2启

    - 第一次网路请求
    - loadOnStartUp（应用程序启动时加载，数字越小加载越早0最小）

    ![image-20211102112821831](https://gitee.com/junyoung2012/blogimg/raw/master/image-20211102112821831.png)

    ![image-20211102112856033](https://gitee.com/junyoung2012/blogimg/raw/master/image-20211102112856033.png)

  - 3停

    - 停应用程序
    - 停应用服务器（tomcat）
    - 停服务器

  - service比doGet/doPost先执行

    - get请求的产生方式
      - 点击连接
      - 地址栏书地址，回车
      - 表单提交 method=get(默认)
      - src会引起get
    - post只有一种
      - 表单提交 method=post

- jsp与Servlet区别？




9.Servlet的常用类和接口（jsp的9个内置对象）



1. | 内置对象名称 | 真实类型            | 功能                                                         |
   | ------------ | ------------------- | ------------------------------------------------------------ |
   | request      | HttpServletRequest  | 1.接收参数<br />2.操作属性<br />3.跳转（前向）页面<br />4.得到session对象 |
   | session      | HttpSession         | 1.操作属性<br />2.得到application对象                        |
   | application  | ServletContext      | 1.操作属性<br />2.获得应用程序级初始化参数                   |
   | config       | ServletConfig       | 1.获得Servlet级初始化参数<br />2.得到application对象         |
   | response     | HttpServletResponse | 1.得到out对象<br />2.可以跳转（重定向）页面<br />3.向浏览器发送cookie |
   | pageContext  | PageContext         | 1.操作属性（4个范围）<br />2.得到其它的8个内置对象           |
   | out          | PrintWriter         | 重写了print方法，负责输出                                    |
   | exception    | Throwable           | 处理异常                                                     |
   | page         | Object              | 相当于this                                                   |

![image-20211103095115263](https://gitee.com/junyoung2012/blogimg/raw/master/image-20211103095115263.png)



10.数据域

- ​	类似于map集合的，存在服务器的数据结构

- 可以增删改查对象

- **数据域**有4个范围(4大银行)

  - ~~page~~
  - request(能经过请求链上多个Servlet)
  - session (连接多个请求数据结构，一个用户多次请求存在在一个session)
  - ~~applicaion(在整个应用程序 存储数据)~~

- 参数和属性的区别？

  - 来源不同，参数是从客户端传上来，属性服务端存，服务端取
  - 类型不同 参数是String或String数组，属性Object
  - 任务不同  客户端信息，传到服务端 ，属性从控制器传输数据到jsp。

  forward 与redirect的区别？

  - 2者都能跳转页面
  - 跳转机理不同 forward 在服务端跳转，redirect，对浏览器发送3XX响应，浏览器重新发送请求，访问服务端
  - 请求次数不一样 forward 始终同一个请求，redirect多次请求，forward 有更高的效率
  - 在request数据域里的属性，不能在redirect后得到。
  - forward 不会引起地址栏变化，redirect地址栏会跳转到最后的地址。

## jsp一定是工作在服务端，工作在Tomcat里，不是在浏览器里工作



jsp中的模板技术支持

## 11.	EL (表达式语言)

- 功能
  - 输出属性的值
  - 简单运算
  - 可以调用对象中的方法
- el表达式11个对象
  - 计算
    - 算术运算
    - 关系运算
    - 逻辑元素

![image-20211103114017425](https://gitee.com/junyoung2012/blogimg/raw/master/image-20211103114017425.png)

### EL运算符

 

| 运算符字符串  | 说明get  | 范例table                                                    | 结果变量 |
| ------------- | -------- | ------------------------------------------------------------ | -------- |
| +List         | 加map    | ${17+5}                                                      | 22       |
| -             | 减       | ${17-5}                                                      | 12       |
| *             | 乘       | ${17*5}                                                      | 85       |
| /或div        | 除       | ${17/5}或${17 div 5}                                         | 3        |
| %或mod        | 取余     | ${17%5}或${17 mod 5}                                         | 2        |
| ==或**eq**    | 等于     | ${5==5}或${5 eq 5}                                           | true     |
| !=或**ne**    | 不等于   | ${5!=5}或${5 ne 5}                                           | false    |
| <或**lt**     | 小于     | ${3<5}或${3 lt 5}                                            | true     |
| >或**gt**     | 大于     | ${3>5}或${3 gt 5}                                            | false    |
| <=或le        | 小于等于 | ${3<=5}或${3 le 5}                                           | true     |
| >=或ge        | 大于等于 | ${3>=5}或${3 ge 5}                                           | false    |
| &&或and       | 而且     | ${true&&false}或${true and false}                            | false    |
| !或not        | 非       | ${!true}或${not true}                                        | false    |
| \|\|或or      | 或者     | ${true\|\|false}或${true or false}                           | true     |
| **==empty==** | 是否为空 | ${empty “”}，能够判断字符串、数组、集合的长度是否为0，为0返回true。empty还能够与not或!一块儿使用。${not empty “”} | true     |

12.JSTL

- ​	JSTL=java standarded tag lib=java标准标签库
  - 核心标签
    - ![image-20211103141750875](https://gitee.com/junyoung2012/blogimg/raw/master/image-20211103141750875.png)
  - 格式化标签
    - formatDate
  - SQL标签
  - XML标签
  - 函数标签
    - 字符串处理
  
  11.过滤器和监听器
  
  - ​		过滤器（装饰者模式）
  
    - 过滤器是一种特殊的servlet
    - 过滤请求，过滤响应
      - 改变
      - 拦截
      - 特点
        - 不改变源码添加功能
        - 动态添加
        - 声明式配置解决
  
  - 怎么创建过滤器
  
    - 一个特殊的类
      - 实现Filter（过滤器）接口
    - 配置
      - xml
      - 注解
  
  - 监听器（观察者模式）
  
    - request
      - create
      - destroy
    - session
    - application
    - requestAttrbute
      - 增
      - 删
      - 改
    - sessionAttrbute
    - applicationAttrbute
  
    
  
  12.会话跟踪技术
  
  ​	为什么要有会话跟踪技术？
  
  - ​		HTTP是无状态协议，本身在多次请求之间没有联系
  - 需要开发者自己维护一个用户的多次请求之间的联系
  
- 会话跟踪技术有4个

  - URL重写

  - 隐藏（只读）表单域

  - session

    - getSeesion

    - invalidate()  让整个session失效

    - session超时

      - setMaxInactiveInterval (秒) 0或者不设置，永不超时

      - 项目web.xml

        - ``` xml
          <session-config>
              <!--此处设置本项目所有的session默认超时时间（单位是分钟）-->
              <session-timeout>5</session-timeout>
            </session-config>
          ```

      - conf/web.xml

        - 整个应用服务器超时时间（分钟）
        - ![image-20211105094859997](https://gitee.com/junyoung2012/blogimg/raw/master/image-20211105094859997.png)

    如何设置超时时间

    - 小说站
    - 网速慢
    - 个人网银（短）

  - cookie

    - 其实是服务端响应对象，让浏览器建立的在客户端机器上保存的文本文件。
    - 小数据库
    - 就是为了让请求，隐式的当作参数，每次都传到服务端。
    - cookie是根据**同源策略**上传的
    - cookie的超时设置
      - 正数（以秒为单位超时时间）
      - 0 立即删除
      - 负数 表示，浏览器退出删除

  13.文件上传

- 分页

- 登录

  - 权限验证
  - 记住用户名
  - 验证码

- 购物车





登录操作可能会访问的表：

1. ​	用户
2. 角色
3. 权限
4. 用户-角色
5. 角色-权限
6. 在线用户表
7. 积分表
8. 访问历史
9. 我的订单
10. 访问偏好
