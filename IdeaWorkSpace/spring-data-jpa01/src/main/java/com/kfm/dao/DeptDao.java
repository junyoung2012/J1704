package com.kfm.dao;

import com.kfm.model.Dept;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface DeptDao extends JpaRepository<Dept,Integer> {
    List<Dept> findByDname(String name);
    List<Dept> findByOrderByDnameDesc();
    @Query("from Dept where loc=:loc" )//JPQL 也是面向对象的
    List<Dept> test1(String loc);
    @Query(nativeQuery = true,value = "select * from Dept where loc=:loc" )//SQL
    List<Dept> test2(String loc);
}
