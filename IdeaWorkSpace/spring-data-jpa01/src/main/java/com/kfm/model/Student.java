package com.kfm.model;

import lombok.Data;

import javax.persistence.Id;
import javax.persistence.IdClass;

@Data
//@Entity
@IdClass(StudentPk.class)
public class Student {
    @Id
    private int schoolId;
    @Id
    private int code;
//    private StudentPk pk;
}
