package com.kfm.model;

import javax.persistence.Entity;

@Entity
//@DiscriminatorColumn(name="type",
//        discriminatorType = DiscriminatorType.STRING,
//        length=30)
//@DiscriminatorValue("s")
public class Scientist extends Person {
    private long contrbution;

    public long getContrbution() {
        return contrbution;
    }

    public void setContrbution(long contrbution) {
        this.contrbution = contrbution;
    }
}
