package com.kfm.model;

import javax.persistence.Id;
import javax.persistence.OneToOne;

//@Entity
public class Husband {
    @Id
    private int id;
    private String name;
    @OneToOne
    private Wife wife;
}
