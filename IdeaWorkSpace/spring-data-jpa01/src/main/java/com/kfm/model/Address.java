package com.kfm.model;

import javax.persistence.Embeddable;

@Embeddable
public class Address {
    private String city;
    private String road;
}
