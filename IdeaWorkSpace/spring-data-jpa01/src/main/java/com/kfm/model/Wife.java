package com.kfm.model;

import javax.persistence.Id;
import javax.persistence.OneToOne;

//@Entity
public class Wife {
    @Id
    private int id;
    private String name;
    @OneToOne(mappedBy = "wife")
    private Husband husband;
}
