package com.kfm.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

/**
 * @Getter and @Setter
 * @FieldNameConstants
 * @ToString
 * @EqualsAndHashCode
 * @AllArgsConstructor, @RequiredArgsConstructor and @NoArgsConstructor
 * @Log, @Log4j, @Log4j2, @Slf4j, @XSlf4j, @CommonsLog, @JBossLog, @Flogger, @CustomLog
 * @Data
 * @Builder
 * @SuperBuilder
 * @Singular
 * @Delegate
 * @Value
 * @Accessors
 * @Wither
 * @With
 * @SneakyThrows
 * @val
 * @var
 * experimental @var
 * @UtilityClass
 * Lombok config system
 * Code inspections
 * Refactoring actions (lombok and delombok)
 */
//@Data
//@Setter
//@Getter
//@EqualsAndHashCode(of = "deptno")
//@AllArgsConstructor
//@NoArgsConstructor
//@RequiredArgsConstructor
@Data
@Slf4j
@NoArgsConstructor
//@Entity
//@Table(name = "T_Dept")
public class Dept {
    @Id //标识属性
    @GeneratedValue(strategy = GenerationType.IDENTITY )
    private int deptno;
    private String dname;
//    @Column(name = "dloc",length = 40)
    @Transient
    private String loc;
    @Temporal(TemporalType.DATE)
    private Date createDate;
//    部门的所有员工
    @OneToMany(fetch = FetchType.LAZY,mappedBy = "dept")
    private Set<Emp> emps;

    public Dept(String dname, String loc) {
        this.dname = dname;
        this.loc = loc;
    }

    @Override
    public String toString() {
        return "Dept{" +
                "deptno=" + deptno +
                ", dname='" + dname + '\'' +
                ", loc='" + loc + '\'' +
                "}\n";
    }
    //    public static void main(String[] args) {
//        Dept dept=new Dept();
////        dept
//        log.debug();
//    }
}
