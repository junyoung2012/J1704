package com.kfm.model;


import javax.persistence.ElementCollection;
import javax.persistence.Id;
import java.util.Map;

//@Entity
public class User {
//    基本
    @Id
    private String nikeName;
    private String email;
    private String password;
//    @ElementCollection
//    @CollectionTable(
//            name = "user_hobbies",
//            joinColumns = {
//                    @JoinColumn(name = "nikeName")
//            }
//    )
//    @OrderColumn(name = "idx")
////    @JoinTable(name = "hobbies")
//    @Column(name = "hhhh")
//    private List<String> hobbies;
    @ElementCollection
    private Map<String,Address> map;
//    高级
//    @Embedded
//    private AdvanceInfo advanceInfo;

}
