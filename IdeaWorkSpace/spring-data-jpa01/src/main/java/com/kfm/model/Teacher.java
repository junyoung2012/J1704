package com.kfm.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.util.Set;

@Entity
public class Teacher {
    @Id
    private int id;
    private String name;
    @ManyToMany(mappedBy = "teachers")
    private Set<Course> courses;
}
