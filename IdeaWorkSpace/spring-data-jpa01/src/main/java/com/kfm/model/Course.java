package com.kfm.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.util.Set;

@Entity
public class Course {
    @Id
    private int id;
    private String name;
    @ManyToMany
    private Set<Teacher> teachers;
}
