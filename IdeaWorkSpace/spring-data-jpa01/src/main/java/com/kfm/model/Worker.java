package com.kfm.model;

import javax.persistence.Entity;

@Entity
//@DiscriminatorColumn(name="type",
//        discriminatorType = DiscriminatorType.STRING,
//        length=30)
//@DiscriminatorValue("w")
public class Worker extends Person {
    private int sal;

    public int getSal() {
        return sal;
    }

    public void setSal(int sal) {
        this.sal = sal;
    }
}
