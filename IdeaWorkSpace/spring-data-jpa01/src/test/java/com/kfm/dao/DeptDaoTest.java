package com.kfm.dao;

import com.kfm.model.Dept;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import java.util.List;

@SpringBootTest
class DeptDaoTest {
    @Autowired
    private DeptDao deptDao;
//    @Autowired
//    SessionFactory sessionFactory;
    @Test
    public void add(){
        Dept dept=new Dept("设计软件开发事业部","西安高新区");
        System.out.println(deptDao.save(dept));
        dept=new Dept("研发部","西安高新区");
        System.out.println(deptDao.save(dept));
        dept=new Dept("工程部","西安高新区");
        System.out.println(deptDao.save(dept));
        dept=new Dept("技术部","西安高新区");
        System.out.println(deptDao.save(dept));
        dept=new Dept("品质保证部","西安高新区");
        System.out.println(deptDao.save(dept));
        dept=new Dept("管理信息系统部","西安高新区");
        System.out.println(deptDao.save(dept));
       dept=new Dept("客户维护部","西安高新区");
        System.out.println(deptDao.save(dept));
    }
    @Test
    public void getAll(){
        System.out.println(deptDao.findAll());
    }
    @Test
    public void getAll2(){
//        Sort sort=Sort.by("deptno");
        Sort sort=Sort.by(Sort.Direction.DESC,"deptno");
        System.out.println(deptDao.findAll(sort));
    }
    @Test
    public void getAll3(){
        PageRequest pageRequest=PageRequest.of(1,3);
        Page<Dept> deptPage= deptDao.findAll(pageRequest);
        System.out.println(deptPage.getContent());
        System.out.println(deptPage.getPageable().getPageNumber());
        System.out.println(deptPage.getTotalPages());
    }
    @Test
    public void findByDname(){
        List<Dept> deptPage= deptDao.findByDname("品质保证部");
        System.out.println(deptPage);
    }
    @Test
    public void findByOrderByDnameDesc(){
        List<Dept> deptPage= deptDao.findByOrderByDnameDesc();
        System.out.println(deptPage);
    }
    @Test
    public void test1(){
        List<Dept> depts= deptDao.test1("西安高新区");
        System.out.println(depts);
    }
    @Test
   public void test2(){
        List<Dept> depts= deptDao.test2("西安高新区");
        System.out.println(depts);
    }
   @Test
   public void test3(){
       System.out.println("111");
//       Worker worker=new Worker();
//       worker.setId(111);
//       worker.setSal(800);

    }
}