package com.kfm.test;

public class ConvertTest {
    private static String[] maps="0123456789ABCDEFGHIJKLMNOPQRSTUV".split("");

    public static void main(String[] args) {
//        System.out.println(maps[31]);
        long i=conve32To10("1234567");
        System.out.println(i);
        String s=conver10To32(i);
        System.out.println(s);
    }

    private static String conver10To32(long i) {
        StringBuilder sb=new StringBuilder();
        do{
            String map = maps[(int) (i % 32)];

            sb.insert(0,map);
        }while((i /= 32)>0);
        return sb.toString();
    }

    private static long conve32To10(String s) {
        int sum=0;
        for (int i = 0; i <s.length() ; i++) {
            int wight = getWight(String.valueOf(s.charAt(s.length() - 1 - i)));
            long k= (long) (wight*Math.pow(32,i));
//            System.out.println(k);
            sum+=wight*Math.pow(32,i);

        }
        return sum;
    }

    private static int getWight(String s) {
        for (int i = 0; i < maps.length; i++) {
            if(s.equals(maps[i])) return i;
        }
        return -1;
    }
}
