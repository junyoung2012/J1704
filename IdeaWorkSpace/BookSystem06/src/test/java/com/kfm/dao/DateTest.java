package com.kfm.dao;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.Temporal;
import java.time.temporal.TemporalAdjuster;
import java.time.temporal.TemporalAdjusters;
import java.util.Date;

public class DateTest {
    public static void main(String[] args) {
//        testLocalDate();
//        testLocalDate2();
//        testFormat();
//        testConvert();
//        testJudge();
//        testStringToLocalDate();
//        testPlus();
//        testWith();
//        testTemporalAdjusters();
//        testInstant();
        testDuration();
    }

    private static void testDuration() {
        Instant start = Instant.now();
        for (int i = 0; i < 15; i++) {
            System.out.println(i);
        }
        Instant end = Instant.now();
        Duration duration = Duration.between(start, end);
//        long l = duration.toNanos();
        long l = duration.toMillis();

        //间隔的时间
        System.out.println("循环耗时："+l+"纳秒");
    }

    private static void testInstant() {
        //  Instant 时间戳类从1970 -01 - 01 00:00:00 截止到当前时间的毫秒值
        Instant now = Instant.now();
        System.out.println(now); //获取的是默认时区，获取的不是中国 的时区

        //获取当前时区的，我们可以添加偏移量,返回偏移过后的日期
        OffsetDateTime offsetDateTime = now.atOffset(ZoneOffset.ofHours(8));
        System.out.println(offsetDateTime);
        System.out.println("===========================");

        //从1970 - 01 - 01 00:00:00 截止到当前时间的毫秒值
        long l = System.currentTimeMillis();
        System.out.println(l);
        long time = new Date().getTime();

        //JDK1.8 Instant 时间戳类从1970 -01 - 01 00:00:00 截止到当前时间的毫秒值
        Instant now1 = Instant.now();

        //toEpochMilli():从1970 -01 - 01 00:00:00 截止到当前时间间隔的毫秒值
        long l1 = now1.toEpochMilli();
        System.out.println(l1);

        //获取从1970 -01 - 01 00:00:00 截止到当前时间间隔的秒值
        long epochSecond = now1.getEpochSecond();
        System.out.println(epochSecond);

        System.out.println("==========================");
        //给计算机元年增加相应的时间量
        Date date = new Date(1000 * 60 * 60*24);
        System.out.println(date);

        //现在 给计算机元年增加相应的时间量
        //5. ofEpochSecond() 方法 给计算机元年增加秒数
        //ofEpochMilli() 给计算机元年增加毫秒数
        Instant instant = Instant.ofEpochMilli(1000 * 60 * 60 * 24);
        System.out.println(instant);

        //ofEpochSecond() 方法 给计算机元年增加秒数
        Instant instant1 = Instant.ofEpochSecond(60 * 60 * 24);
        System.out.println(instant1);
    }

    private static void testTemporalAdjusters() {
        LocalDate now = LocalDate.now();
        //指定日期
        //对于一些特殊的日期，可以通过一个工具类TemporalAdjusters 来指定
        TemporalAdjuster temporalAdjuster = TemporalAdjusters.firstDayOfMonth();    //见名知意，本月第一天
        LocalDate with = now.with(temporalAdjuster);
        System.out.println(with);

        TemporalAdjuster next = TemporalAdjusters.next(DayOfWeek.SUNDAY);   //下周周末
        LocalDate with1 = now.with(next);
        System.out.println(with1);
        System.out.println("===================================");


        LocalDate now1 = LocalDate.now();
        //自定义日期 —— 下一个工作日
        LocalDate with2 = now1.with(new TemporalAdjuster() {
            @Override
            //参数 nowDate 当前的日期对象
            public Temporal adjustInto(Temporal nowDate) {
                //向下转型
                LocalDate date= (LocalDate) nowDate;
                if(date.getDayOfWeek().equals(DayOfWeek.FRIDAY)){
                    LocalDate localDate = date.plusDays(3);
                    return localDate;
                }else if(date.getDayOfWeek().equals(DayOfWeek.SATURDAY)){
                    LocalDate localDate = date.plusDays(2);
                    return localDate;
                }else{
                    LocalDate localDate = date.plusDays(1);
                    return localDate;
                }
            }
        });
        System.out.println("下一个工作日是：" + with2);
    
    }

    private static void testWith() {
        //指定某个日期的方法 with()方法
        LocalDate now2 = LocalDate.now();
        LocalDate localDate = now2.withYear(2014);
        System.out.println(localDate);

        // TemporalAdjusters工具类，提供了一些获取特殊日期的方法
        LocalDate with = now2.with(TemporalAdjusters.firstDayOfMonth());
        System.out.println(with);
        LocalDate with1 = now2.with(TemporalAdjusters.lastDayOfMonth());
        System.out.println(with1);

        //获取这个月的第几个星期几是几号,比如 TemporalAdjusters.dayOfWeekInMonth(2, DayOfWeek.FRIDAY)
        // 代表的意思是这个月的第二个星期五是几号
        LocalDate with2 = now2.with(TemporalAdjusters.dayOfWeekInMonth(2, DayOfWeek.FRIDAY));
        System.out.println(with2);
    }

    private static void testPlus() {
        //增加时间量的方法 plusXXX系类的方法 返回的是一个新的日期对象
        LocalDateTime now = LocalDateTime.now();
        //可以给当前的日期增加时间量
        LocalDateTime newDate= now.plusYears(1);
        int year = newDate.getYear();
        System.out.println(year);

        System.out.println("================================");
        //减去时间量的方法minusXXX 系列的方法 返回的是一个新的日期对象
        LocalDate now1 = LocalDate.now();
        LocalDate newDate2 = now1.minusDays(10);
        int dayOfMonth = newDate2.getDayOfMonth();
        System.out.println(dayOfMonth);
    }

    private static void testStringToLocalDate() {
        //给出一个符合默认格式要求的日期字符串
        String dateStr="2020-01-01";

        //把日期字符串解析成日期对象 如果日期字符串时年月日 解析时用  LocalDate
        LocalDate parse = LocalDate.parse(dateStr);
        System.out.println(parse);

        System.out.println("===========================================");
        //给出一个符合默认格式要求的 时分秒 字符串
        String dateTimeStr="14:20:30";

        //把 时分秒 字符串解析成时分秒对象
        LocalTime parse1 = LocalTime.parse(dateTimeStr);
        System.out.println(parse1);

        System.out.println("=========================================");
        //给出一个符合默认格式要求的 日期时分秒 字符串
        String str="2018-12-12T14:20:30";

        //把 日期时分秒 字符串解析成时分秒对象
        LocalDateTime parse2 = LocalDateTime.parse(str);
        System.out.println(parse2);

        System.out.println("========================================");
        //给出一个自定义日期时分秒格式字符串
        String dateStr2="2020年12月12日 12:13:14";

        //给出一个自定义解析格式
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy年MM月dd日 HH:mm:ss");

        //按照指定的格式去解析
        LocalDateTime parse3 = LocalDateTime.parse(dateStr2,formatter);
        System.out.println(parse3);
    }


    private static void testJudge() {
        //获取当前的日期
        LocalDate now = LocalDate.now();

        //指定的日期
        LocalDate of = LocalDate.of(2015, 12, 12);

        //判断一个日期是否在另一个日期之前
        boolean before = of.isBefore(now);
        System.out.println(before);

        //判断一个日期是否在另一个日期之后
        boolean after = of.isAfter(now);
        System.out.println(after);

        //判断这两个日期是否相等
        boolean after1 = now.equals(of);
        System.out.println(after1);

        //判断闰年
        boolean leapYear = of.isLeapYear();
        System.out.println(leapYear);
    }

    private static void testConvert() {
        LocalDateTime now = LocalDateTime.now();
        System.out.println(now);

        //将目标LocalDateTime转换为相应的LocalDate对象
        LocalDate localDate = now.toLocalDate();
        System.out.println(localDate);

        ///将目标LocalDateTime转换为相应的LocalTime对象
        LocalTime localTime = now.toLocalTime();
        System.out.println(localTime);
    }

    private static void testFormat() {
        //获取当前日期时分秒
        LocalDateTime now = LocalDateTime.now();

        //默认格式  年-月-日T时:分:秒
        System.out.println(now);

        //指定格式
        DateTimeFormatter ofPattern = DateTimeFormatter.ofPattern("yyyy年MM月dd日 HH时mm分ss秒");
        //传入格式
        String dateStr = now.format(ofPattern);
        System.out.println(dateStr);
    }

    private static void testLocalDate2() {
        //获取日期时分秒
        LocalDateTime now = LocalDateTime.now();

        //获取年份
        int year = now.getYear();
        System.out.println(year);

        ///获取月份枚举
        //Month 枚举类，定义了十二个月份
        Month month = now.getMonth();
        System.out.println(month);

        //获取月份的数值
        int monthValue = now.getMonthValue();
        System.out.println(monthValue);

        //获取当天在本月的第几天
        int dayOfMonth = now.getDayOfMonth();
        System.out.println(dayOfMonth);

        //获取小时
        int hour = now.getHour();
        System.out.println(hour);

        //获取分钟
        int minute = now.getMinute();
        System.out.println(minute);

        //获取秒值
        int second = now.getSecond();
        System.out.println(second);
    }

    //    JODA
    private static void testLocalDate() {
        /* 通过静态方法 now() 返回该类的实例 */
        //获取当前的日期时分秒
        LocalDateTime now = LocalDateTime.now();
        System.out.println(now);

        LocalDate localDate=LocalDate.now();
        System.out.println(localDate);

        LocalTime localTime=LocalTime.now();
        System.out.println(localTime);

        /* 静态方法 of() 返回该类的实例 */
        //指定日期时分秒
        LocalDateTime localDateTime = LocalDateTime.of(2048, 11, 25, 12, 00, 30);
        System.out.println(localDateTime);

        //指定日期
        LocalDate date = LocalDate.of(2020, 12, 12);
        System.out.println(date);

        //指定时分秒
        LocalTime time = LocalTime.of(14, 20, 30);
        System.out.println(time);
    }
}
