package com.kfm.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:applicationContext.xml")
public class ServiceBusinessTest {
    @Autowired
    private ServiceBusiness serviceBusiness;

    @Test
    public void add() {
    }

    @Test
    public void delete() {
    }

    @Test
    public void getService() {
    }

    @Test
    public void update() {
    }

    @Test
    public void getServices() {
        System.out.println(serviceBusiness.getServices());
    }

    @Test
    public void getServices1() {
    }

    @Test
    public void getServiceTypes() {
    }

    @Test
    public void getTechnicains() {
    }

    @Test
    public void getPageCount() {
    }
}