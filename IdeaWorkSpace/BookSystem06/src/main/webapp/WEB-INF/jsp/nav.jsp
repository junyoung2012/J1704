<%@ page pageEncoding="UTF-8"%>
<div class="nav">
	<ul>
		<li>
			服务类型管理
			<ul>
				<li><a href="serviceType/add.do">注册服务类型</a></li>
				<li><a href="serviceType/list.do">服务类型信息</a></li>
				<li><a href="servlet/ExportExcel">打印类型报表</a></li>
			</ul>
		</li>
		<li>
			服务管理
			<ul>
				<li><a href="service/add.do">注册服务</a></li>
				<li><a href="service/list.do">服务信息</a></li>
			</ul>
		</li>
		<li>
			技师管理
			<ul>
				<li><a href="servlet/TechnicainAction?act=add">注册技师</a></li>
				<li><a href="servlet/TechnicainAction?act=list">服务技师</a></li>
			</ul>
		</li>
		<li><a href="servlet/UserAction?act=updatePassword">修改密码</a></li>
		<li><a href="LoginAction">退出系统</a></li>
	</ul>
</div>
