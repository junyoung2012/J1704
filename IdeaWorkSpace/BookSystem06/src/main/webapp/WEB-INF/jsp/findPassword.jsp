<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <title>${initParam.appCation}-服务管理</title>
        <base href="${pageContext.request.contextPath}/">
        <link rel="stylesheet" href="style/base.css" />
        <link rel="stylesheet" href="style/style.css" />
    </head>
    <body>
        <%@ include file="head.jsp" %>
        <div class="main">
            <div class="content">
                <h2>找回密码</h2>
                <h2>${msg}</h2>
                <form action="FindPasswordAction" method="post">
                    <table>
                        <tr>
                            <th></th>
                            <th></th>
                        </tr>
                        <tr>
                            <td>手机号码</td>
                            <td class="aleft">
                                <input name="mob" type="text" >
                            </td>
                        </tr>
                        <tr>
                            <td>电子邮箱</td>
                            <td class="aleft">
                                <input name="email" type="text" >
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <input class="btn" type="submit" value="保存" />
                                <input class="btn" type="reset" value="取消" />
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
    </body>
</html>
