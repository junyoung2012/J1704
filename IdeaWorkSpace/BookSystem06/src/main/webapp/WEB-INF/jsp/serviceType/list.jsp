<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <title>${initParam.appCation}-服务类型管理</title>
        <base href="${pageContext.request.contextPath}/">
        <link rel="stylesheet" href="style/base.css" />
        <link rel="stylesheet" href="style/style.css" />
    </head>
    <body>
        <%@ include file="../head.jsp" %>
        <div class="main">
            <%@ include file="../nav.jsp" %>
            <div class="content">
                <h2>服务类型列表</h2>
                <h2>${msg}</h2>
                    <table>
                        <tr>
                            <th>类型编号</th>
                            <th>类型标题</th>
                            <th>类型Icon</th>
                            <th>删除</th>
                            <th>修改</th>
                        </tr>
                        <c:forEach var="serviceType" items="${serviceTypes}">
                            <tr>
                                <td>${serviceType.id}</td>
                                <td>${serviceType.title}</td>
                                <td>${serviceType.icon}</td>
                                <td><a onclick="return confirm('确认删除吗?')" href="serviceType/delete.do?id=${serviceType.id}">删除</a></td>
                                <td><a href="serviceType/update.do?id=${serviceType.id}">修改</a></td>
                            </tr>
                        </c:forEach>
                    </table>
            </div>
        </div>
    </body>
</html>
