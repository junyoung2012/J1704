<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <title>${initParam.appCation}-服务类型管理</title>
        <base href="${pageContext.request.contextPath}/">
        <link rel="stylesheet" href="style/base.css" />
        <link rel="stylesheet" href="style/style.css" />
    </head>
    <body>
        <%@ include file="../head.jsp" %>
        <div class="main">
            <%@ include file="../nav.jsp" %>
            <div class="content">
                <h2>${act=="serviceType/addSave.do"?"新建":"修改"}服务类型</h2>
                <h2>${msg}</h2>
                <form action="${act}" method="post">
                    <table>
                        <tr>
                            <th></th>
                            <th></th>
                        </tr>
                        <tr>
                            <td>服务名称</td>
                            <td class="aleft">
                                <%--隐藏表单域--%>
                                <input name="id" type="hidden" value="${empty serviceType.id?0:serviceType.id}">
                                <input name="title" type="text" value="${serviceType.title}">
                            </td>
                        </tr>
                        <tr>
                            <td>服务Icon</td>
                            <td class="aleft">
                                <input name="icon" type="text" value="${serviceType.icon}">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <input class="btn" type="submit" value="保存" />
                                <input class="btn" type="reset" value="取消" />
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
    </body>
</html>
