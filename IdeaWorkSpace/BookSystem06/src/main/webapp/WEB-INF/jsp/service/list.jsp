<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <title>${initParam.appCation}-服务类型管理</title>
        <base href="${pageContext.request.contextPath}/">
        <link rel="stylesheet" href="style/base.css" />
        <link rel="stylesheet" href="style/style.css" />
        <style>
            #tip{
                position: absolute;
                border: 1px solid black;
                background-color: black;
                color: white;
                text-align: center;
                font-size: 1.2rem;
                padding-bottom: 10px;
                display: none;
            }
           #tip img{
                height: 350px;
                border: 2px solid black;
               margin-bottom: 10px;
            }
        </style>
        <script>
            $(function(){
                $(".content img").mouseover(function(e){
                    $("<div id='tip'><img src='"+this.src+"'><br>"+this.alt+"</div>").appendTo("body");
                    $("#tip").css({
                        top:e.pageY+10,
                        left:e.pageX+10
                    }).show(600);
                }).mousemove(function(e){
                    $("#tip").css({
                        top:e.pageY+10,
                        left:e.pageX+10
                    });
                }).mouseout(function(){
                    $("#tip").remove();
                })
            });
        </script>
    </head>
    <body>
        <%@ include file="../head.jsp" %>
        <div class="main">
            <%@ include file="../nav.jsp" %>
            <div class="content">
                <h2>服务类型列表</h2>
                <h2>${msg}</h2>
                    <table>
                        <tr>
                            <th>服务编号</th>
                            <th>服务名称</th>
                            <th>服务coverpath</th>
                            <th>服务价格</th>
                            <th>服务口号</th>
                            <th>服务类型</th>
                            <th>服务教练</th>
                            <th>服务简介</th>
                            <th>服务详情</th>
                            <th>服务展示图</th>
                            <th>删除</th>
                            <th>修改</th>
                        </tr>
                        <c:forEach var="service" items="${services}">
                            <tr >
                                <td>${service.id}</td>
                                <td>${service.subject}</td>
                                <td>
                                    <img class="img" src="${initParam.uploadDir}/${service.coverpath}" alt="${service.subject}"></td>
                                <td>${service.price}</td>
                                <td>${service.message}</td>
                                <td>${service.serviceType.title}</td>
                                <td>${service.technicain.nickname}</td>
                                <td>${service.summry}</td>
                                <td>${service.detail}</td>
                                <td>
                                    <img class="img" src="${initParam.uploadDir}/${service.picture}" alt="${service.subject}"></td>
                                </td>
                                <td><a onclick="return confirm('确认删除吗?')" href="service/delete.do?id=${service.id}">删除</a></td>
                                <td><a href="service/update.do?id=${service.id}">修改</a></td>
                            </tr>
                        </c:forEach>
                        <tr>
                            <td colspan="10" class="page">
                                <c:if test="${page!=1}">
                                    <a href="servlet/ServiceAction?act=list&page=1">首页</a>
                                    <a href="servlet/ServiceAction?act=list&page=${page-1}">上一页</a>
                                </c:if>
                                <c:if test="${page==1}">
                                    <label>首页</label>
                                    <label>上一页</label>
                                </c:if>
                                <label>第${page}页</label>
                                <c:if test="${page!=pageCount}">
                                    <a href="servlet/ServiceAction?act=list&page=${page+1}">下一页</a>
                                    <a href="servlet/ServiceAction?act=list&page=${pageCount}">末页</a>
                                </c:if>
                               <c:if test="${page==pageCount}">
                                   <label>下一页</label>
                                   <label>末页</label>
                                </c:if>
                                <form action="servlet/ServiceAction" method="post" style="display: inline-block">
                                    <input name="page" type="number" style="width: 50px;">
                                    <input type="submit" value="go">
                                </form>
                            </td>
                        </tr>
                    </table>
            </div>
        </div>
    </body>
</html>
