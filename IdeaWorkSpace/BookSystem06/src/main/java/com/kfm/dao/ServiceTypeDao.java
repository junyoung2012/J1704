package com.kfm.dao;

import com.kfm.model.ServiceType;

import java.util.List;

public interface ServiceTypeDao {
	public boolean add(ServiceType serviceType);
	public boolean delete(int id);
	public boolean update(ServiceType serviceType);
	public ServiceType get(int id);
	public List<ServiceType> getAll();
}
