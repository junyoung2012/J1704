package com.kfm.dao;

import com.kfm.model.Service;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface ServiceDao  {
	public boolean add(Service service);
	public List<Service> getServicesByTypeId(int serviceTypeId);
    public boolean delete(int id) ;
	public Service get(int id);
	public boolean update(Service service);
	public List<Service> getAll();
	public List<Service> getAllPage(@Param("start") long start, @Param("length") long length);
	public long getPageCount();
}
