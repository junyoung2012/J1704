package com.kfm.dao;

import com.kfm.model.Technicain;

import java.util.List;

public interface TechnicainDao {
	public List<Technicain> getAll();
}
