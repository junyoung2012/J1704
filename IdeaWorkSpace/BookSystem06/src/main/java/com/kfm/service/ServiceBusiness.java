package com.kfm.service;

import com.kfm.dao.ServiceDao;
import com.kfm.dao.ServiceTypeDao;
import com.kfm.dao.TechnicainDao;
import com.kfm.model.Service;
import com.kfm.model.ServiceType;
import com.kfm.model.Technicain;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@org.springframework.stereotype.Service
@Transactional(propagation =  Propagation.REQUIRED)
public class ServiceBusiness {
    @Resource
    private ServiceTypeDao serviceTypeDao;
    @Resource
    private ServiceDao serviceDao;
    @Resource
    private TechnicainDao technicainDao;

    public boolean add(Service service) {
        return serviceDao.add(service);
    }

    public boolean delete(int id) {
        return serviceDao.delete(id);
    }

    public Service getService(int id) {
        return serviceDao.get(id);
    }

    public boolean update(Service service) {
        return serviceDao.update(service);
    }

    public List<Service> getServices() {
        return serviceDao.getAll();
    }
    public List<Service> getServices(long size,long page) {
        return serviceDao.getAllPage(size,page);
    }

    public List<ServiceType> getServiceTypes(){
        return serviceTypeDao.getAll();
    }

    public List<Technicain> getTechnicains(){
        return technicainDao.getAll();
    }

    public long getPageCount(long size) {
        return (long)Math.ceil((double)serviceDao.getPageCount()/size);
    }

}
