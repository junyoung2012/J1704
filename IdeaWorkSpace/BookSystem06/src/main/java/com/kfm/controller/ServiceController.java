package com.kfm.controller;

import com.kfm.model.Service;
import com.kfm.service.ServiceBusiness;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/service")
public class ServiceController {
    @Autowired
    private ServiceBusiness serviceBusiness;
    private String realPath;

    @RequestMapping("/add")
    public ModelAndView add(){
        ModelAndView modelAndView=new ModelAndView("service/input");
        modelAndView.addObject("act","service/addSave.do")
            .addObject(serviceBusiness.getServiceTypes())
            .addObject(serviceBusiness.getTechnicains());
        return modelAndView;
    }
    @RequestMapping("/addSave")
    public ModelAndView addSave(@Valid Service service
            , BindingResult bindingResult
            , @RequestParam("coverpath1") MultipartFile coverpath
            , @RequestParam("picture1") MultipartFile picture){
        ModelAndView modelAndView;
        if(bindingResult.hasErrors()){
            List<FieldError> fieldErrors = bindingResult.getFieldErrors();
            Map<String, String> errors = new HashMap<>();
            for (FieldError fieldError : fieldErrors) {
                errors.put(fieldError.getField(), fieldError.getDefaultMessage());
            }
            modelAndView =add()
                    .addObject("service",service)
                    .addObject("errors", errors);
            return modelAndView;
        }
        if(StringUtils.isEmpty(realPath)){
            WebApplicationContext context = ContextLoader.getCurrentWebApplicationContext();
            realPath= (String) context.getServletContext().getInitParameter("uploadDir");
            realPath=context.getServletContext().getRealPath(realPath);
            System.out.println(realPath);
            File dir=new File(realPath);
            if(!dir.exists()){
                dir.mkdirs();
            }
        }

        String name=renameAndSave(coverpath);
        service.setCoverpath(name);
        name=renameAndSave(picture);
        service.setPicture(name);

        if(serviceBusiness.add(service)){
            modelAndView =list().addObject("msg","保存成功");
        }else{
            modelAndView =add()
                    .addObject("msg","保存失败")
                    .addObject("service",service);
        }
        return modelAndView;
    }

    private String renameAndSave(MultipartFile coverpath) {
        if(coverpath==null) return null;
        String originalFilename = coverpath.getOriginalFilename();
        if(StringUtils.isEmpty(originalFilename)) return "";
        int pos=originalFilename.lastIndexOf(".");
        SimpleDateFormat dateFormat=new SimpleDateFormat("yyyyMMddHHmmssSSS");

        String name = originalFilename.substring(0, pos) + dateFormat.format(new Date()) + originalFilename.substring(pos);
        System.out.println(name);
        try {
            coverpath.transferTo(new File(realPath,name));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return name;
    }

    @RequestMapping("/delete")
    public ModelAndView delete(int id){
        String msg=serviceBusiness.delete(id)?"删除成功":"删除失败";
        return list().addObject("msg",msg);
    }
    @RequestMapping("/update")
    public ModelAndView update(int id){
        ModelAndView modelAndView=add()
                .addObject("act","service/updateSave.do")
                .addObject("service",serviceBusiness.getService(id));
        return modelAndView;
    }
    @RequestMapping("/updateSave")
    public ModelAndView updateSave(@Valid Service service
            , BindingResult bindingResult
            , @RequestParam("coverpath1") MultipartFile coverpath
            , @RequestParam("picture1") MultipartFile picture){
        ModelAndView modelAndView;
        if(bindingResult.hasErrors()){
            List<FieldError> fieldErrors = bindingResult.getFieldErrors();
            Map<String, String> errors = new HashMap<>();
            for (FieldError fieldError : fieldErrors) {
                errors.put(fieldError.getField(), fieldError.getDefaultMessage());
            }
            modelAndView =add()
                    .addObject("act","service/updateSave.do")
                    .addObject("service",service)
                    .addObject("errors", errors);
            return modelAndView;
        }

        if(StringUtils.isEmpty(realPath)){
            WebApplicationContext context = ContextLoader.getCurrentWebApplicationContext();
            realPath= (String) context.getServletContext().getInitParameter("uploadDir");
            realPath=context.getServletContext().getRealPath(realPath);
            File dir=new File(realPath);
            if(!dir.exists()){
                dir.mkdirs();
            }
        }

        String name=renameAndSave(coverpath);
        service.setCoverpath(name);
        name=renameAndSave(picture);
        service.setPicture(name);
        if(serviceBusiness.update(service)){
            modelAndView =list().addObject("msg","保存成功");
        }else{
            modelAndView =add()
                    .addObject("act","service/updateSave.do")
                    .addObject("msg","保存失败")
                    .addObject("service",service);
        }
        return modelAndView;
    }
    @RequestMapping("/list")
    public ModelAndView list(){
        ModelAndView modelAndView=new ModelAndView("service/list");
        modelAndView.addObject("services",serviceBusiness.getServices());
        return modelAndView;
    }
}
