package com.kfm.controller;

import com.kfm.model.ServiceType;
import com.kfm.service.ServiceBusiness;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
//@ResponseBody
@RequestMapping("/api")
public class ApiController {
    @Autowired
    private ServiceBusiness serviceBusiness;

    @GetMapping("/getIndexNavData")
    public List<ServiceType> getIndexNavData(){
        return serviceBusiness.getServiceTypes();
    }
}
