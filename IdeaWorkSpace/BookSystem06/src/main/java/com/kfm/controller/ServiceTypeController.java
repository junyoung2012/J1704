package com.kfm.controller;

import com.kfm.dao.ServiceTypeDao;
import com.kfm.model.ServiceType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;

@Controller
@RequestMapping("/serviceType")
public class ServiceTypeController {
    @Resource
    private ServiceTypeDao serviceTypeDao;

    @RequestMapping("/add")
    public ModelAndView add(){
        ModelAndView modelAndView=new ModelAndView("serviceType/input");
        modelAndView.addObject("act","serviceType/addSave.do");
        return modelAndView;
    }
    @RequestMapping("/addSave")
    public ModelAndView addSave(ServiceType serviceType){
        ModelAndView modelAndView;
        if(serviceTypeDao.add(serviceType)){
            modelAndView =list().addObject("msg","保存成功");
        }else{
            modelAndView =add()
                    .addObject("msg","保存失败")
                    .addObject("serviceType",serviceType);
        }
        return modelAndView;
    }
    @RequestMapping("/delete")
    public ModelAndView delete(int id){
        String msg=serviceTypeDao.delete(id)?"删除成功":"删除失败";
        return list().addObject("msg",msg);
    }
    @RequestMapping("/update")
    public ModelAndView update(int id){
        ModelAndView modelAndView=new ModelAndView("serviceType/input");
        modelAndView.addObject("act","serviceType/updateSave.do")
                    .addObject("serviceType",serviceTypeDao.get(id));
        return modelAndView;
    }
    @RequestMapping("/updateSave")
    public ModelAndView updateSave(ServiceType serviceType){
        ModelAndView modelAndView;
        if(serviceTypeDao.update(serviceType)){
            modelAndView =list().addObject("msg","保存成功");
        }else{
            modelAndView=new ModelAndView("serviceType/input")
                    .addObject("act","serviceType/updateSave.do")
                    .addObject("serviceType",serviceType);
        }
        return modelAndView;
    }
    @RequestMapping("/list")
    public ModelAndView list(){
        ModelAndView modelAndView=new ModelAndView("serviceType/list");
        modelAndView.addObject("serviceTypes",serviceTypeDao.getAll());
        return modelAndView;
    }
}
