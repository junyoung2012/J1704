package com.kfm.controller;

import com.kfm.model.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/test3")
public class TestController3 {
    @RequestMapping("/reg")
    public String reg() {
        return "reg";
    }

    @RequestMapping("/test1")
    public String test(@Valid User user, BindingResult bindingResult, Map<String, Object> map, Model model) {
        System.out.println(user);


        if (bindingResult.hasErrors()) {
//           List<ObjectError> allErrors = bindingResult.getAllErrors();
//           map.put("allErrors",allErrors);
            List<FieldError> fieldErrors = bindingResult.getFieldErrors();
//           map.put("fieldErrors",fieldErrors);
            Map<String, String> errors = new HashMap<>();
            for (FieldError fieldError : fieldErrors) {
                errors.put(fieldError.getField(), fieldError.getDefaultMessage());
            }
            model.addAttribute("errors", errors);
            return "reg";
        }
        return "scuess";
    }


}
