<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <title>${initParam.appCation}-技师类型管理</title>
        <base href="${pageContext.request.contextPath}/">
        <link rel="stylesheet" href="style/base.css" />
        <link rel="stylesheet" href="style/style.css" />
        <script type="text/javascript" src="https://webapi.amap.com/maps?v=1.4.15&key=您申请的key值"></script>
        <style>
            #map{
                width: 100%;
                height: 100%;
                position: absolute;
                top: 0px;
                left: 0px;
                display: none;
            }
            #map [type="button"]{
                position: absolute;
                padding: 5px;
                z-index: 99;
                top: 5px;
                right: 5px;
            }
            #container {
                width: 100%;
                height: calc(100% - 20px);
            }
        </style>
    </head>
    <body>
        <%@ include file="../head.jsp" %>
        <div class="main">
            <%@ include file="../nav.jsp" %>
            <div class="content">
                <h2>${act=="addSave"?"新建":"修改"}技师类型</h2>
                <h2>${msg}</h2>
                <form action="servlet/TechnicainAction?act=${act}" method="post">
                    <table>
                        <tr>
                            <th></th>
                            <th></th>
                        </tr>
                        <tr>
                            <td>所属公司</td>
                            <td class="aleft">
                                <%--隐藏表单域--%>
                                <input name="id" type="hidden" value="${technicain.id}">
                                <input name="company" type="text" value="${technicain.company}">
                            </td>
                        </tr>
                        <tr>
                            <td>昵    称</td>
                            <td class="aleft">
                                <input name="nickname" type="text" value="${technicain.nickname}">
                            </td>
                        </tr>
                         <tr>
                            <td>所在位置</td>
                            <td class="aleft">
                                纬度：<input style="width: 80px;" id="lat" name="lat" type="text" readonly>
                                经度：<input style="width: 80px;" id="lon" name="lon" type="text" readonly>
                                <input style="padding: 5px" name="nickname" type="button" value="打开地图..." onclick="openMap()">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <input class="btn" type="submit" value="保存" />
                                <input class="btn" type="reset" value="取消" />
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
        <div id="map">
            <input type="button" value="X" onclick="closeMap()">
            <div id="container"></div>
        </div>

    </body>
    <script type="text/javascript">
        var map = new AMap.Map('container', {
            zoom: 13
        });
        map.on('click', function(e) {
            console.log(e.lnglat);
            document.getElementById("lat").value=e.lnglat.lat;
            document.getElementById("lon").value=e.lnglat.lng;
            var marker = new AMap.Marker({
                icon: 'https://a.amap.com/jsapi_demos/static/demo-center/icons/poi-marker-default.png',
                position: [e.lnglat.lng, e.lnglat.lat],
                offset: new AMap.Pixel(-25, -60)
            });
            marker.setMap(map);
            setTimeout(function(){
                marker.setMap(null);
            },2000);
        })
        function openMap(){
            document.getElementById("map").style.display="block";
        }
        function closeMap(){
            document.getElementById("map").style.display="none";
        }
    </script>
</html>
