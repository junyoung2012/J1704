<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <title>${initParam.appCation}-服务管理</title>
        <base href="${pageContext.request.contextPath}/">
        <link rel="stylesheet" href="style/base.css" />
        <link rel="stylesheet" href="style/style.css" />
    </head>
    <body>
        <%@ include file="head.jsp" %>
        <div class="main">
            <%@ include file="nav.jsp" %>
            <div class="content">
                <h2>修改密码</h2>
                <h2>${msg}</h2>
                <form action="servlet/UserAction?act=updatePassword" method="post">
                    <table>
                        <tr>
                            <th></th>
                            <th></th>
                        </tr>
                        <tr>
                            <td>密码</td>
                            <td class="aleft">
                                <input name="password" type="password" >
                            </td>
                        </tr>
                        <tr>
                            <td>密码确认</td>
                            <td class="aleft">
                                <input name="password1" type="password" >
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <input class="btn" type="submit" value="保存" />
                                <input class="btn" type="reset" value="取消" />
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
    </body>
</html>
