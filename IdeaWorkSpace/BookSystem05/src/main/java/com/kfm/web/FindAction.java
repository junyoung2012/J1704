package com.kfm.web;

import com.kfm.dao.EmpDao;
import com.kfm.model.Emp;
import com.mysql.cj.util.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(urlPatterns = "/FindAction")
public class FindAction extends HttpServlet {
    private EmpDao empDao=new EmpDao();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String act = request.getParameter("act");
        switch (act){
            case "updatePassword":
                String password = request.getParameter("password");
                String password1 = request.getParameter("password1");
                if(StringUtils.isEmptyOrWhitespaceOnly(password)) {
                    forward(request,response,"msg","密码不能省略.","../WEB-INF/jsp/updatePassword.jsp");
                    return;
                }
                if(!password.matches("\\w{4,8}")) {
                    forward(request,response,"msg","密码应该为4-8位英文或数字.","../WEB-INF/jsp/updatePassword.jsp");
                    return;
                }
                if(!password.equals(password1)) {
                    forward(request,response,"msg","您的密码2次输入不一致。","../WEB-INF/jsp/updatePassword.jsp");
                    return;
                }
                Emp emp= (Emp) request.getSession().getAttribute("loginEmp");
                emp.setPassword(password);
                forward(request,response,"msg",
                        empDao.updatePassword(emp)?"修改密码成功":"修改密码失败",
                        "../WEB-INF/jsp/updatePassword.jsp");
                break;
        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String act = request.getParameter("act");
        String sessionId = request.getParameter("a");
        String token = request.getParameter("b");
        HttpSession session = MySessionContext.getInstance().getSession(sessionId);
        if(session==null){
            response.sendRedirect("login.jsp");
        }
        System.out.println(session==request.getSession());
        request.getSession().setAttribute("loginEmp",session.getAttribute(token));//相当于登录
        System.out.println(session);
        switch (act){
            case "updatePassword":
                forward(request,response,"WEB-INF/jsp/updatePassword.jsp");
                break;
            case "findPassword":
                forward(request,response,"WEB-INF/jsp/findPassword.jsp");
                break;
        }

    }

    private void forward(HttpServletRequest request, HttpServletResponse response, String url) throws ServletException, IOException {
        request.getRequestDispatcher(url).forward(request, response);
    }
    private void forward(HttpServletRequest request, HttpServletResponse response, String key,Object value, String url) throws ServletException, IOException {
        request.setAttribute(key, value);
        request.getRequestDispatcher(url).forward(request, response);
    }

}
