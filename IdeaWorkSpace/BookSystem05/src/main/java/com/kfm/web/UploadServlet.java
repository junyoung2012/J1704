package com.kfm.web;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.IOException;

@WebServlet(urlPatterns = "/UploadServlet")
@MultipartConfig(maxFileSize=10*1024*1024)
public class UploadServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        ServletInputStream inputStream = request.getInputStream();
//        int i=0;
//        while ((i=inputStream.read())!=-1){
//            System.out.print((char)i);
//        }
        Part part = request.getPart("coverpath");
        System.out.println("文件大小："+part.getSize());
        System.out.println("文件名称："+part.getName());
        System.out.println("提交文件名称："+part.getSubmittedFileName());
        System.out.println("文件类型："+part.getContentType());
        part.write("d:/test.png");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
