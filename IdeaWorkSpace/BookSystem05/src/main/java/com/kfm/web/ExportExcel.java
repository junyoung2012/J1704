package com.kfm.web;

import com.kfm.dao.ServiceTypeDao;
import com.kfm.model.ServiceType;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.usermodel.HorizontalAlignment;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;

@WebServlet(urlPatterns = "/servlet/ExportExcel")
public class ExportExcel extends HttpServlet {
    private ServiceTypeDao serviceTypeDao=new ServiceTypeDao();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        createExcel(response,"服务类型列表.xls");
    }

    public  void createExcel(HttpServletResponse response, String filePath) throws IOException {
        try {
            String fileName = URLEncoder.encode(filePath, "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.getMessage();
        }

        OutputStream out = null;
        // 下面几行是为了解决文件名乱码的问题
        response.setHeader("Content-Disposition",
                "attachment;filename=" + new String(filePath.getBytes(), "iso-8859-1"));
//        响应头被设置成输出excel
        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
//        不要缓存
        response.setHeader("Pragma", "no-cache");
        response.setHeader("Cache-Control", "no-cache");
        response.setDateHeader("Expires", 0);
        out = response.getOutputStream();

        // 第一步，创建一个webbook，对应一个Excel文件
        HSSFWorkbook wb = new HSSFWorkbook();
        // 第二步，在webbook中添加一个sheet,对应Excel文件中的sheet
        HSSFSheet sheet = wb.createSheet("sheet1");
        sheet.setDefaultColumnWidth(20);// 默认列宽
        // 第三步，在sheet中添加表头第0行,注意老版本poi对Excel的行数列数有限制short
        HSSFRow row = sheet.createRow((int) 0);
        // 第四步，创建单元格，并设置值表头 设置表头居中
        HSSFCellStyle style = wb.createCellStyle();
        // 创建一个居中格式
        style.setAlignment(HorizontalAlignment.LEFT);

        // 添加excel title
        HSSFCell cell = null;
        int i=1;
        cell = row.createCell((short) i++);
        cell.setCellValue("类型编号");
        cell.setCellStyle(style);


        cell = row.createCell((short) i++);
        cell.setCellValue("类型编号");
        cell.setCellStyle(style);


        cell = row.createCell((short) i++);
        cell.setCellValue("类型标题");
        cell.setCellStyle(style);


        cell = row.createCell((short) i++);
        cell.setCellValue("类型Icon");
        cell.setCellStyle(style);



        // 第五步，写入实体数据 实际应用中这些数据从数据库得到,list中字符串的顺序必须和数组strArray中的顺序一致
        List<ServiceType> serviceTypes = serviceTypeDao.getAll();
        for (int i1 = 0; i1 < serviceTypes.size(); i1++) {
            row = sheet.createRow((int) i1 + 1);
            int j=1;
            row.createCell((short) j++).setCellValue(serviceTypes.get(i1).getId());
            row.createCell((short) j++).setCellValue(serviceTypes.get(i1).getTitle());
            row.createCell((short) j++).setCellValue(serviceTypes.get(i1).getIcon());
        }

        // 第六步，将文件存到指定位置
        try {
            wb.write(out);//将Excel用response返回
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
