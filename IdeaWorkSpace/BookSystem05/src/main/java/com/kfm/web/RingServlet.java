package com.kfm.web;

import com.kfm.dao.ServiceTypeDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

@WebServlet(urlPatterns = "/RingServlet")
public class RingServlet extends HttpServlet {
    private ServiceTypeDao serviceTypeDao=new ServiceTypeDao();
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        System.out.println(new Date());
        if(serviceTypeDao.getAll().size()>5){
            out.write("1");
        }else{
            out.write("0");
        }
        out.flush();
        out.close();
    }
}
