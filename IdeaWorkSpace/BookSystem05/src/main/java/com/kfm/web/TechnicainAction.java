package com.kfm.web;

import com.kfm.dao.TechnicainDao;
import com.kfm.model.Technicain;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/servlet/TechnicainAction")
public class TechnicainAction extends HttpServlet {
    private TechnicainDao technicainDao=new TechnicainDao();

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        web应用MVC结构控制器的8大功能
//        控制框架（SpringMVC,Struts 2,xWork）
//                  请求路由：分清当前的业务请求
//                收参
//                转化数据类型
//                填充数据对象
//                数据验证
//                调用dao ，执行业务逻辑
//                 把结果存放到数据域
//                跳转页面
//        request.setCharacterEncoding("UTF-8");
//        response.setContentType("text/html;charset=UTF-8");
        String act = request.getParameter("act");
        if(act==null){
            act="list";//控制保护
        }
        Technicain technicain=getTechnicainFromRequest(request);

        switch (act){
            case "add"://无参
                forward(request, response, "act","addSave","../WEB-INF/jsp/technicain/input.jsp");
                break;
            case "addSave"://2参 title,icon (post)
                String msg=checkTechnicain(technicain);
                if(!"".equals(msg)){
                        request.setAttribute("act","addSave");
                        request.setAttribute("technicain",technicain);
                        forward(request, response, msg, "../WEB-INF/jsp/technicain/input.jsp");
                        return;
                }

                if(technicainDao.add(technicain)){
                        forward(request, response,"保存成功","TechnicainAction?act=list");
                    }else{
                        request.setAttribute("act","addSave");
                    request.setAttribute("technicain",technicain);
                        forward(request, response, "保存失败", "../WEB-INF/jsp/technicain/input.jsp");
                    }
                break;
            case "delete"://1参 id
//                if(technicainDao.delete(technicain.getId())){
//                    forward(request, response,"删除成功","TechnicainAction?act=list");
//                }else{
//                    forward(request, response,"删除失败","TechnicainAction?act=list");
//                }
                forward(request, response,
                        technicainDao.delete(technicain.getId())?"删除成功":"删除失败","TechnicainAction?act=list");
                break;
            case "update"://1参 id
                Technicain technicain1 = technicainDao.get(technicain.getId());
                request.setAttribute("technicain",technicain1);
                forward(request, response, "act","updateSave","../WEB-INF/jsp/technicain/input.jsp");
                break;
            case "updateSave"://1参 id,title,icon (post)
                msg=checkTechnicain(technicain);
                if(!"".equals(msg)){
                    request.setAttribute("act","updateSave");
                    request.setAttribute("technicain",technicain);
                    forward(request, response, msg, "../WEB-INF/jsp/technicain/input.jsp");
                    return;
                }

                if(technicainDao.update(technicain)){
                    forward(request, response,"保存成功","TechnicainAction?act=list");
                }else{
                    request.setAttribute("act","updateSave");
                    request.setAttribute("technicain",technicain);
                    forward(request, response, "保存失败", "../WEB-INF/jsp/technicain/input.jsp");
                }
                break;
                default://查 无参 (Get,post)
//                    List<Technicain> technicains = technicainDao.getAll();
//                    forward(request,response,"technicains",technicains,"../technicain/list.jsp");
                    forward(request,response,"technicains",technicainDao.getAll(),"../WEB-INF/jsp/technicain/list.jsp");

        }
    }

    private void forward(HttpServletRequest request, HttpServletResponse response, String key,Object value, String url) throws ServletException, IOException {
        request.setAttribute(key, value);
        request.getRequestDispatcher(url).forward(request, response);
    }
    private void forward(HttpServletRequest request, HttpServletResponse response, String msg, String url) throws ServletException, IOException {
        request.setAttribute("msg", msg);
        request.getRequestDispatcher(url).forward(request, response);
    }

    private String checkTechnicain(Technicain technicain) {
        String msg="";
        return msg;
    }

    private Technicain getTechnicainFromRequest(HttpServletRequest request) {
        Technicain Technicain=new Technicain();
        return Technicain;
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        /*
        * 打开新增页
        * 新增保存
        * 删除
        * 打开修改页
        * 修改保存
        * 查询全部
        * */
        doGet(request,response);
    }
}
