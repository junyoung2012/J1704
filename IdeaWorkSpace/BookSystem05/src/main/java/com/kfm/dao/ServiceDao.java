package com.kfm.dao;

import com.kfm.model.Service;
import com.kfm.model.ServiceType;
import com.kfm.model.Technicain;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

public class ServiceDao extends BaseDao implements ResultSetHandler<List<Service>> {
	public boolean add(Service service){
		String sql="INSERT INTO service (\n" +
				"  SUBJECT,coverpath,price,message,serviceTypeId,\n" +
				"  technicainId,summry,detail,picture\n" +
				")\n" +
				"VALUES\n" +
				"  (?,?,?,?,?,?,?,?,?)";
		try {
			return queryRunner.execute(sql,
					service.getSubject(),
					service.getCoverpath(),
					service.getPrice(),
					service.getMessage(),
					service.getServiceType().getId(),
					service.getTechnicain().getId(),
					service.getSummry(),
					service.getDetail(),
					service.getPicture()
			)==1;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	public List<Service> getServicesByTypeId(int serviceTypeId){
		try {
			String sql="select S.id sid, S.subject, S.coverpath, S.price sprice, S.message smessage,"
					+ " S.servicetypeid, S.technicainid, S.summry , S.detail, S.picture,"
					+ "  ST.id stid, ST.icon, ST.title,"
					+ "  T.id tid, T.company, T.logo, T.avatar, T.nickname,"
					+ "  T.price tprice, T.message tmessage, T.detail tdetail,lat, lon"
					+ "  from service S"
					+ "  left join servicetype ST on S.servicetypeid=ST.id"
					+ "  left join Technicain T on S.Technicainid=T.Id "
					+ " where servicetypeid=?";
//			return queryRunner.query(sql,new BeanListHandler<>(Service.class),serviceTypeId);
			return queryRunner.query(sql,this,serviceTypeId);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<Service> handle(ResultSet rs) throws SQLException {
		List<Service> list=new LinkedList<Service>();
		while(rs.next()) {
			list.add(new Service(
					rs.getInt("sid"),
					rs.getString("subject"),
					rs.getString("coverpath"), 
					rs.getInt("sprice"),
					rs.getString("smessage"),
					new ServiceType(
						rs.getInt("stid"),
						rs.getString("icon"),
						rs.getString("title")
					),
					new Technicain(
							rs.getInt("tid"),
							rs.getString("company"),
							rs.getString("logo"),
							rs.getString("avatar"),
							rs.getString("nickname"),
							rs.getInt("tprice"),
							rs.getString("tmessage"),
							rs.getString("tdetail"),
							rs.getDouble("lat"),
							rs.getDouble("lon")
					),
					rs.getString("summry"),
					rs.getString("detail"),
					rs.getString("picture")
			));
		}
		return list;
	}

    public boolean delete(int id) {
		try {
			String sql="DELETE\n" +
					" FROM\n" +
					"  service\n" +
					" WHERE id = ?";
			return queryRunner.execute(sql,id)==1;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
    }

	public Service get(int id) {
		try {
			String sql="select S.id sid, S.subject, S.coverpath, S.price sprice, S.message smessage,"
					+ " S.servicetypeid, S.technicainid, S.summry , S.detail, S.picture,"
					+ "  ST.id stid, ST.icon, ST.title,"
					+ "  T.id tid, T.company, T.logo, T.avatar, T.nickname,"
					+ "  T.price tprice, T.message tmessage, T.detail tdetail,lat, lon"
					+ "  from service S"
					+ "  left join servicetype ST on S.servicetypeid=ST.id"
					+ "  left join Technicain T on S.Technicainid=T.Id "
					+ " where S.id=?";
			return queryRunner.query(sql, new ResultSetHandler<Service>() {
				@Override
				public Service handle(ResultSet rs) throws SQLException {
					if(rs.next()){
						return new Service(
								rs.getInt("sid"),
								rs.getString("subject"),
								rs.getString("coverpath"),
								rs.getInt("sprice"),
								rs.getString("smessage"),
								new ServiceType(
										rs.getInt("stid"),
										rs.getString("icon"),
										rs.getString("title")
								),
								new Technicain(
										rs.getInt("tid"),
										rs.getString("company"),
										rs.getString("logo"),
										rs.getString("avatar"),
										rs.getString("nickname"),
										rs.getInt("tprice"),
										rs.getString("tmessage"),
										rs.getString("tdetail"),
										rs.getDouble("lat"),
										rs.getDouble("lon")
								),
								rs.getString("summry"),
								rs.getString("detail"),
								rs.getString("picture")
						);
					}
					return null;
				}
			},id);

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public boolean update(Service service) {
		if(service.getCoverpath()==null || service.getPicture()==null ){
			Service service1=get(service.getId());
			if(service.getCoverpath()==null){
				service.setCoverpath(service1.getCoverpath());
			}
			if(service.getPicture()==null){
				service.setPicture(service1.getPicture());
			}
		}
		String sql="UPDATE\n" +
				"  service\n" +
				" SET\n" +
				"  SUBJECT = ?,\n" +
				"  coverpath = ?,\n" +
				"  price = ?,\n" +
				"  message = ?,\n" +
				"  serviceTypeId = ?,\n" +
				"  technicainId = ?,\n" +
				"  summry = ?,\n" +
				"  detail = ?,\n" +
				"  picture = ?\n" +
				"  WHERE id = ?";
		try {
			return queryRunner.execute(sql,
				service.getSubject(),
				service.getCoverpath(),
				service.getPrice(),
				service.getMessage(),
				service.getServiceType().getId(),
				service.getTechnicain().getId(),
				service.getSummry(),
				service.getDetail(),
				service.getPicture(),
				service.getId()
			)==1;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public List<Service> getAll() {
		try {
			String sql="select S.id sid, S.subject, S.coverpath, S.price sprice, S.message smessage,"
					+ " S.servicetypeid, S.technicainid, S.summry , S.detail, S.picture,"
					+ "  ST.id stid, ST.icon, ST.title,"
					+ "  T.id tid, T.company, T.logo, T.avatar, T.nickname,"
					+ "  T.price tprice, T.message tmessage, T.detail tdetail,lat, lon"
					+ "  from service S"
					+ "  left join servicetype ST on S.servicetypeid=ST.id"
					+ "  left join Technicain T on S.Technicainid=T.Id "
					+ " order by sid";
//			return queryRunner.query(sql,new BeanListHandler<>(Service.class),serviceTypeId);
			return queryRunner.query(sql,this);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public List<Service> getAll(int size,long page) {
		try {
			String sql="select S.id sid, S.subject, S.coverpath, S.price sprice, S.message smessage,"
					+ " S.servicetypeid, S.technicainid, S.summry , S.detail, S.picture,"
					+ "  ST.id stid, ST.icon, ST.title,"
					+ "  T.id tid, T.company, T.logo, T.avatar, T.nickname,"
					+ "  T.price tprice, T.message tmessage, T.detail tdetail,lat, lon"
					+ "  from service S"
					+ "  left join servicetype ST on S.servicetypeid=ST.id"
					+ "  left join Technicain T on S.Technicainid=T.Id "
					+ " order by sid" +
					" LIMIT ?, ?";
//			return queryRunner.query(sql,new BeanListHandler<>(Service.class),serviceTypeId);
			return queryRunner.query(sql,this,(page-1)*size,size);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public long getPageCount(int size){
		try {
			String sql="SELECT COUNT(1) FROM service";
			double ret=queryRunner.query(sql,new ScalarHandler<Long>());
			return (long) Math.ceil(ret/size);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}
}
