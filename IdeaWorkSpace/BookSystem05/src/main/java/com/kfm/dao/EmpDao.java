package com.kfm.dao;

import com.kfm.model.Emp;
import com.kfm.utils.MD5Utils;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import java.sql.SQLException;
import java.util.List;

public class EmpDao extends BaseDao {
	public boolean add(Emp emp){
		try {
			String sql="INSERT INTO db_book.emp ( NAME, mob, PASSWORD)\n" +
					"VALUES\n" +
					"  (?,?,?)";
			return queryRunner.execute(sql,
					emp.getName(),
					emp.getMob(),
					MD5Utils.stringToMD5(emp.getPassword(),emp.getMob())
			)==1;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	public boolean delete(int id){
		try {
			String sql="DELETE\n" +
					" FROM\n" +
					"  emp\n" +
					"WHERE id = ?";
			return queryRunner.execute(sql,id)==1;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	public boolean update(Emp emp){
		try {
			String sql="UPDATE\n" +
					"  emp\n" +
					" SET\n" +
					"  NAME = ?, \n" +
					"  mob = ? \n" +
					" WHERE id = ?";
			return queryRunner.execute(sql,
					emp.getName(),
					emp.getMob(),
					emp.getId()
			)==1;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public boolean updatePassword(Emp emp){
		try {
			String sql="UPDATE\n" +
					"  emp\n" +
					" SET\n" +
					"  password = ?" +
					" WHERE id = ?";
			return queryRunner.execute(sql,
					MD5Utils.stringToMD5(emp.getPassword(),emp.getMob()),
					emp.getId()
			)==1;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	public Emp get(int id){
		try {
			String sql="SELECT  id,NAME,mob FROM emp "
					+ " where id = ?";
			return queryRunner.query(sql, new BeanHandler<>(Emp.class),id);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	public Emp get(String mob,String email){
		try {
			String sql="SELECT  id,NAME,mob FROM emp "
					+ " where mob = ? and email=?";
			return queryRunner.query(sql, new BeanHandler<>(Emp.class),mob,email);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	public Emp login(String mob,String password){
		try {
			String sql="SELECT  id,NAME,mob FROM emp "
					+ " where mob = ? and PASSWORD=?";
			return queryRunner.query(sql, new BeanHandler<>(Emp.class),mob,
					MD5Utils.stringToMD5(password,mob));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	public List<Emp> getAll(){
		try {
			String sql="SELECT  id,NAME,mob FROM emp "
					+ " order by id";
			return queryRunner.query(sql, new BeanListHandler<>(Emp.class));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
}
