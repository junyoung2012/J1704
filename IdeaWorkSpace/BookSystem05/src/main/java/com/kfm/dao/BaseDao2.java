package com.kfm.dao;

import org.apache.commons.dbutils.QueryRunner;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

public class BaseDao2 {
	private static DataSource ds;

	protected QueryRunner queryRunner=new QueryRunner(ds);
	
	static {
		try {
			System.out.println("正在打开连接池...");
			Context cxt=new InitialContext();
//获取与逻辑名相关联的数据源对象
//			Tomcat JNDI java目录服务
			ds= (DataSource) cxt.lookup("java:comp/env/jdbc/BOOK");
			System.out.println(ds);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
