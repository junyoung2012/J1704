package com.kfm.dao;

import com.kfm.model.Emp;
import org.junit.Test;

import static org.junit.Assert.*;

public class EmpDaoTest {
    private EmpDao empDao=new EmpDao();

    @Test
    public void add() {
        assertTrue(empDao.add(new Emp("管理员1", "13100010001", "123456")));
        assertTrue(empDao.add(new Emp("管理员2", "13100010002", "123456")));
    }

    @Test
    public void delete() {
    }

    @Test
    public void update() {
    }

    @Test
    public void get() {
    }

    @Test
    public void login() {
        System.out.println(empDao.login("13100010001","12345"));
        System.out.println(empDao.login("13100010001","123456"));
    }

    @Test
    public void getAll() {
    }
}