package com.kfm.dao;

import com.kfm.model.Service;
import com.kfm.model.ServiceType;
import com.kfm.model.Technicain;
import org.junit.Test;

import static org.junit.Assert.*;

public class ServiceDaoTest {
    private ServiceDao serviceDao=new ServiceDao();

    @Test
    public void add() {
        for (int i = 0; i < 95; i++) {
            assertTrue(serviceDao.add(
                    new Service(
                        i,
                        "subject"+(i+1),
                        "coverpath",
                        100+i,
                        "message"+(i+1),
                        new ServiceType((i%5)+5),
                        new Technicain((i%2)+1),
                        "summry"+(i+1),
                        "detail"+(i+1),
                        "picture"
                    )
            ));
        }
    }

    @Test
    public void getServicesByTypeId() {
    }

    @Test
    public void delete() {
    }

    @Test
    public void get() {
    }

    @Test
    public void update() {
    }

    @Test
    public void getAll() {
    }

    @Test
    public void getPageCount() {
        System.out.println(serviceDao.getPageCount(10));
    }
}