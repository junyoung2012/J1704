<%@ page import="com.kfm.dao.ServiceTypeDao" %>
<%@ page import="java.util.List" %>
<%@ page import="com.kfm.model.ServiceType" %><%--
  Created by IntelliJ IDEA.
  User: Admin
  Date: 2021/11/1
  Time: 11:38
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title>$Title$</title>
  </head>
  <body>
  <%
    ServiceTypeDao ServiceTypeDao=new ServiceTypeDao();
    List<ServiceType> serviceTypes=ServiceTypeDao.getAll();
  %>
  <table>
    <tr>
      <th>编号</th>
      <th>图标</th>
      <th>标题</th>
    </tr>
    <% for (ServiceType serviceType : serviceTypes) {
      
   %>
      <tr>
        <td><%=serviceType.getId()%></td>
        <td><%=serviceType.getIcon()%></td>
        <td><%=serviceType.getTitle()%></td>
      </tr>
    <% }  %>
  </table>
  </body>
</html>
