package com.kfm.dao;

import com.kfm.model.ServiceType;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import java.sql.SQLException;
import java.util.List;

public class ServiceTypeDao extends BaseDao {
	public List<ServiceType> getAll(){
		try {
			String sql="select id, icon, title from servicetype"
					+ " order by id";
			return queryRunner.query(sql, new BeanListHandler<>(ServiceType.class));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
}
