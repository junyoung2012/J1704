package com.kfm.test;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

@WebServlet(urlPatterns = "/JstlServlet")
public class JstlServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Set<String> set=new HashSet<>();
        set.add("沟通");
        set.add("勇气");
        set.add("反馈");
        set.add("简单");
        request.setAttribute("set",set);

        List<Person> list=new ArrayList<>();
        list.add(new Person("韩愈",1));
        list.add(new Person("柳宗元",1));
        list.add(new Person("欧阳修",1));
        list.add(new Person("曾巩",1));
        request.setAttribute("list",list);

        Map<String,Person> map=new HashMap<>();
        map.put("及时雨",new Person("宋江",1));
        map.put("浪里白条",new Person("张顺",1));
        map.put("立地太岁",new Person("阮小二",1));

        request.setAttribute("map",map);
        request.getRequestDispatcher("test05.jsp").forward(request,response);
    }
}
