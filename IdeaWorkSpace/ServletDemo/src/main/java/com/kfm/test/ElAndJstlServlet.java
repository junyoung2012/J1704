package com.kfm.test;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/ElAndJstlServlet")
public class ElAndJstlServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("msg","request属性");
        request.setAttribute("person1",new Person("刘备",1));
        request.getSession().setAttribute("msg","session属性");
        getServletContext().setAttribute("msg","application属性");
        request.getRequestDispatcher("test04.jsp").forward(request,response);
    }
}
