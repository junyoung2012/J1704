package com.kfm.test;

public class Person {
    private String name;
    private int sex;

    public Person(String name, int sex) {
        this.name = name;
        this.sex = sex;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", sex=" + sex +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSex() {
        return sex;
    }
    public String getSex1() {
        return sex==0?"女":"男";
    }

    public void setSex(int sex) {
        this.sex = sex;
    }
}
