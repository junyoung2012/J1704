package com.kfm.test;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/RequestTest2Servlet")
public class RequestTest2Servlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.setAttribute("aa","业精于勤荒于嬉");
        request.setAttribute("aa","行成于思毁于随");
//        request.removeAttribute("aa");
        Object aa = request.getAttribute("aa");

        System.out.println(aa);


        System.out.println("路过");
//        RequestDispatcher requestDispatcher = request.getRequestDispatcher("test03.jsp");
//        requestDispatcher.forward(request,response);
        response.sendRedirect("test03.jsp");
        ServletContext application=request.getSession().getServletContext();
        System.out.println(application.getInitParameter("caption"));
    }



    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doPost(request,response);
    }
}
