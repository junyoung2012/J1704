package com.kfm.test;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Map;

@WebServlet(urlPatterns = "/RequestTestServlet")
public class RequestTestServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String result=getParameter5(request);
        PrintWriter out = response.getWriter();
        out.print(result);
        out.close();
    }

    /**
     * 接收显式参数
     * @param request
     * @return
     */
    private String getParameter1(HttpServletRequest request) {
        String username = request.getParameter("userName");
        return username;
    }
   private String getParameter2(HttpServletRequest request) {
       String[] hobby = request.getParameterValues("hobby");
       return Arrays.toString(hobby);
    }
   private String getParameter3(HttpServletRequest request) {
//       Enumeration<String> parameterNames = request.getParameterNames();//获得所有的参数名称
       Map<String, String[]> parameterMap = request.getParameterMap();
       StringBuilder sb=new StringBuilder();
       for (Map.Entry<String, String[]> entry : parameterMap.entrySet()) {
           sb.append(entry).append("<br>");
       }
       return sb.toString();
   }

    /**
     * 收取请求头
     * @param request
     * @return
     */
    private String getParameter4(HttpServletRequest request) {
        Enumeration<String> headerNames = request.getHeaderNames();
        StringBuilder sb=new StringBuilder();
        while (headerNames.hasMoreElements()){
            String name=headerNames.nextElement();
            sb.append(name).append("=")
                    .append(request.getHeader(name)).append("<br>");
        }
        return sb.toString();
    }

    /**
     * 收取网络信息
     * @param request
     * @return
     */
    private String getParameter5(HttpServletRequest request) {
        StringBuilder sb=new StringBuilder();
        sb.append("协议：").append(request.getScheme()).append("<br>");
        sb.append("RemoteHost：").append(request.getRemoteHost()).append("<br>");
        sb.append("ServerName：").append(request.getServerName()).append("<br>");
        sb.append("端口：").append(request.getServerPort()).append("<br>");
        sb.append("应用程序名：").append(request.getContextPath()).append("<br>");
        sb.append("QueryString：").append(request.getQueryString()).append("<br>");
        sb.append("LocalAddr：").append(request.getLocalAddr()).append("<br>");
        sb.append("getLocalName：").append(request.getLocalName()).append("<br>");
        sb.append("getLocale：").append(request.getLocale()).append("<br>");
        return sb.toString();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doPost(request,response);
    }
}
