package com.kfm.test;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/CookieAction")
public class CookieAction extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Cookie cookie=new Cookie("user","张三");
        cookie.setPath("/");//访问服务器什么网址cookie会被返回
        cookie.setMaxAge(60*60);
        resp.addCookie(cookie);
        System.out.println("写入cookie");
        Cookie[] cookies = req.getCookies();
        for (Cookie cookie1 : cookies) {
            System.out.println(cookie1.getName()+":"+cookie1.getValue());
        }
        req.getRequestDispatcher("test06.jsp").forward(req,resp);
    }
}
