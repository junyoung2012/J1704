package com.kfm.test;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
@WebServlet({"/aaa","/bbb"})
public class TestServlet /*implements Servlet*/ /*extends GenericServlet*/
        extends HttpServlet {
    @Override
    public void init(ServletConfig servletConfig) throws ServletException {
        System.out.println(servletConfig.getInitParameter("userName"));
    }

    @Override
    public ServletConfig getServletConfig() {
        return null;
    }
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println(req.getMethod());//请求的提交方式
        super.service(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("doGet");
        System.out.println(this.getInitParameter("userName"));
        resp.getWriter().print("doGet");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("doPost");
        resp.getWriter().print("doPost");
    }
    //    @Override
//    public void service(ServletRequest request, ServletResponse response) throws ServletException, IOException {
//
//        PrintWriter out = response.getWriter();
//        out.println("welcom to Itlaobing.com");
//        out.flush();
//        out.close();
//    }

    @Override
    public String getServletInfo() {
        return null;
    }

    @Override
    public void destroy() {

    }
}
