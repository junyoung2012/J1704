<%--
  Created by IntelliJ IDEA.
  User: Admin
  Date: 2021/11/3
  Time: 8:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>接收参数</title>

    <%--<base href="http://localhost:9090/ServletDemo/">--%>
    <%
        String path=request.getScheme()+"://"+request.getServerName()
                +":"+request.getServerPort()
                +request.getContextPath()+"/2/";
    %>
    <%--<base href="<%=path%>">--%>
    <base href="<%=request.getContextPath()+"/"%>">
</head>
<body>
<ul>
    <li><a href="RequestTestServlet?userName=john">RequestTestServlet?userName=john</a></li>
    <li><a href="RequestTestServlet?hobby=文学&hobby=音乐">RequestTestServlet?hobby=文学&hobby=音乐</a></li>
    <li><a href="RequestTestServlet?userName=john&hobby=文学&hobby=音乐">
        RequestTestServlet?userName=john&hobby=文学&hobby=音乐
    </a></li>
</ul>
<form action="RequestTestServlet">
    <input type="text" name="userName" value="rose">
    <input type="checkbox" name="hobby" value="文学">文学
    <input type="checkbox" name="hobby" value="艺术">艺术
    <input type="checkbox" name="hobby" value="音乐">音乐
    <input type="submit">
</form>
<hr>
<ul>
    <li><a href="RequestTest2Servlet">forward到test03.jsp</a></li>
</ul>
<hr>
<ul>
    <li><a href="ElAndJstlServlet">el表达式</a></li>
</ul>
</body>
</html>
