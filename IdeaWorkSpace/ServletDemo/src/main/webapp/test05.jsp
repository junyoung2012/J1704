<%--
  Created by IntelliJ IDEA.
  User: Admin
  Date: 2021/11/3
  Time: 8:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java"  %>
<%--引入标签库--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html>
<head>
    <title>${initParam.caption}-forward</title>
    <base href="${pageContext.request.contextPath}/">
</head>
<body>
    <h1>${set}</h1>
    <h2>定长循环</h2>
    <ul>
        <c:forEach begin="1" end="6" step="2" varStatus="vs">
            <li>${vs.index}-${vs.count}-${vs.first}-${vs.last}-${vs.current}-${vs.count%2==0}</li>
        </c:forEach>
    </ul>
    <h2>foreach循环</h2>
    <ol>
        <c:forEach var="it" items="${set}">
            <li>${it}</li>
        </c:forEach>
    </ol>
    <h2>foreach list</h2>
    <ul>
        <c:forEach var="person" items="${list}">
            <li>${person.name}</li>
        </c:forEach>
    </ul>
    <h2>foreach map</h2>
    <ul>
        <c:forEach var="item" items="${map}">
            <li>${item.value.name}</li>
        </c:forEach>
    </ul>

    <h1>${list[2].name}</h1>
    <h1>${map.及时雨.name}</h1>
    <h1>${map['及时雨'].name}</h1>

    <h2>forTokens</h2>
    <ul>
        <c:forTokens var="s" items="aa,bb,cc" delims=",">
            <li>${s}</li>
        </c:forTokens>
    </ul>

    <hr>

    <h2>控制标签</h2>
    <c:set var="age" value="17"></c:set>
    <h2>${age}</h2>
    <c:if test="${age>18}">
        <h2>成年</h2>
    </c:if>
    <c:choose>
        <c:when test="${age<18}">
            <h2>未成年</h2>
        </c:when>
        <c:otherwise>
            <h2>成年</h2>
        </c:otherwise>
    </c:choose>
    <hr>
    <jsp:useBean id="date1" class="java.util.Date"/>
    <h2>${date1}</h2>
    <h2><fmt:formatDate value="${date1}" pattern="yyyy-MM-dd" /></h2>
    <h2><fmt:formatNumber value="${age}" pattern="000,000,000.00" /></h2>
    <h2>${fn:contains("abcde", "de")}</h2>
    <h2>${fn:indexOf("abcde", "de")}</h2>
    <h2>${fn:substringAfter("abc-de", "-")}</h2>
</body>
</html>
