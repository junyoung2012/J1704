<%--
  Created by IntelliJ IDEA.
  User: Admin
  Date: 2021/11/3
  Time: 8:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java"  %>
<html>
<head>
    <title>${initParam.caption}-forward</title>
    <base href="${pageContext.request.contextPath}/">
</head>
<body>
    <h1><%= request.getAttribute("msg")%></h1>
    <h1><%
        if(request.getAttribute("msg1")!=null){
            out.print(request.getAttribute("msg"));
        }
        pageContext.setAttribute("msg","page的属性");
    %></h1>
    <hr>
    <h1>${msg}</h1>
    <h1>${pageScope.msg}</h1>
    <h1>${requestScope.msg}</h1>
    <h1>${sessionScope.msg}</h1>
    <h1>${applicationScope.msg}</h1>
    <h1>${pageContext.request.contextPath}</h1>
    <h1>${initParam.caption}</h1>
    <h1>${param.user}</h1>
    <h1>${paramValues}</h1>
    <h1>${paramValues.user[0]}</h1>
    <h1>${headerValues}</h1>
    <h1>${header.user-agent}</h1>
    <h1>${cookie}</h1>
    <h1>${cookie.JSESSIONID.value}</h1>
    <hr>
    <h1>${1+1}</h1>
    <h1>${empty msg1}</h1>
    <h1>${person1.getName()}</h1>
    <h1>${person1.name}</h1>
    <h1>${person1.setSex(0)}</h1>
    <h1>${person1.sex}</h1>
    <h1>${person1.sex1}</h1>
</body>
</html>
