package com.kfm.controller;

import com.kfm.model.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.Date;

@Controller
@RequestMapping("/test4")
public class TestController4 {

    @RequestMapping("/test1")
    public String test() {
//        WEB-INF/jsp/  scuess .jsp
//        return "scuess";
//        return "forward:scuess.jsp";
        ModelAndView modelAndView=new ModelAndView();

        return "redirect:scuess.jsp";
    }

    @RequestMapping("/test2")
    public String test2(Model model) {
        User user = new User();
        user.setUserName("aaa");
        user.setRealName("bbb");
        user.setBirthday(new Date());
        user.setSalary(1111);
        model.addAttribute(user);
        return "jianli";
    }


}
