package com.kfm.controller;

import com.kfm.model.Emp;
import com.kfm.model.Student;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Locale;

@Controller
//@RestController
//@ResponseBody
@RequestMapping("/test2")
public class TestController2 {
    @RequestMapping("/test1")
    public String test1(@RequestParam("age1")Integer age){
        System.out.println(age);
        return "scuess";
    }
    @RequestMapping("/test2")
    public String test1(Emp emp){
        System.out.println(emp);
        return "scuess";
    }
    @RequestMapping("/test3")
    public String test3(HttpServletRequest request, HttpServletResponse response, HttpSession session){
        System.out.println(request);
        System.out.println(response);
//        response.addCookie();
        System.out.println(session);
        return "scuess";
    }
    @RequestMapping("/test4")
    public String test4(WebRequest request){
        System.out.println(request);
        return "scuess";
    }

    @RequestMapping(value = "/test5")
    public void test5(OutputStream os) throws IOException {
        Resource res = new ClassPathResource("/img1.jpg");//读取类路径下的图片文件
        FileCopyUtils.copy(res.getInputStream(), os);//将图片写到输出流中
    }
    @RequestMapping(value = "/test6")
    public void test6(Locale locale) throws IOException{
        System.out.println(locale);//国际化
    }
    @RequestMapping(value = "/test7")
    public ModelAndView test7(@RequestParam("logo") MultipartFile logo,HttpServletRequest request) throws IOException{
        if (!logo.isEmpty()) {
            try {
                // 文件存放服务端的位置
                String rootPath = "/upload";
                File dir = new File(request.getSession().getServletContext().getRealPath(rootPath));
                if (!dir.exists()){
                    dir.mkdirs();
                }
                // 写文件到服务器
                File serverFile = new File(dir.getAbsolutePath() + File.separator + logo.getOriginalFilename());
                logo.transferTo(serverFile);
                ModelAndView modelAndView=new ModelAndView("scuess");
                modelAndView.addObject("logo",logo.getOriginalFilename());
                return modelAndView;
            } catch (Exception e) {
                return null;
            }
        } else {
            return null;
        }
    }
    @RequestMapping("/test8")
    @ResponseBody
    public Emp test8(@RequestBody Emp emp){
        System.out.println(emp);
        return emp;
    }

    @RequestMapping("/test9")
    @ResponseBody
    public Student test9(Student student){
        System.out.println(student);
        return student;
    }


}
