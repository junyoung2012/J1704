package com.kfm.controller;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
//@RequestMapping("/")
public class LoginController {
    @RequestMapping("/login")
    public ModelAndView login(String user, String password){
        System.out.println(user);
        if("张三".equals(user) && "123456".equals(password)){
            ModelAndView modelAndView=new ModelAndView();
            modelAndView.setViewName("scuess");
            modelAndView.addObject("username",user);
            return modelAndView;
        }
//        /SpringMvcDemo/WEB-INF/jsp/fail.jsp
        return new ModelAndView("fail");
    }
}
