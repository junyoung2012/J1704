package com.kfm.controller;

import com.kfm.model.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import javax.validation.Valid;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/test3")
@SessionAttributes("user")
public class TestController3 {
    @RequestMapping("/reg")
    public String reg() {
        return "reg";
    }

    @RequestMapping("/test1")
    public String test(@Valid User user, BindingResult bindingResult, Map<String, Object> map, Model model) {
        System.out.println(user);


        if (bindingResult.hasErrors()) {
//           List<ObjectError> allErrors = bindingResult.getAllErrors();
//           map.put("allErrors",allErrors);
            List<FieldError> fieldErrors = bindingResult.getFieldErrors();
//           map.put("fieldErrors",fieldErrors);
            Map<String, String> errors = new HashMap<>();
            for (FieldError fieldError : fieldErrors) {
                errors.put(fieldError.getField(), fieldError.getDefaultMessage());
            }
            model.addAttribute("errors", errors);
            return "reg";
        }
        return "scuess";
    }
//
//    @ModelAttribute("user")
//    public User getUser() {
//        System.out.println("路过1");
//        User user = new User();
//        user.setUserName("aaa");
//        user.setRealName("bbb");
//        user.setBirthday(new Date());
//        user.setSalary(1111);
//        return user;
//    }

    @RequestMapping("/test2")
    public String test2(@ModelAttribute("user") User user) {
        System.out.println("路过2");
        System.out.println(user);
        return "scuess";
    }

    @RequestMapping("/test3")
    public String test3(@ModelAttribute("user") User user) {
        System.out.println("路过3");
        System.out.println(user);
        return "scuess";
    }

    @RequestMapping("/test4")
    public String test3(User user, Model model, ModelMap modelMap,SessionStatus sessionStatus) {
        System.out.println("路过4");
        user.setUserName("aaa");
        user.setRealName("bbb");
        user.setBirthday(new Date());
        user.setSalary(1111);
        model.addAttribute("user", user);
        System.out.println(user);
        return "scuess";
    }


}
