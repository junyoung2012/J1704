package com.kfm.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/test1")
public class TestController1 {
    @RequestMapping("/*/test1")
    public String test1(){
        System.out.println("test1");
        return "scuess";
    }
    @RequestMapping("/**/test2")
    public String test2(){
        System.out.println("test2");
        return "scuess";
    }
    @RequestMapping("/??/test3")
    public String test3(){
        System.out.println("test3");
        return "scuess";
    }
    @RequestMapping("test4")
    public String test4(){
        System.out.println("test4");
        return "scuess";
    }
    @RequestMapping("/{aaa}/{bbb}/test5")
    public String test5(@PathVariable("bbb") String aaa){
        System.out.println(aaa);
        System.out.println("test5");
        return "scuess";
    }
//    @RequestMapping(path = {"/test6","/test666"},method = RequestMethod.POST)
//    @GetMapping("/test6")
//    @PostMapping("/test6")
//    @PutMapping("/test6")
//    @DeleteMapping("/test6")
    public String test6(){
        System.out.println("test6");
        return "scuess";
    }
}
