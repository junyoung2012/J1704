package com.kfm.controller;

import com.kfm.model.Emp;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
@Controller
@RequestMapping("/emp")
public class EmpController {
//    /emp/add 打开新增页面
    @GetMapping("/add")
    public ModelAndView add(){
        return null;
    }
    //    /emp/add post
//    {
//        "id":101,
//        "name":"张三"
//    }
    @PostMapping("/")
    public ModelAndView addSave(Emp emp){
        return null;
    }
    @DeleteMapping("/{id}")
    public ModelAndView delete(@PathVariable int id){
        return null;
    }
    @GetMapping("/{id}")
    public ModelAndView update(@PathVariable int id){
        return null;
    }
    @PutMapping("/")
    public ModelAndView updateSave(Emp emp){
        return null;
    }
    @GetMapping("/")
    public ModelAndView list(){
        return null;
    }

}
