package com.kfm.controller;

import com.kfm.model.Student;
import org.springframework.core.convert.converter.Converter;

public class StudentConvert implements Converter<String, Student> {
    @Override
    public Student convert(String s) {
        System.out.println(s);
        String[] split = s.split(",");
        Student student=new Student();
        student.setId(Integer.parseInt(split[0]));
        student.setName(split[1]);
        return student;
    }
}
