package com.kfm.model;

public class Address {
    private String city;
    private String road;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getRoad() {
        return road;
    }

    public void setRoad(String road) {
        this.road = road;
    }

    @Override
    public String toString() {
        return "Address{" +
                "city='" + city + '\'' +
                ", road='" + road + '\'' +
                '}';
    }
}
