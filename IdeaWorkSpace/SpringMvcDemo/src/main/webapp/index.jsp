<%@page pageEncoding="UTF-8" %>
<html>
<body>
    <%--<h2>登陆</h2>--%>
    <%--<form action="login.do" method="post">--%>
        <%--user:<input name="user" type="text"><br>--%>
        <%--password:<input name="password" type="text"><br>--%>
        <%--<input type="submit" value="登陆">--%>
    <%--</form>--%>
    <h2>请求路由</h2>
    <ul>
        <li><a href="test1/a/test1.do">test1/a/test1.do</a></li>
        <li><a href="test1/bbb/test1.do">test1/bbb/test1.do</a></li>
        <li><a href="test1/bbb/ccc/test1.do">test1/bbb/ccc/test1.do(不行)</a></li>
        <li><a href="test1/bbb/ccc/test2.do">test1/bbb/ccc/test2.do</a></li>
        <li><a href="test1/test2.do">test1/test2.do（**0级或多级目录）</a></li>
        <li><a href="test1/aa/test3.do">test1/aa/test3.do（?模糊匹配一个字符）</a></li>
        <li><a href="test1/aaa/test3.do">test1/aaa/test3.do（不行）</a></li>
        <li><a href="aa/test3.do">test1/aaa/test3.do</a></li>
        <li><a href="test4.do">test4.do</a></li>
        <li><a href="test1/john/test5.do">test/john/test5.do</a></li>
        <li><a href="test1/john/rose/test5.do">test/john/test5.do</a></li>
        <li><a href="test1/test6.do">test1/test6.do</a></li>
    </ul>
    <h2>收参</h2>
    <ul>
        <li><a href="test2/test1.do?age=12">test2/test1.do?age=12</a></li>
        <li><a href="test2/test2.do?address.city=西安&address.road=明光路">test2/test1.do?address.city=西安&address.road=明光路</a></li>
        <li><a href="test2/test2.do?birthday=2018-08-02 22:05:55">test2/test1.do?address.city=西安&address.road=明光路</a></li>
        <li><a href="test2/test2.do?addresses1[0].city=西安&addresses1[0].road=明光路">test2/test1.do?address.city=西安&address.road=明光路</a></li>
        <li><a href="test2/test5.do">test2/test5.do</a></li>
        <li><a href="test2/test6.do">test2/test6.do</a></li>
    </ul>
    <form action="test2/test2.do" method="post">
        <input type="text" name="addresses1[0].city" value="西安">
        <input type="text" name="addresses1[0].road" value="明光路">
        <input type="submit">
    </form>
    <h2>文件上传</h2>
    <form name="serForm" action="test2/test7.do" method="post"  enctype="multipart/form-data">
        <h1>采用流的方式上传文件</h1>
        <input type="file" name="logo">
        <input type="submit" value="upload"/>
    </form>
    <ul>
        <li><a href="test2/test9.do?student=101,张三">test2/test9.do?student=101,张三</a></li>
    </ul>
    <ul>
        <li><a href="test3/reg.do">注册</a></li>
        <li><a href="test4/test1.do">test4/test1.do</a></li>
        <li><a href="test4/test2.do">test4/test2.do</a></li>
    </ul>
</body>
</html>
