package com.kfm.test;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@WebServlet(urlPatterns = "/ValidateUserServlet")
public class ValidateUserServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");
        String userName = request.getParameter("userName");
        PrintWriter out = response.getWriter();
        if("李四".equals(userName)){
            out.println("<font color=red>本用户名已经被使用</font>");
        }else{
            out.println("<font color=green>本用户名可以使用</font>");
        }
        out.flush();
        out.close();
    }
    List list1=new ArrayList();//（对象）工厂模式
    List list2=new LinkedList();

}
