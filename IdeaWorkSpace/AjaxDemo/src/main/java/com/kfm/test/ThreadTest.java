package com.kfm.test;

public class ThreadTest {
    public static void main(String[] args) {
        Count count=new Count();
        new Thread1(count).start();
        new Thread1(count).start();
        new Thread2(count).start();
        new Thread2(count).start();
    }
}
class Count{
    public int j=0;
}
class Thread1 extends Thread{
    private Count count;
    public Thread1(Count count){
        this.count=count;
    }

    @Override
    public void run() {
        synchronized (count){
            count.j++;
        }
    }
}
class Thread2 extends Thread{
    private Count count;
    public Thread2(Count count){
        this.count=count;
    }

    @Override
    public void run() {
        synchronized (count){
            count.j--;
        }
    }
}
