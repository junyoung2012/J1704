<%--
  Created by IntelliJ IDEA.
  User: Admin
  Date: 2021/11/16
  Time: 10:10
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <script>
        function validateUserBySync(user){
            var user=user.value;
            // 1.创建ajax对象
            var xmlHttp=new XMLHttpRequest();
            // 2.调用xmlHttp的open
            var url="ValidateUserServlet?userName="+user;//必须同源
            //提交方式
            //url网址
            //是否异步false同步
            xmlHttp.open("GET",url,false);
            // 3.调用xmlHttp的Send
            xmlHttp.send();
            // 4.接收响应
            // 存有 XMLHttpRequest 的状态。从 0 到 4 发生变化。
            //
            // 0: 请求未初始化
            // 1: 服务器连接已建立
            // 2: 请求已接收
            // 3: 请求处理中
            // 4: 请求已完成，且响应已就绪
            if(xmlHttp.readyState==4 &&
                xmlHttp.status==200){
                // 5.局部刷新
                document.getElementById("msg").innerHTML=xmlHttp.responseText;
            }

        }
        function validateUserByAnsync(user){
            var user=user.value;
            // 1.创建ajax对象
            var xmlHttp=new XMLHttpRequest();
            // 2.调用xmlHttp的open
            var url="ValidateUserServlet?userName="+user;//必须同源
            //提交方式
            //url网址
            //是否异步true异步
            xmlHttp.open("GET",url,true);
            // 3.调用xmlHttp的Send
            xmlHttp.send();

            // 存有 XMLHttpRequest 的状态。从 0 到 4 发生变化。
            //
            // 0: 请求未初始化
            // 1: 服务器连接已建立
            // 2: 请求已接收
            // 3: 请求处理中
            // 4: 请求已完成，且响应已就绪
            // 4.挂接事件处理函数
            xmlHttp.onreadystatechange=function () {
                // 5.接收响应
                if(xmlHttp.readyState==4 &&
                    xmlHttp.status==200){
                    // 6.局部刷新
                    document.getElementById("msg").innerHTML=xmlHttp.responseText;
                }

            }


        }
        function validateUserByPost(user){
            var user=user.value;
            // 1.创建ajax对象
            var xmlHttp=new XMLHttpRequest();
            // 2.调用xmlHttp的open
            var url="ValidateUserServlet";//必须同源
            //提交方式
            //url网址
            //是否异步true异步
            xmlHttp.open("POST",url,true);
            xmlHttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
            // 3.调用xmlHttp的Send
            xmlHttp.send("userName="+user);

            // 存有 XMLHttpRequest 的状态。从 0 到 4 发生变化。
            //
            // 0: 请求未初始化
            // 1: 服务器连接已建立
            // 2: 请求已接收
            // 3: 请求处理中
            // 4: 请求已完成，且响应已就绪
            // 4.挂接事件处理函数
            xmlHttp.onreadystatechange=function () {
                // 5.接收响应
                if(xmlHttp.readyState==4 &&
                    xmlHttp.status==200){
                    // 6.局部刷新
                    document.getElementById("msg").innerHTML=xmlHttp.responseText;
                }

            }


        }
    </script>
</head>
<body>
    <form action="" method="post">
        username:<input type="text" name="username"onchange="validateUserByPost(this)"><span id="msg">aa</span> <br>
        password:<input type="password" name="password" ><br>
        <input type="submit" value="注册">
    </form>
</body>
</html>
