<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; UTF-8" pageEncoding="UTF-8" %>
<html>
<head>
    <title>Servlet测试</title>
</head>
<body>
    index
    <%--<ul>--%>
        <%--<li><a href="Servlet/aaa">/Servlet/*</a></li>--%>
        <%--<li><a href="Servlet/bbb">/Servlet/*</a></li>--%>
        <%--<li><a href="Servlet/bbb.do">*.do</a></li>--%>
        <%--<li><a href="Servlet/aaa/ccc.do">*.do</a></li>--%>
        <%--<li><a href="Servlet/aaa/ccc.do">/</a></li>--%>
        <%--<li><a href="Servlet/aaa/ccc">/</a></li>--%>
        <%--<li><a href="aaa">/aaa</a></li>--%>
    <%--</ul>--%>
    <%--<ul>--%>
        <%--&lt;%&ndash;<script src="TestServlet2"></script>&ndash;%&gt;--%>
        <%--<li><a href="TestServlet2">点击连接get</a></li>--%>
        <%--<form action="TestServlet2" method="post">--%>
            <%--<input type="submit">--%>
        <%--</form>--%>
    <%--</ul>--%>
    <ul>
        <li><a href="JstlServlet">JSTL测试</a></li>
    </ul>
    <%--<c:redirect url="http://www.baidu.com"/>--%>
</body>
</html>
