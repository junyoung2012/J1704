package com.kfm.web;

import com.kfm.dao.ServiceTypeDao;
import com.kfm.model.ServiceType;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(urlPatterns = "/ServiceTypeAction")
public class ServiceTypeAction extends HttpServlet {
    private ServiceTypeDao serviceTypeDao=new ServiceTypeDao();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //参数乱码(utf-8->iso8859-1)
        request.setCharacterEncoding("UTF-8");
        //静态内容的乱码
        response.setContentType("text/html;charset=UTF-8");
        String title = request.getParameter("title");
        String icon = request.getParameter("icon");
        PrintWriter out = response.getWriter(

        );

        ServiceType serviceType=new ServiceType(icon,title);
        if(serviceTypeDao.add(serviceType)){
            out.print("保存成功");
        }else {
            out.print("保存失敗");

        }
        out.flush();;
        out.close();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
