package com.kfm.dao;

import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ProcedureTest2 {
    public static void main(String[] args) throws SQLException {
        String url="jdbc:oracle:thin:@localhost:1521:xe";
        String user="scott";
        String password="tiger";

        Connection connection = DriverManager.getConnection(url,user,password);
        int[] ids = {1, 3, 5, 7};
        String sql = "{call P_HomeSet(?)}";
        CallableStatement statement = connection.prepareCall(sql);
//        调用数组类型参数
        ArrayDescriptor descriptor=
                ArrayDescriptor.createDescriptor("gidarrays".toUpperCase(),connection);

        oracle.sql.ARRAY array=new ARRAY(descriptor,connection,ids);
        statement.setArray(1,array);
        System.out.println(statement.executeUpdate());
    }
}
