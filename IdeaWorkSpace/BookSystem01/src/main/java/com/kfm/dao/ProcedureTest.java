package com.kfm.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

public class ProcedureTest {
    public static void main(String[] args) throws SQLException {
        Connection connection = BaseDao.getDs().getConnection();
        int[] ids = {1, 3, 5, 7};
        String sql = "{call P_HomeSet(?,?,?,?)}";
        CallableStatement statement = connection.prepareCall(sql);
        statement.setInt(1, ids[0]);
        statement.setInt(2, ids[1]);
        statement.setInt(3, ids[2]);
        statement.setInt(4, ids[3]);


        System.out.println(statement.executeUpdate());
    }
}
