package com.kfm.dao;

import com.kfm.model.ServiceType;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import java.sql.SQLException;
import java.util.List;

public class ServiceTypeDao extends BaseDao {
	public boolean add(ServiceType serviceType){
		try {
			String sql="insert into servicetype\n" +
					" (id, icon, title)\n" +
					" values\n" +
					" ((select nvl(max(id),0)+1 from servicetype), ?, ?)";
			return queryRunner.execute(sql,
					serviceType.getIcon(),
					serviceType.getTitle()
			)==1;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	public List<ServiceType> getAll(){
		try {
			String sql="select id, icon, title from servicetype"
					+ " order by id";
			return queryRunner.query(sql, new BeanListHandler<>(ServiceType.class));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
}
