<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <title>服务类型管理</title>
        <link rel="stylesheet" href="/style/base.css" />
        <link rel="stylesheet" href="/style/style.css" />
    </head>
    <body>
        <%@ include file="../head.jsp" %>
        <div class="main">
            <%@ include file="../nav.jsp" %>
            <div class="content">
                <h2>服务类型管理-服务类型</h2>
                <h2>${msg}</h2>
                <form action="/ServiceTypeAction" method="post">
                    <table>
                        <tr>
                            <th></th>
                            <th></th>
                        </tr>
                        <tr>
                            <td>服务名称</td>
                            <td class="aleft">
                                <%--<input name="id" type="hidden" value="${product.id}">--%>
                                <input name="title" type="text">
                            </td>
                        </tr>
                        <tr>
                            <td>服务Icon</td>
                            <td class="aleft">
                                <input name="icon" type="text">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <input class="btn" type="submit" value="保存" />
                                <input class="btn" type="reset" value="取消" />
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
    </body>
</html>
