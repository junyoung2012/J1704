<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <title>服务类型管理</title>
        <link rel="stylesheet" href="/style/base.css" />
        <link rel="stylesheet" href="/style/style.css" />
    </head>
    <body>
        <%@ include file="../head.jsp" %>
        <div class="main">
            <%@ include file="../nav.jsp" %>
            <div class="content">
                <h2>服务类型管理-服务类型列表</h2>
                <h2>${msg}</h2>
                    <table>
                        <tr>
                            <th>类型编号</th>
                            <th>类型标题</th>
                            <th>类型Icon</th>
                            <th>删除</th>
                            <th>修改</th>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </table>
            </div>
        </div>
    </body>
</html>
