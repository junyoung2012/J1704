package com.kfm.messagequeuedemo.model;

public class Mail {
    private String title;
    private String to;
    private String content;

    public Mail() {
    }

    public Mail(String title, String to, String content) {
        this.title = title;
        this.to = to;
        this.content = content;
    }

    @Override
    public String toString() {
        return "Mail{" +
                "title='" + title + '\'' +
                ", to='" + to + '\'' +
                ", content='" + content + '\'' +
                '}';
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
