package com.kfm.messagequeuedemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MessageQueueDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(MessageQueueDemoApplication.class, args);
    }

}
