package com.kfm;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
//<bean id="rich2" class="com.kfm.Rich2"
public class Rich2 {
    @Value("小王富贵")
    private String name;
//    @Resource(name="bmw")
//    先名字，后类型
//    name="bmw 设置注入的名称
//    private Car byd;
    @Autowired
    @Qualifier("bmw")
    private Car car;

//    public Rich2(Car car) {
//        this.car = car;
//    }

    @Override
    public String toString() {
        return "Rich2{" +
                "name='" + name + '\'' +
                ", bmw=" + car +
                '}';
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
