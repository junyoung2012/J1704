package com.kfm.aop.test;

public class TimeApp extends App {
    @Override
    public void run() {
        long begin=System.currentTimeMillis();
        super.run();
        long end=System.currentTimeMillis();
        System.out.println("本次执行时间:"+(end-begin)+"ms");
    }
}
