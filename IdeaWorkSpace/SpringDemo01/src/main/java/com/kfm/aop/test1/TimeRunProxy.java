package com.kfm.aop.test1;

import com.kfm.aop.test.MyRun;

/**
 * 代理
 */
public class TimeRunProxy implements MyRun {
    private MyRun target;

    public TimeRunProxy(MyRun target) {
        this.target = target;
    }

    @Override
    public void run() {
        long begin=System.currentTimeMillis();
        target.run();
        long end=System.currentTimeMillis();
        System.out.println("本次执行时间:"+(end-begin)+"ms");
    }
}
