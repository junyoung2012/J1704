package com.kfm.aop.cglib;

import net.sf.cglib.proxy.MethodInterceptor;

import java.lang.reflect.Method;

public class Intermediary implements MethodInterceptor {

    @Override
    public Object intercept(Object o, Method method, Object[] objects, net.sf.cglib.proxy.MethodProxy methodProxy) throws Throwable {
        Object intercept = methodProxy.invokeSuper(o, objects);
        System.out.println("中介：该房源已发布！");
        return intercept;
    }
}