package com.kfm.aop.test;

public class LogApp extends App {
    @Override
    public void run() {
        long begin=System.currentTimeMillis();
        super.run();
        long end=System.currentTimeMillis();
        System.out.println("本次执行已经被记入日志");
    }
}
