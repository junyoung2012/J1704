package com.kfm.aop.test1;

import com.kfm.aop.test.MyRun;

/**
 * 代理
 */
public class LogRunProxy implements MyRun {
    private MyRun target;

    public LogRunProxy(MyRun target) {
        this.target = target;
    }

    @Override
    public void run() {
        target.run();
        System.out.println("本次执已经被记录到日志");
    }
}
