package com.kfm.aop.test1;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class DynimicProxy implements InvocationHandler {
    private Object target;

    public DynimicProxy(Object target) {
        this.target = target;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        long begin=System.currentTimeMillis();
        Object invoke = method.invoke(target, args);
        long end=System.currentTimeMillis();
        System.out.println("本次执行时间:"+(end-begin)+"ms");
        return invoke;
    }
}
