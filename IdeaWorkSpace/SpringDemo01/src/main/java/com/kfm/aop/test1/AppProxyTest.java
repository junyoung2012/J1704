package com.kfm.aop.test1;

import com.kfm.aop.test.App;
import com.kfm.aop.test.MyRun;

public class AppProxyTest {
    public static void main(String[] args) {
        MyRun target=new App();
        MyRun app=new LogRunProxy(new TimeRunProxy(target));
        app.run();
    }
}
