package com.kfm.aop.cglib;

import net.sf.cglib.proxy.Enhancer;

public class Test {
    public static void main(String[] args) {
        Intermediary intermediary = new Intermediary();
        
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(Landlord.class);
        enhancer.setCallback(intermediary);
        
        Landlord rentProxy = (Landlord) enhancer.create();
        rentProxy.rent();
    }
}