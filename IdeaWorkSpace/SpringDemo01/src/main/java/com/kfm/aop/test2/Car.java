package com.kfm.aop.test2;

public interface Car {
    void front();
    void back(int i);
    void tunRound();
    int run();
    void throwException() throws Exception;
}
