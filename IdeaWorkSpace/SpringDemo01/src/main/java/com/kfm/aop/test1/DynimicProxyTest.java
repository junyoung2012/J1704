package com.kfm.aop.test1;

import com.kfm.aop.test.App;
import com.kfm.aop.test.MyRun;

import java.lang.reflect.Proxy;

public class DynimicProxyTest {
    public static void main(String[] args) {
        App app=new App();
        DynimicProxy proxy=new DynimicProxy(app);
        MyRun myRunProxy= (MyRun) Proxy.newProxyInstance(app.getClass().getClassLoader()
                ,app.getClass().getInterfaces(),proxy);
        myRunProxy.run();
    }
}
