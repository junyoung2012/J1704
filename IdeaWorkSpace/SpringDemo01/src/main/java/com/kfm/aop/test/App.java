package com.kfm.aop.test;

import java.util.Random;

public class App implements MyRun {
    @Override
    public void run(){

        System.out.println("do sth...begin");
        try {
            Thread.sleep(new Random().nextInt(100));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("do sth... end");
    }
}
