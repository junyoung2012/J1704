package com.kfm.aop.test2;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@Configuration //配置类
@ComponentScan(basePackageClasses = {com.kfm.aop.test2.Car.class})
@EnableAspectJAutoProxy(proxyTargetClass = true)
public class AppConfig {
}