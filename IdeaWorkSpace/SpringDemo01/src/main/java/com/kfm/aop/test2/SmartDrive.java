package com.kfm.aop.test2;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;
@Aspect //生命该类是切面类
@Component
public class SmartDrive {
    @Before("execution(* com.kfm.aop.test2.Car.f*())")
    public void tipSafe() {
        System.out.println("道路千万条，安全第一条...");
    }
    @After("execution(public void back(**) throws Exception)")
    public void closeLight() {
        System.out.println("自动关闭倒车灯...");
    }
    @Around("execution(* com.kfm.aop.test2.Car.tunRound())")
    public void autoLight(ProceedingJoinPoint point) {
        System.out.println("自动打开转向灯...");
        try {
            point.proceed();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        System.out.println("自动关闭转向灯...");
    }
    @AfterReturning(value = "execution(* com.kfm.aop.test2.Car.run())",returning = "length")
    public void log(int length){
        System.out.println("本次行驶"+length+"公里，已经被记录");
    }
    @AfterThrowing(value = "execution(* com.kfm.aop.test2.Car.throwException())",throwing = "ex")
    public void dealException(Exception ex){
        System.out.println(ex.getMessage()+",已经被自动处理");
    }

}
