package com.kfm.aop.test2;

import org.springframework.stereotype.Component;

import java.util.Random;
@Component
public class Santana implements Car {
    @Override
    public void front() {
        System.out.println("开车了...");
    }

    @Override
    public void back(int i) {
        System.out.println("倒车ing");
    }

    @Override
    public void tunRound() {
        System.out.println("拐了...");
    }

    @Override
    public int run() {
        int i = new Random().nextInt(100);
        System.out.println("行驶"+i+"公里");
        return i;
    }

    @Override
    public void throwException() throws Exception {
        throw new Exception("抛锚了...");
    }
}
