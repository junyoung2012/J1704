package com.kfm;

public class Person {
    private String name;
    private int age;
    private Car car;

    public Person(String name, int age, Car car) {
        this.name = name;
        this.age = age;
        this.car = car;
    }

    public void run(){
        car.run();
    }
    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    public void sheng(){
        System.out.println("Person被创建了");
    }
    public void si(){
        System.out.println("Person被销毁了");
    }

    public String getName() {
        return name;
    }


    public int getAge() {
        return age;
    }

    public Car getCar() {
        return car;
    }

}
