package com.kfm;

public class Address {
    private String city;
    private String road;

    @Override
    public String toString() {
        return "Address{" +
                "city='" + city + '\'' +
                ", road='" + road + '\'' +
                '}';
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getRoad() {
        return road;
    }

    public void setRoad(String road) {
        this.road = road;
    }
}
