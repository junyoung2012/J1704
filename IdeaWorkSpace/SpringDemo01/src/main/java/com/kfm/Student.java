package com.kfm;

import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

public class Student {
    private String name;
    private Student friend;
    private Set<String> hobbys;
    private List<Course> courses;
    private Map<String,Address> addressMap;
    private Properties properties;//配置属性

    @Override
    public String toString() {
        return this.properties.toString();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Student getFriend() {
        return friend;
    }

    public void setFriend(Student friend) {
        this.friend = friend;
    }

    public Set<String> getHobbys() {
        return hobbys;
    }

    public void setHobbys(Set<String> hobbys) {
        this.hobbys = hobbys;
    }

    public List<Course> getCourses() {
        return courses;
    }

    public void setCourses(List<Course> courses) {
        this.courses = courses;
    }

    public Map<String, Address> getAddressMap() {
        return addressMap;
    }

    public void setAddressMap(Map<String, Address> addressMap) {
        this.addressMap = addressMap;
    }

    public Properties getProperties() {
        return properties;
    }

    public void setProperties(Properties properties) {
        this.properties = properties;
    }
}
