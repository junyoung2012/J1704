package com.kfm;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SpringTest {
    public static void main(String[] args) {
//        1.得到Spring的容器
        BeanFactory beanFactory=new ClassPathXmlApplicationContext("classpath:beans.xml");
//        2.获取SpringBean
//        Person person = (Person) beanFactory.getBean("person");//通过名称
//        Person person =beanFactory.getBean(Person.class);//通过类型获得（改类型的bean要唯一）
        Person person =beanFactory.getBean("person2",Person.class);//不用强转
//      Person person2 =beanFactory.getBean("person1",Person.class);//不用强转
//        System.out.println(person==person2);//不管取多次，都获取的是同一对象，单例模式
        System.out.println(person);
        person.run();
//        Person p=new Person();
////        p.setName("张三");
//        p.setAge(22);
////        p.run();
//        System.out.println(p);
        ((ClassPathXmlApplicationContext) beanFactory).close();

    }
}
