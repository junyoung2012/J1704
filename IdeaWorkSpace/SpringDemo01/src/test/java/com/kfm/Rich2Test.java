package com.kfm;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Rich2Test {
    public static void main(String[] args) {
        BeanFactory beanFactory=new ClassPathXmlApplicationContext("classpath:beans.xml");
        Rich2 rich2 = (Rich2) beanFactory.getBean("rich2");
        System.out.println(rich2);
    }
}
