package com.kfm;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class StudentTest {
    public static void main(String[] args) {
        BeanFactory beanFactory=new ClassPathXmlApplicationContext("classpath:beans.xml");
        Student student = (Student) beanFactory.getBean("student");
        System.out.println(student);
    }
}
