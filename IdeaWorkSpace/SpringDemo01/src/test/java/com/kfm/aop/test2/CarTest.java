package com.kfm.aop.test2;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:beans2.xml")
public class CarTest {
    @Autowired
    private Car car;

    @org.junit.Test
    public void front() {
        car.front();
    }

    @org.junit.Test
    public void back() {
        car.back(23);
    }

    @org.junit.Test
    public void tunRound() {
        car.tunRound();
    }

    @org.junit.Test
    public void run() {
        car.run();
    }

    @org.junit.Test
    public void throwException() {
        try {
            car.throwException();
        } catch (Exception e) {
//            e.printStackTrace();
        }
    }
}