package com.kfm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import springfox.documentation.oas.annotations.EnableOpenApi;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableJpaAuditing

@EnableOpenApi
@EnableSwagger2
@EnableCaching
public class VueshopApi01Application {

    public static void main(String[] args) {
        SpringApplication.run(VueshopApi01Application.class, args);
    }

}
