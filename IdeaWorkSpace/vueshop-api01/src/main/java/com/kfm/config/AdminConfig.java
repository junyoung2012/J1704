package com.kfm.config;

import com.kfm.model.Admin;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AdminConfig {
    @Bean(name = "currentAdmin")
    public Admin getAdmin(){
        Admin admin = new Admin();
        admin.setName("bookuser");
        return admin;
    }

}
