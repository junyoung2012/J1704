package com.kfm.config;

import com.kfm.model.Admin;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;

import javax.annotation.Resource;
import java.util.Optional;

//AuditorAware 实现类，指定当前操作用户信息
@Configuration
public class UserAuditorConfig implements AuditorAware<String> {
    @Resource(name = "currentAdmin")
    private Admin currentAdmin;

    @Override
    public Optional<String> getCurrentAuditor() {
//        return Optional.of(SpringSecurityHolder.currentUserName());

        return Optional.of(currentAdmin.getName());
    }
}

interface B{
    public static final int k=10;
}
class A implements  B{
    private static int k;
    public static void main(String[] args) {
        A a=new A();
        System.out.println( a.k);//正确
        System.out.println( k);//正确
    }
}
