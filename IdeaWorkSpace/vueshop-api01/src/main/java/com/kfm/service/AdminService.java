package com.kfm.service;

import com.kfm.model.Admin;
import com.kfm.model.Promise;
import com.kfm.model.QueryInfo;
import com.kfm.model.Role;

import java.util.List;

public interface AdminService {
    boolean addAdmin(Admin admin);
    boolean deleteAdmin(Admin admin);
    boolean updateAdmin(Admin admin);
    Admin getAdminById(Integer id);
    List<Admin> getAdmins();
    List<Admin> getAdmins(QueryInfo queryInfo);

    boolean addRole(Role role);
    List<Role> getRoles();
    boolean setRolesToAdmin(Admin admin);

    boolean addPromise(Promise promise);
    List<Promise> getPromises();
    Promise getPromise(Integer id);
    boolean setPromisesToRole(Role role);
    List<Promise> getPromisesByAdmin(int id);

    Admin login(String loginName, String password);


    boolean setStateToAdmin(Integer id, int state);
}
