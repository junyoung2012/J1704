package com.kfm.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import com.kfm.dao.AdminDao;
import com.kfm.dao.PromiseDao;
import com.kfm.dao.RoleDao;
import com.kfm.model.Admin;
import com.kfm.model.Promise;
import com.kfm.model.QueryInfo;
import com.kfm.model.Role;
import com.kfm.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * 功    能：管理员业务模块
 *   开发者：John
 * 开发时间:
 *   修改者：
 * 修改时间:
 */
@Service
@Transactional
public class AdminServiceImpl implements AdminService {
    @Autowired
    private AdminDao adminDao;
    @Autowired
    private RoleDao roleDao;
    @Autowired
    private PromiseDao promiseDao;


    @Override
    public boolean addAdmin(Admin admin){
        try {
            adminDao.save(admin);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
    @Override
    @CacheEvict
    public boolean deleteAdmin(Admin admin){
        try {
            adminDao.delete(admin);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    @CacheEvict
    public boolean updateAdmin(Admin admin){
        try {
            Admin old=adminDao.getById(admin.getId());
            //复制新的所有非null来覆盖旧的
            BeanUtil.copyProperties(admin,old,
                    CopyOptions.create().setIgnoreNullValue(true).setIgnoreError(true));
            adminDao.save(old);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    @Cacheable(value = "users")
    public Admin getAdminById(Integer id){
        return adminDao.findById(id).get();
    }
    @Override
    public List<Admin> getAdmins(){
        return adminDao.findAll();
    }

    @Override
    public List<Admin> getAdmins(QueryInfo queryInfo) {
        if(StringUtils.isEmpty(queryInfo.getQuery()))
            return getAdmins();
        return adminDao.getAdminByLimit(queryInfo.getQuery(),
                (queryInfo.getPagenum()-1)*queryInfo.getPagesize(),
                queryInfo.getPagesize());
    }

    public boolean addRole(Role role){
        try {
            roleDao.save(role);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public List<Role> getRoles(){
        return roleDao.findAll();
    }

    @Override
    public boolean setRolesToAdmin(Admin admin) {
        try {
            Admin old=adminDao.findById(admin.getId()).get();
            //复制新的所有非null来覆盖旧的
            BeanUtil.copyProperties(admin,old,
                    CopyOptions.create().setIgnoreNullValue(true).setIgnoreError(true));
            old.setRoles(admin.getRoles1());
            adminDao.save(old);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean addPromise(Promise promise) {
        try {
            promiseDao.save(promise);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public Promise getPromise(Integer id) {
        return promiseDao.findById(id).get();
    }
    @Override
    public List<Promise> getPromises() {
        return promiseDao.findAll();
    }

    @Override
    public boolean setPromisesToRole(Role role) {
        try {
            Role old=roleDao.getById(role.getId());
            //复制新的所有非null来覆盖旧的
            BeanUtil.copyProperties(role,old,
                    CopyOptions.create().setIgnoreNullValue(true).setIgnoreError(true));
            roleDao.save(old);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    @Cacheable(value = "users")
    public List<Promise> getPromisesByAdmin(int id) {
        List<Promise> list = promiseDao.findPromisesByAdmin(id);
        for (Promise promise : list) {
            promise.setChildren(promiseDao.findByParent(promise));
        }
        return list;
    }

    @Override
    @Transactional(readOnly=true)
    public Admin login(String loginName, String password) {
        Admin admin = adminDao.login(loginName,password);
        if(admin!=null){
            admin.setPassword(null);
        }
        return admin;
    }

    @Override
    public boolean setStateToAdmin(Integer id, int state) {
        Admin admin=new Admin();
        admin.setId(id);
        admin.setState(state);
        return setRolesToAdmin(admin);
    }
}
