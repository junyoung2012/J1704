package com.kfm.dao;

import com.kfm.model.Promise;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface PromiseDao extends JpaRepository<Promise,Integer> {
    @Query(nativeQuery = true,value = "SELECT \n" +
            "  DISTINCT  P.*\n" +
            " FROM\n" +
            "  role_promises RP\n" +
            "  LEFT JOIN admin_roles AR ON  RP.role_id = AR.roles_id \n" +
            "  LEFT JOIN promise P ON P.id=RP.promises_id\n" +
            "  WHERE admin_id=:id")
    List<Promise> findPromisesByAdmin(@Param("id") Integer id);
    List<Promise> findByParent(Promise promise);
}
