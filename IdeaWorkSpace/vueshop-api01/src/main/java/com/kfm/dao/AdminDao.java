package com.kfm.dao;

import com.kfm.model.Admin;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface AdminDao extends JpaRepository<Admin,Integer> {
//    @Query(value = "select A from Admin A " +
//            " LEFT JOIN FETCH A.roles R" +
//            " LEFT join FETCH R.promises P " +
//            " where (A.name=:loginName or mobile=:loginName or email=:loginName)" +
//            " and password=:password and P.parent is null ")
    @Query(value = "from Admin " +
            " where (name=:loginName or mobile=:loginName or email=:loginName)" +
            " and password=:password")
    Admin login(@Param("loginName")String loginName,@Param("password")String password );
    @Query(nativeQuery = true,value = "select * from Admin " +
            " where name=:query or mobile=:query or email=:query" +
            " limit :begin , :size")
    List<Admin> getAdminByLimit(@Param("query") String query,
                                @Param("begin") int begin,
                                @Param("size") int size);
}