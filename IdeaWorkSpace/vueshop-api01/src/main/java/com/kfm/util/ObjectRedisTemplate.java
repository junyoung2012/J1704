package com.kfm.util;

import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.JdkSerializationRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

public class ObjectRedisTemplate extends RedisTemplate<String, Object> {
	
	public ObjectRedisTemplate(RedisConnectionFactory redisConnectionFactory) {
		this.setConnectionFactory(redisConnectionFactory);
		// key 使用字符串
		this.setKeySerializer(StringRedisSerializer.UTF_8);
		// value 使用jdk的序列化方式
		this.setValueSerializer(new JdkSerializationRedisSerializer());
	}
}
