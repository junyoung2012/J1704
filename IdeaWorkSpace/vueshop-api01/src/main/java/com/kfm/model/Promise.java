package com.kfm.model;
//package com.kfm.entity;
//package com.kfm.domin;
//package com.kfm.bean;
//package com.kfm.po; vo viewObject value object dto
//package com.kfm.pojo;//错

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
import javax.validation.constraints.NotBlank;
import java.util.List;
import java.util.Objects;

/**
 * 功    能：管理员
 *   开发者：John
 * 开发时间:
 *   修改者：
 * 修改时间:
 */
@ApiModel
@Data
@Slf4j
@NoArgsConstructor
@Entity
@DynamicUpdate
public class Promise extends BaseModel {
    private final static long serialVersionUID=1L;
    @ApiModelProperty(value = "权限名称")
    @NotBlank(message = "权限名称不能为空")
    private String title;
    @ApiModelProperty(value = "权限URL")
    @NotBlank(message = "权限URL不能为空")
    private String url;
    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnore
    private Promise parent;
//    @OneToMany(mappedBy = "parent",fetch = FetchType.LAZY)
    @Transient
    private List<Promise> children;

    public Promise(Integer id) {
        setId(id);
    }

    public Promise( String title,  String url) {
        this.title = title;
        this.url = url;
    }

    public Promise(String title, String url, Promise parent) {
        this.title = title;
        this.url = url;
        this.parent = parent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BaseModel baseModel = (BaseModel) o;
        return id == baseModel.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Promise{}";
    }
}
