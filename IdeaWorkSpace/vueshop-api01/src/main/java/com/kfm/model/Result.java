package com.kfm.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;
@ApiModel
@Data
@Slf4j
@NoArgsConstructor
public class Result implements Serializable {
    @ApiModelProperty(value = "返回数据")
    private Object data;
    @ApiModelProperty
    private Meta meta;

    public Result(int status,String msg,Object data ) {
        this.data = data;
        Meta meta=new Meta();
        meta.setMsg(msg);
        meta.setStatus(status);
        this.meta = meta;
    }
}
@ApiModel
@Data
@Slf4j
@NoArgsConstructor
class Meta implements Serializable {
    @ApiModelProperty
    private String msg;
    @ApiModelProperty
    private int status;
}
