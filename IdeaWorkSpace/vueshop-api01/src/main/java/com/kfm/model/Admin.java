package com.kfm.model;
//package com.kfm.entity;
//package com.kfm.domin;
//package com.kfm.bean;
//package com.kfm.po; vo viewObject value object dto
//package com.kfm.pojo;//错

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.validation.constraints.NotBlank;
import java.util.Objects;
import java.util.Set;

/**
 * 功    能：管理员
 *   开发者：John
 * 开发时间:
 *   修改者：
 * 修改时间:
 */
@ApiModel
@Data
@Slf4j
@NoArgsConstructor
@Entity
@DynamicUpdate
public class Admin extends BaseModel {
    private final static long serialVersionUID=1L;
    @ApiModelProperty(value = "姓名")
    @NotBlank(message = "姓名不能为空")
    private String name;
//    @JsonIgnore
    @ApiModelProperty(value = "密码")
    private String password;
    @ApiModelProperty(value = "手机号")
    private String mobile;
    @ApiModelProperty(value = "邮箱")
    private String email;
    @ManyToMany(fetch = FetchType.EAGER)
    private Set<Role> roles;
    private String token;

    public Admin(Integer id) {
        setId(id);
    }

    public Admin(String name, String password, String mobile, String email) {
        this.name = name;
        this.password = password;
        this.mobile = mobile;
        this.email = email;
    }
    public Admin(Integer id,String name, String password, String mobile, String email) {
        setId(id);
        this.name = name;
        this.password = password;
        this.mobile = mobile;
        this.email = email;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BaseModel baseModel = (BaseModel) o;
        return id == baseModel.id;
    }
    public String listRoles(){
        StringBuilder sb=new StringBuilder();
        for (Role role : roles) {
            sb.append(role.getName()).append(",");
        }
        return sb.delete(sb.length()-1,sb.length()).toString();
    }
    public Set<Role> getRoles1(){
        return roles;
    }
    public String getRoles(){
        return roles.toString().replaceAll("\\[|\\]","");
    }
    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Admin{}";
    }
}
