package com.kfm.model;

import lombok.Data;

@Data
public class QueryInfo {
    private String query;
    private int pagenum;
    private int pagesize;
}
