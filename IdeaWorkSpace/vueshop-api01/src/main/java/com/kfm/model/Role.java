package com.kfm.model;
//package com.kfm.entity;
//package com.kfm.domin;
//package com.kfm.bean;
//package com.kfm.po; vo viewObject value object dto
//package com.kfm.pojo;//错

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.validation.constraints.NotBlank;
import java.util.Objects;
import java.util.Set;

/**
 * 功    能：管理员
 *   开发者：John
 * 开发时间:
 *   修改者：
 * 修改时间:
 */
@ApiModel
@Data
@Slf4j
@NoArgsConstructor
@Entity
@DynamicUpdate
public class Role extends BaseModel {
    private final static long serialVersionUID=1L;
    @ApiModelProperty(value = "角色名称")
    @NotBlank(message = "角色名称不能为空")
    private String name;

    @ManyToMany(fetch = FetchType.LAZY)
    private Set<Promise> promises;
    public Role(Integer id) {
        setId(id);
    }
    public Role(@NotBlank(message = "角色名称不能为空") String name) {
        this.name = name;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BaseModel baseModel = (BaseModel) o;
        return id == baseModel.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return this.name;
    }
}
