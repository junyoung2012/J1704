package com.kfm.controller;

import com.kfm.model.Admin;
import com.kfm.model.QueryInfo;
import com.kfm.model.Result;
import com.kfm.service.AdminService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/admin")
@Api(tags = "Admin管理")
public class AdminController {
    @Autowired
    private AdminService adminService;
    @ApiOperation("新增数据")
    @ApiImplicitParam(name = "admin", value = "admin实体数据" ,required = true, dataType = "Admin")
    @PostMapping
    public Result add(@RequestBody @Validated(Default.class) Admin admin){
        if(adminService.addAdmin(admin)){
            return new Result(200,"保存成功",null);
        }else{
            return new Result(201,"保存失败",null);
        }
    }
    @ApiOperation("删除数据")
    @DeleteMapping("/{id}")
    public Result delete(@PathVariable Integer id){
        if(adminService.deleteAdmin(new Admin(id))){
            return new Result(200,"删除成功",null);
        }else{
            return new Result(201,"删除失败",null);
        }
    }
    @ApiOperation("修改数据")
    @PutMapping
    public Result update(@RequestBody @Validated(Update.class) Admin admin){
        if(adminService.updateAdmin(admin)){
            return new Result(200,"修改成功",null);
        }else{
            return new Result(201,"修改失败",null);
        }
    }
    @ApiOperation("修改数据")
    @PutMapping("/setRoles")
    public Result setRolesToAdmin(@RequestBody  Admin admin){
        if(adminService.setRolesToAdmin(admin)){
            return new Result(200,"修改成功",null);
        }else{
            return new Result(201,"修改失败",null);
        }
    }
    @ApiOperation("修改用户状态")
    @PutMapping("/{id}/state/{state}")
    public Result setStateToAdmin(@PathVariable  Integer id,@PathVariable int state){
        if(adminService.setStateToAdmin(id,state)){
            return new Result(200,"修改成功",null);
        }else{
            return new Result(201,"修改失败",null);
        }
    }
    @ApiOperation("查询所有数据")
    @GetMapping
    public Result getAll(QueryInfo queryInfo){
        return new Result(200,"ok",adminService.getAdmins(queryInfo));
    }
    @ApiOperation("通过ID查询单条数据")
    @GetMapping("/{id}")
    public Result get(@PathVariable Integer id){
        return new Result(200,"ok",adminService.getAdminById(id));
    }
    @ApiOperation("通过ID查询单条数据")
    @GetMapping("/{id}/getPromises")
    public Result getPromises(@PathVariable Integer id){
        return new Result(200,"ok",adminService.getPromisesByAdmin(id));
    }
}
