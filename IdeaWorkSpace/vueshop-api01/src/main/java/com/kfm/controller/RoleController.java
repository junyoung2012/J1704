package com.kfm.controller;

import com.kfm.model.QueryInfo;
import com.kfm.model.Result;
import com.kfm.service.AdminService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/role")
@Api(tags = "角色管理")
public class RoleController {
    @Autowired
    private AdminService adminService;
    @ApiOperation("查询所有数据")
    @GetMapping
    public Result getAll(QueryInfo queryInfo){
        return new Result(200,"ok",adminService.getRoles());
    }
}
