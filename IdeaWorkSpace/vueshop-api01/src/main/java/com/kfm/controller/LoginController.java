package com.kfm.controller;

import com.kfm.model.Admin;
import com.kfm.model.Result;
import com.kfm.service.AdminService;
import com.kfm.util.TokenUtil;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@Api(tags = "登录")
public class LoginController {
    @Autowired
    private TokenUtil tokenUtil;
    @Autowired
    private AdminService adminService;
    @Resource(name = "currentAdmin")
    private Admin currentAdmin;

    @PostMapping("/login")
    public Result login(String loginName,String password){
        Admin admin=adminService.login(loginName,password);
        if(admin!=null){
            String token = tokenUtil.getToken(String.valueOf(admin.getId()), admin.listRoles());
            currentAdmin.setName(admin.getName());
            admin.setToken(token);
            return new Result(200,"登录成功",admin);
        }else{
            return new Result(401,"登录失败",null);
        }
    }
}
