package com.kfm.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import com.kfm.model.Admin;
import com.kfm.model.Promise;
import com.kfm.model.Role;
import com.kfm.service.AdminService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
class AdminServiceTest {

    @Autowired
    private AdminService adminService;

    @Test
    void addAdmin() {
        Admin admin=new Admin("admin","123456","13100010001","admin@kfm.com");
        assertTrue(adminService.addAdmin(admin));
    }

    @Test
    void deleteAdmin() {
    }

    @Test
    void updateAdmin() {
//        Admin admin=new Admin(7,"admin","1234567",null,"admin@kfm.com");
        Admin admin=new Admin();
        admin.setId(8);
        admin.setName("bbbbc");
        assertTrue(adminService.updateAdmin(admin));
    }

    @Test
    void getAdminById() {
        Admin admin = adminService.getAdminById(8);
        System.out.println(admin);
        admin = adminService.getAdminById(8);
        System.out.println(admin);
    }

    @Test
    void getAdmins() {

    }
    @Test
    void addRole() {
//        Role role=new Role("管理员");
//        assertTrue(adminService.addRole(role));
        Role role=new Role("客服人员");
        assertTrue(adminService.addRole(role));
    }
    @Test
    void getRoles() {
        System.out.println(adminService.getRoles());
    }
    @Test
    void setRolesToAdmin() {
        Admin admin=new Admin();
        admin.setId(8);
        Set<Role> roles=new HashSet<>();
        roles.add(new Role(1));
        roles.add(new Role(2));
        admin.setRoles(roles);
        System.out.println(adminService.setRolesToAdmin(admin));
    }

//    List<Promise> getPromises();
    @Test
    void addPromise() {
        Promise promise=new Promise("执行1","test1");
        assertTrue(adminService.addPromise(promise));
        Promise promise1=new Promise("执行11","test11",promise);
        assertTrue(adminService.addPromise(promise1));
        Promise promise12=new Promise("执行12","test12",promise);
        assertTrue(adminService.addPromise(promise12));
        promise=new Promise("执行2","test2");
        assertTrue(adminService.addPromise(promise));
        promise=new Promise("执行3","test3");
        assertTrue(adminService.addPromise(promise));
        promise=new Promise("执行4","test4");
        assertTrue(adminService.addPromise(promise));
    }
    @Test
    void setPromisesToRole() {
        Role role=new Role();
        role.setId(1);
        Set<Promise> promises=new HashSet<>();
        promises.add(new Promise(1));
        promises.add(new Promise(2));
        role.setPromises(promises);
        adminService.setPromisesToRole(role);

    }
    @Test
    void setPromisesToRole2() {
        Role role=new Role();
        role.setId(2);
        Set<Promise> promises=new HashSet<>();
        promises.add(new Promise(2));
        promises.add(new Promise(3));
        promises.add(new Promise(4));
        role.setPromises(promises);
        adminService.setPromisesToRole(role);
    }

    @Test
    void getPromisesByAdmin() {
//        Admin admin=new Admin(8);
//        List<Promise> promiseList = adminService.getPromisesByAdmin(admin);
        System.out.println(adminService.getPromisesByAdmin(8));
        System.out.println(adminService.getPromisesByAdmin(8));
        System.out.println(adminService.getPromisesByAdmin(8));
//        for (Promise promise : promiseList) {
//            System.out.println(promise.getTitle());
//        }
    }

   @Test
    void getPromise() { Promise promise=adminService.getPromise(5);
       System.out.println(promise);
    }
    @Test
    void login() {
        Admin admin = adminService.login("bbbbc", "123456");
        System.out.println(admin);
//        assertTrue(adminService.login("admin@kfm.com", "123456")!=null);
//        assertTrue(adminService.login("13100010001", "123456")!=null);
//        assertTrue(adminService.login("13100010001", "1234567")==null);
    }

    @Test
    public static void main(String[] args) {
        Admin admin=new Admin("admin","a3456","a3100010001","admin@kfm.com");
        Admin admin1=new Admin(2);
        admin1.setName("bbbb");
        BeanUtil.copyProperties(admin1,admin, CopyOptions.create().setIgnoreNullValue(true));
        System.out.println(admin);
    }
}