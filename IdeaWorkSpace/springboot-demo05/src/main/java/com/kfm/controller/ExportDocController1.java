package com.kfm.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/export1")
public class ExportDocController1 {
    @RequestMapping(value = "/exportWord", method= RequestMethod.GET)
    public String  exportWord( HttpServletResponse response, Map<String,Object> dataMap) throws Exception {
            String fileName = "江西省政协委员履职信息登记反馈系统-测试报告.doc"; //文件名称
            dataMap = new HashMap<>();
            dataMap.put("userName","张三");
            dataMap.put("realName","张小三");
            dataMap.put("birthday",new Date());
            dataMap.put("salary",800);
            return "jianli";
    }
}