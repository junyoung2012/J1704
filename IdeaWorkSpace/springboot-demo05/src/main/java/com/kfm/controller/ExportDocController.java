package com.kfm.controller;

import com.kfm.utils.ExportWord;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/export")
public class ExportDocController {
    @RequestMapping(value = "/exportWord", method= RequestMethod.GET)
    public void exportWord(HttpServletRequest request, HttpServletResponse response) throws Exception {
            String fileName = "江西省政协委员履职信息登记反馈系统-测试报告.doc"; //文件名称
            Map<String,Object> dataMap = new HashMap<>();
            dataMap.put("userName","张三");
            dataMap.put("realName","张小三");
            dataMap.put("birthday",new Date());
            dataMap.put("salary",800);
            new ExportWord("UTF-8").exportDoc(response, fileName, "jianli.ftl", dataMap);
    }
}