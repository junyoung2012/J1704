package com.kfm.dao;

import com.kfm.model.Technicain;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import java.sql.SQLException;
import java.util.List;

public class TechnicainDao extends BaseDao {
	public List<Technicain> getAll(){
		try {
			String sql="select id, company, logo, avatar, nickname,"
					+ " price, message, detail, lat, lon from technicain"
					+ " order by id";
			return queryRunner.query(sql,new BeanListHandler<>(Technicain.class));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}



}
