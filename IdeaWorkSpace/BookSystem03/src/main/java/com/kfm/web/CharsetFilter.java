package com.kfm.web;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;
@WebFilter(urlPatterns = "/servlet/*")
public class CharsetFilter implements Filter {
    private String charset;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        System.out.println("init");
//        filterConfig.getServletContext().getInitParameter("")
        charset=filterConfig.getServletContext().getInitParameter("charset");
    }

    @Override
    public void destroy() {
        System.out.println("destroy");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
                         FilterChain filterChain) throws IOException, ServletException {
//        String charset = ((HttpServletRequest) request).getSession().getServletContext().getInitParameter("charset");
        request.setCharacterEncoding(charset);//改变了请求响应的属性
        response.setContentType("text/html;charset="+charset);
        filterChain.doFilter(request,response);//把请求响应继续下去，不写这句，请求响应过程会停止。
    }
}
