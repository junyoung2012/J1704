package com.kfm.web;

import com.kfm.model.Service;
import com.kfm.model.ServiceType;
import com.kfm.model.Technicain;
import com.kfm.service.ServiceService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/servlet/ServiceAction")
public class ServiceAction extends HttpServlet {
    private ServiceService serviceService=new ServiceService();

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String act = request.getParameter("act");
        if(act==null){
            act="list";//控制保护
        }

        Service service=getServiceFromRequest(request);

        switch (act){
            case "add"://无参
                request.setAttribute("serviceTypes",serviceService.getServiceTypes());
                request.setAttribute("technicains",serviceService.getTechnicains());
                forward(request, response, "act","addSave","../WEB-INF/jsp/service/input.jsp");
                break;
            case "addSave"://2参 title,icon (post)
                String msg=checkService(service);
                if(!"".equals(msg)){
                        request.setAttribute("act","addSave");
                        request.setAttribute("service",service);
                        request.setAttribute("serviceTypes",serviceService.getServiceTypes());
                        request.setAttribute("technicains",serviceService.getTechnicains());
                        forward(request, response, msg, "../WEB-INF/jsp/service/input.jsp");
                        return;
                }

                if(serviceService.add(service)){
                        forward(request, response,"保存成功","ServiceAction?act=list");
                    }else{
                        request.setAttribute("act","addSave");
                        request.setAttribute("service",service);
                        request.setAttribute("serviceTypes",serviceService.getServiceTypes());
                        request.setAttribute("technicains",serviceService.getTechnicains());
                        forward(request, response, "保存失败", "../WEB-INF/jsp/service/input.jsp");
                    }
                break;
            case "delete"://1参 id
                forward(request, response,
                        serviceService.delete(service.getId())?"删除成功":"删除失败","ServiceAction?act=list");
                break;
            case "update"://1参 id
                Service service1 = serviceService.getService(service.getId());
                request.setAttribute("service",service1);
                request.setAttribute("serviceTypes",serviceService.getServiceTypes());
                request.setAttribute("technicains",serviceService.getTechnicains());
                forward(request, response, "act","updateSave","../WEB-INF/jsp/service/input.jsp");
                break;
            case "updateSave"://1参 id,title,icon (post)
                msg=checkService(service);
                if(!"".equals(msg)){
                    request.setAttribute("act","updateSave");
                    request.setAttribute("service",service);
                    request.setAttribute("serviceTypes",serviceService.getServiceTypes());
                    request.setAttribute("technicains",serviceService.getTechnicains());
                    forward(request, response, msg, "../WEB-INF/jsp/service/input.jsp");
                    return;
                }

                if(serviceService.update(service)){
                    forward(request, response,"保存成功","ServiceAction?act=list");
                }else{
                    request.setAttribute("act","updateSave");
                    request.setAttribute("service",service);
                    request.setAttribute("serviceTypes",serviceService.getServiceTypes());
                    request.setAttribute("technicains",serviceService.getTechnicains());
                    forward(request, response, "保存失败", "../WEB-INF/jsp/service/input.jsp");
                }
                break;
                default://查 无参 (Get,post)
//                    List<ServiceType> services = serviceDao.getAll();
//                    forward(request,response,"services",services,"../service/list.jsp");
                    forward(request,response,"services",serviceService.getServices(),"../WEB-INF/jsp/service/list.jsp");

        }
    }

    private void forward(HttpServletRequest request, HttpServletResponse response, String key,Object value, String url) throws ServletException, IOException {
        request.setAttribute(key, value);
        request.getRequestDispatcher(url).forward(request, response);
    }
    private void forward(HttpServletRequest request, HttpServletResponse response, String msg, String url) throws ServletException, IOException {
        request.setAttribute("msg", msg);
        request.getRequestDispatcher(url).forward(request, response);
    }

    private String checkService(Service service) {
        String msg="";
//        if(service.getTitle()==null ||  !service.getTitle().matches("\\S{1,5}")){
//            return  "您输入的标题应该在1-5个字符。";
//        }
        return msg;
    }

    private Service getServiceFromRequest(HttpServletRequest request) {
        Service service=new Service();
        String id = request.getParameter("id");
        if(id!=null && id.matches("\\d{1,9}")){
            service.setId(Integer.parseInt(id));
        }

        service.setSubject(request.getParameter("subject"));
        service.setCoverpath(request.getParameter("coverpath"));
        String price = request.getParameter("price");
        if(price!=null && price.matches("\\d{1,9}")){
            service.setPrice(Integer.parseInt(price));
        }
        service.setMessage(request.getParameter("message"));

        String serviceTypeId = request.getParameter("serviceTypeId");
        if(serviceTypeId!=null && serviceTypeId.matches("\\d{1,9}")){
            service.setServiceType(new ServiceType(Integer.parseInt(serviceTypeId)));
        }

        String technicainId = request.getParameter("technicainId");
        if(technicainId!=null && technicainId.matches("\\d{1,9}")){
            service.setTechnicain(new Technicain(Integer.parseInt(technicainId)));
        }

        service.setSummry(request.getParameter("summry"));
        service.setDetail(request.getParameter("detail"));
        service.setPicture(request.getParameter("picture"));

        return service;
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        /*
        * 打开新增页
        * 新增保存
        * 删除
        * 打开修改页
        * 修改保存
        * 查询全部
        * */
        doGet(request,response);
    }
}
