package com.kfm.model;
public class ServiceType {
	private int id;
	private String icon;
	private String title;
	
	public ServiceType() {
	}

	public ServiceType(int id) {
		this.id = id;
	}

	public ServiceType(String icon, String title) {
		this.icon = icon;
		this.title = title;
	}

	public ServiceType(int id, String icon, String title) {
		this.id = id;
		this.icon = icon;
		this.title = title;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}

}
