package com.kfm.model;

import java.sql.Date;
import java.util.Objects;

public class Student {
    private int sno;
    private String sname;
    private Date enroldate;
    private int age;

    public Student() {
    }

    public Student(int sno, String sname, Date enroldate, int age) {
        this.sno = sno;
        this.sname = sname;
        this.enroldate = enroldate;
        this.age = age;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return sno == student.sno;
    }

    @Override
    public int hashCode() {
        return Objects.hash(sno);
    }

    public int getSno() {
        return sno;
    }

    public void setSno(int sno) {
        this.sno = sno;
    }

    public String getSname() {
        return sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }

    public Date getEnroldate() {
        return enroldate;
    }

    public void setEnroldate(Date enroldate) {
        this.enroldate = enroldate;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
