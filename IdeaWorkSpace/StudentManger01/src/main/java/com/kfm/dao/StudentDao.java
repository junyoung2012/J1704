package com.kfm.dao;

import com.kfm.model.Student;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.RowProcessor;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public class StudentDao extends BaseDao implements RowProcessor {
    public boolean add(Student student){
        try {
            String sql="INSERT INTO student (sno, sname, enroldate, age)\n" +
                    "VALUES\n" +
                    "  (?, ?, ?, ?)";
            return queryRunner.execute(sql,
                    student.getSno(),
                    student.getSname(),
                    student.getEnroldate(),
                    student.getAge()
            )==1;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
    public boolean delete(Student student){
        try {
            String sql="delete\n" +
                    " from\n" +
                    "  student\n" +
                    " where sno =?";
            return queryRunner.execute(sql,
                    student.getSno()
            )==1;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
    public boolean update(Student student){
        try {
            String sql="update\n" +
                    "  student\n" +
                    " set\n" +
                    "  enroldate = ?,\n" +
                    "  age = ?\n" +
                    " where sno = ?";
            return queryRunner.execute(sql,
                    student.getEnroldate(),
                    student.getAge(),
                    student.getSno()
                    )==1;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public Student get(Student student){
        try {
            String sql="select\n" +
                    "  sno,\n" +
                    "  sname,\n" +
                    "  enroldate,\n" +
                    "  age\n" +
                    "  from\n" +
                    "  student\n" +
                    "  where sno=?";
            return queryRunner.query(sql, new ResultSetHandler<Student>() {
                @Override
                public Student handle(ResultSet rs) throws SQLException {
                    if(rs.next()){
                        return new Student();//省略
                    }
                    return null;
                }
            }, student.getSno());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
    public List<Student> getAll(){
        try {
            String sql="select\n" +
                    "  sno,\n" +
                    "  sname,\n" +
                    "  enroldate,\n" +
                    "  age\n" +
                    "  from\n" +
                    "  student\n" +
                    "  order by sno";
            return queryRunner.query(sql, new ResultSetHandler<List<Student>>() {
                @Override
                public List<Student> handle(ResultSet rs) throws SQLException {
                    while(rs.next()){
                        return null;//省略
                    }
                    return null;
                }
            });
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Object[] toArray(ResultSet rs) throws SQLException {
        return new Object[0];
    }

    @Override
    public <T> T toBean(ResultSet rs, Class<? extends T> type) throws SQLException {
        return null;
    }

    @Override
    public <T> List<T> toBeanList(ResultSet rs, Class<? extends T> type) throws SQLException {
        return null;
    }

    @Override
    public Map<String, Object> toMap(ResultSet rs) throws SQLException {
        return null;
    }
}
