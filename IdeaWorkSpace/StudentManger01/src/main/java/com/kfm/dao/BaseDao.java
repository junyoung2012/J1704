package com.kfm.dao;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.apache.commons.dbutils.QueryRunner;

public class BaseDao {
	private static ComboPooledDataSource ds;

	protected QueryRunner queryRunner=new QueryRunner(ds);
	
	static {
		try {
			System.out.println("正在打开连接池...");
			ds=new ComboPooledDataSource();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
