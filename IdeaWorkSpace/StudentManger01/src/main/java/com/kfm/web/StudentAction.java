package com.kfm.web;

import com.kfm.dao.StudentDao;
import com.kfm.model.Student;
import com.mysql.cj.util.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Date;
import java.util.HashMap;
import java.util.Map;

@WebServlet(urlPatterns = "/StudentAction")
public class StudentAction extends HttpServlet {
    private StudentDao studentDao=new StudentDao();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");

        String act = request.getParameter("act");
        if(act==null || "".equals(act)){
            act="list";
        }
        Student student=getStudentFromReq(request);
        switch (act){
            case "add"://
                forward("WEB-INF/jsp/student/input.jsp","act","addSave",request,response);
                break;
            case "addSave"://
               Map<String,String> errors=checkStudent(student,true);
               if(errors.size()>0){
                   request.setAttribute("student",student);
                   forward("WEB-INF/jsp/student/input.jsp","errors",errors,request,response);
                   return;
               }
               if(studentDao.add(student)){
                   forward("StudentAction?act=list","msg","保存成功",request,response);
               }else{
                   request.setAttribute("student",student);
                   forward("WEB-INF/jsp/student/input.jsp","msg","保存失败",request,response);
               }
               break;
            case "delete"://
                break;
            case "update"://
                break;
            case "updateSave"://
                errors=checkStudent(student,false);
                if(errors.size()>0){
                    request.setAttribute("student",student);
                    forward("WEB-INF/jsp/student/input.jsp","errors",errors,request,response);
                    return;
                }
                if(studentDao.update(student)){
                    forward("StudentAction?act=list","msg","保存成功",request,response);
                }else{
                    request.setAttribute("student",student);
                    forward("WEB-INF/jsp/student/input.jsp","msg","保存失败",request,response);
                }
                break;
                default:
                    forward("WEB-INF/jsp/student/list.jsp","students",
                            studentDao.getAll(),request,response);

        }
    }

    private void forward(String url, String key,Object value,
                         HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setAttribute(key,value);
        request.getRequestDispatcher(url).forward(request,response);
    }

    private Map<String, String> checkStudent(Student student,boolean newFlag) {
        Map<String, String> errors=new HashMap<>();
        if(newFlag){
            if(studentDao.get(student)!=null){
                errors.put("sno","改学号已经被使用");
            }
            if(StringUtils.isEmptyOrWhitespaceOnly(student.getSname())){
                errors.put("sname","请填写姓名");
            }
        }
        if(student.getAge()<0){
            errors.put("age","年龄非法");
        }
        return errors;
    }

    private Student getStudentFromReq(HttpServletRequest request) {
        Student student=new Student();
        String sno = request.getParameter("sno");
        if(sno!=null && sno.matches("\\d{1,9}")){
            student.setSno(Integer.parseInt(sno));
        }
        student.setSname(request.getParameter("sname"));
        String enroldate = request.getParameter("enroldate");
        if(enroldate!=null && enroldate.matches("\\d{4}-\\d{2}-\\d{2}")){
            student.setEnroldate(Date.valueOf(enroldate));
        }
        String age = request.getParameter("age");
        if(age!=null && age.matches("\\d{1,9}")){
            student.setAge(Integer.parseInt(age));
        }

        return student;
    }
}
