CREATE TABLE Student(
	sno INT PRIMARY KEY,
	sname VARCHAR(30) NOT NULL,
	enroldate DATE,
	age INT
);


INSERT INTO student (sno, sname, enroldate, age)
VALUES
  ('sno', 'sname', 'enroldate', 'age');



DELETE
FROM
  student
WHERE sno = 'sno';



UPDATE
  student
SET
  sname = 'sname',
  enroldate = 'enroldate',
  age = 'age'
WHERE sno = 'sno';

SELECT
  sno,
  sname,
  enroldate,
  age
FROM
  student
LIMIT 0, 1000;

