package com.kfm.springbootdemo1.service;

import com.kfm.springbootdemo1.model.Person;

public interface PersonService {
    public Person getPersonInfo();
}
 