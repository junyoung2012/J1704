package com.kfm.springbootdemo1.service;

import com.kfm.springbootdemo1.model.Person;
import org.springframework.beans.factory.annotation.Autowired;

public class PersonServiceImpl implements PersonService {
    @Autowired
    private Person person;
    @Override
    public Person getPersonInfo() {
        return person;
    }
}