package com.kfm.springbootdemo1.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class IndexController {
 
    @RequestMapping(value = "/say")
    public ModelAndView say() {
        ModelAndView mv=new ModelAndView();
        mv.addObject("msg","Hello , SpringBoot!!!");
        mv.setViewName("result");
        return mv;
    }
 
    @RequestMapping(value = "/speak")
    public String speak(Model model) {
        model.addAttribute("msg","Hello , World!!!");
        return "result";
    }
}