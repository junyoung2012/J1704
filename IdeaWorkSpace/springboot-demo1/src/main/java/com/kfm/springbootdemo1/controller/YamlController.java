package com.kfm.springbootdemo1.controller;

import com.kfm.springbootdemo1.model.Person;
import com.kfm.springbootdemo1.model.Person1;
import com.kfm.springbootdemo1.model.Person2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/yml")
public class YamlController {

    @Autowired
    private Person person;
    @Autowired
    private Person1 person1;
   @Autowired
   private Person2 person2;

    @RequestMapping("/test1")
    public Person test1(){
        return person;
    }
    @RequestMapping("/test2")
    public Person1 test2(){
        return person1;
    }
    @RequestMapping("/test3")
    public Person2 test3(){
        return person2;
    }
}
