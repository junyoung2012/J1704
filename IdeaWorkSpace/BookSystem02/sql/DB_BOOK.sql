CREATE USER bookuser IDENTIFIED BY '123456';

GRANT ALL ON *.* TO bookuser;

CREATE DATABASE DB_BOOK;

ALTER USER 'bookuser'@'%' 
	IDENTIFIED WITH mysql_native_password BY '123456';

FLUSH PRIVILEGES;

USE DB_BOOK;

CREATE TABLE ServiceType(
       id INT AUTO_INCREMENT PRIMARY KEY,
       icon VARCHAR(100) NOT NULL,
       title VARCHAR(15) NOT NULL
);

SELECT
  id,
  icon,
  title
FROM
  db_book.servicetype
LIMIT 0, 1000;


DELETE
FROM
  servicetype
WHERE id = 'id';


UPDATE
  servicetype
SET
  icon = 'icon',
  title = 'title'
WHERE id = 'id';

INSERT INTO db_book.service (
  id,
  SUBJECT,
  coverpath,
  price,
  message,
  serviceTypeId,
  technicainId,
  summry,
  detail,
  picture
)
VALUES
  (
    'id',
    'subject',
    'coverpath',
    'price',
    'message',
    'serviceTypeId',
    'technicainId',
    'summry',
    'detail',
    'picture'
  );
  
  
UPDATE
  service
SET
  SUBJECT = 'subject',
  coverpath = 'coverpath',
  price = 'price',
  message = 'message',
  serviceTypeId = 'serviceTypeId',
  technicainId = 'technicainId',
  summry = 'summry',
  detail = 'detail',
  picture = 'picture'
WHERE id = 'id';

DELETE
FROM
  service
WHERE id = 'id';


SELECT
  id,
  SUBJECT,
  coverpath,
  price,
  message,
  serviceTypeId,
  technicainId,
  summry,
  detail,
  picture
FROM
  db_book.service
  ORDER BY id
LIMIT 20, 10 --srart(从0开始),LENGTH


SELECT COUNT(1) FROM service


CREATE TABLE Emp(
	id INT AUTO_INCREMENT PRIMARY KEY,
	NAME VARCHAR(30) NOT NULL,
	mob VARCHAR(13) UNIQUE NOT NULL,
	PASSWORD VARCHAR(32) NOT NULL
);

ALTER TABLE Emp
	ADD email VARCHAR(30) NOT NULL;
	

INSERT INTO db_book.emp ( NAME, mob, PASSWORD)
VALUES
  ( 'name', 'mob', 'password');

DELETE
FROM
  db_book.emp
WHERE id = 'id';

UPDATE
  emp
SET
  NAME = 'name',
  mob = 'mob',
  PASSWORD = 'password'
WHERE id = 'id';

SELECT  id,NAME,mob, FROM emp
LIMIT 0, 1000;

