package com.kfm.web;

import com.kfm.dao.ServiceTypeDao;
import com.kfm.model.ServiceType;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/servlet/ServiceTypeAction")
public class ServiceTypeAction extends HttpServlet {
    private ServiceTypeDao serviceTypeDao=new ServiceTypeDao();

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        web应用MVC结构控制器的8大功能
//        控制框架（SpringMVC,Struts 2,xWork）
//                  请求路由：分清当前的业务请求
//                收参
//                转化数据类型
//                填充数据对象
//                数据验证
//                调用dao ，执行业务逻辑
//                 把结果存放到数据域
//                跳转页面
//        request.setCharacterEncoding("UTF-8");
//        response.setContentType("text/html;charset=UTF-8");
        String act = request.getParameter("act");
        if(act==null){
            act="list";//控制保护
        }
        ServiceType serviceType=getServiceTypeFromRequest(request);

        switch (act){
            case "add"://无参
                forward(request, response, "act","addSave","../WEB-INF/jsp/serviceType/input.jsp");
                break;
            case "addSave"://2参 title,icon (post)
                String msg=checkServiceType(serviceType);
                if(!"".equals(msg)){
                        request.setAttribute("act","addSave");
                        request.setAttribute("serviceType",serviceType);
                        forward(request, response, msg, "../WEB-INF/jsp/serviceType/input.jsp");
                        return;
                }

                if(serviceTypeDao.add(serviceType)){
                        forward(request, response,"保存成功","ServiceTypeAction?act=list");
                    }else{
                        request.setAttribute("act","addSave");
                    request.setAttribute("serviceType",serviceType);
                        forward(request, response, "保存失败", "../WEB-INF/jsp/serviceType/input.jsp");
                    }
                break;
            case "delete"://1参 id
//                if(serviceTypeDao.delete(serviceType.getId())){
//                    forward(request, response,"删除成功","ServiceTypeAction?act=list");
//                }else{
//                    forward(request, response,"删除失败","ServiceTypeAction?act=list");
//                }
                forward(request, response,
                        serviceTypeDao.delete(serviceType.getId())?"删除成功":"删除失败","ServiceTypeAction?act=list");
                break;
            case "update"://1参 id
                ServiceType serviceType1 = serviceTypeDao.get(serviceType.getId());
                request.setAttribute("serviceType",serviceType1);
                forward(request, response, "act","updateSave","../WEB-INF/jsp/serviceType/input.jsp");
                break;
            case "updateSave"://1参 id,title,icon (post)
                msg=checkServiceType(serviceType);
                if(!"".equals(msg)){
                    request.setAttribute("act","updateSave");
                    request.setAttribute("serviceType",serviceType);
                    forward(request, response, msg, "../WEB-INF/jsp/serviceType/input.jsp");
                    return;
                }

                if(serviceTypeDao.update(serviceType)){
                    forward(request, response,"保存成功","ServiceTypeAction?act=list");
                }else{
                    request.setAttribute("act","updateSave");
                    request.setAttribute("serviceType",serviceType);
                    forward(request, response, "保存失败", "../WEB-INF/jsp/serviceType/input.jsp");
                }
                break;
                default://查 无参 (Get,post)
//                    List<ServiceType> serviceTypes = serviceTypeDao.getAll();
//                    forward(request,response,"serviceTypes",serviceTypes,"../serviceType/list.jsp");
                    forward(request,response,"serviceTypes",serviceTypeDao.getAll(),"../WEB-INF/jsp/serviceType/list.jsp");

        }
    }

    private void forward(HttpServletRequest request, HttpServletResponse response, String key,Object value, String url) throws ServletException, IOException {
        request.setAttribute(key, value);
        request.getRequestDispatcher(url).forward(request, response);
    }
    private void forward(HttpServletRequest request, HttpServletResponse response, String msg, String url) throws ServletException, IOException {
        request.setAttribute("msg", msg);
        request.getRequestDispatcher(url).forward(request, response);
    }

    private String checkServiceType(ServiceType serviceType) {
        String msg="";
        if(serviceType.getTitle()==null ||  !serviceType.getTitle().matches("\\S{1,5}")){
            return  "您输入的标题应该在1-5个字符。";
        }
        return msg;
    }

    private ServiceType getServiceTypeFromRequest(HttpServletRequest request) {
        ServiceType ServiceType=new ServiceType();
        String id = request.getParameter("id");
        if(id!=null && id.matches("\\d{1,9}")){
            ServiceType.setId(Integer.parseInt(id));
        }

        String title = request.getParameter("title");
        ServiceType.setTitle(title);
        String icon = request.getParameter("icon");
        ServiceType.setIcon(icon);
        return ServiceType;
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        /*
        * 打开新增页
        * 新增保存
        * 删除
        * 打开修改页
        * 修改保存
        * 查询全部
        * */
        doGet(request,response);
    }
}
