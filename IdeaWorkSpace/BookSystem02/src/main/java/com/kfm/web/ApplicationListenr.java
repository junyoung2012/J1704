package com.kfm.web;

import com.kfm.dao.BaseDao;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class ApplicationListenr implements ServletContextListener {
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        System.out.println("应用程序启动");
        new BaseDao();
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        System.out.println("应用程序销毁");

    }
}
