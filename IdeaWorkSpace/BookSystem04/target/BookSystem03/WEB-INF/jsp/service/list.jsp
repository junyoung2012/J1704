<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <title>${initParam.appCation}-服务类型管理</title>
        <base href="${pageContext.request.contextPath}/">
        <link rel="stylesheet" href="style/base.css" />
        <link rel="stylesheet" href="style/style.css" />
    </head>
    <body>
        <%@ include file="../head.jsp" %>
        <div class="main">
            <%@ include file="../nav.jsp" %>
            <div class="content">
                <h2>服务类型列表</h2>
                <h2>${msg}</h2>
                    <table>
                        <tr>
                            <th>服务编号</th>
                            <th>服务名称</th>
                            <th>服务coverpath</th>
                            <th>服务价格</th>
                            <th>服务口号</th>
                            <th>服务类型</th>
                            <th>服务教练</th>
                            <th>服务简介</th>
                            <th>服务详情</th>
                            <th>服务展示图</th>
                            <th>删除</th>
                            <th>修改</th>
                        </tr>
                        <c:forEach var="service" items="${services}">
                            <tr>
                                <td>${service.id}</td>
                                <td>${service.subject}</td>
                                <td>${service.coverpath}</td>
                                <td>${service.price}</td>
                                <td>${service.message}</td>
                                <td>${service.serviceType.title}</td>
                                <td>${service.technicain.nickname}</td>
                                <td>${service.summry}</td>
                                <td>${service.detail}</td>
                                <td>${service.picture}</td>
                                <td><a onclick="return confirm('确认删除吗?')" href="servlet/ServiceAction?act=delete&id=${service.id}">删除</a></td>
                                <td><a href="servlet/ServiceAction?act=update&id=${service.id}">修改</a></td>
                            </tr>
                        </c:forEach>
                    </table>
            </div>
        </div>
    </body>
</html>
