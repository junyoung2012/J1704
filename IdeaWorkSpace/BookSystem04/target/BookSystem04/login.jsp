<%@ page pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>${initParam.appCation}</title>
		<base href="${pageContext.request.contextPath}/">
		<link rel="stylesheet" href="style/base.css" />
		<style>
			.top{
				height: 400px;
				line-height:400px ;
				text-align: center;
				font-family: "黑体",sans-serif;
				font-weight: bold;
				letter-spacing:1.25rem;
				font-size: 56px;
				color: #8f853a;
			}
			.middle{
				height: 300px;
				background:-moz-linear-gradient(bottom,#F0954E,#FFF);
				background:-webkit-linear-gradient(bottom,#F0954E,#FFF);
				background:linear-gradient(bottom,#F0954E,#FFF);	
			}
			.middle .main{
				width: 500px;
				height: 220px;
				margin: 0 auto;
				padding-top: 80px;
				background-color: #ca6d15;
				border-radius: 10px 50px;
				text-align: center;
			}
			.middle .main .txt{
				margin: 10px;
				border-radius: 5px;
				height: 20px;
				width: 200px;
				padding-left: 5px;
			}
			.middle .main  .btn{
				border-radius: 5px;
				margin: 10px;
				width: 100px;
				font-weight: bold;
				color: #ca6d15;
			}
			.bottom{
				height: calc(100% - 700px);
				background-color: #F0954E;
			}
			.tip{
				color: white;
				font-size: 1.2em;
				font-weight: bold;
			}
			label{
				padding-left: 5px;
				font-weight: bold;
			}
		</style>
	</head>
	<body>
		<div class="top">
			${initParam.appCation}
		</div>
		<div class="middle">
			<div class="main">
				<form action="LoginAction" method="post">
					<div class="tip">
						${msg}
					</div>
					<div>
						<input name="mob" class="txt" type="text"
						 value="${cookie.mob.value}" placeholder="输入手机号" />
					</div>
					<div>
						<input name="password" class="txt" type="password" placeholder="输入密码" />
					</div>
					<div>
						<label><input name="saveUserFlag" type="checkbox" value="1" checked />&nbsp;记住用户</label>
					</div>
					<div>
						<input class="btn" type="submit" value="登录" />
						<input class="btn" type="reset" value="取消" />
					</div>
				</form>
			</div>
		</div>
		<div class="bottom"></div>
	</body>
</html>
