<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <title>${initParam.appCation}-服务管理</title>
        <base href="${pageContext.request.contextPath}/">
        <link rel="stylesheet" href="style/base.css" />
        <link rel="stylesheet" href="style/style.css" />
    </head>
    <body>
        <%@ include file="../head.jsp" %>
        <div class="main">
            <%@ include file="../nav.jsp" %>
            <div class="content">
                <h2>${act=="addSave"?"新建":"修改"}服务</h2>
                <h2>${msg}</h2>
                <form action="servlet/ServiceSaveAction?act=${act}" method="post" enctype="multipart/form-data">
                    <table>
                        <tr>
                            <th></th>
                            <th></th>
                        </tr>
                        <tr>
                            <td>服务名称</td>
                            <td class="aleft">
                                <%--隐藏表单域--%>
                                <input name="id" type="hidden" value="${service.id}">
                                <input name="subject" type="text" value="${service.subject}">
                            </td>
                        </tr>
                        <tr>
                            <td>服务coverpath</td>
                            <td class="aleft">
                                <input name="coverpath" type="file" value="${service.coverpath}">
                                <img class="img" src="${initParam.uploadDir}/${service.coverpath}" alt="${service.subject}"></td>
                            </td>
                        </tr>
                        <tr>
                            <td>服务价格</td>
                            <td class="aleft">
                                <input name="price" type="number" value="${service.price}">
                            </td>
                        </tr>
                        <tr>
                            <td>服务口号</td>
                            <td class="aleft">
                                <textarea name="message">${service.message}</textarea>
                            </td>
                        </tr>
                        <tr>
                            <td>服务类型</td>
                            <td class="aleft">
                                <c:forEach var="serviceType" items="${serviceTypes}">
                                    <label><input ${service.serviceType.id==serviceType.id?'checked':''}  name="serviceTypeId" type="radio" value="${serviceType.id}">
                                            ${serviceType.title}
                                    </label>
                                </c:forEach>
                            </td>
                        </tr>
                        <tr>
                            <td>服务教练</td>
                            <td class="aleft">
                                <select name="technicainId">
                                    <option value="0">请选择教练</option>
                                    <c:forEach var="technicain" items="${technicains}">
                                        <option  ${service.technicain.id==technicain.id?"selected":""}
                                                value="${technicain.id}">${technicain.nickname}</option>
                                    </c:forEach>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>服务简介</td>
                            <td class="aleft">
                                <textarea name="summry">${service.summry}</textarea>
                            </td>
                        </tr>
                        <tr>
                            <td>服务详情</td>
                            <td class="aleft">
                                <textarea name="detail">${service.detail}</textarea>
                            </td>
                        </tr>
                        <tr>
                            <td>服务展示图</td>
                            <td class="aleft">
                                <input type="file" name="picture" value="${service.picture}">
                                <img class="img" src="${initParam.uploadDir}/${service.picture}" alt="${service.subject}"></td>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <input class="btn" type="submit" value="保存" />
                                <input class="btn" type="reset" value="取消" />
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
    </body>
</html>
