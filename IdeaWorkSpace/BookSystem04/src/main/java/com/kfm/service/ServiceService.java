package com.kfm.service;

import com.kfm.dao.ServiceDao;
import com.kfm.dao.ServiceTypeDao;
import com.kfm.dao.TechnicainDao;
import com.kfm.model.Service;
import com.kfm.model.ServiceType;
import com.kfm.model.Technicain;

import java.util.List;

public class ServiceService {
    private ServiceDao serviceDao=new ServiceDao();
    private ServiceTypeDao serviceTypeDao=new ServiceTypeDao();
    private TechnicainDao technicainDao=new TechnicainDao();


    public boolean add(Service service) {
        return serviceDao.add(service);
    }

    public boolean delete(int id) {
        return serviceDao.delete(id);
    }

    public Service getService(int id) {
        return serviceDao.get(id);
    }

    public boolean update(Service service) {
        return serviceDao.update(service);
    }

    public List<Service> getServices() {
        return serviceDao.getAll();
    }
    public List<Service> getServices(int size,long page) {
        return serviceDao.getAll(size,page);
    }

    public List<ServiceType> getServiceTypes(){
        return serviceTypeDao.getAll();
    }

    public List<Technicain> getTechnicains(){
        return technicainDao.getAll();
    }

    public Long getPageCount(int size) {
        return serviceDao.getPageCount(size);
    }
}
