package com.kfm.web;

import com.kfm.model.Service;
import com.kfm.service.ServiceService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/servlet/ServiceAction")
public class ServiceAction extends HttpServlet {
    private ServiceService serviceService=new ServiceService();

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String act = request.getParameter("act");
        if(act==null){
            act="list";//控制保护
        }

        Service service=getServiceFromRequest(request);

        switch (act){
            case "add"://无参
                request.setAttribute("serviceTypes",serviceService.getServiceTypes());
                request.setAttribute("technicains",serviceService.getTechnicains());
                forward(request, response, "act","addSave","../WEB-INF/jsp/service/input.jsp");
                break;
            case "delete"://1参 id
                forward(request, response,
                        serviceService.delete(service.getId())?"删除成功":"删除失败","ServiceAction?act=list");
                break;
            case "update"://1参 id
                Service service1 = serviceService.getService(service.getId());
                request.setAttribute("service",service1);
                request.setAttribute("serviceTypes",serviceService.getServiceTypes());
                request.setAttribute("technicains",serviceService.getTechnicains());
                forward(request, response, "act","updateSave","../WEB-INF/jsp/service/input.jsp");
                break;
                default://查 无参 (Get,post)
                    int size=Integer.parseInt(getServletContext().getInitParameter("pageSize"));
                    int page=1;
                    String page1 = request.getParameter("page");
                    if(page1!=null && page1.matches("\\d{1,9}")){
                        page=Integer.parseInt(page1);
                    }
                    request.setAttribute("page",page);
                    request.setAttribute("pageCount",serviceService.getPageCount(size));
                    forward(request,response,"services",serviceService.getServices(size,page),"../WEB-INF/jsp/service/list.jsp");

        }
    }

    private void forward(HttpServletRequest request, HttpServletResponse response, String key,Object value, String url) throws ServletException, IOException {
        request.setAttribute(key, value);
        request.getRequestDispatcher(url).forward(request, response);
    }
    private void forward(HttpServletRequest request, HttpServletResponse response, String msg, String url) throws ServletException, IOException {
        request.setAttribute("msg", msg);
        request.getRequestDispatcher(url).forward(request, response);
    }

    private Service getServiceFromRequest(HttpServletRequest request) throws IOException, ServletException {
        Service service=new Service();
        String id = request.getParameter("id");
        if(id!=null && id.matches("\\d{1,9}")){
            service.setId(Integer.parseInt(id));
        }
        return service;
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request,response);
    }
}
