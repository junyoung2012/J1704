package com.kfm.dao;

import com.kfm.model.ServiceType;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import java.sql.SQLException;
import java.util.List;

public class ServiceTypeDao extends BaseDao {
	public boolean add(ServiceType serviceType){
		try {
			String sql="insert into servicetype\n" +
					" ( icon, title)\n" +
					" values\n" +
					" ( ?, ?)";
			return queryRunner.execute(sql,
					serviceType.getIcon(),
					serviceType.getTitle()
			)==1;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	public boolean delete(int id){
		try {
			String sql="DELETE\n" +
					" FROM\n" +
					"  servicetype\n" +
					"WHERE id = ?";
			return queryRunner.execute(sql,id)==1;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	public boolean update(ServiceType serviceType){
		try {
			String sql="UPDATE\n" +
					"  servicetype\n" +
					" SET\n" +
					"  icon = ?,\n" +
					"  title = ?\n" +
					"WHERE id = ?";
			return queryRunner.execute(sql,
					serviceType.getIcon(),
					serviceType.getTitle(),
					serviceType.getId()
			)==1;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	public ServiceType get(int id){
		try {
			String sql="select id, icon, title from servicetype"
					+ " where id = ?";
			return queryRunner.query(sql, new BeanHandler<>(ServiceType.class),id);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	public List<ServiceType> getAll(){
		try {
			String sql="select id, icon, title from servicetype"
					+ " order by id";
			return queryRunner.query(sql, new BeanListHandler<>(ServiceType.class));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
}
