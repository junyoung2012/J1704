package com.kfm.dao;

import com.kfm.model.ServiceType;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;

public class ServiceTypeDaoTest {
    private static ServiceTypeDao serviceTypeDao;
    @BeforeClass
    public static void beforeClass(){
        try {
            InputStream stream = Resources.getResourceAsStream("mybatis-config.xml");
            SqlSessionFactoryBuilder builder=new SqlSessionFactoryBuilder();
            SqlSessionFactory sqlSessionFactory = builder.build(stream);
            SqlSession sqlSession=sqlSessionFactory.openSession(true);
            serviceTypeDao= sqlSession.getMapper(ServiceTypeDao.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    @Test
    public void testAdd(){
        ServiceType ServiceType=new ServiceType("icon","美睫");
        System.out.println(serviceTypeDao.add(ServiceType));
        System.out.println(ServiceType);
    }
    @Test
    public void testGet(){
        System.out.println(serviceTypeDao.get(8));
    }
    @Test
    public void testGetBytitle(){

        System.out.println(serviceTypeDao.getBytitle("%美%"));
        System.out.println(serviceTypeDao.getBytitle("美"));
    }

}