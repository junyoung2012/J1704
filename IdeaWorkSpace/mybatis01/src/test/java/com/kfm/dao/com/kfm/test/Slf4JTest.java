package com.kfm.dao.com.kfm.test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//@Slf4j
//Lombak 龙目岛
public class Slf4JTest {
    private static Logger logger= LoggerFactory.getLogger(Slf4JTest.class);

    public static void main(String[] args) {
        logger.trace("trace消息");
        logger.debug("debug消息");
        logger.info("info消息");
        logger.warn("warn消息");
        logger.error("error消息");
    }
}
