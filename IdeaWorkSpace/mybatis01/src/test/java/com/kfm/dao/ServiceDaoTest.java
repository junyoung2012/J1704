package com.kfm.dao;

import com.kfm.model.Service;
import com.kfm.model.ServiceType;
import com.kfm.model.Technicain;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertTrue;

public class ServiceDaoTest {
    private static ServiceDao serviceDao;
    @BeforeClass
    public static void beforeClass(){
        try {
            InputStream stream = Resources.getResourceAsStream("mybatis-config.xml");
            SqlSessionFactoryBuilder builder=new SqlSessionFactoryBuilder();
            SqlSessionFactory sqlSessionFactory = builder.build(stream);
            SqlSession sqlSession=sqlSessionFactory.openSession(true);
            serviceDao= sqlSession.getMapper(ServiceDao.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    @Test
    public void add() {
        for (int i = 0; i < 95; i++) {
            assertTrue(serviceDao.add(
                    new Service(
                        i,
                        "subject"+(i+1),
                        "coverpath",
                        100+i,
                        "message"+(i+1),
                        new ServiceType((i%4)+5),
                        new Technicain((i%2)+1),
                        "summry"+(i+1),
                        "detail"+(i+1),
                        "picture"
                    )
            ));
        }
    }

    @Test
    public void getServicesByTypeId() {
    }

    @Test
    public void delete() {
    }

   @Test
    public void deleteBanch() {
       List<Integer> list= Arrays.asList(100,101,102);
       serviceDao.deleteBanch(list);
    }

    @Test
    public void get() {
        System.out.println(serviceDao.get(98));
        System.out.println(serviceDao.get(98));
    }
    @Test
    public void get1() {
        try {
            InputStream stream = Resources.getResourceAsStream("mybatis-config.xml");
            SqlSessionFactoryBuilder builder=new SqlSessionFactoryBuilder();
            SqlSessionFactory sqlSessionFactory = builder.build(stream);
            SqlSession sqlSession=sqlSessionFactory.openSession(true);
            serviceDao= sqlSession.getMapper(ServiceDao.class);
            Service x1 = serviceDao.get(98);
            System.out.println(x1);
            sqlSession.close();
            sqlSession=sqlSessionFactory.openSession(true);
            serviceDao= sqlSession.getMapper(ServiceDao.class);
            Service x2 = serviceDao.get(98);
            System.out.println(x2);
            System.out.println(x1==x2);
            sqlSession.close();


        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void update() {
        Service service = serviceDao.get(98);
        service.setSubject("修改后");
        service.setCoverpath("abc.jpg");
        service.setPicture(null);
        assertTrue(serviceDao.update(service));
    }

    @Test
    public void getAll() {
    }

    @Test
    public void getPageCount() {
        System.out.println(serviceDao.getPageCount(10));
    }
}