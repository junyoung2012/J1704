package com.kfm.dao.com.kfm.test;

import org.apache.log4j.Logger;

public class Log4jTest {
    private static Logger logger =Logger.getLogger(Log4jTest.class);

    public static void main(String[] args) {
        logger.trace("trace消息");
        logger.debug("debug消息");
        logger.info("info消息");
        logger.warn("warn消息");
        logger.error("error消息");
        logger.fatal("fatal消息");
    }
}
