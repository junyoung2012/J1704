package com.kfm.dao;

import com.kfm.model.ServiceType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:applicationContext.xml")
public class ServiceTypeDaoTest2 {
    @Resource
    private  ServiceTypeDao serviceTypeDao;
    @Test
    public void testAdd(){
        ServiceType ServiceType=new ServiceType("icon","美睫");
        System.out.println(serviceTypeDao.add(ServiceType));
    }
    @Test
    public void testGet(){
        System.out.println(serviceTypeDao.get(8));
    }

}