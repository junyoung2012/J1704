package com.kfm.dao;

import com.kfm.model.ServiceType;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class ServiceTypeDaoTest1 {
    private static ServiceTypeDao serviceTypeDao;
    @BeforeClass
    public static void beforeClass(){
        BeanFactory beanFactory=new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
        serviceTypeDao=beanFactory.getBean(ServiceTypeDao.class);

    }
    @Test
    public void testAdd(){
        ServiceType ServiceType=new ServiceType("icon","美睫");
        System.out.println(serviceTypeDao.add(ServiceType));
    }
    @Test
    public void testGet(){
        System.out.println(serviceTypeDao.get(8));
    }

}