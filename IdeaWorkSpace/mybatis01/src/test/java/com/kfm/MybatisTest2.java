package com.kfm;

import com.kfm.dao.ServiceTypeDao;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;

public class MybatisTest2 {
    public static void main(String[] args) throws IOException {
        testGetAll();
//        testGet();
//        testAdd();
    }

    private static void testGetAll() throws IOException {
        InputStream inputStream = Resources.getResourceAsStream("mybatis-config.xml");
        SqlSessionFactory sessionFactory= new SqlSessionFactoryBuilder().build(inputStream);
        SqlSession sqlSession=sessionFactory.openSession();
        ServiceTypeDao serviceTypeDao=sqlSession.getMapper(ServiceTypeDao.class);
        System.out.println(serviceTypeDao.getAll());
    }

}
