package com.kfm;

import com.kfm.model.ServiceType;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class MybatisTest1 {
    public static void main(String[] args) throws IOException {
//        testGetAll();
//        testGet();
        testAdd();
    }

    private static void testGetAll() throws IOException {
        InputStream inputStream = Resources.getResourceAsStream("mybatis-config.xml");
        SqlSessionFactory sessionFactory= new SqlSessionFactoryBuilder().build(inputStream);
        SqlSession sqlSession=sessionFactory.openSession();
        List<ServiceType> serviceTypes=sqlSession.selectList("com.kfm.mapper.ServiceType.getAll");
        System.out.println(serviceTypes);
    }
    private static void testGet() throws IOException {
        InputStream inputStream = Resources.getResourceAsStream("mybatis-config.xml");
        SqlSessionFactory sessionFactory= new SqlSessionFactoryBuilder().build(inputStream);
        SqlSession sqlSession=sessionFactory.openSession();
        ServiceType serviceType=sqlSession.selectOne("com.kfm.mapper.ServiceType.get",8);
        System.out.println(serviceType);
    }
    private static void testAdd() throws IOException {
        ServiceType ServiceType=new ServiceType("icon","美睫");
        InputStream inputStream = Resources.getResourceAsStream("mybatis-config.xml");
        SqlSessionFactory sessionFactory= new SqlSessionFactoryBuilder().build(inputStream);
        SqlSession sqlSession=sessionFactory.openSession();
        int i = sqlSession.insert("com.kfm.mapper.ServiceType.add", ServiceType);
        sqlSession.commit();
        System.out.println(i);
    }
}
