package com.kfm.model;

import java.io.Serializable;
import java.util.Set;

public class ServiceType implements Serializable {
	private int id;
	private String icon;
	private String title;
	private Set<Service> serviceSet;
	
	public ServiceType() {
	}

	public ServiceType(int id) {
		this.id = id;
	}

	public ServiceType(String icon, String title) {
		this.icon = icon;
		this.title = title;
	}

	public ServiceType(int id, String icon, String title) {
		this.id = id;
		this.icon = icon;
		this.title = title;
	}

	@Override
	public String toString() {
		return "ServiceType{" +
				"id=" + id +
				", icon='" + icon + '\'' +
				", title='" + title + '\'' +
				'}'+"\n";
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}

	public Set<Service> getServiceSet() {
		return serviceSet;
	}

	public void setServiceSet(Set<Service> serviceSet) {
		this.serviceSet = serviceSet;
	}
}
