package com.kfm.model;

import java.io.Serializable;

public class Technicain implements Serializable {
	private int id;
	private String company;
	private String logo;
	private String avatar;
	private String nickname;
	private int price;
	private String message;
	private String detail;
	private long distance;
	private double lat;
	private double lon;
	
	public Technicain() {
	}

	public Technicain(int id) {
		this.id = id;
	}

	public Technicain(int id, String company, String logo, String avatar, String nickname, int price, String message,
					  String detail, double lat, double lon) {
		this.id = id;
		this.company = company;
		this.logo = logo;
		this.avatar = avatar;
		this.nickname = nickname;
		this.price = price;
		this.message = message;
		this.detail = detail;
		this.lat = lat;
		this.lon = lon;
	}

	@Override
	public String toString() {
		return "Technicain{" +
				"id=" + id +
				", company='" + company + '\'' +
				", logo='" + logo + '\'' +
				", avatar='" + avatar + '\'' +
				", nickname='" + nickname + '\'' +
				", price=" + price +
				", message='" + message + '\'' +
				", detail='" + detail + '\'' +
				", distance=" + distance +
				", lat=" + lat +
				", lon=" + lon +
				'}';
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Technicain other = (Technicain) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getLogo() {
		return logo;
	}
	public void setLogo(String logo) {
		this.logo = logo;
	}
	public String getAvatar() {
		return avatar;
	}
	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	public long getDistance() {
		return distance;
	}
	public void setDistance(long distance) {
		this.distance = distance;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLon() {
		return lon;
	}

	public void setLon(double lon) {
		this.lon = lon;
	}
}
