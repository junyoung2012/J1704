package com.kfm.dao;

import com.kfm.model.ServiceType;

import java.util.List;

public interface ServiceTypeDao {
//	@Insert("INSERT INTO servicetype(icon, title)\n" +
//			"            VALUES\n" +
//			"              (#{icon}, #{title})")
	public boolean add(ServiceType serviceType);
//	@Delete(" DELETE\n" +
//			"            FROM\n" +
//			"              servicetype\n" +
//			"            WHERE id = #{id}")
	public boolean delete(int id);
	public boolean update(ServiceType serviceType);
	public ServiceType get(int id);
	public List<ServiceType> getAll();
	public List<ServiceType> getBytitle(String title);
}
