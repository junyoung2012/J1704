# Object之研究

1.Object是所有类的基类，是继承树上的根节点

2.除了基本数据类型，所有对象都是Object

3.Object方法是所有对象公用的

Object常用方法

- toString  ()
  - 返回该对象的字符串表示。
- hashCode
  - 返回该对象的hash值。
- equals
  - 指示其他对象是否与此对象“相等”。
- Clone
  - 创建并返回此对象的一个副本。

Equals与==de 区别？

- ​	都可以进行对象==比较
- ==可以比较基本数据类型，equals只能比较引用类型
- 语义上，比较的是内存地址，不是同一地址比较一定是false，equals是比较内容的，如何比较却决于类重写equals的规则。但是没有重写，Object，equals和==是相同的。



![image-20210923113123880](https://gitee.com/junyoung2012/blogimg/raw/master/image-20210923113123880.png)

![image-20210923113151434](https://gitee.com/junyoung2012/blogimg/raw/master/image-20210923113151434.png)

```java
if((new Object()).equals(new Object())) {
    System.out.println("equal");
}else {
    System.out.println("not equal");
}
```

考点：equals与==区别？

​		没有重写的equals和==是等同的。

![image-20210923113648512](https://gitee.com/junyoung2012/blogimg/raw/master/image-20210923113648512.png)

Cloneable接口时一个标识性接口，没有需要实现的抽象方法

标识该类是否可以合法的克隆。



浅克隆和深克隆？

​	浅克隆基本数据类型复制为副本，引用数据类型时公用的。

深克隆基本数据类型复制为副本，引用数据类型也是另外的副本。

java Object是浅克隆