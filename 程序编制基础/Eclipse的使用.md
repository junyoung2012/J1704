# Eclipse的使用

1.什么是Eclipse?

- ​	一种IDE(集成开发环境)
- ​	开源的开发环境

2.为什么要用集成开发环境？

- ​	良好的编辑环境（自动提示，自动补全，自动格式化，各种不同主题方案显示）
- 自动编译（会不会提示编译错误（代码行上）？）
- 调试环境（跟踪代码的执行流程，查看当时的执行状态）

3.如何下载安装eclipse?

- eclipse是一个java程序，提前需要安装java环境
- eclipse使用了swt gui技术，丧失了跨平台特性

4.使用eclipse

- ​	创建快捷方式
- 窗口->首选项
  - 配置JDK
  - ![image-20210826145329264](https://gitee.com/junyoung2012/blogimg/raw/master/image-20210826145329264.png)

![image-20210826145439640](https://gitee.com/junyoung2012/blogimg/raw/master/image-20210826145439640.png)

配置字体

​	![image-20210826145554848](https://gitee.com/junyoung2012/blogimg/raw/master/image-20210826145554848.png)

5.eclipse（日蚀）的窗口

- 透视图（persptive）
  - 一组相关的视图
  - java透视图
  - debug视图
- 视图(view)
  - 包视图
  - 编辑试图
  - 控制台视图

6.Eclipse代码组织形式

- workSpace工作空间
- project 项目（工程）
  - ![image-20210826151059657](https://gitee.com/junyoung2012/blogimg/raw/master/image-20210826151059657.png)

- package 包
- class 类



7.扩展工作

- 把你的设置成豆绿色的工作环境
-  把你的eclipse 汉化成中文版（插件安装）
  - 把所有的菜单点点，看看里面有什么（**打游戏不就是这么学会的**）

