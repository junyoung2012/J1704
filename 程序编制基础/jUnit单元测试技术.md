# jUnit单元测试技术

Unit是一个Java语言的单元测试框架。每开发完一个模块，我们就需要进行测试，我们就称作单元测试。

## 特性

​	JUnit是一个[开放源代码](https://baike.baidu.com/item/开放源代码/114160)的[Java测试](https://baike.baidu.com/item/Java测试/16087571)框架，用于编写和运行可重复的测试。他是用于[单元测试](https://baike.baidu.com/item/单元测试)框架体系xUnit的一个实例（用于java语言）。它包括以下特性：

1、用于测试期望结果的断言（Assertion）

2、用于共享共同测试数据的测试工具

3、用于方便的组织和运行测试的测试套件

4、图形和文本的测试运行器



eclipse创建junit测试用例



![image-20210910110222607](https://gitee.com/junyoung2012/blogimg/raw/master/image-20210910110222607.png)

![image-20210910110253543](https://gitee.com/junyoung2012/blogimg/raw/master/image-20210910110253543.png)

![image-20210910110828583](https://gitee.com/junyoung2012/blogimg/raw/master/image-20210910110828583.png)