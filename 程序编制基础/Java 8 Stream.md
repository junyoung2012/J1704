# Java 8 Stream

- Lambda表达式

  - 什么是Lambda
  - Lambda的语法
  - Lambda的简化写法

- 函数接口

  - 什么是函数接口
  - @FunctionInterface
  
- 方法引用

  - 方法引用是java8的新特性之一， 可以直接引用已有Java类或对象的方法或构造器。方法引用与[lambda表达式](https://www.jianshu.com/p/8d7f98116693)结合使用，可以进一步简化代码。
  - 引用方式
    - 类方法引用
    - 实例方法引用
    - 构造方法引用
  - java预定义函数接口

  | 接口名            | 抽象方法             | 说明                                             | 备注     |
  | ----------------- | -------------------- | ------------------------------------------------ | -------- |
  | Supplier          | T get()              | 无输入参数，通过一系列操作产生一个结果返回       | 无中生有 |
  | Consumer          | void accept(T t)     | 一个输入参数，针对参数做一系列操作，无返回值     | 消费掉了 |
  | BiConsumer<T,U>   | void accept(T t,U u) | 两个输入参数，针对参数做一系列操作，无返回值     | 消费掉了 |
  | Function<T,R>     | R apply(T t)         | 一个参数，一个返回值，针对参数生成一个返回值     | 一因一果 |
  | BiFunction<T,U,R> | R apply(T t,U u)     | 两个输入参数，一个返回值，根据参数生成一个返回值 | 多因一果 |
  | Predicate         | boolean test(T t)    | 一个参数，返回校验boolean结果                    | 校验参数 |
| UnaryOperator     | T apply(T t)         | 一个T型参数，通过操作返回一个T型结果             | 一元操作 |
  | BinaryOperator    | T apply(T t1,T t2)   | 两个T型参数，通过操作返回一个T型结果             | 二元操作 |
  
- Stream

  - Stream 是 Java8 中处理集合的关键抽象概念，它可以指定你希望对集合进行的操作，可以执行非常复杂的查找、过滤和映射数据等操作。使用Stream API 对集合数据进行操作，就类似于使用 SQL 执行的数据库查询。也可以使用 Stream API 来并行执行操作。简而言之，Stream API 提供了一种高效且易于使用的处理数据的方式。

    特点：

            1 . 不是数据结构，不会保存数据。
            2. 不会修改原来的数据源，它会将操作后的数据保存到另外一个对象中。（保留意见：毕竟peek方法可以修改流中元素）
        
            3. 惰性求值，流在中间处理过程中，只是对操作进行了记录，并不会立即执行，需要等到执行终止操作的时候才会进行实际的计算。

  - stream的创建方式

    - 使用Collection下的 stream() 和 parallelStream() 方法
    - 使用Arrays 中的 stream() 方法，将数组转成流
    - 使用Stream中的静态方法：of()、iterate()、generate()
    - 使用 BufferedReader.lines() 方法，将每行内容转成流
    -  使用 Pattern.splitAsStream() 方法，将字符串分隔成流
  -  ![img](https://gitee.com/junyoung2012/blogimg/raw/master/20181223012834784.png)
  

无状态：指元素的处理不受之前元素的影响；

有状态：指该操作只有拿到所有元素之后才能继续下去。

 非短路操作：指必须处理所有元素才能得到最终结果；
 短路操作：指遇到某些符合条件的元素就可以得到最终结果，如 A || B，只要A为true，则无需判断B的结果。

  - 流的中间操作
  
    - 筛选与切片
    - 映射
    - 排序
    - 消费
  
  - 流的终止操作
  
  - 匹配、聚合操作
    -  allMatch：接收一个 Predicate 函数，当流中每个元素都符合该断言时才返回true，否则返回false
            noneMatch：接收一个 Predicate 函数，当流中每个元素都不符合该断言时才返回true，否则返回false
              anyMatch：接收一个 Predicate 函数，只要流中有一个元素满足该断言则返回true，否则返回false
              findFirst：返回流中第一个元素
              findAny：返回流中的任意元素
              count：返回流中元素的总个数
              max：返回流中元素最大值
              min：返回流中元素最小值
  
  - Collector 工具库：Collectors
  
    - 

