# java的异常处理机制

1.什么是异常？

​	当程序违反Java编程的语义约束时  ，Java虚拟机将这个错误作为一个异常；

2.异常的分类

​	产生了异常，需要进行处理，根据**处理的方式**异常分为3类

- 根本处理不了（Error）例如：虚拟机异常，IO错误，动态链接库错误
- 必须进行处理（Checked Exception）受控异常,不处理会报编译错误，而不是运行错误
  - ClassNotFoundException
  - IOException,FileNotFoundException,CharacterCodingException
  - NoSuchFieldException, NoSuchMethodException
  - ParseException
  - PrintException
  - SQL Exception
- 你可以处理也可以不处理 （RuntimeException）,jvm就可以默认处理了，因为处理方式也基本就是提示异常信息。

  - NullPointerException 

  - ArrayIndexOutOfBoundsException

  - StringIndexOutOfBoundsException

  - ArithmeticException

  - ClassCastException

![image-20210923090815292](https://gitee.com/junyoung2012/blogimg/raw/master/image-20210923090815292.png)

3.异常的处理方式

​	利用try...catch...finally关键字，捕获异常

- ​		try表示异常捕获的范围，后面要有不饿能省略的大括号
- catch(异常参数)加大括号
  - 异常处理的代码
  - 可以有多个
  - 异常参数类型JDK7以后可以有多个
- finally
  - 无论异常发生与否，必须执行的代码
    //			资源释放处理释放代码
  - 只能由一个

  利用throws关键字，将异常处理的责任，交给上级（本方法的调用者）

4.Throwable的2个方法

- printStatckTrace
- getMessage

5.throw和throws的区别？

- ​	异常处理的关键字
- 长的像
- 使用位置不同
  - throw再方法体，throws方法的签名上
- 功能不一样
  - throw是动词，抛出异常对象
  - throws是副词，表示把处理异常的责任，交给方法的调用者
- 产生的效果不一样
  - throw异常一定发生
  - throws异常是不一定发生的

6.先抓小的，后抓大的

- 先捕获子异常，再捕获符异常
- 如果后面捕获的异常，是前面的子异常，会报编译错误

7.重写中异常处理的3种方式

- throws原异常
- 不再throws任何异常
- throws原异常的子异常

![image-20210923101944812](https://gitee.com/junyoung2012/blogimg/raw/master/image-20210923101944812.png)

考点：

​	自定义异常

​	异常的处理方式throws

答案：B

![image-20210923102138649](https://gitee.com/junyoung2012/blogimg/raw/master/image-20210923102138649.png)

考点：先抓小的，后抓大的

​		违反此规则，会引发编译错误，注意错误产生的位置再第6行

8.处理技术的应用

能try就try,指用于处理业务信息足够的，并且不需要让上一级调用者了解的异常

throws可以让代码简介清晰，提高可读性。

​			业务信息不足不能处理所以，交给调用者处理

先try，再throw也是常见的处理方式

AOP(面向切面编程)的方式处理异常，是商业开发的主流。

异常处理：

- 提示
- 记录错误信息
- 报告上级

9.不好的处理方式

- ​	Catch all
- 嵌套catch
- 静默处理（完全不提示异常）

10.jdk7新处理异常方式

​	try-with-resources语句是一种声明了一种或多种资源的try语句。资源是指在程序用完了之后必须要关闭的对象。try-with-resources语句保证了每个声明了的资源在语句结束的时候都会被关闭。任何实现了java.lang.AutoCloseable接口的对象，和实现了java.io.Closeable接口的对象，都可以当做资源使用。

```java
package jdk7_new_character;

public class Test {

    @org.junit.Test
    public void test() {
        // fail("Not yet implemented");
    }

    @org.junit.Test
    public void testAutoClose() {
        System.out.println("begin testAutoClose");
        /*
         * try { AutoCloseableTestClass autoCloseableTestClass = new
         * AutoCloseableTestClass(); } catch (Exception e) { } finally {
         * 
         * }
         */

        try (AutoCloseableTestClass autoCloseableTestClass = new AutoCloseableTestClass();
             AutoCloseableTestClass2 autoCloseableTestClass2 = new AutoCloseableTestClass2()
        ) {
            System.out.println("try{}");
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("over testAutoClose");
    }

}
```

可以看到你可以在一个try-with-resources语句里面声明多个资源。当代码块中代码终止，不管是正常还是异常，对象的close方法都会自动按声明的**相反顺序**调用。

注意：try-with-resources语句也可以像普通的try语句一样，有catch和finally代码块。**在try-with-resources语句中，任何的catch和finally代码块都在所有被声明的资源被关闭后执行。**