# java集合框架

1.集合的分类

![image-20210928091804079](https://gitee.com/junyoung2012/blogimg/raw/master/image-20210928091804079.png)

- Set 无序无重   比较强调集体
- List 有序可重   中庸，集体和个体都强调
- Map 由键得值 比较强调个体

2List之研究

- list对标的数组
- 不考虑长度
- 读写不如数组方便
- 除了读写操作外，有很多集合操作
- 轻松方便的删除方式
- 不主动应用数组

java Se的注解

- @Override 方法需要重写
-  @Deprecated 方法或类被废弃了
- @SuppressWarnings({ "rawtypes", "unchecked" }) 抑制编译警告

3.泛型

​	1.什么是泛型技术？

- ​	是一种参数化类型技术，可以用来限制处理数据的类型
- 类、接口，方法上都可以使用泛型
- 使用了泛型，首先让数据变得更安全
- 使用了泛型，让编程变得更简洁，不需要再进行数据类型转换
- jdk5.0以后产生的。

4.list的原理

- ​	list的存储结构

  - 数组  
  - ![image-20210928113143339](https://gitee.com/junyoung2012/blogimg/raw/master/image-20210928113143339.png)
  - 链表 linkedList双链表
  - ArrayList和linkedlist区别？
    - list接口
    - 实现不同
    - add
      - append ArrayList慢一点
      - insert ArrayList慢很多
    - 删除
      - ArrayList慢很多
    - 修改
      -  ArrayList快
    - 查
      - 遍历
        - 一样
      - 随机访问
        - 数组快很多
  - 

- ArrayList扩容

  - 初始化
    - 无参构造，默认长度是0的空数组。
    - 有参构造，是可以指定容量
  - 扩容
    - 第1次调用add方法
      - 没有指定容量，一定扩容长度到10
      - ![image-20210928114556396](https://gitee.com/junyoung2012/blogimg/raw/master/image-20210928114556396.png)
    - 其它情况，扩容数组长度的1.5倍
  - ![image-20210928114429088](https://gitee.com/junyoung2012/blogimg/raw/master/image-20210928114429088.png)

  

收缩数组

​	![image-20210928114855089](https://gitee.com/junyoung2012/blogimg/raw/master/image-20210928114855089.png)





JCF框架

- 1个图

- 2个类（工具）

- 3个知识点

  - 自动装箱、自动拆箱
  - 泛型
  - 增强for循环
    - 只能使用在数组或者是实现了Iterable接口的对象
    - 底层会使用Iterable接口，得到Iterator对象，实际上会调用next（）进行循环

- 8个接口

  - Collection
  - Set
  - List
  - Map
  - Iterator 迭代器
    - hasNext()
    - next()
    - remove();//删除？？？
  - Queue（单向队列）
    - ![image-20210929092156073](https://gitee.com/junyoung2012/blogimg/raw/master/image-20210929092156073.png)
  - Deque（双向队列）

  ![image-20210929092405285](https://gitee.com/junyoung2012/blogimg/raw/master/image-20210929092405285.png)

  - ​	Comparable

- 实现类
  - Vector 实现了List接口的类，底层是数组实现，可以随机访问，线程安全

HashCode的研究

1.什么是hashCode?

​	对象的一个int类型的哈希值，散列值，是由对象的hashcode()方法提供的。提供的hahscode是用来再hash实现的存储结构种确定bucket（桶）存储位置，hash算法应该高效，相对平均概率的利用存储位置。

编制hashcode 的3原则

- 对同一对象多次调用 hashCode 方法时，必须一致地返回相同的整数
- equals(Object) 为true,2对象的hashcode必须相等
- equals(Object) 为false,2对象的hashcode可以相等，也可以不等

2.重写equals的原则

​	指示其他某个对象是否与此对象“相等”。 内容比较。

- （自反性）对于任何非空引用值 x，x.equals(x) 都应返回 true。 
- （对称性）对于任何非空引用值 x 和 y，当且仅当 y.equals(x) 返回 true 时，x.equals(y) 才应返回 true。
- （传递性）对于任何非空引用值 x、y 和 z，如果 x.equals(y) 返回 true，并且 y.equals(z) 返回 true，那么 x.equals(z) 应返回 true。
- （一致性）对于任何非空引用值 x 和 y，多次调用 x.equals(y) 始终返回 true 或始终返回 false，前提是对象上 equals 比较中所用的信息没有被修改。
- 对于任何非空引用值 x，x.equals(null) 都应返回 false。 **没有报异常**

TreeSet底层实现 Treemap 是一个红黑树

- ​	树

- 节点

  - 子节点的个数称作节点的

- 树的度？

  - 最大的子节点的度

- 二叉树

  - 树的度为2

- 完全二叉树

- 满二叉树

- 查找二叉树

  - 树上的节点有序的（）
    - 左子树所有节点都小于根节点
    - 所有右子树的节点都大于根节点

- 平衡二叉树

  - 旋转操作

- 红黑树

  - 红黑树是一种平衡二叉查找树的变体，它的左右子树高差有可能大于 1，所以红黑树不是严格意义上的[平衡二叉树](https://baike.baidu.com/item/平衡二叉树/10421057)（AVL），但 对之进行平衡的代价较低， 其平均统计性能要强于 AVL 。

  - 由于每一棵红黑树都是一颗[二叉排序树](https://baike.baidu.com/item/二叉排序树/10905079)，因此，在对红黑树进行查找时，可以采用运用于普通二叉排序树上的查找算法，在查找过程中不需要颜色信息。 

  - 特征

    性质1. 结点是红色或黑色。 [3] 

    性质2. 根结点是黑色。 [3] 

    性质3. 所有叶子都是黑色。（叶子是NIL结点） [3] 

    性质4. 每个红色结点的两个子结点都是黑色。（从每个叶子到根的所有路径上不能有两个连续的红色结点）

    性质5. 从任一节结点其每个叶子的所有路径都包含相同数目的黑色结点。 [3] 

    **从根到叶子的最长的可能路径不多于最短的可能路径的两倍长**。结果是这个树大致上是平衡的。因为操作比如插入、删除和查找某个值的最坏情况时间都要求与树的高度成比例，这个在高度上的理论上限允许红黑树在最坏情况下都是高效的，而不同于普通的二叉查找树。 [3] 

    ![image-20210929112149209](https://gitee.com/junyoung2012/blogimg/raw/master/image-20210929112149209.png)

​	

2个工具类

​	Arrays

​	Collection**s**

3.Map

- HashMap1.8以后的实现。

  - ![image-20210930114342151](https://gitee.com/junyoung2012/blogimg/raw/master/image-20210930114342151.png)

- 默认hash数组的长度是16

- 默认的加载（填充）因子是0.75（桶数组的填满程度）

- ![image-20210930112126744](D:\J1704\程序编制基础\images\image-20210930112126744.png)

- ![image-20210930112004625](https://gitee.com/junyoung2012/blogimg/raw/master/image-20210930112004625.png)

- capacity 即容量，默认16。

- loadFactor 加载因子，默认是0.75

- threshold 阈值。阈值=容量*加载因子。默认12。当元素数量超过阈值时便会触发扩容。

- 一般情况下，**当元素数量超过阈值时**便会触发扩容。每次扩容的容量都是之前容量的**2倍**。

- 扩容时机 hash数组的长度*加载（填充）因子被使用，进行扩容

- jd8 hahsmap与jdk7的区别？

  - **数据结构上的区别**
  - 1.JDK7中的数据结构主要是：**数组＋链表**，数组和链表的节点的实现类是Entry类
    2.JDK8中的数据结构主要是：**数组＋链表/红黑树**，当链表的元素个数大于等于8的时候转为红黑树，元素个数小于等于6时，红黑树结构还原成链表,数组和链表的节点的实现类是Node类
  - ![image-20210930113609650](https://gitee.com/junyoung2012/blogimg/raw/master/image-20210930113609650.png)

- **Hash值的计算区别**

  - JDK7：h^ =(h>>>20)^(h>>>12) return h ^(h>>>7) ^(h>>>4);
    2.JDK8：(key==null)?0:(h=key.hashCode())^(h>>>16) 
    剖析:![image-20210930113811431](https://gitee.com/junyoung2012/blogimg/raw/master/image-20210930113811431.png)
  - https://blog.csdn.net/zmj199536/article/details/100990213

- **.链表数据插入的区别**

  - 1.JDK7：使用的是头插入法，扩容后与原位置相反(resize会导致环形链表)
    2.JDK8：使用的尾插法，扩容后位置与原链表相同
  - ![image-20210930113956713](https://gitee.com/junyoung2012/blogimg/raw/master/image-20210930113956713.png)

- **扩容机制的不同**
  1.JDK7扩容条件:元素个数 > 容量(16) * 加载因子 (0.75) && 插入的数组位置有元素存在
  2.JDK8扩容条件 :元素个数 > 容量 (16) * 加载因子(0.75)
  剖析:

- ![image-20210930114103552](https://gitee.com/junyoung2012/blogimg/raw/master/image-20210930114103552.png)

- HashMap和Hashtable的区别

  1.  对外提供的接口不同

     1. Hashtable比HashMap多提供了elments() 和contains() 两个方法。

        elments() 方法继承自Hashtable的父类Dictionnary。elements() 方法用于返回此Hashtable中的value的枚举。

        contains()方法判断该Hashtable是否包含传入的value。它的作用与containsValue()一致。事实上，contansValue() 就只是调用了一下contains() 方法。

        ————————————————
        版权声明：本文为CSDN博主「hahahaha233」的原创文章，遵循CC 4.0 BY-SA版权协议，转载请附上原文出处链接及本声明。
        原文链接：https://blog.csdn.net/wangxing233/article/details/79452946

  2. 继承的父类不同

     1. 。HashMap是继承自AbstractMap类，而HashTable是继承自Dictionary类。不过它们都实现了同时实现了map、Cloneable（可复制）、Serializable（可序列化）这三个接口

  3. 线程安全性不同

     1. Hashtable是线程安全的，它的每个方法中都加入了Synchronize方法。在多线程并发的环境下，可以直接使用Hashtable，不需要自己为它的方法实现同步

        HashMap不是线程安全的，在多线程并发的环境下，可能会产生死锁等问题。具体的原因在下产生时间

     1. Hashtable是java一开始发布时就提供的键值映射的数据结构，而HashMap产生于JDK1.2。

  4. 对Null key 和Null value的支持不同

     1. Hashtable既不支持Null key也不支持Null value。Hashtable的put()方法的注释中有说明。
        ![这里写图片描述](https://gitee.com/junyoung2012/blogimg/raw/master/20180306020744222)
     2. HashMap中，null可以作为键，这样的键只有一个；可以有一个或多个键所对应的值为null。当get()方法返回null值时，可能是 HashMap中没有该键，也可能使该键所对应的值为null。因此，在HashMap中不能由get()方法来判断HashMap中是否存在某个键， 而应该用containsKey()方法来判断。

  5. 遍历方式的内部实现上不同

     1. Hashtable、HashMap都使用了 Iterator。而由于历史原因，Hashtable还使用了Enumeration的方式 
     2. 

- map集合的遍历方式

- HashMap的Iterator是fail-fast迭代器。当有其它线程改变了HashMap的结构（增加，删除，修改元素），将会抛出ConcurrentModificationException。不过，通过Iterator的remove()方法移除元素则不会抛出ConcurrentModificationException异常。但这并不是一个一定发生的行为，要看JVM。

- 快速失败工作模式

  - 进入快速失败模式后，只要不通过迭代方式就会快速失败，java.util.ConcurrentModificationException
