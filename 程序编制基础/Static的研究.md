# Static的研究

static关键字可以修饰那些类里的元素

​	成员变量

​	方法

​	内部类

​	代码块

static修饰符有什么用？

- ​	不是静态（在java种没有）
- static访问方式不同
- ![image-20210907094702355](https://gitee.com/junyoung2012/blogimg/raw/master/image-20210907094702355.png)

我们为什么要在成员变量，方法，或者是内部类，代码块上加static修饰。

- ​	就是为了在整个的类世界，限制某个成员只有一份，大家共享

以变量为例加static的区别

​	访问方式不同，static，类和对象都能访问

​									不加static就只能通过对象访问。

​		数据的共享性不同 static 类和所有对象共享同一份。

​									不加static  ，是每个对象独有的。

​		表达的思想是不同的 static  在类的世界是是唯一的。