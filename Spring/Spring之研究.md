Spring之研究

1.什么是Spring?

- ​	一个服务端框架
- Spring像胶水，把各层粘合起来
- Spring像个总管，统一管理其它各个层次
- Spring是一个full stack框架
- Spring是一个很大的生态圈，集成了很多子框架

2.Spring有很么用？

​	Spring framework

![image-20211122084024678](https://gitee.com/junyoung2012/blogimg/raw/master/image-20211122084024678.png)

3.Spring是怎么用的？

- ​	引包
- 配置
- 常用类和接口

4.Spring core的作用

- ​	替我们创建了对象

- spring bean的属性是可以通过配置文件设置的。程序现在可以不经编译，改变实现，所以程序有更大的灵活性。

- spring帮我们创建了类里面使用的其它类的(**依赖**)对象，并且把它传递（**注入**）进来了

  - dependcy inject(依赖注入DI)
  - inverse Object controller(控制反转IOC)

- ## Spring的IOC 消除代码之间的耦合性（==消耦，解耦==）

- 什么是Spring bean?

  - java bean一种特殊的类，反义词（pojo）
    - 必有无参构造
    - 属性私有，通过get，set访问

- 是Spring管理生命周期的对象

- 依赖的bean也会被注入

5.**Spring bean的生命周期**

- ​	 scope="singleton" (单例)
  - spring容器创建就创建，对象是单例的，依赖对象会被创建，还会被注入
  - 容器销毁，Spring bean也会被销毁
- ​	scope="prototype"（有状态，有成员变量记录数据）
  - getBean会被创建，每次都是新对象
  - 取决了GC的回收

6.**IOC的注入方式**

- ​	设值注入
  - 利用set方法进行注入
- 构造注入
  - 利用构造方法注入
- ~~接口注入(方法注入)~~
  - doGet(HttpServletRequest,HttpServletResponse response)
- 设值注入好？构造注入好？
  - 能用设值注入就用设值注入
  - 当对构造有顺序，或者没有无参构造的时候就进行构造注入。

7.Spring自动注入

- ​	基于xml自动注入

  - byName
  - byType
  - construtcor(按类型)

- 基于注解

  - xml 

    - ``` 
      <context:component-scan base-package="com.kfm"></context:component-scan>
      ```

    - 类上配置注解

    - ```
       @Component, @Repository, @Service,
      @Controller, @RestController, @ControllerAdvice, and @Configuration
      ```

      ```
      @Resource与@Autowired区别？
      
      1、共同点
      
      两者都可以写在字段和setter方法上。两者如果都写在字段上，那么就不需要再写setter方法。
      
      @Autowired为Spring提供的注解
      @Autowired注解是按照类型（byType）装配依赖对象
      @Resource默认按照ByName自动注入，由J2EE提供
      @Resource有两个重要的属性：name和type，
      
      ```

Spring集成Jdbc的方案

​	JdbcTemplate

​	MybtisTemplate

​	hiberTemplate