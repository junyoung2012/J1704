# SpringDataJpa之研究

https://blog.csdn.net/qq_42495847/article/details/107991361

https://blog.csdn.net/tianya846/article/details/81053343?spm=1001.2101.3001.6650.16&utm_medium=distribute.pc_relevant.none-task-blog-2~default~CTRLIST~default-16.essearch_pc_relevant&depth_1-utm_source=distribute.pc_relevant.none-task-blog-2~default~CTRLIST~default-16.essearch_pc_relevant

1.hibernate

- hibernate是一个持久化框架
  - 它是ORM框架object rational mapping
    - 类对应于表
    - 属性对应字段
    - 每个对象被持久化位1行记录
- JPA是Java Persistence API官方规范
  - ![在这里插入图片描述](https://gitee.com/junyoung2012/blogimg/raw/master/20200813205741451.png)



- Spring Data Jpa是Spring的子框架，在hibenate的基础上做了进一步的封装，更加简化了ORM操作。

- hibernate中表是可以被hibernate自动创建的

  - 先类后表？先表后类？
    - 先类后表设计可以面向对象
    - 先类后表数据库变得透明了，业务系统变更数据库更加容易。

  https://blog.csdn.net/dtttyc/article/details/80001826

  Spring Data JPA

  - ​	是在hibernate基础上，不用配置sql
  - 在model注解配置ORM映射
    - @Entity
    - @Id
  - ![image-20211201105910061](https://gitee.com/junyoung2012/blogimg/raw/master/image-20211201105910061.png)

  普通CRUD

  分页

  排序

  通过合理方法规则，自动生成方法实现

  ![image-20211201110107965](https://gitee.com/junyoung2012/blogimg/raw/master/image-20211201110107965.png)

- @Query
  - 可以在方法上调用JPQL,面向对象
  - 可以直接使用sql

2.hibernate映射研究

- ​	关于对象如何持久化到数据库

  - 类如何标识
  - 数据库如何存储
  - hibernate如何映射

- 单表单类

  - @Entity

    - @Table(name = "T_Dept")

  - @Id

    - -AUTO主键由程序控制, 是默认选项 ,不设置就是这个

      -IDENTITY 主键由数据库生成, 采用数据库自增长, Oracle不支持这种方式

      -SEQUENCE 通过数据库的序列产生主键, MYSQL  不支持

      -Table 提供特定的数据库产生主键, 该方式更有利于数据库的移植

  -  @Column(name = "dloc",length = 40)

  - @Transient

  - @Temporal(TemporalType.DATE)

- 复合主键 (单表多类)

  - @IdClass(StudentPk.class)

- 组件映射

  - @Embedded  @Embeddable

- 集合映射@ElementCollection

  - set
  - list
  - map

- 组件集合映射

  - @Embeddable

- 多类关联

  - 一对多 @OneToMany(fetch = FetchType.LAZY,mappedBy = "dept")
  - 多对一 @ManyToOne
  - 一对一@OneToOne
  - 多对多 @ManyToMany

- 作业

  - 学生、课程、成绩
    - 存入3个学生
    - 出入3个课程
  - 总公司
    - 公司1
      - 分公司11
      - 分公司12
    - 公司2
    - 总经办

- 继承关系

  - 

