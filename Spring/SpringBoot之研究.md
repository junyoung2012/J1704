# SpringBoot之研究

1.SpringBoot是什么？

- ​	SpringBoot服务端控制器框架
- SpringBoot是SpringMVC的竞品。

2.SpringMVC的缺点？

- ​	配置复杂
- 依赖管理复杂。
- 部署也很麻烦
- 没有自我监控

3.SpringBoot的优点

	1. 独立运行的 Spring 项目
	2. 内嵌 Servlet 容器
	3. 提供 starter 简化 Maven 配置
	4. 提供了大量的自动配置
	5. 自带应用监控
	6. 无代码生成和 xml 配置

4.helloWorld

5.SpringBoot starter

6.SpringBoot的配置

- xml

- annotation

- java类（@Configuration）

- YML

  - YAML 全称 YAML Ain't Markup Language，它是一种以数据为中心的标记语言，比 XML 和 JSON 更适合作为配置文件。

    想要使用 YAML 作为属性配置文件（以 .yml 或 .yaml 结尾），

  YAML 的语法如下：

  - 使用缩进表示层级关系。
  - 缩进时不允许使用 Tab 键，只允许使用空格。
  - 缩进的空格数不重要，但同级元素必须左侧对齐。
  - 大小写敏感。

- YAML 支持以下三种数据结构：

  - 对象：键值对的集合

  - 数组：一组按次序排列的值

  - 字面量：单个的、不可拆分的值

  - ``` 
    在 YAML 中，使用“key:[空格]value”的形式表示一对键值对（空格不能省略），如 url: www.biancheng.net。
    ```

  - ## @Value 与 @ConfigurationProperties 对比

    @Value 和 @ConfigurationProperties 注解都能读取配置文件中的属性值并绑定到 JavaBean 中，但两者存在以下不同。

    #### 1. 使用位置不同

    - @ConfigurationProperties：标注在 JavaBean 的类名上；
    - @Value：标注在 JavaBean 的属性上。

    #### 2. 功能不同

    - @ConfigurationProperties：用于批量绑定配置文件中的配置；
    - @Value：只能一个一个的指定需要绑定的配置。

    #### 3. 松散绑定支持不同

    @ConfigurationProperties：支持松散绑定（松散语法），例如实体类 Person 中有一个属性为 lastName，那么配置文件中的属性名支持以下写法：

    - person.firstName
    - person.first-name
    - person.first_name
    - PERSON_FIRST_NAME

    
    @Vaule：不支持松散绑定。

    #### 4. SpEL 支持不同

    - @ConfigurationProperties：不支持 SpEL 表达式;
    - @Value：支持 SpEL 表达式。

    #### 5. 复杂类型封装

    - @ConfigurationProperties：支持所有类型数据的封装，例如 Map、List、Set、以及对象等；
    - @Value：只支持基本数据类型的封装，例如字符串、布尔值、整数等类型。

    #### 6. 应用场景不同

    @Value 和 @ConfigurationProperties 两个注解之间，并没有明显的优劣之分，它们只是适合的应用场景不同而已。

    - 若只是获取配置文件中的某项值，则推荐使用 @Value 注解；
    - 若专门编写了一个 JavaBean 来和配置文件进行映射，则建议使用 @ConfigurationProperties 注解。

7.SpringBoot配置文件加载顺序

Spring Boot 项目中可以存在多个 application.properties 或 apllication.yml。

Spring Boot 启动时会扫描以下 5 个位置的 application.properties 或 apllication.yml 文件，并将它们作为 Spring boot 的默认配置文件。

1. file:./config/
2. file:./config/*/
3. file:./
4. classpath:/config/
5. classpath:/

> 注：file: 指当前项目根目录；classpath: 指当前项目的类路径，即 resources 目录。

以上所有位置的配置文件都会被加载，且它们优先级依次降低，序号越小优先级越高。其次，位于相同位置的 application.properties 的优先级高于 application.yml。

所有位置的文件都会被加载，高优先级配置会覆盖低优先级配置，形成互补配置，即：

- 存在相同的配置内容时，高优先级的内容会覆盖低优先级的内容；
- 存在不同的配置内容时，高优先级和低优先级的配置内容取并集。

## Spring Boot 配置优先级

以下是常用的 Spring Boot 配置形式及其加载顺序（优先级由高到低）：

1. 命令行参数
2. 来自 java:comp/env 的 JNDI 属性
3. Java 系统属性（System.getProperties()）
4. 操作系统环境变量
5. RandomValuePropertySource 配置的 random.* 属性值
6. 配置文件（YAML 文件、Properties 文件）
7. @Configuration 注解类上的 @PropertySource 指定的配置文件
8. 通过 SpringApplication.setDefaultProperties 指定的默认属性

以上所有形式的配置都会被加载，当存在相同配置内容时，高优先级的配置会覆盖低优先级的配置；存在不同的配置内容时，高优先级和低优先级的配置内容取并集，共同生效，形成互补配置



![Spring Boot 配置文件加载顺序](https://gitee.com/junyoung2012/blogimg/raw/master/1544595010-1.png)

7.模板技术：

- ​	ThemeLeaf
  - 衍生在HTML当中Html标记伴随的表达式
- freeMark
  - 适用性比较广
  - 类似于El表达式
  - 利用该技术发动态生成word文件
    - 制作模板，ftl
    -  template.process(数据域, 输出流);
    - response.setContentType("application/msword");

业务层的变化：

- ​	在Service定义接口，AOP
- @Service

控制层变化：

​	Resrful+JSON的方式开发控制层

​	http://localhost/admin(POST)

http://localhost/admin/12   (DELETE)

http://localhost/admin (PUT)

http://localhost/admin (GET)

http://localhost/admin/12 (GET)

参数通过json上行

测试通过postman

8.拦截器

- ​	Spring控制器 类似于过滤器的技术。

9.全局异常处理（利用AOP）

​	@RestControllerAdvice

​	@ExceptionHandler(MethodArgumentNotValidException.class)

​	使用在model上进行注解做格式校验

@Valid和@Validated的总结区分？

​	来源不同：@Valid注解用于校验，所属包为：javax.validation.Valid。

​	@Validated是@Valid 的一次封装，是Spring提供的校验机制使用。

@Validated支持分组校验，@Valid不支持

10.接口发布工具Swagger

11.前后端分离登录验证的问题

- ​	现在的业务在多个域

- 以前的验证信息都存在服务端

- 现在新的思路，验证信息存在客户端

  - tocken(令牌)
    - 加密的字符串

- ## **token和jwt存在什么区别**？

  - token需要查库验证token 是否有效

  - 而JWT不用查库或者少查库，直接在服务端进行校验，并且不用查库

  - Token的认证流程： 

    ​    \1. 用户输入用户名和密码，发送给服务器。 

    ​    \2. 服务器验证用户名和密码，正确的话就返回一个签名过的token（token 可以认为就是个长长的字符串），浏览器客户端拿到这个token。 

    ​    \3. 后续每次请求中，浏览器会把token作为http header发送给服务器，服务器验证签名是否有效，如果有效那么认证就成功，可以返回客户端需要的数据。 特点： 这种方式的特点就是客户端的token中自己保留有大量信息，服务器没有存储这些信息。

    

    JWT 概念：

    ​       JWT是json web token缩写。它将用户信息加密到token里，服务器不保存任何用户信息。服务器通过使用保存的密钥验证token的正确性，只要正确即通过验证。 

    组成： 

    ​    WT包含三个部分： Header头部，Payload负载和Signature签名。由三部分生成token，三部分之间用“.”号做分割



## JWT

什么是JWT？
Json web token (JWT), 是为了在网络应用环境间传递声明而执行的一种基于JSON的开放标准（(RFC 7519).定义了一种简洁的，自包含的方法用于通信双方之间以JSON对象的形式安全的传递信息。因为数字签名的存在，这些信息是可信的，JWT可以使用HMAC算法或者是RSA的公私秘钥对进行签名。

### JWT请求流程

![img](https://gitee.com/junyoung2012/blogimg/raw/master/4630295-7df0d10fcffe831b.png)

**1. 用户使用账号和面发出post请求；**
**2. 服务器使用私钥创建一个jwt；**
**3. 服务器返回这个jwt给浏览器；**
**4. 浏览器将该jwt串在请求头中像服务器发送请求；**
**5. 服务器验证该jwt；**
**6. 返回响应的资源给浏览器。**



JWT的结构
JWT是由三段信息构成的，将这三段信息文本用.链接一起就构成了JWT字符串。
就像这样:

```
eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ
```

- JWT包含了三部分：
  Header 头部(标题包含了令牌的元数据，并且包含签名和/或加密算法的类型)
  Payload 负载 (类似于飞机上承载的物品)
  Signature 签名/签证

#### Header

**JWT的头部承载两部分信息：token类型和采用的加密算法。**

```
{ 
  "alg": "HS256",
   "typ": "JWT"
} 
```

声明类型:这里是jwt
声明加密的算法:通常直接使用 HMAC SHA256

加密算法是单向函数散列算法，常见的有MD5、SHA、HAMC。
MD5(message-digest algorithm 5) （信息-摘要算法）缩写，广泛用于加密和解密技术，常用于文件校验。校验？不管文件多大，经过MD5后都能生成唯一的MD5值
SHA (Secure Hash Algorithm，安全散列算法），数字签名等密码学应用中重要的工具，安全性高于MD5
HMAC (Hash Message Authentication Code)，散列消息鉴别码，基于密钥的Hash算法的认证协议。用公开函数和密钥产生一个固定长度的值作为认证标识，用这个标识鉴别消息的完整性。常用于接口签名验证

## Payload

载荷就是存放有效信息的地方。
有效信息包含三个部分
1.标准中注册的声明
2.公共的声明
3.私有的声明

标准中注册的声明 (建议但不强制使用) ：
iss: jwt签发者
sub: 面向的用户(jwt所面向的用户)
aud: 接收jwt的一方
exp: **过期时间**戳(jwt的过期时间，这个过期时间必须要大于签发时间)
nbf: 定义在什么时间之前，该jwt都是不可用的.
iat: jwt的签发时间
jti: **jwt的唯一身份标识**，主要用来作为一次性token,从而回避重放攻击。

公共的声明 ：
公共的声明可以添加任何的信息，一般添加用户的相关信息或其他业务需要的必要信息.但不建议添加敏感信息，因为该部分在客户端可解密.

私有的声明 ：
私有声明是提供者和消费者所共同定义的声明，一般不建议存放敏感信息，因为base64是对称解密的，意味着该部分信息可以归类为明文信息。

#### Signature

jwt的第三部分是一个签证信息，这个签证信息由三部分组成：
header (base64后的)
payload (base64后的)
secret
这个部分需要`base64`加密后的`header`和`base64`加密后的`payload`使用`.`连接组成的字符串，然后通过`header`中声明的加密方式进行加盐`secret`组合加密，然后就构成了`jwt`的第三部分。
密钥`secret`是保存在服务端的，服务端会根据这个密钥进行生成`token`和进行验证，所以需要保护好。

![image-20210211101506755](https://gitee.com/junyoung2012/blogimg/raw/master/1c6f04e22b30cb758a9805d5f9b5eab5.png)