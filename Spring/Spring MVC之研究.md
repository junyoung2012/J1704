Spring MVC之研究

1.什么是Spring MVC？

- ​	服务端框架
- Web MVC结构的控制器框架

2.有什么用？

- ​	请求路由
- 收参
- 转换数据类型
- 填充到model
- 验证
- 调用业务逻辑
- 像数据域设置属性
- 跳转页面

3.怎么使用？

- 引包
- 配置
  - web.xml
    - SpringMVC 核心Sevvlet
  - 核心的SpringMVC配置文件
    - 控制器所在的包
    - 视图解析器
    - 配置@Controller
- 常用类和接口

4.SpringMVC的工作原理

- ​	SpringMVC还是建立在Servlet基础上的

- web.xml配置核心Servlet

  - loadOnStartUp

    - 应程序启动会加载

    - 加载了SpringMVC的配置文件

    - 指明控制器包在那？

      - 通过反射，找到改包下所有类
      - 通过反射的Class对象，就可以知道有没有配置成@Controller
        - <bean id="testController" class="com.kfm.controller.TestController"/>
      -  得到所有的方法
        - 得到配置了@RequestMapping的方法
      - 记录所有（多个）的Method对象 Map<String,Mehtod>

    - 就是写一个通用的方法，完成，收参，转换数据类型，填充Model

      - request
        - getParmentMap
        - Model类型
          - 的得到参数名称
        - 既然得到控制器类方法需要填充的model类型
          - Model.**[newInstance](../../java/lang/Class.html#newInstance())**()
          - 参数名称获得setXXX方法
          - 需要获得字段的类型
            - 其它类型需要转换
          - setXXX方法填充

    - 通过Spring调用业务逻辑

    - 根据业务逻辑

      - 返回ModelAndView
        - View 
          - viewName
            - ![image-20211124091311440](https://gitee.com/junyoung2012/blogimg/raw/master/image-20211124091311440.png)

      

      - ​							得到model
        - model就是Attrbutes
          - request.setAttrbute
        - request.getRequestDispatcher(viewName).forward(req,resp);

5.restFul+JSON形式

- 不直接在URL地址传递参数
- 每个url代表一类资源
- 提交的method,来表示CRUD
  - ![img](https://gitee.com/junyoung2012/blogimg/raw/master/14699036-a4b9031041a5194c.png)
- 请求和响应数据，一般都使用json传递
- 接口测试工具
  - PostMan