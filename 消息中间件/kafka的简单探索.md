# kafka的简单探索

https://www.cnblogs.com/riches/p/11425044.html



1.简介

​	**定义：**kafka是一个**支持分区、多副本、基于zookeeper协调**的**分布式消息系统**

2.什么是zookeeper？

​	ZooKeeper 是 Apache 软件基金会的一个软件项目，它为大型分布式计算提供开源的分布式配置服务、同步服务和命名注册。

3.消息中间件的用途：解耦、削峰、异步、缓冲

4.使用场景：日志收集、消息系统、用户活动跟踪、运营指标分析等。

- 需要发邮件
- 发短信
- 主动通知性的工作
  - 登录在多个服务器之间我们是可以考虑，在退出登录时，利用消息队列，通知其它服务器



### 启动kafka

![img](D:\J1704\消息中间件\images\1743015-20211028132258846-1659510264.png)

```
##使用配置文件启动kafka
kafka-server-start.bat ..\..\config\server.properties
```



### 创建topic

　　再次进入kafka的bat脚本目录，输入cmd打开命令行窗口，输入以下命令，创建kafka的消息topics【topic名为zhTest】

```
##表示创建一个名为zhTest的Topic，分区为1，副本也为1
kafka-topics.bat --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic J1704
```

　　**replication-factor：**表示在不同的broker中保存几份副本。

　　**partitions：**表示分区数。设置为1表示该topic（同一个brocker上）存在有多少个分区。**PS：**每个分区的数据是不重复的，用于提高吞吐量。





### 启动消费者

　　再次进入kafka的bat脚本目录，输入cmd打开命令行窗口，输入以下命令，创建consumer： 

```
##此命令为修改命令行窗口的名称，此命令可不执行，直接执行下面的命令去启动消费者
title consumer
##--from-beginning参数表示从开头开始消费消息
kafka-console-consumer.bat --bootstrap-server localhost:9092 --topicJ 1704 --from-beginning
```