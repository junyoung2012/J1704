IO流

什么是IO流？

![image-20211007113032525](https://gitee.com/junyoung2012/blogimg/raw/master/image-20211007113032525.png)

​	IO流就是Java用来传输，操作数据的技术。形象把数据流比作像水流一样，好像是通过某种管道进行传输的。

IO流的分类？

- ​	方向分（基准是java程序）
  - 输入流  （读取） 数据源->程序
  - 输出流 （写出）程序->数据源
- 处理的宽度分
  - 字节流
  - 字符流（UTF-16）
- 按功能分
  - 节点流
  - 处理流

IO流的4个抽象基类

|        | 输入        | 输出         |
| ------ | ----------- | ------------ |
| 字节流 | InputStream | OutputStream |
| 字符流 | Reader      | Writer       |



输入字节流

​	1.read()

​		read(byte[] bytes)

​		read(byte[] bytes ,int off,in len)

​	2.skip(long)

​    3.mark,reset,markSupport


​	

输入的字符流

​	为什么要用字符流？

- 字符是人类能看懂的符号，需要人类看懂的数据才需要使用字符流
- 字符是有可能由多个字节构成的。
- 字符流是了解字节组成字符的规律的。

输出的字符流Reader

- write(5种)
- flush
- append(2种)

节点流和处理流

​	节点流。就是对应着资源的流

​	节点流

- 文件资源 File
- 内存资源 Char,Byte,String
- 进程间通讯 Piped
- 网络通讯 从套接字得到
- 键盘输入/控制台输出

处理流（外挂）装饰器模式

​	缓冲流（Buffered）：

- 带缓冲，（读写效率更高）

- BufferedReader  readLine()  读取一个文本行。

  

 转换流

打印流

数据流

对象流			

​	
