package com.kfm.test10081;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

public class FileCopy {
	//java com.kfm.test10081.FileCopy test1.txt test4.txt
	
	public static void main(String[] args) {
		if(args.length!=2) {
			System.out.println("请指定源文件和目标文件");
			return;
		}
		
		if(args[0]==null) {
			System.out.println("请指定源文件");
			return;
		}
		
		File src=new File(args[0]);
		if(!src.exists()) {
			System.out.println("源文件不存在");
			return;
		}
		if(args[1]==null) {
			System.out.println("请指定目标文件");
			return;
		}
		
		File dest=new File(args[1]);
		
		if(dest.getParentFile()!=null && !dest.getParentFile().exists()) {
			System.out.println(dest.getAbsolutePath());
			dest.getParentFile().mkdirs();
		}
		
		try(
			InputStream is=new FileInputStream(src);
			OutputStream os=new FileOutputStream(dest,false);
			){
			int i=0;
//			while((i=is.read())!=-1) {
//				os.write(i);
//			}
			byte[] buffer=new byte[1024];
			while((i=is.read(buffer))!=-1) {
				os.write(buffer,0,i);
				System.out.println(i);
//				os.write(buffer);
				os.flush();
			}
			os.close();;
			is.close();;
			System.out.println("复制成功");
			
		}catch (Exception e) {
			// TODO: handle exception
		}

	}

}
