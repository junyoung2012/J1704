package com.kfm.test10081;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class OutputStreamTest {

	public static void main(String[] args) throws IOException {
//		test1();
//		test2();
//		test3();
		test4();
	}

	private static void test1() throws IOException {
//		文件输出流，会自动创建文件，不能创建目录
		OutputStream outputStream=new FileOutputStream("resource/test2.txt");
		int i=97;
		outputStream.write(i);
		outputStream.close();
		System.out.println("写入完毕");
	}
	private static void test2() throws IOException {
//		文件输出流，会自动创建文件，不能创建目录
//		构造参数append,为true是追加模式，false是覆盖模式（默认）
		OutputStream outputStream=new FileOutputStream("resource/test2.txt",true);
		byte[] array ={99,100,101,102,103};
		outputStream.write(array);
		outputStream.close();
		System.out.println("写入完毕");
	}
	
	private static void test3() throws IOException {
//		文件输出流，会自动创建文件，不能创建目录
//		构造参数append,为true是追加模式，false是覆盖模式（默认）
		OutputStream outputStream=new FileOutputStream("resource/test2.txt",false);
		byte[] array ={99,100,101,102,103};
		outputStream.write(' ');
		outputStream.write(array,1,4);
		outputStream.flush();//把缓冲区内容写入资源
		outputStream.close();//close会flush,在close前应该强制flush，保护性写法
		System.out.println("写入完毕");
	}
	private static void test4() throws IOException {
		OutputStream outputStream=new FileOutputStream("resource/test2.txt",false);
		outputStream.write('缓');//2个字节，只写入了前半个，所以不能显示
		outputStream.flush();//把缓冲区内容写入资源
		outputStream.close();//close会flush,在close前应该强制flush，保护性写法
		System.out.println("写入完毕");
	}

}
