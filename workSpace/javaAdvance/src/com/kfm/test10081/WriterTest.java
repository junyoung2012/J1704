package com.kfm.test10081;

import static org.junit.Assert.*;

import java.io.FileWriter;
import java.io.Writer;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class WriterTest {

	
	@Test
	public void test() {
		try(Writer writer=new FileWriter("resource/test3.txt")) {
			writer.write('妙');
			writer.write(99);
			writer.write("人间清暑殿");
			writer.append("天上广寒宫");
			writer.flush();
			writer.close();
			System.out.println("写入完毕");
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

}
