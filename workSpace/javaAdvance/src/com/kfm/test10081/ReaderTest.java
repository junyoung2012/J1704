package com.kfm.test10081;

import static org.junit.Assert.*;

import java.io.FileReader;
import java.io.Reader;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
/**
 * 
 * @author Admin
 *
 */
public class ReaderTest {

	@Test
	public void test1() {
		String name="resource/test1.txt";
		try(Reader reader=new FileReader(name)) {
			int i=0;
			while((i=reader.read())!=-1) {
				System.out.println((char)i);
			}
			
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

}
