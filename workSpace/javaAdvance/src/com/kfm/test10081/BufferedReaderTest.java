package com.kfm.test10081;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class BufferedReaderTest {

	public static void main(String[] args) throws IOException {
		FileReader fileReader=new FileReader("resource/test4.txt");
		BufferedReader bufferedReader=new BufferedReader(fileReader);
		System.out.println(bufferedReader.readLine());
		System.out.println(bufferedReader.readLine());
		System.out.println(bufferedReader.readLine());
		System.out.println(bufferedReader.readLine());
		System.out.println(bufferedReader.readLine());
		bufferedReader.close();
	}

}
