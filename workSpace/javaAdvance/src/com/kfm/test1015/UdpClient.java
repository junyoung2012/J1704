package com.kfm.test1015;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class UdpClient {

	public static void main(String[] args) throws IOException {
//		一，调用DatagramSocket()创建一个数据报套接字；
		DatagramSocket datagramSocket=new DatagramSocket(); 
//		二，调用DatagramPacket(byte[] buf,int offset,InetAddress address,int port),建立要发送的UDP包
		Person person=new Person("文天祥", 25);
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		ObjectOutputStream objectOutputStream=new ObjectOutputStream(bos);
		objectOutputStream.writeObject(person);
		byte[] buf=bos.toByteArray();
		DatagramPacket datagramPacket=new DatagramPacket(buf, buf.length, 
				InetAddress.getByName("127.0.0.1"), 9999);
//		三，调用DatagramSocket类的send方法发送数据包；
		datagramSocket.send(datagramPacket);
		System.out.println("发送成功");
//		四，关闭数据报套接字。
		datagramSocket.close();
	}

}
