package com.kfm.test1015;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
/**
 * hash加密
 * 单向加密
 * @author Admin
 *
 */
public class MD5Utils {
    public static String stringToMD5(String plainText,String salt) {
        byte[] secretBytes = null;
        try {
            secretBytes = MessageDigest.getInstance("md5").digest(
                    (plainText+salt).getBytes());
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("没有这个md5算法！");
        }
        String md5code = new BigInteger(1, secretBytes).toString(16);
        for (int i = 0; i < 32 - md5code.length(); i++) {
            md5code = "0" + md5code;
        }
        return md5code;
    }
    
    public static void main(String[] args) {
		System.out.println(MD5Utils.stringToMD5("1234","13100010001"));
		System.out.println(MD5Utils.stringToMD5("1234","13100010002"));
//		System.out.println(MD5Utils.stringToMD5("1234"));
//		System.out.println(MD5Utils.stringToMD5("abcdefghijklmnabcdefghijklmnabcdefghijklmnabcdefghijklmnabcdefghijklmnabcdefghijklmnabcdefghijklmn"));
	}

}