package com.kfm.test1015;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class TcpClient {

	public static void main(String[] args) throws IOException {
//		Socket socket=new Socket("127.0.0.1", 9999);
		InetAddress address=InetAddress.getByName("127.0.0.1");
		Socket socket=new Socket(address, 9999);//连接服务端
		System.out.println("连接上服务端");
		DataInputStream dataInputStream=new DataInputStream(socket.getInputStream());
		System.out.println(dataInputStream.readUTF());
		DataOutputStream dataOutputStream=new DataOutputStream(socket.getOutputStream());
		dataOutputStream.writeUTF("你好");
		socket.close();
	}

}
