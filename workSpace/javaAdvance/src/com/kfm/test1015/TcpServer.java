package com.kfm.test1015;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class TcpServer {

	public static void main(String[] args) throws IOException {
		// 1.建立服务端的监听
		
		ServerSocket serverSocket=new ServerSocket(9999);
		System.out.println("服务端开始监听");
		Socket socket = serverSocket.accept();
		System.out.println("有客户端连接了");
		InputStream inputStream = socket.getInputStream();
		DataInputStream dataInputStream=new DataInputStream(inputStream);
		String input=dataInputStream.readUTF();
		System.out.println("收到客户端："+input);
		OutputStream outputStream = socket.getOutputStream();
		DataOutputStream dataOutputStream=new DataOutputStream(outputStream);
		dataOutputStream.writeUTF("收到客户端："+input);
		System.out.println("ok");
		serverSocket.close();

	}

}
