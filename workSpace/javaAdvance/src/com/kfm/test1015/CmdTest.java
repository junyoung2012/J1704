package com.kfm.test1015;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class CmdTest {

	public static void main(String[] args) throws IOException {
		Process process = Runtime.getRuntime().exec("cmd /c ping 127.0.0.1");
		BufferedReader bufferedReader1=new BufferedReader(
				new InputStreamReader(process.getInputStream()));
		String s;
		while((s=bufferedReader1.readLine())!=null) {
			System.out.println(s);
		}

	}

}
