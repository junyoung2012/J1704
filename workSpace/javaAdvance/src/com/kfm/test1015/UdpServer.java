package com.kfm.test1015;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

public class UdpServer {

	public static void main(String[] args) throws IOException, ClassNotFoundException {
//		一，调用DatagramSocket(int port)创建一个数据报套接字，绑定在指定端口上；
		DatagramSocket datagramSocket=new DatagramSocket(9999);
//		二，调用DatagramPacket(byte[] buf,int length),建立一个字节数组来接收UDP包；
		byte[] buf=new byte[1024];
		DatagramPacket datagramPacket=new DatagramPacket(buf, buf.length);
		
//		三，调用DatagramSocket.receive()；
		System.out.println("waiting ...");
		datagramSocket.receive(datagramPacket);
//		显示数据
		ByteArrayInputStream arrayInputStream=new ByteArrayInputStream(buf);
		ObjectInputStream objectInputStream=new ObjectInputStream(arrayInputStream);
		Person person=(Person) objectInputStream.readObject();
		System.out.println(person);
//		四，最后关闭数据报套接字。
		datagramSocket.close();
	}

}
