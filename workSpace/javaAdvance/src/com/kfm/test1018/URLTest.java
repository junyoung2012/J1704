package com.kfm.test1018;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

public class URLTest {

	public static void main(String[] args) throws IOException {
//		urlTest();
//		urlEncode();
		urlDecoder();
		
	}

	private static void urlDecoder() {
		String str="http%3A%2F%2Fwww.baidu.com%3Fq%3D%B7%E8%BF%F1java%BD%B2%D2%E5";
		System.out.println(URLDecoder.decode(str));
	}

	private static void urlEncode() {
		String urlStr="http://www.baidu.com?q=疯狂java讲义";
		System.out.println(URLEncoder.encode(urlStr));
	}

	private static void urlTest() throws MalformedURLException, IOException, ProtocolException {
		String urlStr="http://www.baidu.com";
		URL url=new URL(urlStr);
		
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		// 设置连接方式：get
		connection.setRequestMethod("GET"); //http下协议提交方式  get,post
		// 设置连接主机服务器的超时时间：15000毫秒
		connection.setConnectTimeout(5000);
		// 设置读取远程返回的数据时间：60000毫秒
		connection.setReadTimeout(6000);
		
		// 发送请求
		connection.connect();
		
		
		// 通过connection连接，获取输入流
//		200 成功的得到响应
		if (connection.getResponseCode() == 200) {
			InputStream inputStream = connection.getInputStream();
			BufferedReader bufferedReader = 
					new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8));
			StringBuilder sbf = new StringBuilder();
			String temp;
			while ((temp = bufferedReader.readLine()) != null) {
				sbf.append(temp);
				sbf.append(System.getProperty("line.separator"));
			}
			String result = sbf.toString();
			System.out.println(result);
		}
	}

}
