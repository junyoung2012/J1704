package com.kfm.test1007;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

public class InputStreamTest {

	public static void main(String[] args) throws IOException {
		test1();
//		test2();
//		test3();
//		test4();
//		test5();
	}

	private static void test4() {
		String name="resource/test1.txt";
		try(InputStream inputStream=new FileInputStream(name)) {
			byte[] buffer=new byte[5];
			
			System.out.println(inputStream.read(buffer,1,2));
			//后2个参数表示的是，读取的数据是在数组种的那个位置存，
//			而不是读取文件指定位置的数据！！！
			System.out.println(Arrays.toString(buffer));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
	private static void test5() {
		String name="resource/test1.txt";
		try(InputStream inputStream=new FileInputStream(name)) {
			System.out.println(inputStream.markSupported());//markSupported mark reset
			inputStream.skip(2);//跳过数据读取
			System.out.println((char)inputStream.read());
			System.out.println((char)inputStream.read());
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
	private static void test3() {
//		try-resource 异常处理方式 JDK7以后支持
		String name="resource/test1.txt";
		try(InputStream inputStream=new FileInputStream(name)) {
//			一次读取多个字节
			byte[] buffer=new byte[5];
//			按照数组大小批量读取
//			返回的数据表示的是成功读取的字节数
//			数组不会被自动清除内容，上次的数据还存在
//			数据读取完毕返回-1
			
			System.out.println(inputStream.read(buffer));
			System.out.println(Arrays.toString(buffer));
			
			System.out.println(inputStream.read(buffer));
			System.out.println(Arrays.toString(buffer));
			
			System.out.println(inputStream.read(buffer));
			System.out.println(Arrays.toString(buffer));
			
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}

	private static void test2() {
		String name="resource/test1.txt";
		InputStream inputStream=null;
		try {
			inputStream=new FileInputStream(name);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			try {
				inputStream.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	private static void test1() throws FileNotFoundException, IOException {
		File file=new File("resource/test1.txt");
		InputStream inputStream=new FileInputStream(file);
//		System.out.println((char)inputStream.read());
//		System.out.println((char)inputStream.read());
//		System.out.println((char)inputStream.read());
//		System.out.println((char)inputStream.read());
//		System.out.println((char)inputStream.read());
//		System.out.println((char)inputStream.read());
//		System.out.println((char)inputStream.read());
//		System.out.println((char)inputStream.read());
//		System.out.println(inputStream.read());
		int input;
//		0-255 读取的是一个字节
		while((input=inputStream.read())!=-1) {
			System.out.println((char)input);
		}
		inputStream.close();
	}
}
