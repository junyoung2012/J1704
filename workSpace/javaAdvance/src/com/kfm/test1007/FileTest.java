package com.kfm.test1007;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Arrays;

public class FileTest {

	public static void main(String[] args) throws IOException {
//		test1();
//		test2();
//		test3();
		test4();
	}
	private static void test4() throws IOException {
		String pathname="d:/aaa/test1";
		File file=new File(pathname);
		System.out.println(file.exists()?"存在":"不存在");
////		System.out.println(file.createNewFile());
////		System.out.println(file.exists()?"存在":"不存在");
//		File createTempFile = file.createTempFile("pre", ".sfx");
//		System.out.println(createTempFile.exists());
//		System.out.println(createTempFile.getAbsolutePath());
		System.out.println(file.setExecutable(true));
		System.out.println(file.setLastModified(0));
		System.out.println(file.setReadOnly());
		
		System.out.println(file.canExecute());
		System.out.println(file.canWrite());
		System.out.println(file.canRead());
		
		System.out.println(file.length());
	}
	/**
	 * 目录操作（查）
	 * @throws IOException 
	 */
	private static void test3() throws IOException {
		String pathname="d:/aaa";
		File file=new File(pathname);
		System.out.println(file.exists());
		System.out.println(file.getAbsolutePath());//d:\aaa
		System.out.println(file.getCanonicalPath());//D:\aaa
		System.out.println(file.getFreeSpace());//所在磁盘的剩余空间
		System.out.println(file.getName());//aaa
		System.out.println(file.getParent());//d:\
		System.out.println(file.getTotalSpace());//所在磁盘的容量
		
		System.out.println(file.isAbsolute());//
		System.out.println(file.isDirectory());//true是否目录
		System.out.println(file.isFile());//false是否文件
		System.out.println(file.isHidden());//false是否隐藏
		
		
		String[] list = file.list();
		System.out.println(Arrays.toString(list));
		File[] listFiles = file.listFiles();
		System.out.println(Arrays.toString(listFiles));
		
		File file2=new File("src/com/kfm/test1007");
		System.out.println(file2.getAbsolutePath());
		String[] list2 = file2.list(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				if(name.endsWith(".java")) return true;
				return false;
			}
		});
		System.out.println(Arrays.toString(list2));
	}
	/**
	 * 目录操作
	 */
	private static void test2() {
//		String pathname="d:"+File.separator+"test1.txt";
		String pathname="d:/aaaa";
		System.out.println(pathname);
		File file=new File(pathname);
		System.out.println(file.exists()?"存在":"不存在");
//		创建目录
//		System.out.println(file.mkdir());//创建时只能创建单级路径，父目录必须存在
//		System.out.println(file.mkdirs());
//		System.out.println(file.delete());//删除此抽象路径名表示的文件或目录。如果此路径名表示一个目录，则该目录必须为空才能删除。
		System.out.println(file.renameTo(new File("d:/aaa")));//修改名称
		System.out.println(file.exists()?"存在":"不存在");
//		file.deleteOnExit();//在虚拟机终止时，请求删除表示的文件或目录。文件（或目录）将以与注册相反的顺序删除。只有在虚拟机正常终止时，才会尝试执行删除操作
	}

	private static void test1() {
//		io=input output(输入，输出)
//		String pathname="d:\\test1.txt"; //只能在windows下使用（不用）
//		String pathname="d:/test1.txt";//兼容unix,linux，windows下使用
		String pathname="d:"+File.separator+"test1.txt"; //官方推荐，兼容各个操作系统
		File file=new File(pathname);//只代表文件和目录，不代表物理存在
		System.out.println(file.exists()?"存在":"不存在");
		System.out.println("ok");
	}

}
