package com.kfm.test1025;

public class TicketsTest {

	public static void main(String[] args) {
		Tickets tickets=new Tickets();
		new SaleMan("售票员1",tickets).start();
		new SaleMan("售票员2",tickets).start();
		new SaleMan("售票员3",tickets).start();
		new SaleMan("售票员4",tickets).start();
		new SaleMan("售票员5",tickets).start();
	}

}
class Tickets{
	private int i=8;

	public int sale() {
		System.out.println("销售了"+i);
		return i--;
	}
}

class SaleMan extends Thread{
	private String name; 
	private Tickets tickets;
	
	public SaleMan(String name, Tickets tickets) {
		this.name = name;
		this.tickets = tickets;
	}

	@Override
	public void run() {
		synchronized (tickets) {
			System.out.print(name+ " ");
			tickets.sale();
		}
	}
	
}
