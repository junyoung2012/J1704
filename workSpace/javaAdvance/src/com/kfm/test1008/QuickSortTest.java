package com.kfm.test1008;

import java.util.List;

public class QuickSortTest {
//	分治法 ， 贪心，动态规划，回溯，分支限界
	public static void main(String[] args) {
		// 一个基准数，经过一趟排序，一定要来到合适的位置，基准数为中心，左面小于，右面大于
		
//		右面是小于于基准数的无序数组 基准数      右面是大于基准数的无序数组
//		排序左面的数组   
//		排序右面的数组   
		
//		快、 时间复杂度O( nlong2n)
//		List<T> list;
		int[] array= {};
		int start;
		int end;
		quickSort(array,0,array.length-1);
	}

	private static void quickSort(int[] array, int start, int end) {
//		结束条件
		if(start>=end) return;
		
		// 把基准数放到合适的位置
		int mid=9;
		int i=start;//左哨兵
		int j=end;//右哨兵
		while(i<j) {	
		//	右哨兵扫描到第一不符序列
		//	左哨兵扫描到第一不符序列
//			交换左右哨兵的值
		}	
//		在此i一定等于j
//		交换基准数和i(j)的位置
		
//		排序左面的数组   
		quickSort(array,start,mid-1);
//		排序右面的数组   
		quickSort(array,mid+1,end);
	}

}
