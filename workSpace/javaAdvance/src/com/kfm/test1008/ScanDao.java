package com.kfm.test1008;

import java.io.File;
import java.util.List;

public class ScanDao {
	private FileItemDao itemDao=new FileItemDao();
//	1.扫描
	public void scanPath(File path ){
//		1.1 是个文件
//			添加集合
		itemDao.add(path);
//		1.1 是个目录
		File[] listFiles = path.listFiles();
		for (File file : listFiles) {
			scanPath(file);
		}
	}
//	2.记录
//	3 删除
	public boolean clearDuces(FileItem fileItem) {
		return false;
	}
//	批量删除
	public boolean clearDuces(List<FileItem> fileItems) {
		return false;
	}
}
