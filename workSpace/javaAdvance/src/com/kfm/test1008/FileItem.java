package com.kfm.test1008;
/**
 * 记录查重信息
 *
 *
 *
 *
 *    序号  文件名称              大小  重复次数
 *    1   test1.txt   2k    3
 *    
 *    
 *    输入序号，查看重复的详细信息
 *    选择性删除
 *    全部删除
 *    
 */

import java.io.File;
import java.util.List;
import java.util.Set;

public class FileItem {
	private String name;//文件名称
	private long length;//文件大小
	private List<File> files;//保存重复文件列表
}
