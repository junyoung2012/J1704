package com.kfm.test1011;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Arrays;

public class DataStreamTest {

	public static void main(String[] args) throws IOException {
//		OutputStream outputStream;
//		int i=1;
//		outputStream.write(i);
		ByteArrayOutputStream byteOutputStream=new ByteArrayOutputStream();
		DataOutputStream dataOutputStream=new DataOutputStream(byteOutputStream);
		dataOutputStream.writeInt(1);	
		dataOutputStream.writeLong(2);	
		byte[] bytes = byteOutputStream.toByteArray();
		dataOutputStream.flush();
		dataOutputStream.close();
		System.out.println(Arrays.toString(bytes));
		
//		ByteInputStream byteInputStream=new ByteInputStream(bytes, bytes.length);
		ByteArrayInputStream byteInputStream=new ByteArrayInputStream(bytes);
		DataInputStream dataInputStream=new DataInputStream(byteInputStream);
		System.out.println(dataInputStream.readInt());//1
		System.out.println(dataInputStream.readLong());//2
		dataInputStream.close();
	}

}
