package com.kfm.test1011;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Student implements Serializable/*,Externalizable*/ {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2L;
	private int code;
	private String name;
	private Date birthday;//transient透明，该字段不进行序列化操作
	private int score;
	private transient String password;//transient透明，该字段不进行序列化操作,部分控制序列化
	
//	构造方法
//	hashCode/equals
//	toString
//	getter/setter
	
	private final SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
	
	public Student() {
	}

	public Student(int code, String name, Date birthday, int score) {
		this.code = code;
		this.name = name;
		this.birthday = birthday;
		this.score = score;
	}
	
	public Student(String code, String name, String birthday, String score) {
		this.code = Integer.parseInt(code);
		this.name = name;
		try {
			this.birthday = dateFormat.parse(birthday);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.score =  Integer.parseInt(score);
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + code;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Student other = (Student) obj;
		if (code != other.code)
			return false;
		return true;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getCode() {
		return this.code;
	}
	
	public void setCode(int code) {
		this.code=code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public SimpleDateFormat getDateFormat() {
		return dateFormat;
	}

	@Override
	public String toString() {
		return "Student [code=" + code + ", name=" + name + ", birthday=" + birthday + ", score=" + score
				+ ", password=" + password + ", dateFormat=" + dateFormat + "]";
	}
//
//	@Override
//	public void writeExternal(ObjectOutput out) throws IOException {
//		out.writeInt(code);
//		
//	}
//
//	@Override
//	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
//		this.code=in.readInt();
//	}
}
 