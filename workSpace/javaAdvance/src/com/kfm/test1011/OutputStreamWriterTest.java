package com.kfm.test1011;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;

public class OutputStreamWriterTest {

	public static void main(String[] args) throws IOException {
		OutputStreamWriter writer=new OutputStreamWriter(new FileOutputStream("resource/utf-8.txt"), "utf-8");
		writer.write("心有灵犀一点通");
		writer.flush();
		writer.close();
		System.out.println("ok");
	}

}
