package com.kfm.test1011;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;

public class PringStreamTest {

	public static void main(String[] args) throws FileNotFoundException {
//		System.out
//		System.err
		
		PrintStream printStream=new PrintStream(new FileOutputStream("resource/test5.txt"));
		
//		System.out=printStream;
//		记录控制台对象
		PrintStream out =System.out;
		System.setOut(printStream);
		
		System.out.println("人生若只如初见，何事秋风悲画扇。");
		System.out.println("等闲变却故人心，却道故心人易变。");
		System.out.println("骊山语罢清宵半，泪雨霖铃终不怨。 ");
		System.out.println("何如薄幸锦衣郎，比翼连枝当日愿。");
		
		System.setOut(out);
		System.out.println("输出完毕");
	}

}
