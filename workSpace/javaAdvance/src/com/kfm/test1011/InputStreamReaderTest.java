package com.kfm.test1011;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class InputStreamReaderTest {

	public static void main(String[] args) throws IOException {
//		int input = System.in.read();
//		System.out.println((char)input);
		
//		InputStreamReader reader=new InputStreamReader(System.in,"UTF-8");
		InputStreamReader reader=new InputStreamReader(System.in,"GBK");//简体中文操作系统的默认字符集使用的默认字符集是GBK
//		InputStreamReader reader=new InputStreamReader(System.in,"BIG5");
//		System.out.println((char) reader.read());
//		System.out.println((char) reader.read());
//		
		BufferedReader br=new BufferedReader(reader);
		System.out.println(br.readLine());
		
		Scanner scanner=new Scanner(System.in);
		scanner.next();
	}

}
