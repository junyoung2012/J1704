package com.kfm.test1011;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
/**
 * 序列化，（串化），把对象转换成数据流，就是一个个字节。用来保存到磁盘，或者在网络上传输。
 * 反序列化，就是把字节流，转化成内存中的对象。可能来自于文件，也可能来自与网络。
 * 序列化接口：是一个标志性接口，表示该类的对象具有序列化和反序列化的能力。不实现改接口，在序列化过程中会产生java.io.NotSerializableException: 
 * @author Admin
 *
 */
public class ObjectStreamTest {

	public static void main(String[] args) throws IOException, ClassNotFoundException {
//		writeStudent();
		readStudent();
	}

	private static void readStudent() throws IOException, ClassNotFoundException {
		FileInputStream fileInputStream=new FileInputStream("resource/students.dat");
		ObjectInputStream objectInputStream=new ObjectInputStream(fileInputStream);
		Student student=(Student) objectInputStream.readObject();
//		List<Student> students=(List<Student>) objectInputStream.readObject();
		System.out.println(student);
	}

	private static void writeStudent() throws IOException {
		Student student=new Student(1001, "张三", new Date(), 66);
		student.setPassword("123456");
//		List<Student> students=new LinkedList<Student>();
//		students.add(student);
//		students.add(new Student(1002, "张三", new Date(), 66));
//		students.add(new Student(1003, "张三", new Date(), 66));
		FileOutputStream fileOutputStream=new FileOutputStream("resource/students.dat");
		ObjectOutputStream objectOutputStream=new ObjectOutputStream(fileOutputStream);
		objectOutputStream.writeObject(student);
//		objectOutputStream.writeObject(students);
		objectOutputStream.flush();
		objectOutputStream.close();
		System.out.println("写入ok");
	}

}
