package com.kfm.test1013;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ThreadTest8 {
	private  int i=1;
	static class MyRunner1 implements Runnable{
		private ThreadTest8 obj;
		private Lock lock=new ReentrantLock();//可重入锁
		

		public MyRunner1(ThreadTest8 obj, Lock lock) {
			this.obj = obj;
			this.lock = lock;
		}

		@Override
		public void run() {
//			if(!lock.tryLock()) {
//				System.out.println("我还没被阻塞");
//			}
			lock.lock();
//			System.out.println("我还没被阻塞");
			obj.i++;
			lock.lock();
			lock.unlock();
			try {
				Thread.sleep(1500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println(Thread.currentThread().getName()+":" + obj.i);
			lock.unlock();
		}
	}
	
	
	
	public static void main(String[] args) throws InterruptedException {
		ThreadTest8 test4=new ThreadTest8();
		Lock lock=new ReentrantLock();
		MyRunner1 target = new MyRunner1(test4,lock);
		new Thread(target).start();
		new Thread(target).start();
	}
}
