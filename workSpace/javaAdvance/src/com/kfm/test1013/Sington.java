package com.kfm.test1013;

public class Sington {
	private static Sington instance;
	private Sington() {
		
	}
	public synchronized static Sington getInstance() {
		if(instance==null) {
			instance=new Sington();
		}
		return instance;
	}
}
