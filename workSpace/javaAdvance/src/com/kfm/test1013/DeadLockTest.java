package com.kfm.test1013;

public class DeadLockTest {
	static class MyTheadA extends Thread{
		private Object a;
		private Object b;
		
		public MyTheadA(Object a, Object b) {
			this.a = a;
			this.b = b;
		}

		@Override
		public void run() {
			synchronized (a) {
				System.out.println("线程MyTheadA成功锁定资源a");
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				synchronized (b) {
					System.out.println("线程MyTheadA成功锁定资源b");
				}
			}
		}
	}
	static class MyTheadB extends Thread{
		private Object a;
		private Object b;
		
		public MyTheadB(Object a, Object b) {
			this.a = a;
			this.b = b;
		}
		
		@Override
		public void run() {
			synchronized (b) {
				System.out.println("线程MyTheadA成功锁定资源b");
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				synchronized (a) {
					System.out.println("线程MyTheadA成功锁定资源a");
				}
			}
		}
	}
	public static void main(String[] args) {
		Object a=new Object();
		Object b=new Object();
		new MyTheadA(a, b).start();
		new MyTheadB(a, b).start();
	}

}
