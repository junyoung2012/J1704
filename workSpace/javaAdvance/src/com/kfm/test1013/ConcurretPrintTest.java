package com.kfm.test1013;

import java.util.Random;

public class ConcurretPrintTest {
	static class ThreadA extends Thread {
		private Object flag;

		public ThreadA(Object flag) {
			this.flag = flag;
		}

		private String[] array = "1,2,3".split(",");

		@Override
		public void run() {
			synchronized (flag) {
				for (String string : array) {
					System.out.print(string + ",");
					try {
						Thread.sleep(new Random().nextInt(50));
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					// 通知线程B工作
//					flag.notify();// 随机唤醒对象监视器（阻塞的线程池）中的任意1个线程
//					System.out.println("ThreadA唤醒其它线程");
					 flag.notifyAll();//唤醒对象监视器（阻塞的线程池）中所有线程
					// 自己停止
					try {
						flag.wait();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
	}
	static class ThreadB extends Thread{
		private Object flag;
		private String[] array="a,b,c".split(",");
		
		public ThreadB(Object flag) {
			this.flag = flag;
		}

		@Override
		public void run() {
			synchronized(flag) {
				try {
//					System.out.println("ThreadB锁定");
					flag.wait();//让使用这个信号灯线程等待
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
//				System.out.println("ThreadB唤醒");
				for (String string : array) {
					System.out.print(string+",");
					try {
						Thread.sleep(new Random().nextInt(50));
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
//				通知线程A工作
//				自己停止
					flag.notify();
//				自己停止
					try {
						flag.wait();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
	}
	public static void main(String[] args) {
		Object xiHaoDeng=new Object();
		new ThreadB(xiHaoDeng).start();
		new ThreadA(xiHaoDeng).start();
	}
}
