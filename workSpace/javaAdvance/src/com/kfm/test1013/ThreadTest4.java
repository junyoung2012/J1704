package com.kfm.test1013;

public class ThreadTest4 {
	private  int i=1;
	static class MyRunner1 implements Runnable{
		private ThreadTest4 obj;
		
		public MyRunner1(ThreadTest4 obj) {
			this.obj = obj;
		}

		@Override
		public void run() {
			synchronized (obj) { //同步体里包含所有同步的代码,保证在同一范围只能有1个线程能在该区域运行
								//同步体线程运行到该区域，尝试加锁
//									如果没有锁，直接加锁
//									没有加锁成功，尝试加锁的线程，就进入阻塞状态
//									直到运行的线程离开同步体，阻塞才能解除，以前通过操作系统的核心态的锁机制
//								离开同步体，释放锁
//								synchronized是用来控制使用同步对象的线程。
				obj.i++;
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println("线程1:" + obj.i);
			}
		}
	}
	static class MyRunner2 implements Runnable{
		private ThreadTest4 obj;
		
		public MyRunner2(ThreadTest4 obj) {
			this.obj = obj;
		}
		
		@Override
		public  void run() {
			synchronized (obj) { //同步体里包含所有同步的代码
				obj.i++;
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println("线程2:" + obj.i);
			}
		}
	}
	
	public static void main(String[] args) throws InterruptedException {
		ThreadTest4 test4=new ThreadTest4();
		new Thread(new MyRunner1(test4)).start();
		new Thread(new MyRunner2(test4)).start();
	}
}
