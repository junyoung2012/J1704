package com.kfm.test1013;

public class ThreadTest6 {
	private static int i=1;
	
	public static void main(String[] args) throws InterruptedException {
		Thread thread1=new Thread(()->{
			synchronized (ThreadTest6.class) {
				i++;
				try {
					Thread.sleep(5);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println("线程1:"+i);
			}
			
		}) ; 
		Thread thread2=new Thread(()->{
			synchronized (ThreadTest6.class) {
				i++;
				try {
					Thread.sleep(5);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println("线程2:"+i);
			}
		}) ; 
		thread1.start();
		Thread.sleep(15);
		thread2.start();
		test1();
	}

	private synchronized  static void test1() {
//		synchronized(ThreadTest6.class) {
			i++;
			try {
				Thread.sleep(5);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("线程1:"+i);
//		}
	}
	private synchronized  static void test2() {
//		synchronized(ThreadTest6.class) {
		i++;
		try {
			Thread.sleep(5);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("线程2:"+i);
//		}
	}
}
