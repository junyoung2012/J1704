package com.kfm.test1013;

public class DeadLockTest2 {
	static class ObjectC{
		private Object a;
		private Object b;
		public ObjectC(Object a, Object b) {
			this.a = a;
			this.b = b;
		}
		public Object getA() {
			return a;
		}
		public void setA(Object a) {
			this.a = a;
		}
		public Object getB() {
			return b;
		}
		public void setB(Object b) {
			this.b = b;
		}
		
	}
	static class MyTheadA extends Thread{
		private ObjectC c;
		
		

		public MyTheadA(ObjectC c) {
			this.c = c;
		}



		@Override
		public void run() {
			synchronized (c) {
				System.out.println("线程MyTheadA成功锁定资源a");
				System.out.println("线程MyTheadA成功锁定资源b");
			}
		}
	}
	static class MyTheadB extends Thread{
		private Object a;
		private Object b;
		
		public MyTheadB(Object a, Object b) {
			this.a = a;
			this.b = b;
		}
		
		@Override
		public void run() {
			synchronized (b) {
				System.out.println("线程MyTheadA成功锁定资源b");
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				synchronized (a) {
					System.out.println("线程MyTheadA成功锁定资源a");
				}
			}
		}
	}
	public static void main(String[] args) {
		Object a=new Object();
		Object b=new Object();
//		new MyTheadA(a, b).start();
//		new MyTheadB(a, b).start();
	}

}
