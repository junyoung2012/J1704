package com.kfm.test1013;

public class ThreadTest5 {
	private  int i=1;
	static class MyRunner1 implements Runnable{
		private ThreadTest5 obj;
		
		public MyRunner1(ThreadTest5 obj) {
			this.obj = obj;
		}

		@Override
		public synchronized void run() {
			obj.i++;
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println(Thread.currentThread().getName()+":" + obj.i);
		}
	}
	
	
	
	public static void main(String[] args) throws InterruptedException {
		ThreadTest5 test4=new ThreadTest5();
		MyRunner1 target = new MyRunner1(test4);
		new Thread(target).start();
		new Thread(target).start();
	}
}
