package com.kfm.test10131;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.concurrent.CountDownLatch;

public class FileCopyStudy {
	private static final int threadCount=5;
	private static int endFlag=threadCount;
	
	static class CopyFile implements Runnable{
		private RandomAccessFile srcAccessFile;
		private RandomAccessFile destAccessFile;
		private CountDownLatch countDownLatch;
		
		private long start;
		private long end;
		
		public CopyFile(RandomAccessFile srcAccessFile, RandomAccessFile destAccessFile, long start, long end) {
			this.srcAccessFile = srcAccessFile;
			this.destAccessFile = destAccessFile;
			this.start = start;
			this.end = end;
		}

		public CopyFile(RandomAccessFile srcAccessFile, RandomAccessFile destAccessFile, CountDownLatch countDownLatch,
				long start, long end) {
			this.srcAccessFile = srcAccessFile;
			this.destAccessFile = destAccessFile;
			this.countDownLatch = countDownLatch;
			this.start = start;
			this.end = end;
		}

		@Override
		public void run() {
			byte[] buffer=new byte[1024];
			try {
				//移动到合理的文件指针
				srcAccessFile.seek(start);
				destAccessFile.seek(start);
				int length;
				while ((length = srcAccessFile.read(buffer)) != -1) {
					destAccessFile.write(buffer, 0, length);
					if (destAccessFile.getFilePointer() > end)
						return;
				} 
//				endFlag--;
				countDownLatch.countDown();//向下计数
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
	}
	public static void main(String[] args) throws IOException, InterruptedException {
		copyFileBySingelByte();
		copyFileByBuffer(12);
		copyFileByMultiThread();
	}

	private static long copyFileByMultiThread() throws IOException, InterruptedException {
		long  begin=System.currentTimeMillis();
//		begin=System.nanoTime()
		// 通过多个线程分割整个拷贝任务
		File src=new File("");
		RandomAccessFile srcAccessFile =new RandomAccessFile(src, "r");
		File dest=new File("");
		RandomAccessFile destAccessFile =new RandomAccessFile(dest, "rw");
		long length = srcAccessFile.length();
		destAccessFile.setLength(length);
		
		//创建线程完成copy
		
		CountDownLatch countDownLatch=new CountDownLatch(threadCount);
		
		long copyFileSize=length/threadCount;
		Thread[] threads=new Thread[threadCount];
		for (int i = 0; i < threadCount; i++) {
			CopyFile copyFile;
			if(i == threadCount-1) {
				copyFile=new CopyFile(srcAccessFile, destAccessFile, 
						countDownLatch,(threadCount-1)*copyFileSize,length );
			}else {
				copyFile=new CopyFile(srcAccessFile, destAccessFile,
						countDownLatch, i*copyFileSize, (i+1)*copyFileSize);
			}
			new Thread(copyFile).start();
		}
		
		srcAccessFile.close();
		destAccessFile.close();
//		while(checkState(threads)) {
//		}
//		while(endFlag>0);
		countDownLatch.await();;
		long end=System.currentTimeMillis();
		
		return end-begin;
	}

	private static boolean checkState(Thread[] threads) {
		for (int i = 0; i < threads.length; i++) {
			if(threads[i].getState()==Thread.State.RUNNABLE) {
				return true;
			}
		}
		return false;
	}

	private static void copyFileByBuffer(long bufferSize) {
//		读取到字符的个数
//		byte[] buffer=new byte[bufferSize];
//		read(buffer)
//		InputStream 
//		BufferedInputStream bufferedInputStream=new BufferedInputStream(in, size)
		
	}

	private static void copyFileBySingelByte() {
		// 输入流
//		输出流
//		循环
		
	}

}
