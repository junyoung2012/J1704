package com.kfm.test1014;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Properties;

import org.junit.internal.runners.TestMethod;

public class ReflectTest {

	public static void main(String[] args) throws Exception {
//		testGetClass();
//		testGetInstance();
//		testPlay();
//		testConstruct();
//		testMethod();
		testFileld();
	}
	private static void testFileld() throws Exception {
		String className="com.kfm.test1014.Person";
		Class klazz=Class.forName(className);
		Field[] fields = klazz.getDeclaredFields();
		for (Field field : fields) {
//			System.out.println(field);
		}
		Field field=klazz.getDeclaredField("name");
		System.out.println(field);
		Person person=(Person) klazz.newInstance();
		System.out.println(person);
		field.setAccessible(true);
		field.set(person, "李四");
		System.out.println(person);
	}
	private static void testMethod() throws ClassNotFoundException, NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		String className="com.kfm.test1014.Person";
		Class klazz=Class.forName(className);
//		Method[] methods = klazz.getMethods();
		Method[] methods = klazz.getDeclaredMethods();
		for (Method method : methods) {
//			System.out.println(method);
		}
		Method method=klazz.getDeclaredMethod("setAge", Integer.TYPE);
		System.out.println(method);
		Person person=(Person) klazz.newInstance();
		System.out.println(person);
		method.setAccessible(true);//通过反射技术，改变权限修饰
		method.invoke(person, 25);
		System.out.println(person);
		method=klazz.getDeclaredMethod("getAge");
		Object result = method.invoke(person);
		System.out.println(result);//也能接收到返回值
	}
	
	private static void testConstruct() throws ClassNotFoundException, NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		String className="com.kfm.test1014.Person";
		Class klazz=Class.forName(className);
//		Constructor[] constructors = klazz.getConstructors();//只能得到public
		Constructor[] constructors = klazz.getDeclaredConstructors();//得到所有的
		for (Constructor constructor : constructors) {
//			System.out.println(constructor);
		}
		Constructor constructor=klazz.getConstructor(String.class,Integer.TYPE);
		System.out.println(constructor);
		Person person=(Person) constructor.newInstance("张三",22);
		System.out.println(person);
		
	}
	static void  play(BasketBall ball) {
		ball.play();
	}
	static void  play(Playable playable) {
		playable.play();
	}
	private static void testPlay() throws InstantiationException, IllegalAccessException, ClassNotFoundException, FileNotFoundException, IOException {
		
//		BasketBall basketBall=new BasketBall();
//		play(basketBall);
//		FootBall footBall=new FootBall();
		Properties properties=new Properties();
		properties.load(new FileReader("resource/play.properties"));
		String className=properties.getProperty("playTarget");
		Playable playable=(Playable) Class.forName(className).newInstance();
		play(playable);//增加了系统的弹性，消除了系统的耦合性
	}

	private static void testGetInstance() throws ClassNotFoundException, InstantiationException, IllegalAccessException {
		// new，以下是不使用new创建对象的方式
//		常量（String ,包装类）
//		枚举
//		工厂方法
//		clone
//		反序列化
//		反射
		
//		String className="java.lang.String";
//		Class klazz=Class.forName(className);
//		String str=(String) klazz.newInstance();
//		System.out.println(str);
//		
		String className="com.kfm.test1014.Person";
		Class klazz=Class.forName(className);
		Person person=(Person) klazz.newInstance();//本质上需要使用无参构造
		System.out.println(person);
		
	}

	private static void testGetClass() throws ClassNotFoundException {
		Class class1=String.class;//类的class属性
		Class class2="abc".getClass();//对象的getClass（）方法
//		Integer
		Class class12=Integer.class;
//		int 
		Class class3=Integer.TYPE;//在反射中，包装类的Type属性，表示的是基本数据类型
		
		
		String className="java.lang.String";
		Class class4=Class.forName(className);
		System.out.println(class4);
		
	}

}
interface Playable{
	void play();
}
class BasketBall implements Playable{

	@Override
	public void play() {
		System.out.println("打篮球");
	}
	
}

class FootBall implements Playable{
	@Override
	public void play() {
		System.out.println("踢足球");
	}
}
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@interface Testable{
	
}

