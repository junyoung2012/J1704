package com.kfm.test1014;

public class ProductAndConstmerTest {
	static class Cake{
		private int index;

		public Cake(int index) {
			this.index = index;
		}

		@Override
		public String toString() {
			return "Cake [index=" + index + "]";
		}
		
	}
	static class CakeShop{
		final int COUNT=5;
		private Cake[] cakes=new Cake[COUNT];
		private int index;
		public synchronized void push(Cake cake) throws InterruptedException {
			System.out.println("生产蛋糕"+cake);
			if(index>=5) {
				this.wait();//蛋糕满了，停止生产
			}
			cakes[index++]=cake;
			this.notifyAll();//通知消费者，可以开始消费了
		}
//		Illegal Monitor State Exception
		public synchronized Cake pop() throws InterruptedException {
			if(index==0) {
				this.wait();//蛋糕卖空了，停止销售
			}
			Cake cake = cakes[--index];
			System.out.println("销售蛋糕"+cake);
			this.notifyAll();//通知生产者，可以开始生产了
			return cake;
		}
		
	}
	static class Producter extends Thread{
		private CakeShop cakeShop;
		
		public Producter(CakeShop cakeShop) {
			this.cakeShop = cakeShop;
		}

		@Override
		public void run() {
			for (int i = 0; i < 10; i++) {
				Cake cake=new Cake(i);
				try {
					cakeShop.push(cake);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
	}
	static class Constomer extends Thread{
		private CakeShop cakeShop;
		
		public Constomer(CakeShop cakeShop) {
			this.cakeShop = cakeShop;
		}

		@Override
		public void run() {
			for (int i = 0; i < 10; i++) {
				try {
					cakeShop.pop();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
	}
	public static void main(String[] args) {
		CakeShop cakeShop=new CakeShop();
		new Producter(cakeShop).start();
		new Constomer(cakeShop).start();
	}

}
