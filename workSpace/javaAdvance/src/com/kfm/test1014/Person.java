package com.kfm.test1014;
@Testable
public class Person {
	private String name;
	private int age;
	
	public Person() {
	}

	private Person(String name) {
		this.name = name;
	}

	public Person(String name, int age) {
		this.name = name;
		this.age = age;
	}

	@Override
	public String toString() {
		Testable testable = this.getClass().getAnnotation(Testable.class);
//		System.out.println(annotation);
		if(testable!=null) {
			System.out.println("我是一个可以测试的类");
		}
		return "Person [name=" + name + ", age=" + age + "]";
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	private void setAge(int age) {
		this.age = age;
	}
	
}
