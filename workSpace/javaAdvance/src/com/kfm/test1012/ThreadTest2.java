package com.kfm.test1012;

public class ThreadTest2 {

	public static void main(String[] args) throws InterruptedException {
//		test1();
//		test2();
//		testJoin();
//		testStopThread();
		testDeamon();
	}
	private static void testDeamon() {
		Thread thread2=new Thread(()->{
			while(true);
		});
		thread2.setDaemon(true);//守护线程
		thread2.start();
		for (int i = 0; i < 100; i++) {
			System.out.println("主线程:"+i);
		}	
//		System.exit(0);
	}
	static class MyRunner implements Runnable   {
		private boolean executeFlag=true;
		
		@Override
		public void run() {
			for (int i = 0; i < 1000; i++) {
				System.out.println(Thread.currentThread().getName()+":"+i);
//				if(!executeFlag) return;
				if(Thread.currentThread().isInterrupted()) {
					System.out.println("綫程關閉");
					return;
				}
			}
		}

		public void setExecuteFlag(boolean executeFlag) {
			this.executeFlag = executeFlag;
		}
	}
	
	private static void testStopThread() {
		MyRunner runner = new MyRunner();
		Thread thread2=new Thread(runner);
		thread2.start();
		for (int i = 0; i < 1000; i++) {
			System.out.println("主线程:"+i);
//			if(i==500) thread2.stop();//
//			if(i==500) thread2.suspend();//挂起挂起(暂停)
//			if(i==5) runner.setExecuteFlag(false);//
			if(i==5) thread2.interrupt();;//
		}
		
	}

	private static void testJoin() throws InterruptedException {
		Thread thread2=new Thread(()->{
			for (int i = 0; i < 1000; i++) {
				System.out.println(Thread.currentThread().getName()+":"+i);
			}
		});
		thread2.start();
		for (int i = 0; i < 1000; i++) {
			System.out.println("主线程:"+i);
			if(i==5) thread2.join();//合并线程到到当前线程
		}
	}

	private static void test2() {
		Thread thread2=new Thread(()->{
			for (int i = 0; i < 1000; i++) {
				System.out.println(Thread.currentThread().getName()+":"+i);
				if(i%100==0) Thread.currentThread().yield();//放弃运行
			}
		});
		thread2.start();
		for (int i = 0; i < 1000; i++) {
			System.out.println("主线程:"+i);
		}
	}

	private static void test1() {
		Runnable runner1=()->{
			for (int i = 0; i < 2; i++) {
				System.out.println(Thread.currentThread().getName()+":"+i);
			}
		};
		Runnable runner2=()->{
			for (int i = 0; i < 2; i++) {
				System.out.println(Thread.currentThread().getName()+":"+i);
			}
		};
		Thread thread1=new Thread(runner1);
		System.out.println(thread1.getState());//NEW
		thread1.setName("线程1");
		Thread thread2=new Thread(runner2);
		thread1.setPriority(Thread.NORM_PRIORITY);//级别1-10级，10级最高
		thread2.setPriority(Thread.MAX_PRIORITY);//级别1-10级，10级最高
		thread1.start();
		System.out.println(thread1.getState());//RUNNABLE
		thread1.stop();
		thread2.start();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(thread1.getState());//TERMINATED
	}

}
