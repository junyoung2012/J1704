package com.kfm.test1012;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;


public class TimeTaskTest {
	public static void main(String[] args) {
		TimerTask task=new TimerTask() {
			@Override
			public void run() {
				System.out.println(new Date());
				
			}
		};
		System.out.println(new Date());
		Timer timer=new Timer();
		Date date = new Date();
//		timer.schedule(task, new Date(date.getTime()+5000));//5秒后执行一次
//		timer.schedule(task,5000);//5秒后执行一次
		timer.scheduleAtFixedRate(task, 3000, 1000);//3秒后,每秒执行一次重复
	}
}
