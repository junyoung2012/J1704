package com.kfm.test1012;
//juc=java的并行开发包
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

public class ThreadTest {
	static class MyThread extends Thread{

		@Override
		public void run() {//线程体
//			另外的线程，需要执行的代码
			for (int i = 0; i < 1000; i++) {
				System.out.println("副线程："+i);
			}
		}
	}
	static class MyRun implements Runnable{
		@Override
		public void run() {
			for (int i = 0; i < 1000; i++) {
				System.out.println("实现Runable的副线程："+i);
			}
		}
	}
	
	static class MyCall implements Callable<Integer>{
		@Override
		public Integer call() throws Exception {
			for (int i = 0; i < 1000; i++) {
				System.out.println("实现Callable的副线程："+i);
			}
			return 666;
		}
	}
	public static void main(String[] args) throws InterruptedException, ExecutionException {
//		testMythread();
//		testMyRun();
//		testMyRunLambda();
		FutureTask<Integer> futureTask = testMyCall();
//		testMyCallLambda();
		for (int i = 0; i < 1000; i++) {
			System.out.println("主线程："+i);
		}
		System.out.println(futureTask.get());
	}
	private static FutureTask<Integer> testMyCall() throws InterruptedException, ExecutionException {
		MyCall myCall=new MyCall();
		FutureTask<Integer> futureTask=new FutureTask<>(myCall);
		new Thread(futureTask).start();
//		while(!futureTask.isDone());
//		System.out.println(futureTask.get());
		return futureTask;
	}
	private static void testMyCallLambda() throws InterruptedException, ExecutionException {
		FutureTask<Integer> futureTask=new FutureTask<Integer>(()-> {
			for (int i = 0; i < 1000; i++) {
				System.out.println("实现CallableLambda的副线程："+i);
			}
			return 888;
		});
		new Thread(futureTask).start();
	//		while(!futureTask.isDone());
		System.out.println(futureTask.get());
	}
	private static void testMyRun() {
		MyRun myRun=new MyRun();
//		myRun.run();	
		new Thread(myRun).start();
	}
	private static void testMyRunLambda() {
		new Thread(()->{
			for (int i = 0; i < 1000; i++) {
				System.out.println("实现Runable的副线程（lambda）："+i);
			}
		}).start();
		
	}
	private static void testMythread() {
		//启动通过继承Thread实现的线程
		MyThread myThread=new MyThread();
//		myThread.run();//也能执行，但不是并行执行，是顺序执行，所以不是多线程效果
		myThread.start();//
	}

}
