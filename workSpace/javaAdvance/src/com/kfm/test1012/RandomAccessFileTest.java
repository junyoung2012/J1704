package com.kfm.test1012;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

public class RandomAccessFileTest {

	public static void main(String[] args) throws IOException {
//		test1();
		test2();
		
	}

	private static void test2() throws IOException {
		String file="resource/test6.txt";
		RandomAccessFile randomAccessFile=new RandomAccessFile(file, "rw");
		randomAccessFile.setLength(10);
		System.out.println(randomAccessFile.getFilePointer());
		randomAccessFile.seek(1);
		System.out.println(randomAccessFile.getFilePointer());
		randomAccessFile.writeInt(1);
		System.out.println(randomAccessFile.getFilePointer());
		randomAccessFile.write(97);
		System.out.println(randomAccessFile.getFilePointer());
		randomAccessFile.seek(6);
		System.out.println(randomAccessFile.getFilePointer());
		randomAccessFile.writeInt(1);
		System.out.println(randomAccessFile.getFilePointer());
		randomAccessFile.close();
		System.out.println("ok");
	}

	private static void test1() throws FileNotFoundException, IOException {
		String file="resource/test1.txt";
		RandomAccessFile randomAccessFile=new RandomAccessFile(file, "r");
		randomAccessFile.seek(2);
		System.out.println((char)randomAccessFile.read());
		randomAccessFile.close();
	}
}
