package com.kfm.util;

import java.sql.ResultSet;

public interface RowMapper<T> {
	T row2Object(ResultSet rs);
}
