package com.kfm.util;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

public class JdbcUtil<T> {
	private static Connection connection;
	static {
		try {
			String diverClassName="oracle.jdbc.driver.OracleDriver";
			Class.forName(diverClassName);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	private Connection getConnection()  {
		try {
			if(connection==null || connection.isClosed()) {
				String url="jdbc:oracle:thin:@localhost:1521:xe";
				Properties info=new Properties();
				info.load(new FileReader("db.properties"));
				connection=DriverManager.getConnection(url, info);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return connection;
	}
	
	public int executeUpdate(String sql,Object...params){
		try {
			PreparedStatement statement = getConnection().prepareStatement(sql);
			for (int i = 0; i < params.length; i++) {
				statement.setObject(i+1, params[i]);
			}
			int result = statement.executeUpdate();
			statement.close();
			return result;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return -1;
	}
	
	public List<T> findList(String sql,RowMapper<T> rowMapper, Object...params){
		try(PreparedStatement statement = getConnection().prepareStatement(sql);) {
			for (int i = 0; i < params.length; i++) {
				statement.setObject(i+1, params[i]);
			}
			ResultSet rs = statement.executeQuery();
			List<T> list=new LinkedList<T>();
			while (rs.next()) {
				list.add(rowMapper.row2Object(rs));
			}
			return list;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	public T findOne(String sql,RowMapper<T> rowMapper,Object...params){
		try(PreparedStatement statement = getConnection().prepareStatement(sql);) {
			for (int i = 0; i < params.length; i++) {
				statement.setObject(i+1, params[i]);
			}
			ResultSet rs = statement.executeQuery();
			if(rs.next()) {
				return rowMapper.row2Object(rs);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	public T findOne(String sql,Class<T> klazz,Object...params){
		try(PreparedStatement statement = getConnection().prepareStatement(sql);) {
			for (int i = 0; i < params.length; i++) {
				statement.setObject(i+1, params[i]);
			}
			ResultSet rs = statement.executeQuery();
			if(rs.next()) {
				ResultSetMetaData metaData = rs.getMetaData();
				int columnCount = metaData.getColumnCount();
				T result=klazz.newInstance();
				for (int i = 1; i <= columnCount; i++) {
					String columnName = metaData.getColumnName(i).toLowerCase();
					Field field = klazz.getDeclaredField(columnName);
					field.setAccessible(true);
					System.out.println(metaData.getColumnTypeName(i));
					System.out.println(metaData.getColumnType(i));
					switch (metaData.getColumnType(i)) {
						case java.sql.Types.NUMERIC:
							field.set(result, rs.getInt(i));
							break;
						case java.sql.Types.VARCHAR:
							field.set(result, rs.getString(i));
							break;
						case java.sql.Types.DATE:
							field.set(result, rs.getDate(i));
							break;
						default:
							field.set(result, rs.getObject(i));
							break;
					}
					
				}
				return result;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (NoSuchFieldException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
