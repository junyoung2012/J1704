package com.kfm.dao;

import java.util.List;

import com.kfm.model.Student;

public interface StudentDao {
	boolean add(Student student);
	boolean delete(int id);
	boolean update(Student student);
	Student get(int id);
	List<Student> getAll();
}
