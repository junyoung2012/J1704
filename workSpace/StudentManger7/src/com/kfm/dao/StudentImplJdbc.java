package com.kfm.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import com.kfm.model.Student;
import com.kfm.util.JdbcUtil;
import com.kfm.util.RowMapper;
/**
 * �����ϵӳ�䣨ORM��
 * @author Admin
 *
 */
public class StudentImplJdbc implements StudentDao,RowMapper<Student> {
	private JdbcUtil<Student> jdbcUtil =new JdbcUtil<>();
	
	@Override
	public boolean add(Student student) {
		String sql="insert into student"
				+ " (id, name, bitthday)"
				+ " values"
				+ " (?,?,?)";
		return jdbcUtil.executeUpdate(sql,
				student.getId(), 
				student.getName(),
				student.getBitthday()
		)==1;		
	}

	@Override
	public boolean delete(int id) {
		String sql="delete student"
				+ " where id = ?";
		return jdbcUtil.executeUpdate(sql,id)==1;		
	}

	@Override
	public boolean update(Student student) {
		String sql="update student"
				+ " set name = ?,"
				+ " bitthday = ?"
				+ " where id = ?";
		return jdbcUtil.executeUpdate(sql,
				student.getName(),
				student.getBitthday(),
				student.getId()
		)==1;		
	}

//	@Override
//	public Student get(int id) {
//		String sql="select id, name, bitthday from student"
//				+ " where id = ?";
//		return jdbcUtil.findOne(sql, this, id);
////		return jdbcUtil.findOne(sql, new RowMapper<Student>() {
////			
////			@Override
////			public Student row2Object(ResultSet rs) {
////				try {
////					return new Student(
////							rs.getInt("id"),
////							rs.getString("name"),
////							rs.getDate("bitthday")
////							);
////				} catch (SQLException e) {
////					e.printStackTrace();
////				}
////				return null;
////			}
////		}, id);
//	}

	@Override
	public List<Student> getAll() {
		String sql="select id, name, bitthday from student"
				+ " order by id";
		return jdbcUtil.findList(sql, this);
//		return jdbcUtil.findList(sql, (rs)->{
//			try {
//				return new Student(
//						rs.getInt("id"),
//						rs.getString("name"),
//						rs.getDate("bitthday")
//						);
//			} catch (SQLException e) {
//				e.printStackTrace();
//			}
//			return null;
//		});
	}

	@Override
	public Student row2Object(ResultSet rs) {
		try {
			return new Student(
					rs.getInt("id"),
					rs.getString("name"),
					rs.getDate("bitthday")
			);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Student get(int id) {
		String sql="select id, name, bitthday from student"
				+ " where id = ?";
		return jdbcUtil.findOne(sql, Student.class, id);
	}
}
//class StudentRowMaper implements RowMapper<Student>{
//
//	@Override
//	public Student row2Object(ResultSet rs) {
//		try {
//			return new Student(
//					rs.getInt("id"),
//					rs.getString("name"),
//					rs.getDate("bitthday")
//			);
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//		return null;
//	}
//	
//}
