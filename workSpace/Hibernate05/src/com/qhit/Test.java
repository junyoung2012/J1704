package com.qhit;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.tool.hbm2ddl.SchemaExport;

public class Test {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Configuration cfg=new Configuration().configure();
		SessionFactory sf=cfg.buildSessionFactory();
		//输出sql脚本，在数据库创建表
		new SchemaExport(cfg).create(true, true);
		Session session=sf.openSession();
		Stu s=new Stu();
		s.setAge(20);
		s.setName("zhangsan");
		
		session.beginTransaction();
		session.save(s);
		session.getTransaction().commit();
		session.close();

	}
	

}
