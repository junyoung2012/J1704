package com.kfm.dao.test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;

import com.kfm.dao.StudentDao;
import com.kfm.dao.StudentDaoImplByList;
import com.kfm.model.Student;

public class StudentDaoImplByListTest {
	private StudentDao studentDao=new StudentDaoImplByList();
	@Test
	public void testAdd() {
		assertTrue(studentDao.add(new Student(1001, "����")));
		assertTrue(studentDao.add(new Student(1002, "����")));
	}

	@Test
	public void testDelete() {
		testAdd();
		assertTrue(studentDao.delete(1001));
		testGet();
	}

	@Test
	public void testUpdate() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetInt() {
		fail("Not yet implemented");
	}

	@Test
	public void testGet() {
		System.out.println(studentDao.get());
	}

}
