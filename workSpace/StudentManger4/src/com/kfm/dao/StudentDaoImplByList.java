package com.kfm.dao;

import java.util.LinkedList;
import java.util.List;

import com.kfm.model.Student;

public class StudentDaoImplByList implements StudentDao {
	private List<Student> students=new LinkedList<>();
	
	@Override
	public boolean add(Student student) {
		if(students.contains(student)) return false;
		return students.add(student);
	}

	@Override
	public boolean delete(int code) {
//		return students.remove(get(code));
		return students.remove(new Student(code));//由于重写equals方法，指定学生相等条件是学号，删除时，只要知道学号就行了
	}

	@Override
	public boolean update(Student student) {
		for (int i = 0; i < students.size(); i++) {
			if(students.get(i).equals(student)) {
				students.set(i, student);
				return true;
			}
//			if(students.get(i).getCode()==student.getCode()) {
//				
//			}
		}
		return false;
	}

	@Override
	public Student get(int code) {
		for (Student student : students) {
			if(student.getCode()==code) {
				return student;
			}
		}
		return null;
	}

	@Override
	public List<Student> get() {
		return students;
	}

}
