package com.kfm.dao;

import java.util.List;

import com.kfm.model.Student;

public interface StudentDao {
	public boolean add(Student student);
	public boolean delete(int code);
	public boolean update(Student student);
	public Student get(int code);
	public List<Student> get();
}
