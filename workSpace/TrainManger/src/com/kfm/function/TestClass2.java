package com.kfm.function;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.function.Consumer;
import java.util.regex.Pattern;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class TestClass2 {
	public static void main(String[] args) {
//		test1();
//		test2();
//		test3();
//		test4();
//		test5();
//		test6();
//		test7();
		test8();
	}

	private static void test8() {
		int[] array=new int[5];
		for (int i = 0; i < array.length; i++) {
			array[i]=i+1;
		}
		IntStream stream = Arrays.stream(array);
		System.out.println(stream.reduce((x1, x2) -> x1 + x2).getAsInt());
	}

	private static void test7() {
		Student student1=new Student("aaa", 21);
		Student student2=new Student("bbb", 22);
		List<Student> list=Arrays.asList(student1,student2);
		list.stream().peek((s)->s.setAge(20))
			.forEach(System.out::println);
	}

	private static void test6() {
		Stream<Integer> stream=Stream.of(1,2,3,5,6,2);
		stream
//			.filter((a)->a%2==0)
//			.distinct()
		.map(a->a+2)
			.forEach(System.out::println);
	}

	private static void test5() {
		Pattern pattern = Pattern.compile(",");
		Stream<String> stream=pattern.splitAsStream("2,4,6,8");
	}

	private static void test4() {
		try (BufferedReader bufferedReader=new BufferedReader(
				new FileReader("resource/test5.txt"))){
			Stream<String> stream=bufferedReader.lines();
			System.out.println(stream.count());
		} catch (Exception e) {
			e.printStackTrace();
		};
	}

	private static void test3() {
		Stream<Integer> stream=Stream.of(1,2,3,5,6);
		Stream<Integer> stream2=Stream.iterate(0,(x)->x+3).limit(6);
//		stream2.forEach(System.out::println);
		Stream<Integer> stream3=Stream.generate(new Random(10)::nextInt).limit(6);
		stream3.forEach(System.out::println);
		
	}

	private static void test2() {
		Integer[] array=new Integer[10];
		Stream<Integer> stream = Arrays.stream(array);
	}

	private static void test1() {
		List<Integer> list=new ArrayList<>();
		Stream<Integer> stream = list.stream();
		Stream<Integer> parallelStream = list.parallelStream();
	}
}
