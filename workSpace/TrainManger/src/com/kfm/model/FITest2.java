package com.kfm.model;

public class FITest2 {
	interface IString{
		String test(int a,int b);
	}
	interface IConstructureRef{
		String test(String s);
	}
	
	
	public static void main(String[] args) {
		String s1="abcd";
		IString method=s1::substring;
		System.out.println(method.test(1,4));
//		IConstructureRef constructureRef=String::new;
		IConstructureRef constructureRef=s->new String(s);
		System.out.println(constructureRef.test("erw"));
	}
}
