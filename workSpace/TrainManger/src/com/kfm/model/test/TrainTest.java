package com.kfm.model.test;

import com.kfm.model.Train;
import com.kfm.model.TrainDate;
import com.kfm.model.TrainType;

public class TrainTest {

	public static void main(String[] args) {
		String tip="列车类型"
				+ "1.直特"
				+ "2.特快"
				+ "请输入列车类型序号";
		String input="2";
		TrainType trainType=TrainType.convertNum2TrainType(input);
		input="2, 21, 30";
		
		TrainDate trainDate=new TrainDate(2, 21, 30);
		Train train=new Train("Z105", trainType,trainDate);
		
		System.out.println(train);

	}

}
