package com.kfm.model;

public class TrainDate {
	private int day;
	private int hour;
	private int minute;
	
	public TrainDate(String input) {
		String[] array=input.split("[,��]");
		this.day = Integer.parseInt(array[0]);
		this.hour = Integer.parseInt(array[1]);
		this.minute = Integer.parseInt(array[2]);
	}
	public TrainDate(int day, int hour, int minute) {
		this.day = day;
		this.hour = hour;
		this.minute = minute;
	}
	@Override
	public String toString() {
		String result="";
		if(day==1) {
			result+="����";
		}else if(day>1) {
			result+="��"+day+"��";
		}
		if(hour==0 && day==0) {
			result+="----";
			return result;
		}
		
		if(hour<10) {
			result+="0"+hour;
		}else {
			result+=hour;
		}
		result+=":";
		
		if(minute<10) {
			result+="0"+minute;
		}else {
			result+=minute;
		}
		
		return result;
	}
}
