package com.kfm.model;

public class Train {
	private String code; 
	private TrainType type;
	private TrainDate trainDate;
	
	public Train(String code, TrainType type) {
		this.code = code;
		this.type = type;
	}

	public Train(String code, TrainType type, TrainDate trainDate) {
		this.code = code;
		this.type = type;
		this.trainDate = trainDate;
	}

	@Override
	public String toString() {
		return code + "\t" + type+ "\t"+trainDate;
	} 
	
	
}
