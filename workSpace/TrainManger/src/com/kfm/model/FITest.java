package com.kfm.model;

public class FITest {
	interface IMethod{
		boolean test(int a);
	}
	static boolean isE(int a) {
		return a%2==0;
	}
	public static void main(String[] args) {
		IMethod method=FITest::isE;
//		IMethod method=a->a%2==0;
		System.out.println(method.test(4));
	}
}
