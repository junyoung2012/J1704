package com.kfm.view;

import java.util.Scanner;

public class StudentView {
	private static Scanner scanner=new Scanner(System.in);//多个方法会用，多次复用
	
	public static void main(String[] args) {
		System.out.println("欢迎进入学生管理系统");
//		System.out.println("1.注册学生");
//		System.out.println("2.删除学生");
//		System.out.println("3.修改学生");
//		System.out.println("4.显式学生");
//		System.out.println("0.退出系统");
//		
//		第2种效率，略高
		showHomePage();
	}

	private static void showHomePage() {
		String tip="1.注册学生\n"
				+ "2.删除学生\n"
				+ "3.修改学生\n"
				+ "4.显示学生\n"
				+ "0.退出系统\n";
		print(tip);
		
//		Scanner scanner=new Scanner(System.in);//只准备用一次
		int input=scanner.nextInt();//0-4
		
		//分支结构，多选一结构
//		else if
//		if() {
//			
//		}else if() {
//			
//		}
//		switch
		switch (input) {
			case 1://注册学生
//				tip="请输入您需要注册的学生的学号，姓名，用逗号分开";
//				System.out.println(tip);
				showAddPage();
				break;
			case 2://删除学生
				showDeltePage();
				break;
			case 3://修改学生
				showUpdatePage();
				break;
			case 4://显示学生
				showListPage();
				break;
			case 0://退出系统
				showExitPage();
				break;
			default://错误输入
				showErrorPage();
				break;
		}
	}
//	打印函数
	private static void print(String tip) {
		System.out.println(tip);
	}
//	用户输入有误
	private static void showErrorPage() {
		print("您的输入有误");
		showHomePage();
	}
//	尽早的测试，是高速编码的的基础
	private static void showExitPage() {
		print("感谢使用，欢迎再来");
	}

	private static void showListPage() {
		// TODO Auto-generated method stub
		
	}

	private static void showUpdatePage() {
		// TODO Auto-generated method stub
		
	}

	private static void showDeltePage() {
		// TODO Auto-generated method stub
		
	}

	private static void showAddPage() {
		String tip="请输入您需要注册的学生的学号，姓名，用逗号分开\n"
				+ "0.返回上级";
//		1001,端木赐
//		0
		System.out.println(tip);
//		这个变量是什么数据类型？
		String input= scanner.next();
		
//		if ("0".equals(input)) {//0
//			showHomePage();
//		} else {//1001,端木赐
//			String[] array=input.split(",");//切割函数，把字符串，依据传入的字符串，切割成数组
//			System.out.println(array[0]);
//			System.out.println(array[1]);
//			
////			add();
//		}
//		//代码少的夹在判断体里
//		if(input.equals("0")) {
//		}
//		防止input空指针异常
		if("0".equals(input)) {//0
			showHomePage();
			return ;//跳出函数
		}
		
//		//1001,端木赐
		String[] array=input.split(",");//切割函数，把字符串，依据传入的字符串，切割成数组
//		System.out.println(array[0]);
//		System.out.println(array[1]);
		String code=array[0];
		String name=array[1];
		
//		Student student=new Student();
//		student.setCode(Integer.parseInt(code));
//		student.setName(name);
		Student student=new Student(code,name);
		if(add(student)) {//添加学生成功
			print("添加学生成功");
		}
	}

//	private static void add(int code,String name) {
//		// TODO Auto-generated method stub
//		
//	}
//	/**
//	 * 添加学生
//	 * @param code 学号
//	 * @param name 姓名
//	 * @return
//	 * 		true  添加成果
//	 *      false 添加失败
//	 *      
//	 *      当你不知道返回何种数据类型（boolean,int）
//	 */
//	private static boolean add(int code,String name) {
//		// TODO Auto-generated method stub
//		
//	}
	private static boolean add(Student student) {
//		return true;   成功的值，合法的值域   真出口
		return false;  //失败的值，不合法值域  假出口
//		没有做任何实现，默认写个假出口？？？没有实现，业务逻辑一定是错的。
//		n个假出口
//		1个真出口
		
//		n个真出口
//		1个假出口
	}
	
	private int getAge() {
//		return -1; 假出口
		return 1; //真出口
	}
}
