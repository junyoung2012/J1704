package com.kfm.view;

public class Student {
	private int code;
	private String name;
//	private int age;
	
	public Student(String code, String name) {
		this.code=Integer.parseInt(code);
		this.name=name;
	}
	
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
