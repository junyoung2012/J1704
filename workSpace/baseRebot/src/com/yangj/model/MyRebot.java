package com.yangj.model;
import com.yangj.server.Rebot;

public class MyRebot extends Rebot {
	
	@Override
	public void dealInput(String input) {
		if(input==null || input.isEmpty() ) {
			textMsg("我是铁蛋，你有什么问题");
			return;
		}
		inputMsg(input);//显示上行信息
		
		textMsg("收到"+input);//显示下行信息
	}

	@Override
	public String getName() {
		return "机器人名字";
	}

}
