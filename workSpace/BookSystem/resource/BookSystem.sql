create table ServiceType(
       id number(9) primary key,
       icon varchar2(100) not null,
       title varchar2(15) not null
);
-- drop table Technicain;
create table Technicain(
    id number(9) primary key,
    company varchar2(90) not null,
    logo varchar2(100) not null,
    avatar varchar2(100) not null,
    nickname varchar2(30) not null,
    price number(7) not null,
    message varchar2(450) not null,
    detail varchar2(450) not null,
    distance number(4) not null
);
--drop table Service;
create table Service(
    id number(9) primary key,
    subject varchar2(60) not null,
    coverpath varchar2(100) not null,
    price number(7) not null,
    message varchar2(450) not null,
    serviceTypeId number(9) references ServiceType(id),
    technicainId number(9) references Technicain(id),
    summry varchar2(300) not null,
    detail varchar2(450) not null,
    picture varchar2(100) not null
);




select id, icon, title from servicetype
       order by id
       
       
select id, company, logo, avatar, nickname, price, message, detail, distance from technicain 


select S.id, S.subject, S.coverpath, S.price, S.message,
 S.servicetypeid, S.technicainid, S.summry, S.detail, S.picture
  ST.id, ST.icon, ST.title,
  T.id, T.company, T.logo, T.avatar, T.nickname,
  T.price, T.message, T.detail, T.distance
  from service S
  left join servicetype ST on S.servicetypeid=ST.id
  left join Technicain T on S.Technicainid=T.Id
  
      
