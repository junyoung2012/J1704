package com.kfm.dao;

import java.sql.SQLException;
import java.util.List;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import com.kfm.model.Technicain;

public class TechnicainDao extends BaseDao {
	public List<Technicain> getTechnicains(){
		try {
			String sql="select id, company, logo, avatar, nickname,"
					+ " price, message, detail, lat, lon from technicain"
					+ " order by id";
			return queryRunner.query(sql,new BeanListHandler<>(Technicain.class));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	
}
