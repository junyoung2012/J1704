package com.kfm.dao;

import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.handlers.BeanListHandler;

import com.kfm.model.ServiceType;

public class ServiceTypeDao extends BaseDao {
	public List<ServiceType> getAll(){
		try {
			String sql="select id, icon, title from servicetype"
					+ " order by id";
			return queryRunner.query(sql, new BeanListHandler<>(ServiceType.class));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
}
