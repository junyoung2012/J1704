package com.kfm.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import com.kfm.model.Service;
import com.kfm.model.ServiceType;
import com.kfm.model.Technicain;

public class ServiceDao extends BaseDao implements ResultSetHandler<List<Service>> {
	public List<Service> getServicesByTypeId(int serviceTypeId){
		try {
			String sql="select S.id sid, S.subject, S.coverpath, S.price sprice, S.message smessage,"
					+ " S.servicetypeid, S.technicainid, S.summry , S.detail, S.picture,"
					+ "  ST.id stid, ST.icon, ST.title,"
					+ "  T.id tid, T.company, T.logo, T.avatar, T.nickname,"
					+ "  T.price tprice, T.message tmessage, T.detail tdetail,lat, lon"
					+ "  from service S"
					+ "  left join servicetype ST on S.servicetypeid=ST.id"
					+ "  left join Technicain T on S.Technicainid=T.Id "
					+ " where servicetypeid=?";
//			return queryRunner.query(sql,new BeanListHandler<>(Service.class),serviceTypeId);
			return queryRunner.query(sql,this,serviceTypeId);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<Service> handle(ResultSet rs) throws SQLException {
		List<Service> list=new LinkedList<Service>();
		while(rs.next()) {
			list.add(new Service(
					rs.getInt("sid"),
					rs.getString("subject"),
					rs.getString("coverpath"), 
					rs.getInt("sprice"),
					rs.getString("smessage"),
					new ServiceType(
						rs.getInt("stid"),
						rs.getString("icon"),
						rs.getString("title")
					),
					new Technicain(
							rs.getInt("tid"),
							rs.getString("company"),
							rs.getString("logo"),
							rs.getString("avatar"),
							rs.getString("nickname"),
							rs.getInt("tprice"),
							rs.getString("tmessage"),
							rs.getString("tdetail"),
							rs.getDouble("lat"),
							rs.getDouble("lon")
					),
					rs.getString("summry"),
					rs.getString("detail"),
					rs.getString("picture")
			));
		}
		return list;
	}
}
