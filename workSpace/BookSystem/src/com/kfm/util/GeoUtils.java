package com.kfm.util;

public class GeoUtils {
	/**
	 * 计算两经纬度点之间的距离(单位：米)
	 * @param lng1
	 *            经度
	 * @param lat1
	 *            纬度
	 * @param lng2
	 * @param lat2
	 * @return
	 */
	public static double getDistance(double lng1, double lat1, double lng2, double lat2) {
		double radlat1 = Math.toRadians(lat1);
		double radlat2 = Math.toRadians(lat2);
		double a = radlat1 - radlat2;
		double b = Math.toRadians(lng1) - Math.toRadians(lng2);
		double s = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(a / 2), 2) + Math.cos(radlat1)
				* Math.cos(radlat2) * Math.pow(Math.sin(b / 2), 2)));
		s = s * 6378137.0;// 取wgs84标准参考椭球中的地球长半径(单位:m)
		s = Math.round(s * 10000) / 10000;
		return s;
	}

	/**
	 * 
	 * 计算tp值
	 * 
	 * @param curpoint
	 *            当前点
	 * @param relatedpoint
	 *            偏移点
	 * @param isgeography
	 *            是否是地理坐标 false为2d坐标
	 * @return tp值
	 */

	public static double getDirangle(Point curpoint, Point relatedpoint, boolean isgeography) {
		double result = 0;
		if (isgeography) {
			double y2 = Math.toRadians(relatedpoint.getLat());
			double y1 = Math.toRadians(curpoint.getLat());
			double alpha = Math.atan2(relatedpoint.getLat() - curpoint.getLat(),
					(relatedpoint.getLng() - curpoint.getLng()) * Math.cos((y2 - y1) / 2));// 纬度方向乘以cos(y2-y1/2)
			double delta = alpha < 0 ? (2 * Math.PI + alpha) : alpha;
			result = Math.toDegrees(delta);
		} else {
			double alpha = Math.atan2(relatedpoint.getLat() - curpoint.getLat(),
					relatedpoint.getLng() - curpoint.getLng());
			double delta = alpha < 0 ? (2 * Math.PI + alpha) : alpha;
			result = Math.toDegrees(delta);
		}
		return result;
	}

	public static void main(String[] args) {
//		System.out.println(getDistance(121.446014, 31.215937, 121.446028464238, 31.2158502442799));
		 System.out.println("经纬度距离计算结果：" + getDistance(109.371319, 22.155406, 108.009758, 21.679011) + "米");
	}
	class Point{
		private double lat;
		private double lng;
		public double getLat() {
			return lat;
		}
		public void setLat(double lat) {
			this.lat = lat;
		}
		public double getLng() {
			return lng;
		}
		public void setLng(double lng) {
			this.lng = lng;
		}
	}
}