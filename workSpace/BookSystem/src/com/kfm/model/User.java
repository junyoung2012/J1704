package com.kfm.model;

public class User {
	private String nickName;

	public User() {
	}

	public User(String nickName) {
		this.nickName = nickName;
	}

	@Override
	public String toString() {
		return "User [nickName=" + nickName + "]";
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
}
