package com.kfm.model;

public class Service {
    private int id;
    private String subject;
    private String coverpath;
    private int price;
    private String message;
    private ServiceType serviceType;
    private Technicain technicain;
    private String summry;
    private String detail;
    private String picture;
    
	public Service() {
	}

	public Service(int id) {
		this.id = id;
	}

	public Service(int id, String subject, String coverpath, int price, String message, ServiceType serviceType,
			Technicain technicain, String summry, String detail, String picture) {
		this.id = id;
		this.subject = subject;
		this.coverpath = coverpath;
		this.price = price;
		this.message = message;
		this.serviceType = serviceType;
		this.technicain = technicain;
		this.summry = summry;
		this.detail = detail;
		this.picture = picture;
	}

	@Override
	public String toString() {
		return "Service [id=" + id + ", subject=" + subject + ", coverpath=" + coverpath + ", price=" + price
				+ ", message=" + message + ", serviceType=" + serviceType + ", technicain=" + technicain + ", summry="
				+ summry + ", detail=" + detail + ", picture=" + picture + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Service other = (Service) obj;
		if (id != other.id)
			return false;
		return true;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getCoverpath() {
		return coverpath;
	}

	public void setCoverpath(String coverpath) {
		this.coverpath = coverpath;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public ServiceType getServiceType() {
		return serviceType;
	}

	public void setServiceType(ServiceType serviceType) {
		this.serviceType = serviceType;
	}

	public Technicain getTechnicain() {
		return technicain;
	}

	public void setTechnicain(Technicain technicain) {
		this.technicain = technicain;
	}

	public String getSummry() {
		return summry;
	}

	public void setSummry(String summry) {
		this.summry = summry;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}
}