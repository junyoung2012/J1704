package com.kfm.web;

import java.util.List;
import java.util.Map;

import com.kfm.dao.ServiceTypeDao;
import com.kfm.model.ServiceType;

import net.sf.json.JSONArray;
public class GetNavTopItems extends JsonServlet {
	private ServiceTypeDao  serviceTypeDao=new ServiceTypeDao();
			
	@Override
	public String dealInput(Map<String, String[]> input) {
		List<ServiceType> serviceTypes=serviceTypeDao.getAll();
		System.out.println(JSONArray.fromObject(serviceTypes));
		return JSONArray.fromObject(serviceTypes).toString();
	}
}
