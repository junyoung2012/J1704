package com.kfm.web;

import java.util.List;
import java.util.Map;

import org.gavaghan.geodesy.Ellipsoid;
import org.gavaghan.geodesy.GeodeticCalculator;
import org.gavaghan.geodesy.GeodeticCurve;
import org.gavaghan.geodesy.GlobalCoordinates;

import com.kfm.dao.TechnicainDao;
import com.kfm.model.Technicain;
import com.kfm.util.GeoUtils;

import net.sf.json.JSONArray;
public class GetSkilledData extends JsonServlet {
	private TechnicainDao technicainDao=new TechnicainDao();
	@Override
	public String dealInput(Map<String, String[]> input) {
		System.out.println(input);
		double lat=0;
		double lon=0;
		try {
			lat = Double.parseDouble(input.get("lat")[0]);
			lon = Double.parseDouble(input.get("lon")[0]);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
		List<Technicain> technicains = technicainDao.getTechnicains();
		if(lat!=0) {
			for (Technicain technicain : technicains) {
				GlobalCoordinates source = new GlobalCoordinates(lat, lon);
			    GlobalCoordinates target = new GlobalCoordinates(technicain.getLat(), technicain.getLon());
			    double meter1 = getDistanceMeter(source, target, Ellipsoid.Sphere);
		        double meter2 = getDistanceMeter(source, target, Ellipsoid.WGS84);
		        technicain.setDistance((long) meter2);
//				technicain.setDistance((long) GeoUtils.getDistance(lon, lat, technicain.getLon(), technicain.getLat()));
			}
		}
		return JSONArray.fromObject(technicains).toString();
	}
	
	public static double getDistanceMeter(GlobalCoordinates gpsFrom, GlobalCoordinates gpsTo, Ellipsoid ellipsoid){
        //创建GeodeticCalculator，调用计算方法，传入坐标系、经纬度用于计算距离
        GeodeticCurve geoCurve = new GeodeticCalculator().calculateGeodeticCurve(ellipsoid, gpsFrom, gpsTo);
        return geoCurve.getEllipsoidalDistance();
    }
}
