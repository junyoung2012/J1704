package com.kfm.web;

import java.util.List;
import java.util.Map;

import com.kfm.dao.ServiceDao;
import com.kfm.model.Service;

import net.sf.json.JSONArray;
public class GetNavSectionData extends JsonServlet {
	private ServiceDao  serviceDao=new ServiceDao();
	
	@Override
	public String dealInput(Map<String, String[]> input) {
		int typeId=1;
		if(input.get("typeId")!=null && input.get("typeId")[0].matches("\\d{1,9}")) {
			typeId=Integer.parseInt(input.get("typeId")[0]);
		}
		System.out.println(typeId);
		List<Service> services=serviceDao.getServicesByTypeId(typeId);
		System.out.println(JSONArray.fromObject(services));
		return JSONArray.fromObject(services).toString();
	}
}
