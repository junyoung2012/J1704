create table StudentType(
  id number(9) primary key,
  title varchar2(30) not null
);
--drop table Student;
create table Student(
  id number(9) primary key,
  name varchar2(30) not null,
	birthday date not null,
  type_id number(9) references StudentType(id)
);

insert into student
  (id, name, bitthday)
values
  (v_id, v_name, v_bitthday);

delete student
 where id = v_id;
 
update student
   set id = v_id,
       name = v_name,
       bitthday = v_bitthday
 where id = v_id;

select id, name, bitthday from student

