package com.kfm.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import com.kfm.model.Student;
import com.kfm.model.StudentType;
/**
 * �����ϵӳ�䣨ORM��
 * @author Admin
 *
 */
public class StudentDao extends BaseDao {
	public boolean add(Student student) {
		try {
			String sql="insert into student"
					+ " (id, name, birthday,type_id)"
					+ " values"
					+ " (?,?,?,?)";
			return queryRunner.execute(sql,
					student.getId(), 
					student.getName(),
					student.getBirthday(),
					student.getStudentType().getId()
			)==1;
		} catch (SQLException e) {
			e.printStackTrace();
		}		
		return false;
	}

	public boolean delete(int id) {
		try {
			String sql="delete student"
					+ " where id = ?";
			return queryRunner.execute(sql,id)==1;
		} catch (SQLException e) {
			e.printStackTrace();
		}		
		return false;
	}

	public boolean update(Student student) {
		try {
			String sql="update student"
					+ " set name = ?,"
					+ " birthday = ?,"
					+ " type_id = ?"
					+ " where id = ?";
			return queryRunner.execute(sql,
					student.getName(),
					student.getBirthday(),
					student.getStudentType().getId(),
					student.getId()
			)==1;
		} catch (SQLException e) {
			e.printStackTrace();
		}		
		return false;
	}

	public List<Student> getAll() {
		try {
			String sql="select S.id sid, name, birthday,"
					+ " ST.Id stid,ST.Title"
					+ " from student S"
					+ " left join StudentType ST on S.TYPE_ID=ST.ID"
					+ " order by S.id";
			return queryRunner.query(sql, new ResultSetHandler<List<Student>>() {
				@Override
				public List<Student> handle(ResultSet rs) throws SQLException {
					List<Student> list=new LinkedList<>();
					while(rs.next()) {
						list.add(new Student(
								rs.getInt("sid"), 
								rs.getString("name"), 
								rs.getDate("birthday"), 
								new StudentType(
										rs.getInt("stid"), 
										rs.getString("title")
								)
						));
					}
					return list;
				}
			});
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public Student get(int id) {
		try {
			String sql="select S.id sid, name, birthday,"
					+ " ST.Id stid,ST.Title"
					+ " from student S"
					+ " left join StudentType ST on S.TYPE_ID=ST.ID"
					+ " where S.id = ?";
			return queryRunner.query(sql, new ResultSetHandler<Student>() {
				@Override
				public Student handle(ResultSet rs) throws SQLException {
					if(rs.next()) {
						return new Student(
								rs.getInt("sid"), 
								rs.getString("name"), 
								rs.getDate("birthday"), 
								new StudentType(
										rs.getInt("stid"), 
										rs.getString("title")
										)
								);
					}
					return null;
				}
			},id);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
}
