package com.kfm.dao;

import java.io.InputStream;
import java.util.Properties;
import org.apache.commons.dbutils.QueryRunner;
import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.pool.DruidDataSourceFactory;

public class BaseDao {
	private static DruidDataSource ds;
	private final static String CONFIG_FILE = "druid.properties";
	
	protected QueryRunner queryRunner=new QueryRunner(ds);
	
	static {
		try {
			Properties properties=new Properties();
			InputStream inStream=BaseDao.class.getClassLoader()
					.getResourceAsStream(CONFIG_FILE);
			properties.load(inStream);
			ds=(DruidDataSource) DruidDataSourceFactory.createDataSource(properties);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
