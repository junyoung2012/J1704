package com.kfm.dao.test;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.kfm.dao.StudentTypeDao;
import com.kfm.model.StudentType;

class StudentTypeDaoTest {
	private StudentTypeDao studentTypeDao=new StudentTypeDao();
	
	@BeforeEach
	void setUp() throws Exception {
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testAdd() {
		assertTrue(studentTypeDao.add(new StudentType(1, "ıʿ")));
		assertTrue(studentTypeDao.add(new StudentType(2, "Ԫ˧")));
	}

	@Test
	void testDelete() {
		fail("Not yet implemented");
	}

	@Test
	void testUpdate() {
		fail("Not yet implemented");
	}

	@Test
	void testGetAll() {
		System.out.println(studentTypeDao.getAll());
	}

	@Test
	void testGet() {
		fail("Not yet implemented");
	}

}
