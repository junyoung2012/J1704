package com.kfm.dao.test;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.sql.Date;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.kfm.dao.StudentDao;
import com.kfm.model.Student;
import com.kfm.model.StudentType;
import com.kfm.dao.StudentDao;

class StudentDaoTest {
	private StudentDao studentDao=new StudentDao();
	
	@BeforeEach
	void setUp() throws Exception {
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testAdd() {
		assertTrue(studentDao.add(new Student(101, "����", 
				Date.valueOf("2021-10-22"), new StudentType(1))));
		assertTrue(studentDao.add(new Student(102, "����", 
				Date.valueOf("2021-10-22"), new StudentType(1))));
		assertTrue(studentDao.add(new Student(103, "���", 
				Date.valueOf("2021-10-22"), new StudentType(2))));
		assertTrue(studentDao.add(new Student(104, "����", 
				Date.valueOf("2021-10-22"), new StudentType(2))));
	}

	@Test
	void testDelete() {
		fail("Not yet implemented");
	}

	@Test
	void testUpdate() {
		fail("Not yet implemented");
	}

	@Test
	void testGet() {
		System.out.println(studentDao.get(104));
	}

	@Test
	void testGetAll() {
		System.out.println(studentDao.getAll());
	}

}
