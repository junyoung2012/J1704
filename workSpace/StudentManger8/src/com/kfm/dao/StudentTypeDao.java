package com.kfm.dao;

import java.sql.SQLException;
import java.util.List;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import com.kfm.model.StudentType;
/**
 * �����ϵӳ�䣨ORM��
 * @author Admin
 *
 */
public class StudentTypeDao extends BaseDao {
	public boolean add(StudentType studentType) {
		try {
			String sql="insert into StudentType"
					+ " (id, title)"
					+ " values"
					+ " (?,?)";
			return queryRunner.execute(sql,
					studentType.getId(), 
					studentType.getTitle()
			)==1;
		} catch (SQLException e) {
			e.printStackTrace();
		}		
		return false;
	}

	public boolean delete(int id) {
		try {
			String sql="delete StudentType"
					+ " where id = ?";
			return queryRunner.execute(sql,id)==1;
		} catch (SQLException e) {
			e.printStackTrace();
		}		
		return false;
	}

	public boolean update(StudentType studentType) {
		try {
			String sql="update StudentType"
					+ " set title = ?"
					+ " where id = ?";
			return queryRunner.execute(sql,
					studentType.getTitle(),
					studentType.getId()
			)==1;
		} catch (SQLException e) {
			e.printStackTrace();
		}		
		return false;
	}

	public List<StudentType> getAll() {
		try {
			String sql="select id, title from StudentType"
					+ " order by id";
			return queryRunner.query(sql, new BeanListHandler<>(StudentType.class));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public StudentType get(int id) {
		try {
			String sql="select id, title from StudentType"
					+ " where id = ?";
			return queryRunner.query(sql, new BeanHandler<>(StudentType.class),id);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
}
