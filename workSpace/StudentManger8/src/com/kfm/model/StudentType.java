package com.kfm.model;

public class StudentType {
	private int id;
	private String title;
	
	public StudentType() {
	}
	
	public StudentType(int id) {
		this.id = id;
	}

	public StudentType(int id, String title) {
		this.id = id;
		this.title = title;
	}
	
	
	@Override
	public String toString() {
		return "StudentType [id=" + id + ", title=" + title + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StudentType other = (StudentType) obj;
		if (id != other.id)
			return false;
		return true;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
}
