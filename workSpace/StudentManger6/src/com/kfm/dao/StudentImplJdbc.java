package com.kfm.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import com.kfm.model.Student;
import com.kfm.util.JdbcUtil;
/**
 * �����ϵӳ�䣨ORM��
 * @author Admin
 *
 */
public class StudentImplJdbc implements StudentDao {
	private JdbcUtil jdbcUtil =new JdbcUtil();
	
	@Override
	public boolean add(Student student) {
		String sql="insert into student"
				+ " (id, name, bitthday)"
				+ " values"
				+ " (?,?,?)";
		return jdbcUtil.executeUpdate(sql,
				student.getId(), 
				student.getName(),
				student.getBitthday()
		)==1;		
	}

	@Override
	public boolean delete(int id) {
		String sql="delete student"
				+ " where id = ?";
		return jdbcUtil.executeUpdate(sql,id)==1;		
	}

	@Override
	public boolean update(Student student) {
		String sql="update student"
				+ " set name = ?,"
				+ " bitthday = ?"
				+ " where id = ?";
		return jdbcUtil.executeUpdate(sql,
				student.getName(),
				student.getBitthday(),
				student.getId()
		)==1;		
	}

	@Override
	public Student get(int id) {
		String sql="select id, name, bitthday from student"
				+ " where id = ?";
		ResultSet rs=jdbcUtil.executeQuery(sql,id);
		try {
			if(rs.next()) {
				return new Student(
						rs.getInt("id"),
						rs.getString("name"),
						rs.getDate("bitthday")
				);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<Student> getAll() {
		String sql="select id, name, bitthday from student"
				+ " order by id";
		ResultSet rs=jdbcUtil.executeQuery(sql);
		try {
			List<Student> list=new LinkedList<Student>();
			while(rs.next()) {
				list.add(new Student(
						rs.getInt("id"),
						rs.getString("name"),
						rs.getDate("bitthday")
				));
			}
			return list;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
}
