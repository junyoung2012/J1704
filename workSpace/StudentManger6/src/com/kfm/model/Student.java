package com.kfm.model;

import java.util.Date;

public class Student {
	private int id;
	private String name;
	private Date bitthday;
	public Student() {
	}
	public Student(int id, String name, Date bitthday) {
		this.id = id;
		this.name = name;
		this.bitthday = bitthday;
	}
	
	@Override
	public String toString() {
		return "Student [id=" + id + ", name=" 
	+ name + ", bitthday=" + bitthday + "]\n";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Student other = (Student) obj;
		if (id != other.id)
			return false;
		return true;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getBitthday() {
		return bitthday;
	}
	public void setBitthday(Date bitthday) {
		this.bitthday = bitthday;
	}
}
