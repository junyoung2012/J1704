package com.kfm.model;

public class Drame {
	private int id;
	private String key;
	private String content;
	public Drame() {
	}
	public Drame(int id, String key, String content) {
		this.id = id;
		this.key = key;
		this.content = content;
	}
	
	@Override
	public String toString() {
		return "Drame [id=" + id + ", key=" + key + ", content=" + content + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Drame other = (Drame) obj;
		if (id != other.id)
			return false;
		return true;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
}
