package com.kfm.dao;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;

import com.kfm.model.Drame;

public class DrameDao {
	private List<Drame> drames=new LinkedList<>();
	
	public boolean add(Drame Drame ) {
		return false;
	}
	public boolean delete(int id ) {
		return false;
	}
	public boolean update(Drame Drame ) {
		return false;
	}
	public Drame get(int id ) {
		return null;
	}
	public Drame get(String input ) {
		for (Drame drame : drames) {
			if(input.contains(drame.getKey())) return drame;
		}
		return null;
	}
	public List<Drame> getAll() {
		return drames;
	}
	public boolean load(String fileName) {
//		1	体育教练	<p>　　梦见体育教练是什么意思？
		try(BufferedReader bufferedReader=
				new BufferedReader(
						new InputStreamReader(
								new FileInputStream(fileName), "UTF-8"))){
			String readLine = bufferedReader.readLine();
			
			while ((readLine = bufferedReader.readLine())!=null) {
				String[] array = readLine.split("\t");
				Drame drame = new Drame(Integer.parseInt(array[0]), array[1], array[2]);
				drames.add(drame);
			}
			return true;
		}catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
}
