package com.kfm.model;
/**
 * 容器类
 * 保存学生信息的
 * @author Admin
 *
 */
public class Student extends Object {
	private int code;
	private String name;
	
	public Student(int code, String name) {
		this.code=code;
		this.name=name;
	}
	
	public Student(String code, String name) {
		this.code=Integer.parseInt(code);//字符串转int
		this.name=name;
	}
	@Override
	public String toString() {
		return this.code+"\t"+this.name;
	}
	public int getCode() {
		return this.code;
	}
	public String getName() {
		return this.name;
	}
}
