package com.kfm.dao;

import com.kfm.model.Student;
/**
 * 测试类
 * @author Admin
 *
 */
public class StudentDaoTest {
	private static StudentDao studentDao=new StudentDao();
	
	public static void main(String[] args) {
		testAdd();
		testList();
	}

	private static void testList() {
		Student[] students=studentDao.getAll();
		System.out.println(students.length==2);
	}

	private static void testAdd() {
		System.out.println(studentDao.add(new Student(1001, "张三"))==true);
		System.out.println(studentDao.add(new Student(1001, "张三"))==true);
//		System.out.println(studentDao.add(new Student(1001, "张三"))==true);
//		System.out.println(studentDao.add(new Student(1001, "张三"))==true);
//		System.out.println(studentDao.add(new Student(1001, "张三"))==true);
//		System.out.println(studentDao.add(new Student(1001, "张三"))==false);//数组满了
	}
}
