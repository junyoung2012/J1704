package com.kfm.dao;

import com.kfm.model.Student;

/**
 * dao=data access object=数据访问对象（厨子）
 * @author Admin
 *
 */
public class StudentDao {
	private static final int MAX_COUNT = 5;
	private Student[] students=new Student[MAX_COUNT];
	private int index=0;
	/**
	 * 新增学生
	 * @param student
	 * @return
	 */
	public boolean add(Student student) {
//		1.判断数组满了
//			假出口跳出
		if(index==MAX_COUNT) return false;
//		2.合理的当前的位置保存学生
		students[index]=student;
//		3.当前的位置，后移
		index++;
//		4.返回真出口
		return true;
	}
	
	public boolean delete(int index) {
		return false;
	}
	/**
	 * 
	 * @param index 需要修改的学生序号
	 * @param student code,name  修改后学生的编号，姓名 1002，冉有
	 * @return
	 */
	public boolean update(int index,Student student) {
		
		return false;
	}
	
	public Student get(int index) {
		return null;
	}
	
	public Student[] getAll() {
//		1.重新定义一个结果数组
		Student[] results=new Student[index];
//		2.students的前多少项（index），复制到结果数组
		for (int i = 0; i < index; i++) {
			results[i]=students[i];
		}
//		3.返回结果数组
		return results;
	}
}
