package com.kfm.view;

import java.util.Scanner;

import com.kfm.dao.StudentDao;
import com.kfm.model.Student;
/**
 * 界面类
 * @author Admin
 *
 */
public class StudentView {
	private static Scanner scanner=new Scanner(System.in);
	private static StudentDao studentDao=new StudentDao();
	
	public static void main(String[] args) {
		StudentView studentView=new StudentView();
		studentView.showWelcomPage();
		studentView.showHomePage();
	}

	private  void showHomePage() {
		String tip="1.注册学生\r\n" + 
				"2.删除学生\r\n" + 
				"3.修改学生\r\n" + 
				"4.显示学生\r\n" + 
				"0.退出系统\r\n" ;
		int input = getInputInt(tip);
		switch (input) {
			case 1://注册学生
				showAddPage();
				break;
			case 2://删除学生
				showDeletePage();
				break;
			case 3://修改学生
				showUpdatePage();
				break;
			case 4://显示学生
				showListPage();
				break;
			case 0://退出系统
				showExitPage();
				break;
			default:
				showErrorPage();
				break;
		}
	}
	/**
	 * 获得int类型的输入
	 * @param tip
	 * @return
	 */
	private int getInputInt(String tip) {
//		1.发出提示
//		2.获取输入
		print(tip);
		int input=scanner.nextInt();
		return input;
	}
	/**
	 * 获得String类型的输入
	 * @param tip
	 * @return
	 */
	private String getInputString(String tip) {
//		1.发出提示
//		2.获取输入
		print(tip);
		String input=scanner.next();
		return input;
	}

	private  void showErrorPage() {
		print("您的输入有误");
		showHomePage();
	}

	private void showListPage() {
		printStudents();
		showHomePage();
	}

	private void printStudents() {
		print("序号\t学号\t姓名");
		Student[] students=studentDao.getAll();
		for (int i = 0; i < students.length; i++) {
//			print(students[i].getCode()+"\t"+students[i].getName());
			print((i+1)+"\t"+students[i].toString());
		}
//		for (Student student : students) {
//			print(student.toString());
//		}
	}

	private  void showUpdatePage() {
//		1.显示所有学生
		printStudents();
		String tip="请输入您要修改的学生序号\n"
				+ "0.返回上级";
		int input=getInputInt(tip);
		if(input==0) {
			showHomePage();
			return;
		}
//		12 根据序号获得学生？？
		Student student=studentDao.get(input);
		showUpdatePage(student);
	}

	private void showUpdatePage(Student student) {
		// TODO Auto-generated method stub
		
	}

	private  void showDeletePage() {
//		1.显示所有学生
		printStudents();
		String tip="请输入您要删除的学生序号\n"
				+ "0.返回上级";
		int input =getInputInt(tip);
		if(input==0) {
			showHomePage();
			return;
		}
		//一定是个序号
		if(studentDao.delete(input)) {
			
		}else {
			
		}
	}

	private  void showAddPage() {
		String tip="请输入您需要注册的学生的学号，姓名，用逗号分开\r\n" + 
				"0.返回上级";
//		print(tip);
//		String input=scanner.next();
		String input=getInputString(tip);
		if("0".equals(input)) {
			showHomePage();
			return;
		}
//		1001,端木赐
		String[] array=input.split(",");
//		array[0]
//		array[1]
		
//		if(add(code,name))
//		创建一个新的类???
		Student student=new Student(array[0],array[1]);
//		StudentDao studentDao=new StudentDao();
		if(studentDao.add(student)) {
			print("保存成功");
			showHomePage();
		}else {
			print("保存失败");
			showAddPage();
		}
	}

//	private static boolean add(Student student) {
//		
//		return false;
//	}

	private  void showExitPage() {
		print("感谢使用，欢迎再来");
	}

	private  void print(String tip) {
		System.out.println(tip);
	}

	private  void showWelcomPage() {
		print("欢迎进入学生管理系统");
	}
}
