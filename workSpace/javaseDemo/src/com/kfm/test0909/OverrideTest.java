package com.kfm.test0909;

public class OverrideTest {

	public static void main(String[] args) {
		OverideParent parent=new OverideParent();
		OverideParent sub=new OverideSub();//子类对象，能不能指向父类变量？？
//		里氏代换原则：可以，使用父类对象的地方，一定能被子类对象替换
		parent.test();//动态绑定，父类的test方法 why?
		sub.test();//动态绑定，子类的test方法
	}
}
