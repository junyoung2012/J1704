package com.kfm.test0909;

public class CountrorTest {
	private int a;
	
	public void setA(int a) {
		this.a=a;//局部变量，成员变量同名，谁会生效？（就近原则）范围小的（局部变量）遮蔽 成员变量,强制发访问成员变量要加this
	}
//	public void setA(int _a) {
//		a=_a;
//	}
	public int getA() {
		return a;
	}
	
	
}
