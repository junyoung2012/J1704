package com.kfm.test0909;

public class DaoTest {

	public static void main(String[] args) {
		Dog[] dogs=new Dog[5];
		dogs[0]=new Dog();
		dogs[1]=new BlackDog();
		dogs[2]=new YellowDog();
		dogs[3]=new Dog();
		dogs[4]=new BlackDog();
//		dogs[5]=new BlackDog();//???
		for (int i = 0; i < dogs.length; i++) {
			dogs[i].cry();
		}
	}
}
