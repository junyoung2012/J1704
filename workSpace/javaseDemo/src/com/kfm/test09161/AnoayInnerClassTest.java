package com.kfm.test09161;

import com.kfm.test0916.Runable;

public class AnoayInnerClassTest {
	public static void main(String[] args) {
////		定义类，没有类名，但是实现了Runable接口，这个类只需要使用一次，我们就可以匿名
//		Runable runable=new Runable() {
//			
//			@Override
//			public void run() {
//				System.out.println("我没有名字，但是也能跑");
//				
//			}
//		};
//		runable.run();
//		class NoInnerClass implements Runable{
//			
//			@Override
//			public void run() {
//				// TODO Auto-generated method stub
//				
//			}
//			
//		}
//		new NoInnerClass()
		AnoayInnerClassTest test=new AnoayInnerClassTest();
		Runable runable=new Fox();
		test.test1(runable);
		test.test1(new Runable() {
			@Override
			public void run() {
				System.out.println("我只是路过");
				
			}
		});
	}
	
	private void test1(Runable runable) {
		// TODO Auto-generated method stub
		runable.run();
	}
}
class Fox implements Runable{

	@Override
	public void run() {
		System.out.println("狐狸快跑");
	}
}
