package com.kfm.test09161;

import com.kfm.test09161.Outer.Inner;

public class Outer {
	public class Inner{
		
	}
	public static class Inner2{
		
	}
	
	private void test1() {
		Inner inner=new Inner();
		Inner2 inner2=new Inner2();
	}
	
	private static void test2() {
		Outer outer=new Outer();
		Inner inner=outer.new Inner();
		Inner2 inner2=new Inner2();
	}
}

class OuterTest{
	public static void main(String[] args) {
		Outer.Inner inner;
		Outer outer=new Outer();
		inner=outer.new Inner();
		
		inner=new Outer().new Inner();
		
		Outer.Inner2 inner2=new Outer.Inner2();
		
	}
	
	private void test1() {
		Outer.Inner inner=new Outer().new Inner();
		Outer.Inner2 inner2=new Outer.Inner2();
	}
}
