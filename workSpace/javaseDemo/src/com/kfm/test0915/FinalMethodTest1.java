package com.kfm.test0915;

public class FinalMethodTest1 {
	public static  void test1() {
		System.out.print("hi 1");
	}
	public final static  void test2() {
		System.out.print("hi 2");
	}
	public static void main(String[] args) {
		long begin=System.currentTimeMillis();//获得jvm启动的毫秒
		for (int i = 0; i < 10000; i++) {
			test1();
		}
		long end=System.currentTimeMillis();
		System.out.println();
		System.out.println("非final方法运行时间："+(end-begin));
		begin=System.currentTimeMillis();
		for (int i = 0; i < 10000; i++) {
			test2();
		}
		end=System.currentTimeMillis();
		System.out.println();
		System.out.println("final方法运行时间："+(end-begin));
	}
}
class FinalMethodTest2 extends FinalMethodTest1{
//	@Override
//	public void test1() {
//		// TODO Auto-generated method stub
//		super.test1();
//	}
}
