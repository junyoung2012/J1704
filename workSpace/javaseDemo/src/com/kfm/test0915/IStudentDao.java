package com.kfm.test0915;
/**
 * 一个类包含方法的规则
 * @author Admin
 *
 */
public abstract class IStudentDao {
	public abstract boolean add(Student student);
	public abstract boolean delete(int id);
	public abstract boolean update(Student student);
	public abstract Student get(int id);
	public abstract Student[] get();
}
