package com.kfm.test0915;

import com.kfm.test09032.Student;

public class FinalVarTest {
	private static final int MAX_COUNT/*=1*/;//常量（非正式），不可变变量
	
	public FinalVarTest() {
//		MAX_COUNT=5;
	}
	//普通代码块
	{
//		MAX_COUNT=5;
	}
	
	//static代码块
	static{
		MAX_COUNT=5;
	}
	
	private void test1(final int j) {
		// TODO Auto-generated method stub
		final int k=2;
	}
	private static void test2() {
		final String s1="hello";
		final String s2=" world!";
		String s3="hello world!";
		String s4=s1+s2;
//		System.out.println(s3.equals(s4));
		System.out.println(s3==s4);
	}
	private static void test3(final int[] array) {
//		array=new int[5];
		array[0]=2;
		System.out.println(array[0]);
	}
	public static void main(String[] args) {
//		test2();
		int[] array=new int[] {1,3,5};
		test3(array);
		System.out.println(array[0]);
		Student student=new Student();
		student.name="苏秦";
		test4(student);
		System.out.println(student.name);
	}
	private static void test4(final Student student) {
//		student=new Student();
		student.name="张仪";
	}
	
}
