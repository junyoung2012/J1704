package com.kfm.test0927;

import java.text.Format;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.LinkedList;

public class DateTest {

	public static void main(String[] args) {
		testDate();
		
		testCalendar();
		testFormat();
	}

	private static void testFormat() {
		String input="2012-12-31";
//		Format 
//		Date date;
		
	}

	private static void testDate() {
		Date date1=new Date();//系统当前时间。精确到秒
		System.out.println(date1);
		Date date2=new Date(-1000*86400);//java 是使用long保存时间，基准是1970-1-1 00:00:00 毫秒数
		System.out.println(date2);
		
		Date date3=new Date(2012-1900,11,30,13,0,0);
		System.out.println(date3);
	}

	private static void testCalendar() {
//		Calendar calendar=new GregorianCalendar();
		Calendar calendar=Calendar.getInstance();//根据时区，语言设置得到默认日历类
		calendar.set(2012, 11, 31, 13, 0, 0);
//		calendar.add(Calendar.DAY_OF_MONTH, -1);
//		calendar.add(Calendar.DAY_OF_MONTH, 2);
		calendar.roll(Calendar.DAY_OF_MONTH, 1);//滚动(求余)
		System.out.println(calendar.get(Calendar.YEAR));
		System.out.println(calendar.get(Calendar.MONTH)+1);
		System.out.println(calendar.getTime());//日历转日期
		
		
	}

}
