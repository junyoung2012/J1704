package com.kfm.test0927;

public class WarpClassTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String s=null;
		StringBuffer sb=null;
		
		test(s);
		test(sb);
		
		//除了基本数据类型，所有的都是object
//		OOP的一个死角，就是基本数据类型
		Integer integer=new Integer(1);
		test(integer);
//		int i=2;
//		test(i);//竟然也可以
		
//		Integer integer2=i;
//		int j=integer2;
//		int k=integer2.intValue();
//		引用转基本
		
//		字符串转数值
//		String str="100a";
//		String str="100.1"; 
//		String str="-100.1";
//		String str="12345678901";
//		String str=;
//		k=integer.parseInt(str,2);//代表进制
//		k=integer.parseInt(str);//代表进制
//		System.out.println(k);
		
//		System.out.println(Double.parseDouble(str));
//		testBoolean();
		testChacter();
	}
	
	private static void testChacter() {
		String s="abcd";
		System.out.println(s.charAt(1));
//		Character
		System.out.println(Character.isDigit('a'));
		System.out.println(Character.isDigit('2'));
		System.out.println(Character.isDigit('-'));
//		isLetter(char ch) 
//		isLetterOrDigit(char ch) 
//		isLowerCase(char ch) 
//		isWhitespace(char ch) 
	}

	private static void testBoolean() {
		String s="tRue";
		System.out.println(Boolean.parseBoolean(s));
	}

	public static void test(Object param) {
		System.out.println(param);
	}

}
