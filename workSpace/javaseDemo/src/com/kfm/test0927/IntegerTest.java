package com.kfm.test0927;

public class IntegerTest {

	public static void main(String[] args) {
		testConstruct();
	}

	private static void testConstruct() {
		Integer integer1=1;
		Integer integer2=1;
		System.out.println(integer1==integer2);//true  池
		Integer integer3=new Integer(1);
		Integer integer4=new Integer(1);
		System.out.println(integer3==integer4);//false 堆
		
		System.out.println(integer1==integer4);//false
		
		Integer integer5=128;
		Integer integer6=128;
		System.out.println(integer5==integer6);//false 缓存（-128，127）
		Short short1=1;
		
//		System.out.println(integer3.equals(integer4));//true
		System.out.println(integer3.equals(short1));//false
		
		int i=1;
		System.out.println(integer3==i);//基本数据类型和包装类比较，会拆箱为基本数据类型比较
		System.out.println(integer1>integer3);
		System.out.println(integer1.compareTo(integer3));
		
	}
}
