package com.kfm.test0927;
import static java.lang.Math.*;//static导入

import java.util.Random;
public class MathTest {

	public static void main(String[] args) {
//		四舍六入五看前（VB支持）
		System.out.println(Math.round(3.14));
		
		System.out.println(Math.floor(3.14));
		System.out.println(floor(-3.14));//--4.0
		
		System.out.println(Math.sqrt(9));//3.0 开方
		System.out.println(Math.pow(2, 3));
		
		System.out.println(Math.random()); //[0,1）
		
//		产生随机整数
//		1.放大
		System.out.println(Math.random()*16); 
//		2.取整
		System.out.println((int)(Math.random()*16)); 
		Random random=new Random();
		System.out.println(random.nextInt());
		System.out.println(random.nextInt(100));//!!!在 0（包括）和指定值（不包括）之间均匀分布的 int 值。
	}

}
