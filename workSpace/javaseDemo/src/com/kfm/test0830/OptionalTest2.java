package com.kfm.test0830;

public class OptionalTest2 {
	public static void main(String[] args) {
		testTreeOptional2();
		testAssign();
	}
//	赋值运算
	private static void testAssign() {
		int i=1;
		i+=3;//i=i+3;
		
		byte byte1=2;
		byte1+=3;//这样是能通过编译
//		byte1=byte1+3;
	}

	private static void testTreeOptional() {
		// 逻辑表达式(布尔类型)?true的返回值:false的返回值。
		int age=16;
//		String result= age>=18?"成年":"未成年";
//		System.out.println(result);
		if(age>=18) {
			System.out.println("成年");
		}else {
			System.out.println("未成年");
		}
	}
	private static void testTreeOptional2() {
		// 逻辑表达式(布尔类型)?true的返回值:false的返回值。
		int input=0;
		int result= (input==0?0:(input>0?1:-1));
		System.out.println(result);
		
	}
}
