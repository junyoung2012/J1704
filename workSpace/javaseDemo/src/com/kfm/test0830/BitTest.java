package com.kfm.test0830;

public class BitTest {

	public static void main(String[] args) {
//		testBitAnd();
//		testBirOr();
//		int input=12;
//		testGetOption(input);
//		testBitLeft();
//		testBitRight();
		testPri();
	}
	//测试优先级
	private static void testPri() {
		float a=25;
//		System.out.println(a--/a++);
		System.out.println(a--/a++);
		System.out.println(a);//25.0
		System.out.println(a--/a--);
		System.out.println(a);//23.0
		System.out.println(a--);
		System.out.println(a);
	}

	private static void testBitRight() {
		// 右移操作
//		 >> 符号右移 在左面不符号位
//		>>> 向右移位 在左面一律补0
		int i=-4;//1110 1111 -> 0111 0111
//		int result=i>>2;//符号右移：向右移动，除以2的n次方
//		System.out.println(result);
//		01010101 01010101 01010101  01010101 
//		00000000 00000000 01010101 01010101 
		int result=i>>>16;//无符号右移：只适用于字节截取
		
		System.out.println(result);
	}

	private static void testBitLeft() {
		// 左移操作(i*2的n次方)，比乘以2的n次方的效率高，是一种高效率的运算方式。
		int i=4;//0100 -> 0001 0000
		int result=i<<2;//向左移动，右边补0；
		System.out.println(result==16);
	}

	private static void testGetOption(int input) {
//		0000 0011
//		0000 0001
//		0000 0001
		
		System.out.println((input & 1 )==1?"选择A选项":"未选择A选项");
	}

	private static void testBirOr() {
		int i=4;//00000100
		int j=8;//00001000
		int k=i | j;//00001100
		System.out.println(k==12);//
	}

	private static void testBitAnd() {
//		原码
//		反码：正数的反码是源码 （负数）按位求反
//		补码
//		符号位 0 正数 ，1 负数 x-4 = x+(-4)
//		int i=-4;//100000000 000000000 000000000 00000100
//		反码     1111111011
//			 00000000 +0
//		反码     11111111 -0
//		-1   11111111 （补码）  -128-127 不用表示负0,所有的负数等于在补码状态都绝对值-1
//		补码：求反+1
		
		int i=4;//00000100
		int j=8;//00001000
		int k=i & j;//00000000
		System.out.println((i & j)==0);//位运算只支持单&
	}
	
	

}
