package com.kfm.test0830;

import java.util.Scanner;

public class AreaCalculat {

	public static void main(String[] args) {
		Scanner scanner =new Scanner(System.in);
        System.out.println("请输入长方形的宽：");
        double width=scanner.nextDouble();
        System.out.println("请输入长方形的高：");
        double height=scanner.nextDouble();
        double result=getArea(height,width);
        if(result==-1) {
        	System.out.println("您输入宽度有误。");
        	return;
        }
        if(result==-2) {
        	System.out.println("您输入高度度有误。");
        	return;
        }
        System.out.println("面积为"+result);
	}

	private static double getArea(double height, double width) {
		if(height<=0)
			return -1;
		if(width<=0)
			return -2;
		return height*width;
	}

}
