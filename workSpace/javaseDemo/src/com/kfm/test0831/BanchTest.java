package com.kfm.test0831;

public class BanchTest {

	public static void main(String[] args) {
//		testSinge();
//		testTwo();
//		testMulti();
		testSwitch();
	}
	/**
	 * 多选多
	 */
	private static void testSwitch() {
		
		int key=2;
		switch(key) {//判别表达式int和int的朋友（byte,short,char,Integer）,enum(枚举类)，JDK7以后，String ,
			case 1:
				//执行部分
				System.out.println("春暖花开");
//				break;
			case 2:	
				System.out.println("夏日炎炎");
//				break;
			case 3:
				System.out.println("秋高气爽");
//				break;
			case 4:
				System.out.println("冬雪初雯");
//				
				System.out.println("天气不错，可以穿裙子了");
				break;
			default:
				System.out.println("想咋咋地");
		}
	}
	/**
	 * 多选一
	 */
	private static void testMulti() {
		int age=12;
		if(age<10) {
			System.out.println("垂髫");
		}
		else if(age<20) {
			System.out.println("志学");
		}
		else if(age<30) {
			System.out.println("弱冠");
		}
		else if(age<40) {
			System.out.println("而立");
		}
		else if(age<50){
			System.out.println("不惑");
		}else if(age<70) {
			System.out.println("天命");
		}else {
			System.out.println("古稀");
		}
	}
	/**
	 * 双分支
	 */
	private static void testTwo() {
		int age=19;
		if(age<18) {//符合判别表达式执行体
			System.out.println("未成年");
		}else {//不符合判别表达式执行体
			System.out.println("成年");
		}
	}
	/**
	 * 单分支
	 */
	private static void testSinge() {
		int age=14;
//		注意：不要省略判断体的大括号，除非执行体和判别式再同一行
		if(age>=18) 
			System.out.println("成年");
//			System.out.println("可以干些成年的事");
	}
}
