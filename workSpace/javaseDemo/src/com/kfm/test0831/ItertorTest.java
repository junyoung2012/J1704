package com.kfm.test0831;

public class ItertorTest {

	public static void main(String[] args) {
//		testDoWhile();
//		testWhile();
//		testFor();
//		testDeadItertor();
		testExitItertor();
	}

	private static void testExitItertor() {
		int count=5;
		for (int i = 0; i < count; i++) {
//			if(i==3) continue;//跳出本次循环
//			if(i==3) break;//跳出本层循环
			if(i==3) return;//跳出函数
			System.out.println(i);
		}
	}

	private static void testDeadItertor() {
//		for(;;) {
//			
//		};
//		while(true) {
//			
//		};
	}

	private static void testFor() {
		int count=5;
//		循环初值;结束条件；自加器
		int i=0,j=3;
		for(;;) { //语法糖
			if(i<count) {
				System.out.println(i);
				i++;
			}
		}
	}

	private static void testWhile() {
		int count=5;
		int i=0;
		while(i<count) {
			System.out.println(i);
			i++;//循环变量自加
		}
	}

	private static void testDoWhile() {
		int count=5;
		int i=0;
		do {
			System.out.println(i);
			i++;//循环变量自加
		}while(i<count);
		
	}
}
