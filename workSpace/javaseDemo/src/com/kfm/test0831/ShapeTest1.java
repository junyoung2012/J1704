package com.kfm.test0831;
/**
 * 打印菱形
 * @author Admin
 *
 */
public class ShapeTest1 {

	private static final int ROW_COUNT = 7;
	private static final int COL_COUNT = 4;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		printShape();
	}

	private static void printShape() {
//		写代码的顺序
//		依照逻辑结构写代码（打破以往的印刷顺序写代码的习惯）
//		可以测试
		
//		1.确定打印区域
//		1.1 打印正确的行数
		for (int i = 0; i < ROW_COUNT; i++) {
//			System.out.print(i);//新的代码在这句中产生
			if (i<=3) {
//			前3行
				for (int j = 0; j < COL_COUNT+i; j++) {
//					System.out.print("* ");
					if(j>=COL_COUNT-1-i) {
						System.out.print("* ");
					}else {
						System.out.print("  ");
					}
				}
			} else {
//			后3行
				for (int j = 0; j < COL_COUNT+(6-i); j++) {
//					System.out.print("* ");
//					if(j>=COL_COUNT-1-(6-i)) {
					if(j>=COL_COUNT-7+i) {
						System.out.print("* ");
					}else {
						System.out.print("  ");
					}
				}
			}
			
//			System.out.print(i%4);
			System.out.println();//每一个行尾，都要换行
		}
//		2.在区域中，正确的确定分支结构（分别打印不同的字符）
		
	}
	

}
