package com.kfm.test0831;

public class TrigerTest {
	public static void main(String[] args) {
		printTriger();

	}
/**
 * 轻松，清晰，再别人的指导下，也觉得轻松了很多
 * why?
 * 1.大家太聪明(总不想步步为营)
 * 2.急于编程，是低质量代码的主要来源
 * 3.伪实现（代码） 你们做吗？
 * 4.尽早的做了测试（通过测试，尽早发现问题）
 * 5.正确的代码，就永远不要再折腾了。（你的目标太远大了）
 * 6.遇到了问题可以试验（需要预期目标吗？）
 * 7.小步快跑
 * 8.思索下，你以前编程，是不是抄写别人程序？？？
 */
	private static void printTriger() {
//		1.确定打印区域
//			1.1 打印正方形
//				1.1.1 先打印5行
		for (int i = 0; i < 5; i++) {
//				1.1.2 要打印5列
//			1.2 把正方形变形成梯形
			for (int j = 0; j < 5+i; j++) {
//		2.再打印区域里，根据条件打印*.或者是空格符
//				System.out.print("* ");
//				if (j>=4-i) {
				if (
						i==4//底边
						|| j==4+i//右斜边
						|| j==4-i//右斜边
						) {
					System.out.print("* ");
				} else {
					System.out.print("  ");
				}
			}
//			System.out.print("* ");
//			System.out.print("* ");
//			System.out.print("* ");
//			System.out.print("* ");
			System.out.println();
		}
		
	}

}
