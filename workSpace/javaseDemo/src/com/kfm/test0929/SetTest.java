package com.kfm.test0929;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class SetTest {

	public static void main(String[] args) {
		test2();
	}

	private static void test1() {
		Set<Object> set=new HashSet<>();//JDK7后支持的菱形写法
		System.out.println(set.add(1));
		System.out.println(set.add(1));
		System.out.println(set.add("敏而好学"));
		System.out.println(set.add("敏而好学"));
		System.out.println(set.add(new Date()));
		System.out.println(set.add(new Student(101, "韩梅梅")));
//		判断对象是否重复，判断的标准是equels???
		System.out.println(set.add(new Student(101, "韩梅梅1")));
		System.out.println(set);
		
	}
	private static void test2() {
		Set<Object> set=new TreeSet<>();//JDK7后支持的菱形写法
//		System.out.println(set.add(1));
//		System.out.println(set.add(1));
//		System.out.println(set.add(2));
//		System.out.println(set.add("敏而好学"));
//		System.out.println(set.add("敏而好学"));
//		System.out.println(set.add(new Date()));
		System.out.println(set.add(new Student(103, "韩梅梅")));
		System.out.println(set.add(new Student(102, "李华")));
		System.out.println(set);
		
	}

}
