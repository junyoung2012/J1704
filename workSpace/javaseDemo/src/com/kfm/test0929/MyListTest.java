package com.kfm.test0929;

import java.util.Vector;

import com.kfm.test0928.MyList;
import com.kfm.test0928.MyListImpl;

public class MyListTest {

	public static void main(String[] args) {
		MyListImpl list=new MyListImpl();
		list.add(1);
		list.add(3);
		list.add(5);
//		底层会使用Iterable接口，得到Iterator对象，实际上会调用next（）进行循环
		for (Object obj : list) {
			
		}
	}

}
