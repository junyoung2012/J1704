package com.kfm.test0929;

import java.util.Random;

public class Student implements Comparable<Student> {
	private int code;
	private int schoolId;
	private String name;
	public Student(int code, String name) {
		this.code = code;
		this.name = name;
	}
	
//	3 1*31+2 33
//	3 2*31+1 63
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + code;
		return result;
//		return new Random().nextInt(1000);
//		return 1;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Student other = (Student) obj;
		if (code != other.code)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Student [code=" + code + ", name=" + name + "]";
	}

//	@Override
//	public int hashCode() {
//		final int prime = 31;
//		int result = 1;
//		result = prime * result + code; //31+2 33       31+1 32
//		result = prime * result + schoolId;//33*31+1     32*31+2
//		return result;
//	}

//	@Override
//	public boolean equals(Object obj) {
//		if (this == obj)
//			return true;
//		if (obj == null)
//			return false;
//		if (getClass() != obj.getClass())
//			return false;
//		Student other = (Student) obj;
//		if (code != other.code)
//			return false;
//		if (schoolId != other.schoolId)
//			return false;
//		return true;
//	}

	@Override
	public int compareTo(Student o) {
		// TODO Auto-generated method stub
		return this.code - o.code;
	}
	
}
