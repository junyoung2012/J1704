package com.kfm.test0906;

public class Student2 {
	private int code;
	private String name;
	public Student2(){
		System.out.println(code);//null?0?,在这的时候对象有没有产生？产生了
	}
	
	public Student2(int code, String name) {
		this.code = code;
		this.name = name;
	}

	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public String toString() {
		return "Student2 [code=" + code + ", name=" + name + "]";
	}
	
}
