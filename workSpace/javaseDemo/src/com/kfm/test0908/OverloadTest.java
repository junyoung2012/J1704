package com.kfm.test0908;

public class OverloadTest extends Parent {

	public static void main(String[] args) {
		OverloadTest test=new OverloadTest();
//		da1(new BasketBall());
//		打（play）篮球
//		使用打篮球的打，打篮球
		test.da(new BasketBall());
//		打(buy)酱油
//		使用打酱油的打，来打酱油
		test.da(new Oil());
		
//		程序也能做到一个（同名）方法，有多种不同的实现 ？根据处理的对象不同，自己去定位，调用那种实现。
		
		int ret=test.test();
		test.test();
		byte b334=1;
		test.test(b334);
//		System.out.print(obj);
	}
//	OOP 
	public void da(BasketBall basketBall) {
		System.out.println("我在打篮球");
	}
	
	public void da(Oil oli) {
		System.out.println("我在打酱油");
	}
	
	
//	private void test() {
//		// TODO Auto-generated method stub
//
//	}
	private int test() {
		return -1;
	}
	private void test(int i) {
		// TODO Auto-generated method stub
		
	}
	private void test(String str) {
		// TODO Auto-generated method stub
		
	}
	private void test(String str,int i) {
		// TODO Auto-generated method stub
		
	}
	private void test(int i,String str) {
		// TODO Auto-generated method stub
		
	}
	//继承来的
//	public void test(byte b1) {
//		System.out.println("我就是那个byte");
//	}
//	private void test(int i1,String str1) {
//		// TODO Auto-generated method stub
//		
//	}
}
