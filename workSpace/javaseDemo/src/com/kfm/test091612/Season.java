package com.kfm.test091612;

@SuppressWarnings("rawtypes")
//public class Season extends Enum {
	public enum Season {
//	对象的指定
	Spring,Summer,Fall,Winter;//指定这个类只能有着4个对象
	private int i=1;
	Season(){
		
	}
	private void test1() {
		// TODO Auto-generated method stub

	}
	{
		
	}
	public static void main(String[] args) {
//		Season season=Season.Spring;//常量
//		Season season=Spring;//常量
		Season season=Season.valueOf("Spring");//通过字符串创建枚举对象
		System.out.println(season.ordinal());
		System.out.println(season.name());
		
		switch (season) {
			case Spring:
				break;
			case Summer:
				break;
			case Fall:
				break;
			case Winter:
				break;
			default:
				break;
		}
	}
}
