package com.kfm.test091612;

public class Sigleton {
//	饿汉模式
	private static Sigleton instance=new Sigleton();
	private Sigleton(){
		
	}
//	（对象）工厂方法
//	getSigleton
//	getInstace
//	getObject
	static Sigleton getInstace() {
//		饱汉模式
		if(instance==null) {
			instance=new Sigleton();
		}
		return instance ;
	}
	
}
class SigletonTest{
	public static void main(String[] args) {
		Sigleton sigleton1=Sigleton.getInstace();
		Sigleton sigleton2=Sigleton.getInstace();
		System.out.println(sigleton1==sigleton2);
	}
}
