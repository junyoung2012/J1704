package com.kfm.test0923;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class ExceptionTest {

	public static void main(String[] args) throws FileNotFoundException, IOException {
		ExceptionTest test=new ExceptionTest();
//		test.testTry();
//		test.testRuntimeExption();
//		test.testCheckedException();
//		test.testThrows();
		System.out.println(test.testReturn());
	}

	private void testThrows() throws FileNotFoundException,IOException {
		FileReader fileReader=null;
		String file="d:/test.txt";
		fileReader=new FileReader(file);
		fileReader.close();
	}

	private void testCheckedException() {
//		FileReader fileReader=null;
//			String file="d:/test.txt";
//			fileReader=new FileReader(file);
//		}catch (FileNotFoundException e) {
//			// TODO: handle exception
//		}finally {
//			try {
//				fileReader.close();
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}
	}
	private void testTryResource() {
		
		String file="d:/test.txt";
		try(FileReader fileReader=new FileReader(file)){
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	
	}



	private void testTry() {
		try {//异常可能发生的范围（不能省略）
			int input=0;
			int ret=12/input;
		}catch (ArithmeticException  e2) {
			// 进行异常处理的代码
			e2.printStackTrace();
//			System.out.println(e2.getMessage());
		}catch (Exception e) {
			// TODO: handle exception
		}
		
	}
	private void testRuntimeExption() {
			int input=0;
			int ret=12/input;
	}
	
	private int testReturn() {
		try {
			int input=0;
			int ret=12/input;
//			return 1;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return 2;
		}finally {
			return 3;
		}
//		return 4;
	}
	
	private void test1() {
//		catch all
		try {
			
		}
		catch (Exception e) {
			// TODO: handle exception
//			e.printStackTrace();
		}
	}

}
