package com.kfm.test0923;

public class ThrowTest {

	public static void main(String[] args) {
		try {
			testThrowException(15);
		} catch (MyException1 e) {
			e.printStackTrace();
		}
		System.out.println("ok");
	}

	private static void testThrowException(int age){
		MyException1 exception1=new MyException1("你还是太年轻了");
		if(age<18) {
			throw exception1;//异常对象被扔出，代表异常发生了
		}
		
	}

}
//自定义异常(自定义类，继承Exception或者RuntimeException)
class MyException1 extends RuntimeException{

	public MyException1(String string) {
		super(string);
	}
	
}
