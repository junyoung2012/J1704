package com.kfm.test0923;

public class ObjectTest {
	public static void main(String[] args) throws CloneNotSupportedException {
//		testToString();
//		testEqueals();
//		testEquals2();
		testClone();
	}

	private static void testEquals2() {
		Cat cat1=new Cat("开发喵");
		Cat cat2=new Cat("开发喵");
		System.out.println(cat1==cat2);//false
		System.out.println(cat1.equals(cat2));//false
		
		System.out.println(cat1.getName()==cat1.getName());//白盒比较
		System.out.println(cat1.equals(cat2));//黑盒比较，更体现OOP思想
		
		if((new Object()).equals(new Object())) {
			System.out.println("equal");
		}else {
			System.out.println("not equal");
		}
		if((new Cat("开发喵")).equals(new Cat("开发喵"))) {
			System.out.println("equal");
		}else {
			System.out.println("not equal");
		}
	}

	private static void testClone() throws CloneNotSupportedException {
		String[] array={"绿眼睛","金色"};
		Cat cat1=new Cat("开发喵",10,array);
		Cat cat2=(Cat) cat1.clone();
		System.out.println(cat1.equals(cat2));
		cat1.setWeight(8);
		System.out.println(cat2.getWeight());
		System.out.println(cat2.getArray()[0]);
//		cat2.getArray()[0]="蓝眼睛";
		array[0]="黑眼睛";
		System.out.println(cat2.getArray()[0]);
		System.out.println(cat1.getArray()[0]);
		
	}
	
	private static void testEqueals() {
		// TODO Auto-generated method stub
		
	}

	private static void testToString() {
		Cat cat=new Cat("开发喵");
		Cat cat2=new Cat("咪咪");
		System.out.println(cat);
		System.out.println(cat2.toString());
	}
}
class Cat extends Object implements Cloneable{
	private String name;
	private int weight;
	private String[] array;
	
	public Cat(String name) {
		this.name = name;
	}
	
	public Cat(String name, int weight) {
		this.name = name;
		this.weight = weight;
	}

	public Cat(String name, int weight, String[] array) {
		this.name = name;
		this.weight = weight;
		this.array = array;
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		//深克隆要自己实现。
		return super.clone();
	}
	@Override
	public String toString() {
		return "我是猫"+this.name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getWeight() {
		return weight;
	}
	public String[] getArray() {
		return array;
	}

	public void setArray(String[] array) {
		this.array = array;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;//自己一定等于自己
		if (obj == null)
			return false;//自己和null比较，返回false;null值保护
		if (getClass() != obj.getClass())//类型比较
			return false;
		Cat other = (Cat) obj;//不会ClassCastException
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
}
