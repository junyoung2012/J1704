package com.kfm.test0925;

public class StringBuilderTest {
	public static void main(String[] args) {
//		JDK1.0
//		StringBuffer 
//		JDK1.5
//		StringBuilder
		
		StringBuffer sb1=new StringBuffer("hello");
		StringBuffer sb2=new StringBuffer("hello");
		System.out.println(sb1==sb2);//false
		System.out.println(sb1.equals(sb2));//false
		
		String input="abc123abc456abc789";
		System.out.println(countStrCountInString(input,"abcd"));
	}
	
	public static int countStrCountInString(String input,String instr) {
		String[] array = input.split(instr);
		return array.length-1;
	}
}
