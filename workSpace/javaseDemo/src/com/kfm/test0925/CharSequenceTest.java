package com.kfm.test0925;

public class CharSequenceTest {

	public static void main(String[] args) {
		// testString();
//		testIntern();
		testIntern2();
	}

	/**
	 * 缓存思想 内存结构
	 */
	private static void testIntern() {
		String s1 = "Hello world";
		String s2 = new String(s1);
		// System.out.println(s1==s2);//false
		System.out.println(s1 == s2.intern());
		// 返回字符串对象的规范化表示形式。当调用 intern 方法时，如果池已经包含一个等于此 String 对象的字符串（用 equals(Object)
		// 方法确定），则返回池中的字符串。否则，将此 String 对象添加到池中，并返回此 String 对象的引用。
		// 其实就是String对象的常量缓存

		
	}
//	《深入理解java虚拟机》第二版 57页
	private static void testIntern2() {
		String str1 = new StringBuilder("计算机").append("软件").toString();
//		String str1 = new StringBuilder("计算机软件").toString();
		// String str3= new StringBuilder("计算机软件").toString();
		System.out.println(str1.intern() == str1);
		String str2 = new StringBuilder("Java(TM) SE ").append("Runtime Environment").toString();
		System.out.println(str2.intern() == str2);
	}

	private static void testString() {
		String s1 = "Hello world";
		String s2 = "Hello" + " world";
		System.out.println(s1.equals(s2));
		System.out.println(s1 == s2);// true
		String s3 = "Hello";
		String s4 = s3 + " world";// StringBulider对象
		System.out.println(s1 == s4);// false
		String s5 = new String("Hello world");
		String s6 = new String("Hello world");
		System.out.println(s1 == s5);// false
		System.out.println(s1 == s5);// false

	}

}

