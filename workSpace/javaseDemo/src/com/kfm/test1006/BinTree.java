package com.kfm.test1006;
/**
 * 二叉树
 * @author Admin
 * @param <T>
 *
 */
public class BinTree<T> {
	private Node<T> root;
	static class Node<T>{
		private T value;
//		父节点
		private Node<T> parent;
//		左子树
		private Node<T> leftChild;
//		右子树
		private Node<T> rightChild;
		public Node(T value) {
			this.value = value;
		}
		public T getValue() {
			return value;
		}
		public void setValue(T value) {
			this.value = value;
		}
	}
	
	boolean initial(T value) {
		if(root!=null) return false;
		root=new Node<>(value);
		return true;
	}
	
	boolean addLeft(Node<T> parent,Node<T> node) {
		if(parent==null) return false;
		parent.leftChild=node;
		return true;
	}
	
	boolean addRight(Node<T> parent,Node<T> node) {
		if(parent==null) return false;
		parent.rightChild=node;
		return true;
	}
	
	void itertor(Node<T> root,int level) {
		
//		System.out.println("-- "+root.leftChild.value);//左子树
		if(root.leftChild!=null) {
			itertor(root.leftChild,level+1);
		}
		if(root.rightChild!=null) {
			itertor(root.rightChild,level+1);
		}
//		System.out.println("-- "+root.rightChild.value);
		StringBuilder prefix=new StringBuilder();
		for (int i = 0; i < level; i++) {
			prefix.append("--");
		}
		prefix.append(" ");
		System.out.println(prefix.toString()+root.getValue());//根节点
	}
	
	public Node<T> getRoot() {
		return root;
	}

	public void setRoot(Node<T> root) {
		this.root = root;
	}

	public static void main(String[] args) {
		BinTree<Integer> tree=new BinTree<>();
		tree.initial(1);
		Node<Integer> node1 =new Node<>(-1);
		tree.addLeft(tree.getRoot(),node1);
		Node<Integer> node2 =new Node<>(2);
		tree.addRight(tree.getRoot(),node2);
		
		Node<Integer> node11 =new Node<>(-11);
		tree.addLeft(node1,node11);
		
		Node<Integer> node12 =new Node<>(-2);
		tree.addRight(node1,node12);
		
		tree.itertor(tree.getRoot(),0);
	}
	
}
