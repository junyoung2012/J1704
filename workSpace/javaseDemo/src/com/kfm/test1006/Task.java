package com.kfm.test1006;

public class Task implements Comparable<Task> {
	private String message;
	private int priority;

	public Task(String message) {
		this.message = message;
	}
	

	public Task(String message, int priority) {
		this.message = message;
		this.priority = priority;
	}


	@Override
	public String toString() {
		return "Task [message=" + message + "]";
	}

	@Override
	public int compareTo(Task o) {
		return priority-o.priority;
	}
	
}
