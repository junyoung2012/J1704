package com.kfm.test1006;

import java.util.ArrayList;
import java.util.List;

public class CompanysTest {
	static class Company{
		private String title;
		private Company parent;
		private List<Company> childs;
		public Company(String title, Company parent) {
			this.title = title;
			this.parent = parent;
			if(parent!=null ) {
				if(parent.childs==null) {
					parent.childs=new ArrayList<>();
				}
				parent.childs.add(this);
			}
		}
		
	}
	public static void main(String[] args) {
		Company root=new Company("股东大会", null);
//		List<Company> list=new ArrayList<>();
		Company company1=new Company("董事会", root);
		Company company2=new Company("监事会", root);
//		list.add(company1);
//		list.add(company2);
//		root.childs=list;
		
		Company company11=new Company("总经理", company1);
		Company company12=new Company("财务部", company1);
		Company company111=new Company("副总经理", company11);
		Company company112=new Company("副总经理", company11);
		Company company113=new Company("副总经理", company11);
		
		Company company1111=new Company("办公室", company111);
		Company company1112=new Company("企划部", company111);
		Company company1113=new Company("人力资源部", company111);
		Company company1114=new Company("市场拓展部", company111);
		
		Company company1121=new Company("营运部", company112);
		Company company1122=new Company("销售部", company112);
		Company company1123=new Company("品控部", company112);
		
		Company company1131=new Company("采购部", company113);
		Company company1132=new Company("工程部", company113);
		Company company1133=new Company("中央厨房", company113);
		Company company1134=new Company("研发部", company113);
		
		showCompanys(root,0);

	}
	private static void showCompanys(Company company, int level) {
		StringBuilder prefix=new StringBuilder();
		for (int i = 0; i < level; i++) {
			prefix.append("--");
		}
		System.out.println(prefix+company.title);
		if(company.childs==null) return;
		for (Company subCompany : company.childs) {
			showCompanys(subCompany,level+1);
		}
	}
}
