package com.kfm.test1006;

import java.util.PriorityQueue;

public class PriorityQueueTest {

	public static void main(String[] args) {
		PriorityQueue<Task> priorityQueue=new PriorityQueue<>();
		priorityQueue.add(new Task("收假了1",4));
		priorityQueue.add(new Task("收假了3",2));
		priorityQueue.add(new Task("收假了2",2));
		priorityQueue.add(new Task("吃饭了",1));
		
		System.out.println(priorityQueue.poll());
		System.out.println(priorityQueue.poll());
		System.out.println(priorityQueue.poll());
		System.out.println(priorityQueue.poll());
	}
}
