package com.kfm.test1006;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexpTest {

	public static void main(String[] args) {
		// test1();
//		 test2();
//		test3();
//		test4();
//		test5();
//		test6();
//		test7();
		test8();
	}
	private static void test8() {
		// 正则表达式的简化写法（语法糖）
//		捕获的语法糖
		String tip="1.注册1学生"
				+ "2.删除"
				+ "3.修改"
				+ "4.显示学生信息"
				+ "0.退出系统";
		print(tip);
		print("Greedy数量词为“贪婪的”，如名字一样，多吃多占，它会尽可能多的匹配字符，会回溯");
	}
	private static void print(String tip) {
		String regex="(\\B\\d\\.)";//捕获组
//		System.out.println(tip.replaceAll(regex, "--$0-----$1---------$2"));
		System.out.println(tip.replaceAll(regex, "\n$1"));
	}
	private static void test7() {
		String tip="请输入要注册的学生编号，姓名，出生日期，用逗号分开"
				+ "0：返回上级";
//		String input="1001,李四,1999-10-06";
//		String input="0";
		String input="1001,李四,1999-10-06";
		String regex="0|\\d{1,9}[,，][\\u4E00-\\u9FA5]{2,10}[,，]\\d{4}-\\d{2}-\\d{2}";
		System.out.println(input.matches(regex));
		System.out.println("1001,李四,1999-10-06".matches(regex)==true);
		System.out.println("1001李四,1999-10-06".matches(regex)==false);
		System.out.println("0".matches(regex)==true);
	}
//	逻辑运算
	private static void test6() {
		System.out.println("ab".matches("\\Sb")==true);
		System.out.println("ba".matches("\\Sb")==false);
		
		System.out.println("ba".matches("a|b")==false);
		System.out.println("b".matches("a|b")==true);
		System.out.println("a".matches("a|b")==true);
	}
	private static void test5() {
		String input="苹果，草莓,香蕉";
		String[] array=input.split("[,，]");
		System.out.println(Arrays.toString(array));
	}
//	边界条件
	private static void test4() {
		String regex = "a";
		System.out.println("abcabc".replaceAll(regex, "*").equals("*bc*bc"));
		regex = "^a";//不在方括号里，表示一行字符串开头
		System.out.println("abcabc".replaceAll(regex, "*").equals("*bcabc"));
		regex = "a$";//不在方括号里，表示一行字符串结尾
		System.out.println("abcabc".replaceAll(regex, "*").equals("abcabc"));
		System.out.println("abcabca".replaceAll(regex, "*").equals("abcabc*"));
		
		regex = "\\ba";//不在方括号里，单词边界
		System.out.println("abcabca abcabca".replaceAll(regex, "*").equals("*bcabca *bcabca"));
		
		regex = "a\\b";//不在方括号里，单词边界
		System.out.println("abcabca abcabca".replaceAll(regex, "*").equals("abcabc* abcabc*"));
		
		regex = "a\\B";//不在方括号里，非单词边界
		System.out.println("abcabca abcabca".replaceAll(regex, "*").equals("*bc*bca *bc*bca"));
		
		regex = "\\Aa";//表示一个字符串开头
//		System.out.println("abcabca abcabca".replaceAll(regex, "*"));
		
//		System.out.println("abcabca\r\nabcabca".replaceAll("^a", "*"));
//		System.out.println("abcabca\r\nabcabca".replaceAll("\\Aa", "*"));
//		
//		Pattern compile = Pattern.compile("\\Aa",Pattern.MULTILINE);
//		Matcher matcher = compile.matcher("abcabca\r\nabcabca");
//		System.out.println(matcher.replaceAll("*"));
		
		Pattern compile = Pattern.compile("a\\z",Pattern.MULTILINE);
		Matcher matcher = compile.matcher("abcabca\r\na"+'\u2028'+"bcabca");
		System.out.println(matcher.replaceAll("*"));
		
		
		
	}

	// 數量词
	private static void test3() {
		String regex = "a{3}";
		System.out.println("aaa".matches(regex) == true);
		System.out.println("aa".matches(regex) == false);
		System.out.println("aaaa".matches(regex) == false);
		System.out.println("aaab".matches(regex) == false);
		System.out.println("aabab".matches(regex) == false);

		regex = "a{0,3}";
		System.out.println("aa".matches(regex) == true);
		System.out.println("".matches(regex) == true);
		System.out.println("a".matches(regex) == true);
		System.out.println("aaa".matches(regex) == true);
		System.out.println("aaaa".matches(regex) == false);

		regex = "a{1,}";
		System.out.println("".matches(regex) == false);
		System.out.println("a".matches(regex) == true);
		System.out.println("b".matches(regex) == false);
		System.out.println("aaaaaaaaaaaaaaa".matches(regex) == true);

		regex = "a{0,4}";// 4个字符以下
		System.out.println("".matches(regex) == true);
		System.out.println("a".matches(regex) == true);
		System.out.println("aaaa".matches(regex) == true);
		System.out.println("aaaaa".matches(regex) == false);

		regex = "a*";// 0个或多个a{0,}
		System.out.println("".matches(regex) == true);
		System.out.println("a".matches(regex) == true);
		System.out.println("aaaa".matches(regex) == true);
		// 0个或1ge{0,1}
		regex = "a?";// 0个或多个a{0,}
		System.out.println("".matches(regex) == true);
		System.out.println("a".matches(regex) == true);
		System.out.println("aaaa".matches(regex) == false);

		// 0个或1个{1,}
		regex = "a+";// 0个或多个a{0,}
		System.out.println("".matches(regex) == false);
		System.out.println("a".matches(regex) == true);
		System.out.println("aaaa".matches(regex) == true);

	}

	// 字符，字符类
	private static void test2() {
		String regex = "a";
		System.out.println("a".matches(regex) == true);
		System.out.println("b".matches(regex) == false);
		regex = "\\\\";
		System.out.println("\\");
		System.out.println("\\".matches(regex) == true);

		regex = "\\x61";
		System.out.println("a".matches(regex) == true);

		regex = "\\u9999";
		System.out.println("香".matches(regex) == true);

		regex = "\\.";// 代表一个点
		System.out.println(".".matches(regex) == true);
		System.out.println("a".matches(regex) == false);

		// 下列是代表一类字符的一个
		regex = "[123]";// 多选一
		System.out.println("123".matches(regex) == false);
		System.out.println("1".matches(regex) == true);
		System.out.println("2".matches(regex) == true);
		System.out.println("3".matches(regex) == true);
		System.out.println("12".matches(regex) == false);
		System.out.println("[".matches(regex) == false);

		regex = "[^123]";// 不是123的任意一个字符
		System.out.println("123".matches(regex) == false);
		System.out.println("1".matches(regex) == false);
		System.out.println("2".matches(regex) == false);
		System.out.println("3".matches(regex) == false);
		System.out.println("4".matches(regex) == true);
		System.out.println("a".matches(regex) == true);
		System.out.println("".matches(regex) == false);
		// 字符类
		regex = "[a-z]";// 所有的英文小写字母
		System.out.println("abc".matches(regex) == false);
		System.out.println("a".matches(regex) == true);
		System.out.println("z".matches(regex) == true);
		System.out.println("1".matches(regex) == false);
		System.out.println("-".matches(regex) == false);
		System.out.println(" ".matches(regex) == false);

		regex = "[a-zA-Z]";// 所有的英文字母
		System.out.println("a".matches(regex) == true);
		System.out.println("A".matches(regex) == true);

		regex = "[a-zA-Z0-9]";// 所有的英文字母,数字 （常用于密码验证）
		System.out.println("a".matches(regex) == true);
		System.out.println("A".matches(regex) == true);
		System.out.println("5".matches(regex) == true);

		regex = "[a-d[m-p]]";// a 到 d 或 m 到 p：[a-dm-p]（并集）
		regex = "[a-z&&[def]]";// d、e、 f（交集）
		regex = "[a-z&&[^bc]]";// 不是bc的小写字母
		regex = "[a-z&&[^m-p]]";// 不是m-p的小写字母

		// 预定义的字符类
		regex = "\\d";// [0-9]
		regex = "\\D";// [^0-9]

		regex = "\\w";// 单词字符：[a-zA-Z_0-9] 注意还有一个下划线
		regex = "\\W";// 非单词字符：[^a-zA-Z_0-9]

		regex = "\\s";// 空白字符：[ \t\n\x0B\f\r] 注意还有空格
		regex = "\\S";// 非空白字符：[^ \t\n\x0B\f\r]
		regex = ".";// 任何字符（与行结束符可能匹配也可能不匹配）

		regex = "[\\u4E00-\\u9FA5]";// 汉字（简易判别）
		System.out.println("A".matches(regex) == false);
		System.out.println("汉".matches(regex) == true);
	}

	private static void test1() {
		String input = "123a";
		String regex = "\\d{4}";
		System.out.println(input.matches(regex));
	}
}
