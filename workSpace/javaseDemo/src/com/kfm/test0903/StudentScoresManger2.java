package com.kfm.test0903;

import java.util.Scanner;

public class StudentScoresManger2 {

	public static void main(String[] args) {
		//1.从输入获取我们的学生信息
//		使用什么数据结构来保存学生的信息？利用自定义类的数组，来保存多个，相同数据类型的学生信息
		Student[] students= getStudnetsFromInput();
//		Student student=getStudentFromInput();
//		也可以把信息，从箱子里拆出来
//		System.out.println(student.studentName);
//		System.out.println(student.chineseScore);
//		System.out.println(student.mathScore);
//		System.out.println(student.englishScore);
		
//		把信息装入到存储空间
//		saveStudent()
//		2.可以分别打印学生的信息
		printStudentScores2(students);

	}
	private static void printStudentScores(Student[] students) {
		System.out.println("姓名\t\t语文\t数学\t英语\t平均成绩");
		for (int i = 0; i < students.length; i++) {
			System.out.print(students[i].studentName+"\t\t");
			System.out.print(students[i].chineseScore+"\t");
			System.out.print(students[i].mathScore+"\t");
			System.out.print(students[i].englishScore+"\t");
			System.out.println();
		}
	}
	private static void printStudentScores2(Student[] students) {
		System.out.println("姓名\t\t语文\t数学\t英语\t平均成绩");
		for (int i = 0; i < students.length; i++) {
			System.out.println(students[i].getInfo());//面型对象可以让功能的实现，合理分配，让专业的人干专业的事情
		}
	}
	private static Student[] getStudnetsFromInput() {
		System.out.println("请输入需要保存学生的总数");
		Scanner scanner=new Scanner(System.in);
		int studentCount=scanner.nextInt();
		Student[] students=new Student[studentCount];//定义了一个装学生的大箱子
		for (int i = 0; i < students.length; i++) {
			students[i]=getStudentFromInput();//每次调用一个方法，获得一个学生信息装入一个小箱子（Student）
		}
		return students;
	}
	//	1
	private static void saveStudent(String studentName,
			int chineseScore,int mathScore,int englishScore,int score) {
		
	}
//	2 why？有什么好处？规整,消除耦合性，利于扩展
	private static void saveStudent(Student student) {
		
	}
	/**
	 * 从界面输入获得一个学生的信息
	 * 使用引用类型（类），可以把多个信息，组合在一个箱子当中传递
	 */
	static Student getStudentFromInput() {
		Scanner scanner=new Scanner(System.in);
		Student student=new Student();//装入箱子
		System.out.println("请输入学生的姓名");
		student.studentName=scanner.nextLine();
		System.out.println("请输入学生的语文成绩");
		student.chineseScore=scanner.nextInt();
		System.out.println("请输入学生的数学成绩");
		student.mathScore=scanner.nextInt();
		System.out.println("请输入学生的英语成绩");
		student.englishScore=scanner.nextInt();
		return student;//返回的是小写，小写是具体的对象
	}

}
//学生的信息包含4个内容
//1.姓名
//2.语文成绩
//3.数学成绩
//4.英语成绩
//打造自己的容器来保存学生信息
class Student{
	String studentName;
	int chineseScore;
	int mathScore;
	int englishScore;
	int sportScore;//体育成绩
	
	String getInfo() {
		return studentName+"\t"
				+ chineseScore+"\t"
				+ mathScore+"\t"
				+ englishScore+"\t"
				+ (chineseScore+mathScore+englishScore)/3;
	}
}