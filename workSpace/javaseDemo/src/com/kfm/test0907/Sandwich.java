package com.kfm.test0907;
/**
 * 肉
 * @author Admin
 *
 */
class Meal {
	public Meal() {
		super();
		System.out.println("Meal");
	}
}
/**
 * 面包
 * @author Admin
 *
 */
class Bread {
	public Bread() {
		System.out.println("Bread");
	}
}
//奶酪
class Cheese {
	public Cheese() {
		System.out.println("Cheese");
	}
}

class Lettuse {
	public Lettuse() {
		System.out.println("Lettuse");
	}
}
/**
 * 餐
 * @author Admin
 *
 */
class Lunch extends Meal {
	public Lunch() {
		super();
		System.out.println("Lunch");
	}
}
/**
 * 快餐
 * @author Admin
 *
 */
class PortableLunch extends Lunch {
	public PortableLunch() {
		super();
		System.out.println("PortableLunch");
	}
}
/**
 * 三明治
 * @author Admin
 *
 */
class Sandwich extends PortableLunch {
	Bread b = new Bread();
	Cheese c = new Cheese();
	Lettuse l = new Lettuse();
	
	public Sandwich() {
		super();
		System.out.println("Sandwich");
	}

	public static void main(String[] args) {
		new Sandwich();
	}
}
