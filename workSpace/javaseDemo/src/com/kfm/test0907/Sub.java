package com.kfm.test0907;

public class Sub extends Parent {
	public int i=6;
	public static void main(String[] args) {
		Sub sub=new Sub();
		System.out.println(sub.i);
		sub.test1();
	}
	
	public Sub() {
		super(5);
		System.out.println("Sub()");
	}
	private void test1() {
		System.out.println(super.i);
	}
}
