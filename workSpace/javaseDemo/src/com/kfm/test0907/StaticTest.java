package com.kfm.test0907;

public class StaticTest {
	private String str1="实例（对象）成员变量";
	private static String str2="（类）成员变量";
	
	public StaticTest() {
		super();
	}
	StaticTest(String str1){//不行
		this.str1=str1;
	}
	//static的类外访问方式
	private static void test1() {
//		static int i=2;//静态变量（java不支持）
//		System.out.println(str1);//另外的一个类，不能直接访问实例变量
		StaticTest test=new StaticTest();
		System.out.println(test.str1);//实例变量在其它类必须通过对象访问
//		System.out.println(StaticTest.str1);//实例变量在其它类必须通过对象访问，不能通过类名
		System.out.println(StaticTest.str2);//在其它类访问类变量 可以直接使用类名.类变量访问
		System.out.println(test.str2);//在其它类访问类变量 可以直接使用对象.类变量访问
		StaticTest.InnerClass innerClass=new StaticTest.InnerClass();
		StaticTest.InnerClass2 innerClass2=test.new InnerClass2();
	}
//	static的本类访问方式
	private void test2() {//实例方法
//		System.out.println(str1);//实例方法，可以直接访问实例变量
		System.out.println(str2);//实例方法，也可以直接访问类变量
//		test3();
	}
//	static的本类访问方式
	private static void test3() {//类方法
//		System.out.println(str1);//类方法，可不能直接访问实例变量
		System.out.println(StaticTest.str2);//方法，也可以直接访问类变量
		System.out.println(str2);//方法，也可以不加类名前缀直接访问类变量
	}
	
	private static class InnerClass{
		
	}
	private  class InnerClass2{
		
	}
	
	static {
//		System.out.println("代码块");
		System.out.println("只会在类的加载时，执行一次");
		System.out.println("这里适合进行类变量的初始化");
	}
	
	{
		str1="这里是不是也可以初始化";
	}
//	static @Override //不行
//	public String toString() {
//		// TODO Auto-generated method stub
//		return super.toString();
//	}
	public static void main(String[] args) {//偷懒，不写另外的测试类
//		test1();
		test4();
	}
	//研究一下共享性
	private static void test4() {
		StaticTest test1=new StaticTest();
		test1.str1="看看值变不变？？？";
//		System.out.println(test1.str1);
		StaticTest test2=new StaticTest();
//		System.out.println(test2.str1);
//		StaticTest.str2="我改变整个世界";
		test1.str2="这样行不行???";
		System.out.println(test1.str2);
		System.out.println(test2.str2);
		System.out.println(StaticTest.str2);
	}
}
