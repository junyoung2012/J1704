package com.kfm.test0907;

public class ThisTest {
	private int a;
	private int b;
	private String name;
	private int i=0;

	public ThisTest() {
		System.out.println("构造方法1");
	}
	public ThisTest(int a) {
		this.a=a;
		System.out.println("构造方法2");
	}
	public ThisTest(int _a,int _b) {
//		this();
		this(_a);
		b=_b;
		System.out.println("构造方法3");
	}
	
	public static void main(String[] args) {
//		ThisTest test=new ThisTest(2);
//		System.out.println(test.a);
//		ThisTest test2=new ThisTest(3);
//		System.out.println(test.a);
		ThisTest test=new ThisTest();
		test.increment().increment().increment().increment();
		System.out.println(test.i);
	}
	private ThisTest increment() {
		this.i++;
		return this;
	}
	private static void test1() {
//		System.out.println(this);
//		System.out.println(this.a);
//		System.out.println(a);
	}
	
}
