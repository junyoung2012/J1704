package com.kfm.test0928;

import java.util.Random;

public class RedPackageTest {

	public static void main(String[] args) {
		int memony=1000;//
		int count=5;
		int min=1;
		
		int[] packages=getRedPackages(memony,count,min);
		showPackages(packages);
	}

	private static void showPackages(int[] packages) {
		if(packages==null) {
			System.out.println("手气不行，没有红包");
			return;
		}
		System.out.println(String.format("一共%d个红包", packages.length));
		for (int i = 0; i < packages.length; i++) {
			System.out.println(String.format("第%d个红包,金额%.2f", 
					i+1,packages[i]/100.0));
		}
	}

	private static int[] getRedPackages(int memony, int count,int min) {
		if(memony<count) return null;
		
		//红包必须每个人都有，
		int[] packages=new int[count];
		
//		给每个人发基础红包[min,平分/向下取整]
//		3-9
//		0,6+3
		Random random = new Random();
		int minMoney=random.nextInt(memony/count-min)+min;
//		System.out.println(minMoney);
//		把基础红包计入结果
		for (int i = 0; i < packages.length; i++) {
			packages[i]=minMoney;
		}
//		减少余额
		memony-=minMoney*count;
//		红包的金额不能为0
		while(true) {
//			随机钱
			int randomMeony=random.nextInt(memony);
			System.out.println(randomMeony);
//			减余额
			memony-=randomMeony;
//			装进红包
			packages[random.nextInt(count)]+=randomMeony;
			if(memony<=min) {
//				todo
				packages[random.nextInt(count)]+=memony;
				break;
			}
		}
		return packages;
	}
}
