package com.kfm.test0928;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class MyListImpl implements MyList,Iterable {
	private List list=new ArrayList();
	
	@Override
	public boolean add(Object object) {
		return list.add(object);
	}

	@Override
	public boolean add(int index, Object object) {
		try {
			list.add(index, object);
			return true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean addAll(List Objects) {
		return list.addAll(Objects);
	}

	@Override
	public boolean clear() {
		list.clear();
		return true;
	}

	@Override
	public Object get(int index) {
		return list.get(index);
	}

	@Override
	public String toString() {
		return list.toString();
	}

	@Override
	public boolean addAll(int index, List Objects) {
		return list.addAll(index,Objects);
	}

	@Override
	public Object remove(int index) {
		// TODO Auto-generated method stub
		Object remove = list.remove(index);
		return remove;
	}

	@Override
	public boolean remove(Object object) {
		return list.remove(object);
	}

	@Override
	public boolean removeAll(Collection Objects) {
		return list.removeAll(Objects);
	}

	@Override
	public Object set(int index, Object object) {
		return list.set(index, object);
	}

	@Override
	public Object[] toArray() {
		return list.toArray();
	}

	@Override
	public boolean contains(Object o) {
		return list.contains(o);
	}

	@Override
	public boolean containsAll(Collection c) {
		return list.containsAll(c);
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		return list.retainAll(c);
	}

	@Override
	public Iterator iterator() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
}
