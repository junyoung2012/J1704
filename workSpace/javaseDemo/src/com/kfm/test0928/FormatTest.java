package com.kfm.test0928;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.MessageFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FormatTest {

	public static void main(String[] args) throws ParseException {
//		testDateFormat();
//		testDateFormat1();
//		testNumber();
//		testNumber1();
//		testNumber2();
		testMessageFormat();
	}

	private static void testMessageFormat() {
//		String.format(format, args)
		int planet = 7;
		String event = "a disturbance in the Force";
		String result = MessageFormat.format(
		     "At {1,time} on {1,date}, there was {2} on planet {0,number,integer}.",
		     planet, new Date(), event);

		System.out.println(result);
	}

	private static void testNumber2() {
		int code=2;
		NumberFormat numberFormat=new DecimalFormat("00000");
		System.out.println(numberFormat.format(code));
	}

	private static void testNumber1() throws ParseException {
		String input="$10,234,567.87";
		NumberFormat numberFormat=new DecimalFormat("$###,000,000.00");
		double n=(double) numberFormat.parse(input);
		System.out.println(n);
	}

	private static void testNumber() {
//		double num=10234567.865;
		double num=1.023456787E7;
//		��001��234��567.89
//		NumberFormat numberFormat=new DecimalFormat("$000,000,000.##");
		NumberFormat numberFormat=new DecimalFormat("$###,000,000.00");
		System.out.println(numberFormat.format(num));
	}

	private static void testDateFormat1() {
		Date date=new Date();
//		DateFormat dateFormat=new SimpleDateFormat("E");//���ڶ�
//		DateFormat dateFormat=new SimpleDateFormat("F");//4
		DateFormat dateFormat=new SimpleDateFormat("a");//����
		System.out.println(dateFormat.format(date));
		
	}

	private static void testDateFormat() throws ParseException {
		String input="2012-12-31";
//		String input="20121231";
		String pattern="yyyy-MM-dd";
		DateFormat dateFormat =new SimpleDateFormat(pattern);
		Date date=dateFormat.parse(input);
		System.out.println(date);
	}

}
