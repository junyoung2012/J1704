package com.kfm.test0928;

import java.util.Collection;
import java.util.List;

/**
 * 一个有序可重的集合
 * @author Admin
 *
 */
public interface MyList {
	boolean add(Object object);
	boolean add(int index,Object object);
	boolean addAll(List Objects);
	boolean addAll(int index,List Objects);
	
	boolean  clear();//清空
	Object  remove(int index);
	boolean  remove(Object object);
	boolean  removeAll(Collection Objects);
	
	Object set(int index,Object object);
	
	
	Object[] toArray();
	
	Object get(int index);
//	欠一个遍历（是需要依靠其它对象）
	
	boolean contains(Object o);//是否包含
	boolean containsAll(Collection c);
	
	boolean retainAll(Collection<?> c);//求交集
	
}
