package com.kfm.test0928;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.TreeMap;

import org.junit.Test;

public class MyListImplTest {
	private MyListImpl list=new MyListImpl();
	@Test
	public void testAddObject() {
		assertTrue(list.add(new Object()));
		assertTrue(list.add(new Date()));
		assertTrue(list.add(1));
		assertTrue(list.add(2));
		assertTrue(list.add(3));
		assertTrue(list.add(4));
		assertTrue(list.add(new Person("����", 32)));
		System.out.println(list);
	}

	@Test
	public void testAddIntObject() {
		assertTrue(list.add(1));
		assertTrue(list.add(2));
		assertTrue(list.add(3));
		assertTrue(list.add(4));
		assertTrue(list.add(1,new Person("����", 32)));
		System.out.println(list);
		assertFalse(list.add(99,new Person("����", 32)));
	}

	@Test
	public void testAddList() {
		List list1=new ArrayList<>();
		list1.add("abcd");
		list1.add("eg");
		
		assertTrue(list.add(1));
		assertTrue(list.add(2));
		assertTrue(list.add(3));
		assertTrue(list.add(4));
		assertTrue(list.addAll(1,list1));
		System.out.println(list);
	}

	@Test
	public void testClear() {
		assertTrue(list.add(1));
		assertTrue(list.add(2));
		assertTrue(list.add(3));
		assertTrue(list.add(4));
		System.out.println(list);
		list.clear();
		System.out.println(list);
		
	}

	@Test
	public void testDeleteInt() {
//		list.remove(index);//1
//		list.remove(object);//
		assertTrue(list.add(1));
		assertTrue(list.add(2));
//		assertTrue(list.add(3));
//		assertTrue(list.add(4));
		Integer integer=2;
		System.out.println(list.remove(2));
		System.out.println(list.remove((Object)2));
//		System.out.println(list.remove(integer));
		System.out.println(list);
	}

	@Test
	public void testDeleteObject() {
		list.add("abcd");
		list.add("eg");
		System.out.println(list);
		list.remove("eg");
		System.out.println(list);
	}

	@Test
	public void testDeleteList() {
		fail("Not yet implemented");
	}

	@Test
	public void testUpdate() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetAll() {
		fail("Not yet implemented");
	}

	@Test
	public void testGet() {
		list.add("aaa");
		list.add("bbb");
		list.add("bbb");
		list.add("ddd");
		System.out.println(list.get(2));
	}
	@Test
	public void testContrain() {
		list.add("aaa");
		list.add("bbb");
		list.add("bbb");
		list.add("ddd");
		System.out.println(list.contains("ddd"));
	}
	
	@Test
	public void testContrainAll() {
		List list1=Arrays.asList("aaa","bbb","eee");
		list.add("aaa");
		list.add("bbb");
		list.add("bbb");
		list.add("ddd");
		System.out.println(list.containsAll(list1));//ȫ��������ȱһ����
	}
	
	@Test
	public void testRetainAll() {
		List list1=Arrays.asList("aaa","bbb","eee");
		list.add("aaa");
		list.add("bbb");
		list.add("bbb");
		list.add("ddd");
		System.out.println(list.retainAll(list1));
		System.out.println(list);
	}
	
	@Test
	public void testIterator1() {
		List list=new ArrayList<>();
		list.add(new Person("����", 21));
		list.add(new Person("����", 21));
		list.add(new Person("���", 21));
		list.add(new Person("����", 21));
		
		for (int i = 0; i < list.size(); i++) {
			Person person=(Person) list.get(i);
			System.out.println(person);
		}
	}
	
	@Test
	public void testIterator2() {
		List list=new ArrayList<>();
		list.add(new Person("����", 21));
		list.add(new Person("����", 22));
		list.add(new Person("���", 23));
		list.add(new Person("����", 24));
		
		for (Object object : list) {
			Person person=(Person)object;
			System.out.println(person);
		}
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Test
	public void testIterator3() {
		List list=new ArrayList<>();
		list.add(new Person("����", 21));
		list.add(new Person("����", 22));
		list.add(new Person("���", 23));
		list.add(new Person("����", 24));
		
		Iterator iterator = list.iterator();
		
		while(iterator.hasNext()) {
			Person person=(Person) iterator.next();
			System.out.println(person);
		}
		
	}
	
	@Test
	public void testIterator4() {
		List<Person> list=new ArrayList<Person>();
		list.add(new Person("����", 21));
		list.add(new Person("����", 22));
		list.add(new Person("���", 23));
		list.add(new Person("����", 24));
//		list.add(new Date());//�������
//		list.add(1);//�������
		
		for (Person person : list) {
			System.out.println(person);
		}
	}


}
