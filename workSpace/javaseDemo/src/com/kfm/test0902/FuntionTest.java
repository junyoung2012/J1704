package com.kfm.test0902;

public abstract class FuntionTest {

	public static void main(String[] args) {
//		函数的定义方式与要素(FunctionTest)
//
//		1. 函数名称（必须）
		测试();
//		2. 函数返回类型（必须）
//		3. 函数的形参（必须）
//		4. 权限修饰符（暂未学习）
//		5.方法体
	}

	private static void 测试() {
		// 汉语其实也可以作为函数名称
		
	}

//	private static void () {
//		// 函数名称是必须的
//		
//	}
//	方法也可以没有方法体
	abstract int testFunctionNoBoby();
}
