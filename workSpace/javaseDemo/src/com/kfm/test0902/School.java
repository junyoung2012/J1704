package com.kfm.test0902;
/**
 * 学校类
 * @author Admin
 *
 */
public class School {
	String name;//学校名称(属性)
	
//	一个学校有多个班级 怎么记录？
//	班级，有很多的属性和行为
//	类就是那其它的数据类型，组合的新数据类型
//	存复杂的数据，自己定义一个类比较方便
//	Grade grade1;//这样只能保存一个班级
//	Grade grade2;//
//	Grade grade3;//
//	Grade grade4;//
	
	Grade[] grades;
//	定义了方法
	void beginStudy() {
		System.out.println("老师同学们召开了隆重的开营仪式");
	}
	
//	什么时候定义属性？什么时候定义方法？
//	成员变量时表达类的特征，和属性 定义成成员变量
//	主题（这一类）能完成的动作 定义成方法
	
}
