package com.kfm.test0914;

class Super {
	Super(int i) {
		System.out.print("Super" + i + ",");
	}
}

public class Sub extends Super {
	static Super s1 = new Super(1);
	Super s2 = new Super(3);

	Sub(int i) {
		super(i);
		System.out.print("Sub" + i + ",");
	}

	public static void main(String[] args) {
		new Sub(2);
		
	}
}