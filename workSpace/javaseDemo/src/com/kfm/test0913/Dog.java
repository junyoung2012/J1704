package com.kfm.test0913;

public class Dog {

}
class YellowDog extends Dog{
	
}
class DogTest{
	public static void main(String[] args) {
		Dog dog=new Dog();
		Dog dog2=new YellowDog();
		
		YellowDog yellowDog=(YellowDog) dog2;
//		向下转型，一定要确定对象的真实数据类型
//		判断该对象是不是该类型对象
		System.out.println(dog instanceof Dog);
		System.out.println(dog instanceof YellowDog);
		if(dog instanceof YellowDog) {//向下转型，先判断真实类型，再转换
			YellowDog yellowDog2=(YellowDog) dog;//类转换异常
		}
	}
}