package com.kfm.test0901;

public class JIeChengTest {

	public static void main(String[] args) {
		System.out.println(getJieCheng(-1)==1);
		System.out.println(getJieCheng(1)==1);
		System.out.println(getJieCheng(2)==2);
		System.out.println(getJieCheng(3)==6);
		System.out.println(getJieCheng(4)==24);

	}
//	阶乘是一个结果很大的数字（计算100的阶乘）
	private static long getJieCheng(long num) {
//		if(num<=1)return 1;//1的阶乘等于1
		if (num<=1) {
			return 1;
		} else {
			return getJieCheng(num-1)*num;
		}	
	}

}
