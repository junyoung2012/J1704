package com.kfm.test0901;

public class SelectSort {
	public static void main(String[] args) {
//		1.初始化数组
		int[] array= {5,-3,6,1,8,2};
//		2.对数组进行排序
		sortBySelect(array);
//		3.打印我们的排序结果
		printArray(array);
	}

	private static void printArray(int[] array) {
		for (int i = 0; i < array.length; i++) {
			System.out.print(array[i]+" ");
		}
		System.out.println();
	}

	private static void sortBySelect(int[] array) {
//		1.确定排序的趟数
		for (int i = 0; i < array.length-1; i++) {
//			2.进行排序操作
//			2.1定义最新的牌位置为这一趟，最开头的位置
			int minPos=i;
//			2.2 遍历后面所有的元素
			for (int j = i+1; j < array.length; j++) {
//				2.2判断当前值有没有小于最小值
				if(array[j]<array[minPos]) {
//					2.2.1 符合提交 当前位置记录minPOs
					minPos=j;
				}
			}
//			3.我已经找到最小值的下标
//			3.1把最小值下标对应的值，换到 第i个位置
//			第三变量1交换
			int temp;
			temp=array[minPos];
			array[minPos]=array[i];
			array[i]=temp;
		}
	}
}
