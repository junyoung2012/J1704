package com.kfm.test0901;

public class NarcTest {
	public static void main(String[] args) {
		printNarcs();
//		isNarc(1234);
//		153，370，371，407
//		System.out.println(isNarc(0)==false);//
//		System.out.println(isNarc(-1)==false);
//		System.out.println(isNarc(153)==true);
//		System.out.println(isNarc(370)==true);
//		System.out.println(isNarc(371)==true);
//		System.out.println(isNarc(407)==true);
//		System.out.println(isNarc(1234)==false);
//		System.out.println(isNarc(123)==false);
	}

	private static void printNarcs() {
		//1.遍历所有的三位数
		for (int i = 100; i < 1000; i++) {
//		2.判断这个数字是不是水仙花数
			if(isNarc(i)) {
//			2.1 是，打印出来
				System.out.println(i);
			}
		}
	}

	private static boolean isNarc(int num) {
//		做个范围保护
		if(num<100 || num>999) return false;
//		1.分别获取百，十，个的数字
		int geWei=num /1 % 10;
		int shiWei=num / 10% 10;
		int baiWei=num / 100% 10;
//		System.out.println(baiWei);
//		2.判别百位的立方+十位的立方+个位的立方之和是否等于本数
//		if (condition) {
//			return true;
//		} else {
//			return false;
//		}
		return power3(baiWei)+power3(shiWei)+power3(geWei)==num;
	}
	/**
	 * 求取数字的立方
	 * @param baiWei
	 * @return
	 */
	private static int power3(int num) {
		// TODO Auto-generated method stub
		return num*num*num;
	}
}
