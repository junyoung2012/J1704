package com.kfm.test0901;

public class ArrayTest {
	public static void main(String[] args) {
//		testDefine();
//		testInitial();
//		testArrayReadAndWrite();
		testMultiDimesions();
	}
//	多维数组（没有多维数组，元素是数组类型的数组）
	private static void testMultiDimesions() {
		int[][] array= {{1,2,3,4},{9,10},{5,6,7,8}};
//		int[][] array1=new int[3][5];
		int[][] array1=new int[3][];//why?
//		System.out.println(array[0][2]);
		
		printArray(array);
	}
	private static void printArray(int[][] array) {
		for (int i = 0; i < array.length; i++) {
//			System.out.println(array[i]);
			for (int j = 0; j < array[i].length; j++) {
				System.out.print(array[i][j]+"\t");
			}
			System.out.println();
		}
	}
	private static void testArrayReadAndWrite() {
		int[] array=new int[5];
		array[0] = 23;//数组是base 0,下标从0开始
//		System.out.println(array[0] );
//		array[5]=444;//不行，越界了，0-4 ，只要操作array[长度]，一定越界
//		System.out.println(array[5]); 
		array=new int[7];//数组的长度一经初始化，永远不能改变，数组长度不能变，对你可以换数组
		array[6]=444;
//		System.out.println(array[6]); 
//		System.out.println(array[0] );
//		for (int i = 0; i < array.length; i++) {
//			System.out.println(array[i]);
//		}
		for (int current : array) {//foreach简单，确定木有下标
			System.out.println(current);
		}
	}
	/**
	 * 数组的初始化
	 */
	private static void testInitial() {
		//定义一个数组相当于有了一个地址，但是这个地址的大楼还没有创建呢
		String[] array= {"甲","乙","丙","丁","戊","己"};//静态（编译过程）初始化
		
//		System.out.println(array.length);
		int[] array2=new int[5];//该数组分配内存空间，动态初始化（运行后初始化）
		for (int i = 0; i < array2.length; i++) {
			System.out.println(array2[i]);
		}
//		System.out.println(array2[0]);
	}
	/**
	 * 数组的定义
	 */
	private static void testDefine() {
		int[] array1;//第一种(强烈推荐使用此类定义方式)
		int array2[];//第二种
//		int array4StudentScores[];
		int[] array4StudentScores;
		int[][] array;//定义二维数组
		int[][][] array3;//定义三维数组
	}
	
	
}
