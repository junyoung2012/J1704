package com.kfm.test0901;

public class FabTest {

	public static void main(String[] args) {
//		首先书写测试用例，然后再实现
//		敏捷开发（测试驱动开发TDD）
//		斯托沃客
		System.out.println(fab(1)==1);
		System.out.println(fab(2)==1);
		System.out.println(fab(3)==2);
		System.out.println(fab(4)==3);
		System.out.println(fab(5)==5);
		System.out.println(fab(6)==8);
	}

	private static int fab(int number) {
		// 1.前2项 值是1
//		   2. fab(n)=fab(n-1)+fab(n-2)
		if (number<3) {
			return 1;
		} else {
			return fab(number-1)+fab(number-2);
		}
	}
}
