package com.kfm.test0916;
/**
 * 设计模式原则
 * 最小知识原则
 * 里氏代换原则
 * 接口隔离原则
 * 合成复用原则
 * @author Admin
 *
 */
public interface IntefaceD1 {
	default void test1() {
		System.out.println("晕，JDK8以后，接口的方法有可以有带方法体默认实现方法");
	};
	void test2();
	void test3();
	void test4();
	void test5();
//	private void test6() {
//		
//	}
	static void test6() {
		
	}
	
}
interface IntefaceD2 {

	void test6();
	void test7();
	void test8();
	void test9();
	void test10();
}
/**
 * 一个有IntefaceD1接口默认方法实现的类（适配类）
 * @author Admin
 *
 */
abstract class  IntefaceD1Adaper implements IntefaceD1{
//	
	@Override
	public void test1() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void test2() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void test3() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void test4() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void test5() {
		// TODO Auto-generated method stub
		
	}
}
class InterfaceD1Test extends IntefaceD1Adaper{
	@Override
	public void test2() {
		// TODO Auto-generated method stub
		
	}
	
}
