package com.kfm.test0916;
/**
 * 接口是可以多继承的
 * @author Admin
 *
 */
public interface InterfaceC extends InterfaceA,InterfaceB {
	
}
class Cx implements InterfaceC,InterfaceA{

	@Override
	public void a() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void b() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void c() {
		// TODO Auto-generated method stub
		
	}
	
}