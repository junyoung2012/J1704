package com.kfm.test0916;

public class Test {
	public static void main(String[] args) throws ClassNotFoundException {
//		System.out.println(Bx.c);
//		new Bx();
		String className="com.kfm.test0916.Bx";
		Class.forName(className);
	}

}
class AX{
	static {
		System.out.println("A");
	}
}
class Bx extends AX{
	public static final String c="C";
	static {
		System.out.println("B");
	}
}
/*
static代码块什么时候会被执行？
类什么时候被虚拟机加载？
	1.首次new对象,子类对象加载会引起父类，超类的加载。
	2.访问static成员，也会引起class加载
	3.利用反射技术进行加载
	
*/
