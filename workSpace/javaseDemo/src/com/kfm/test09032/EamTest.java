package com.kfm.test09032;

public class EamTest {

	public static void main(String[] args) {
//		int      i     = 1;基本值
		Student student1=new Student();
		student1.code=1001;
		student1.name="冉有";
		Student student2=new Student();
		student2.code=1002;
		student2.name="子贡";
		
//		System.out.println(student1.code+ "  "+student1.name);
//		System.out.println(student2.code+ "  "+student2.name);
		System.out.println(student1.toString());
		System.out.println(student2.toString());
		
		Grade grade=new Grade();
		grade.code=1702;
		grade.title="java全栈班";
		System.out.println(grade.toString());
//		Student[] students2=new Student[3];
//		students2[0]=student1;
//		students2[1]=student2;
		grade.students=new Student[2];
		grade.students[0]=student1;
		grade.students[1]=student2;
//		grade.students=students2;
		
		for (int i = 0; i < grade.students.length; i++) {
			System.out.println(grade.students[i].toString());
		}
//		System.out.println(grade.students[0].toString());
		
	}

}
