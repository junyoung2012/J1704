package com.kfm.test0924;

public class StrignBufferTest {

	public static void main(String[] args) {
		testAdd();
		testDelete();
		testUpdate();
		testQuery();
	}

	private static void testUpdate() {
//		replace(int start, int end, String str) 
//		reverse() 
		StringBuffer sb=new StringBuffer("123456");
//		反转操作
//		System.out.println(sb.reverse());
//		sb.setCharAt(1, 'b');
//		替换字符
//		sb.subSequence(2, 4);
//		sb.substring(2);
		sb.substring(2,99);
		System.out.println(sb);
//		转换成字符串
	}

	private static void testQuery() {
		//indexOf(String str)
//		lastIndexOf(String str) 
//		length() 

		
		
	}

	private static void testDelete() {
		StringBuffer sb=new StringBuffer("123456");
		sb.delete(1, 3);//左包右不包（包括begin，但是不包括end位置）
		System.out.println(sb);
		
	}

	private static void testAdd() {
//		最后追加字符
		StringBuffer stringBuffer=new StringBuffer();
		stringBuffer.append("Hello").append(" World");//优雅的链式编程
//		StringBuffer sb=new StringBuffer();
//		for (int i = 0; i < 7; i++) {
//			System.out.println(sb.append("* "));
//		}
//		任意位置插入
//		insert(int offset, boolean b) 
		stringBuffer.insert(3, "123456");
		System.out.println(stringBuffer);
		
	}

}
