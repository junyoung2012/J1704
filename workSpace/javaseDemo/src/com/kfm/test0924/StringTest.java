package com.kfm.test0924;

import java.io.UnsupportedEncodingException;


public class StringTest {

	public static void main(String[] args) throws UnsupportedEncodingException {
//		testStringConstruct();
		testAdd();
		testDelete();
		testUpdate();
		testQuery();
		
	}

	private static void testQuery() {
		// charAt(int index) 返回指定索引处的 char 值。
//		截取
		String s1="a456103456";
//		System.out.println(s1.charAt(0));//base?char?uncode?StringIndexOutOfBoundsException
//		System.out.println(s1.codePointAt(0));//base?char?uncode?StringIndexOutOfBoundsException
		
//		比较
//		==也可以比较字符串
		String s2="a723456";
		String s3="A723456";
//		System.out.println(s1.compareTo(s2));
//		
//		System.out.println(s2.equals(s3)==false);
//		System.out.println("A723456".equals(s3)==true);
//		
//		CharSequence sequence=new StringBuffer("A723456");
//		System.out.println("A723456".contentEquals(sequence)==true);
//		
//		boolean equals(Object anObject) 
//                   将此字符串与指定的对象比较。 
//        boolean equalsIgnoreCase(String anotherString) 
//        将此 String 与另一个 String 比较，不考虑大小写 

//		compareToIgnoreCase(String str) 
//        按字典顺序比较两个字符串，不考虑大小写。
//		System.out.println(s2.compareToIgnoreCase(s3)==0);//验证码，用户名一般都忽略大小写
//		比较字符串局部是否相等
//		 boolean regionMatches(int toffset, String other, int ooffset, int len)  
		System.out.println(s2.regionMatches(1, s3, 1, 6)==true);
//		判断包含
//		contains
//		System.out.println(s2.contains("7234")==true);
//		System.out.println(s2.contains("17234")==false);
//		返回hash码
		System.out.println(s1.hashCode() );
		
//		System.out.println(s1.indexOf(97)==0);
//		System.out.println(s1.indexOf("a")==0);
//		查找子串
		System.out.println(s1.indexOf("a103")==0);
		System.out.println(s1.indexOf("a13")==-1);//找不到返回-1
		System.out.println(s1.lastIndexOf("456")==7);//倒查找，正计数
//		是否是空的
//		System.out.println(s1.isEmpty());
//		获取长度
//		System.out.println(s1.length());
		
//		是否以某字符开头
		System.out.println("陕A-12345".startsWith("陕A")==true);
//		是否以某字符结束
		System.out.println("世说新语.txt".endsWith(".txt")==true);
	}

	private static void testUpdate() {
		String name="张三";
		int age=20;
//		System.out.println("我是"+name+",我今年"+age+"岁了，至今未婚");
//		形参与实参个数不一致
//		java利用可变参数，支持形参与实参个数不一致，实参是可以为0个或者多个，来对应可变参数的形参，可变参数再形参侧，是被当作数组处理的
//		System.out.println(String.format("我是%s,我今年%d岁了，至今未婚", name,age));
		
//		转换格式
		byte[] bytes = name.getBytes();
		char[] chars = name.toCharArray();
		
		
		String s1="abc123abc";
		System.out.println(s1.toUpperCase());
		s1=s1.replace("abc", "*");//替换所有的匹配其实是replaceAll
		System.out.println(s1);
		System.out.println("  123 123   ".trim().length());
		System.out.println("\t  123 123   ".trim().length());//trim的非开始字符，一定是在字符串两端的
		
//		其它数据类型转字符串类型
		int input=1;
		test1(input+"");
		test1(String.valueOf(input));
		String s2="123456";
//		截取字串
		s2=(String) s2.subSequence(1, 3);
		System.out.println(s2);
	}

	private static void test1(String str) {
		// TODO Auto-generated method stub
		
	}

	private static void testDelete() {
		// TODO Auto-generated method stub
		
	}

	private static void testAdd() {
//		// 字符串拼接
		String s1="Hello";
//		String s2=s1+" world";
////		s1+=" world";
//		
//		String s3="world";
//		s1=s1.concat(s3);//String的操作不会改变源字符串，它会把结果返回为新的字符串对象；
//		System.out.println(s1);
		
		StringBuffer stringBuffer=new StringBuffer("Hello");//Stringbuffer表示内容可变字符串，它的方法能改变字符串对象本身。
		stringBuffer.append("world!");
		System.out.println(stringBuffer);
		for (int i = 0; i < 10000; i++) {
			s1=s1.concat(String.valueOf(i));//不好，太浪费内存，凭空产生了10000个对象
		}
		for (int i = 0; i < 10000; i++) {
			stringBuffer.append(i);//正解
		}
	}

	private static void testStringConstruct() throws UnsupportedEncodingException {
		String s1="hello";
		String s2=new String("Hello");
		char[] value= {'h','e','l','l','o'};
		String s3=new String(value);
//		charset 字符集
		
		byte[] bytes= {97,98,99};
		String s4=new String(bytes,"GBK");;
		StringBuffer charSequence=new StringBuffer();
		String s5=new String(charSequence);
	}
	
	

}
