package com.kfm.test0930;

import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import com.kfm.test0929.Student;

public class SetTest {

	public static void main(String[] args) {
		Set<Object> set=new HashSet<>();//JDK7后支持的菱形写法
		System.out.println(set.add(1));
		System.out.println(set.add(1));
		System.out.println(set.add("敏而好学"));
		System.out.println(set.add("敏而好学"));
		System.out.println(set.add(new Date()));
		System.out.println(set.add(new Student(101, "韩梅梅")));
		
		Iterator<Object> iterator = set.iterator();
		while (iterator.hasNext()) {
			Object object = (Object) iterator.next();
			set.remove("敏而好学");
			//进入快速失败模式后，只要不通过迭代方式就会快速失败，java.util.ConcurrentModificationException
			if("敏而好学".equals(object)) {
//				iterator.remove();
				
			}
			System.out.println(object);
		}
		System.out.println(set);

	}

}
