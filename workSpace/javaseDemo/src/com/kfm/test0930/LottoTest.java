package com.kfm.test0930;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.TreeSet;

public class LottoTest implements Comparator<Integer>{

	public static void main(String[]args){
		LottoTest test=new LottoTest();
		//计算彩票数据
		List<TreeSet<Integer>> datas=test.getData();
//		打印彩票数据
		test.printDates(datas);
	}

	private void printDates(List<TreeSet<Integer>> datas){
		TreeSet<Integer>fronts=datas.get(0);
		TreeSet<Integer>tails=datas.get(1);
		
		Iterator<Integer>iterator=fronts.iterator();
		System.out.print("前：");
//		Set遍历的2种方式
		while(iterator.hasNext()){
			Integer integer=(Integer)iterator.next();
			System.out.print(integer+"\t");
		}
		System.out.print("后：");
		for(Integer integer:tails){
			System.out.print(integer+"\t");
		}
		System.out.println();
	}

	private  List<TreeSet<Integer>> getData(){
//		Comparator<Integer> comparator=new Comparator<Integer>() {
//			@Override
//			public int compare(Integer o1, Integer o2) {
//				// TODO Auto-generated method stub
//				return o2-o1;
//			}
//		};
		List<TreeSet<Integer>> list=new LinkedList<TreeSet<Integer>>();
		TreeSet<Integer> fronts=new TreeSet<>(this);//由顺序的，并且顺序是compareTo比较结果定义的
//		我可以把比较的规则转入进去（让执行程序以计行事），这也是设计模式（策略模式）
		while(fronts.size()<5){
			fronts.add(getRandom(35));
		}
			
		TreeSet<Integer> tails=new TreeSet<>(this);
		while(tails.size()<2){
			tails.add(getRandom(12));
		}

		list.add(fronts);
		list.add(tails);
		return list;
	}

	private static Integer getRandom(int bound){
		return new Random().nextInt(bound)+1;
	}

	@Override
	public int compare(Integer o1, Integer o2) {
		// TODO Auto-generated method stub
		return o2-o1;
	}

}
