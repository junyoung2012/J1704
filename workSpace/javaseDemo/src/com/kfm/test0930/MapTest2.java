package com.kfm.test0930;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.kfm.test0928.Person;

public class MapTest2 {

	public static void main(String[] args) {
//		泛型是不能使用基本数据类型
		Map<Person, String> map=new HashMap<>();
		map.put(new Person("孔子", 30), "圣人");
		map.put(new Person("孟子", 30), "亚圣");
		map.put(new Person("姜子牙", 30), "武圣");
		map.put(new Person("陆羽", 30), "茶圣");
		
		System.out.println(map);
//		map的遍历方式
		Set<Entry<Person, String>> entrySet = map.entrySet();
		for (Entry<Person, String> entry : entrySet) {//推荐使用第1种
			System.out.println(entry.getKey()+":"+entry.getValue());
		}
		Set<Person> keySet = map.keySet();
		for (Person person : keySet) {
			System.out.println(person+"-"+map.get(person));
		}
	}
}
