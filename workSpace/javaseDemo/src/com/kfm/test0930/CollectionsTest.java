package com.kfm.test0930;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class CollectionsTest {

	public static void main(String[] args) {
//		testAddAll();
//		 int binarySearch(List<? extends Comparable<? super T>> list, T key)  
//		testFrequency();
		testIndex();
//		max(Collection<? extends T> coll) 
//		min(Collection<? extends T> coll, Comparator<? super T> comp) 
//		 nCopies(int n, T o) 
//		          返回由指定对象的 n 个副本组成的不可变列表。 
//		replaceAll(List<T> list, T oldVal, T newVal) 
//		testReverse();
//		testShuffle();
		testSort();
	}

	private static void testSort() {
		List<Integer> list=new ArrayList<Integer>();
		Collections.addAll(list, 1,3,5,7,5);
		Collections.sort(list,new Comparator<Integer>() {
			@Override
			public int compare(Integer o1, Integer o2) {
				return o2-o1;
			}
		});
		System.out.println(list);
	}

	private static void testShuffle() {
		List<Integer> list=new ArrayList<Integer>();
		Collections.addAll(list, 1,3,5,7,5);
		Collections.shuffle(list);
		System.out.println(list);
	}

	private static void testReverse() {
		List<Integer> list=new ArrayList<Integer>();
		Collections.addAll(list, 1,3,5,7,5);
		Collections.reverseOrder(new Comparator<Integer>() {

			@Override
			public int compare(Integer o1, Integer o2) {
				// TODO Auto-generated method stub
				return o1-o2;
			}
		});
		Collections.reverse(list);
		System.out.println(list);
		
	}

	private static void testIndex() {
		List<Integer> list=new ArrayList<Integer>();
		Collections.addAll(list, 1,3,5,7,5);
		List<Integer> list1=new ArrayList<Integer>();
		Collections.addAll(list1, 3,5,7);
		System.out.println(Collections.indexOfSubList(list, list1));
	}

	private static void testFrequency() {
		List<Integer> list=new ArrayList<Integer>();
		Collections.addAll(list, 1,3,5,7,5);
		System.out.println(Collections.frequency(list, 5)==2);
		System.out.println(Collections.frequency(list, 1)==1);
		System.out.println(Collections.frequency(list, 2)==0);//找不到是0
	}

	private static void testAddAll() {
		List<Integer> list=new ArrayList<Integer>();
		Collections.addAll(list, 1,3,5,7);
		System.out.println(list);
		Set<Integer> set=new HashSet<Integer>();
		Collections.addAll(set, 1,3,5,7);
		System.out.println(set);
	}
}
