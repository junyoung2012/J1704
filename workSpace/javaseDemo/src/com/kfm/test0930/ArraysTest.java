package com.kfm.test0930;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class ArraysTest {

	public static void main(String[] args) {
//		testCreateFixedList();
//		testBinSearch();
//		testCopy();
//		testEquals();
//		testFill();
		testSort();
	}
	private static void testSort() {
		Integer[] array1= {31,4,1,5,9,2,6};
//		Arrays.sort(array1);
		Arrays.sort(array1,new Comparator<Integer>() {
			@Override
			public int compare(Integer o1, Integer o2) {
				// TODO Auto-generated method stub
				return o2-o1;
			}
		});
		System.out.println(Arrays.toString(array1));
		
	}
	private static void testFill() {
//		Integer[] array1=new Integer[5];
		int[] array1=new int[5];
		Arrays.fill(array1, 8);
		System.out.println(Arrays.toString(array1));
	}
	private static void testEquals() {
		Integer[] array1= {2,5,6,7,9,12};
		Integer[] array2= {2,5,6,7,9,12};
		System.out.println(array1==array2);
		System.out.println(array1.equals(array2));
		
//		if(array1.length!=array1.length) return false;
		System.out.println(Arrays.equals(array1, array2));
		System.out.println(Arrays.deepEquals(array1, array2));
	}
//	二分查找
	private static void testBinSearch() {
		int[] array= {2,5,6,7,9,12};
		System.out.println(Arrays.binarySearch(array, 6)==2);
		System.out.println(Arrays.binarySearch(array, 1)==-1);
		
	}
	
	private static void testCopy() {
		int[] array= {2,5,6,7,9,12};
		int[] array1 = Arrays.copyOf(array, 4);
		System.out.println(Arrays.toString(array1));
		System.out.println(array==array1);//产生新数组
	}

	private static void testCreateFixedList() {
		List<Integer> list = Arrays.asList(1,4,6,7);
		list.add(2);//只读list
		System.out.println(list);
	}

}
