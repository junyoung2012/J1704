package com.kfm.test0827;

public class PrvateModifyTest {
//	6种成员，4种成员是可以用权限修饰符修饰的，都可能需要被其它类调用
	private String str="成员变量";
	public PrvateModifyTest(){
		
	}
	private void test1() {
		
	}
	private class InnerClass{
		
	}
	{
		System.out.println("这是一个代码块");
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return super.toString();
	}
	
}
