package com.kfm.test0827;

public class OptionalTest {

	public static void main(String[] args) {
//		test1();
//		test2();
		test3();
	}
	/**
	 * 逻辑运算
	 */
	private static void test3() {
		boolean a=false;
		System.out.println(!(a)==false);
		System.out.println((a & true)==true);
		System.out.println((a & false)==false);
		System.out.println((a && true)==true);
		System.out.println((a && false)==false);
//		a & false & a & a & a & (8>3)      false
//		System.out.println(a && getBoolean());
//		System.out.println(true | getBoolean());
//		Gril 女朋友=null;
//		if(女朋友！=null && 女朋友.颜值>80) { 利用&&，防止了空指针异常
//			System.out.println("我爱XXX");
//		}
		
//		System.out.println(true || true & !true);
		
		System.out.println(true ^ true ==false);
		System.out.println(true ^ false ==true);
	}
	//该方法相当于一个值true
	private static boolean getBoolean(int order) {
		System.out.println("路过，我被执行了"+order);
		return true;
	}
	/**
	 * 关系运算
	 */
	private static void test2() {
		System.out.println(5>3==true);
		System.out.println(1<3==true);
		System.out.println(5>=3==true);
		System.out.println(5>=5==true);
		System.out.println(4>=5==false);
		System.out.println(3!=4==true);
		System.out.println(3==4==false);//左结合性
		System.out.println(3.14==3.14==true);//
		System.out.println((3.14-3.14<10E-6)==true);//对于浮点数的比较，使用差值小于10的-6次方更加安全
		System.out.println('a'=='a'==true);//char类型也可以进行比较运算
	}
	/**
	 * 文档性注释
	 * 测试常用算术运算
	 */
	private static void test1() {
//		算术运算符：  +，-，*，/，%，++，--
		int a=1;
		int b=2;
//		System.out.println(a+b);
		System.out.println(a+b==3);
		System.out.println(a-b==-1);
		System.out.println(a*b==2);
		System.out.println(a/b==0);
		System.out.println(7%5==2);
		b++;
//		b=b+1;
		System.out.println(b==3);//自动增长
		System.out.println(b++==3);//先执行，后自加
		System.out.println(b==4);
		System.out.println(++b==5);//先自加，后执行
		
		System.out.println(b--==5);//先执行，后自减
		System.out.println(b==4);
		System.out.println(--b==3);//先自减，后执行
		
	}

}
