package com.kfm.test0922;

public class Parent {
	public int i=2;
	public void test() {
		System.out.println("Parent");
	}
}
class Sub extends Parent{
	public int i=3;
	public void test() {
		System.out.println("Sub");
	}
}
class Test{
	public static void main(String[] args) {
		Parent parent1=new Parent();
		Parent parent2=new Sub();
		parent1.test();//调用父类
		parent2.test();//调用子类
		System.out.println(parent1.i);
//		成员变量不构成重写，调用同名成员变量，取决于对象变量de类型
		System.out.println(parent2.i);
		System.out.println(((Sub)parent2).i);
	}
}