package com.kfm.dao.test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;

import com.kfm.dao.PhoneDao;
import com.kfm.model.Phone;

public class PhoneDaoTest {
	private PhoneDao phoneDao=new PhoneDao();
	@Test
	public void testAdd() {
		assertTrue(phoneDao.add(
				new Phone("13100010001", "浙江", "杭州", "0571", "310000", "中国移动", "")));
		assertTrue(phoneDao.add(
				new Phone("13100010002", "浙江", "杭州", "0571", "310000", "中国移动", "")));
		assertTrue(phoneDao.add(
				new Phone("13100010003", "浙江", "杭州", "0571", "310000", "中国移动", "")));
		assertTrue(phoneDao.add(
				new Phone("13100010004", "浙江", "杭州", "0571", "310000", "中国移动", "")));
	}

	@Test
	public void testGetAll() {
		testAdd();
		Phone[] phones = phoneDao.getAll();
		for (Phone phone : phones) {
			System.out.println(phone);
		}
	}

}
