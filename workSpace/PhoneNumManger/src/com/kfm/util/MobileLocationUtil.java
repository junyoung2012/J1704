package com.kfm.util;

import net.sf.json.JSONObject;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

import com.kfm.model.Phone;

public class MobileLocationUtil {
	// 手机归属地查询接口地址
	public static String API_URL = "http://apis.juhe.cn/mobile/get";
	// 接口请求Key
	public static String API_KEY = "0ea8e44e4612fb794c29f4979de48ef7";
	public static void main(String[] args) {
		String mobile = "18912341234";
		queryMobileLocation(mobile);
	}

	/**
	 * 根据手机号码/手机号码前7位查询号码归属地
	 * 
	 * @param mobile
	 */
	public static Phone queryPhone(String mobile) {
		Map<String, Object> params = new HashMap<>();// 组合参数
		params.put("phone", mobile);
		params.put("key", API_KEY);
		String queryParams = urlencode(params);

		String response = doGet(API_URL, queryParams);
		try {
			JSONObject jsonObject = JSONObject.fromObject(response);
			int error_code = jsonObject.getInt("error_code");
			if (error_code == 0) {
				System.out.println("调用接口成功");
				JSONObject result = jsonObject.getJSONObject("result");
				return new Phone(
					mobile, 
					result.getString("province"),
					result.getString("city"),
					result.getString("areacode"),
					result.getString("zip"),
					result.getString("company"),
					result.getString("card")
				);
			} 
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static void queryMobileLocation(String mobile) {
		Map<String, Object> params = new HashMap<>();// 组合参数
		params.put("phone", mobile);
		params.put("key", API_KEY);
		String queryParams = urlencode(params);

		String response = doGet(API_URL, queryParams);
		System.out.println(response);
//		{"resultcode":"200","reason":"Return Successd!","result":{"province":"江苏","city":"常州","areacode":"0519","zip":"213000","company":"电信","card":""},"error_code":0}
		try {
			JSONObject jsonObject = JSONObject.fromObject(response);
			int error_code = jsonObject.getInt("error_code");
			if (error_code == 0) {
				System.out.println("调用接口成功");

				JSONObject result = jsonObject.getJSONObject("result");

				System.out.printf("省份：%s%n", result.getString("province"));
				System.out.printf("城市：%s%n", result.getString("city"));
				System.out.printf("区号：%s%n", result.getString("areacode"));
				System.out.printf("邮编：%s%n", result.getString("zip"));
				System.out.printf("运营商：%s%n", result.getString("company"));
				
				Phone phone=(Phone) JSONObject.toBean(jsonObject.getJSONObject("result"),Phone.class);
				System.out.println(phone);

			} else {
				System.out.println("调用接口失败：" + jsonObject.getString("reason"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * get方式的http请求
	 *
	 * @param httpUrl
	 *            请求地址
	 * @return 返回结果
	 */
	public static String doGet(String httpUrl, String queryParams) {
		HttpURLConnection connection = null;
		InputStream inputStream = null;
		BufferedReader bufferedReader = null;
		String result = null;// 返回结果字符串
		try {
			// 创建远程url连接对象
			URL url = new URL(new StringBuffer(httpUrl).append("?").append(queryParams).toString());
			// 通过远程url连接对象打开一个连接，强转成httpURLConnection类
			connection = (HttpURLConnection) url.openConnection();
			// 设置连接方式：get
			connection.setRequestMethod("GET");
			// 设置连接主机服务器的超时时间：15000毫秒
			connection.setConnectTimeout(5000);
			// 设置读取远程返回的数据时间：60000毫秒
			connection.setReadTimeout(6000);
			// 发送请求
			connection.connect();
			// 通过connection连接，获取输入流
			if (connection.getResponseCode() == 200) {
				inputStream = connection.getInputStream();
				// 封装输入流，并指定字符集
				bufferedReader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8));
				// 存放数据
				StringBuilder sbf = new StringBuilder();
				String temp;
				while ((temp = bufferedReader.readLine()) != null) {
					sbf.append(temp);
					sbf.append(System.getProperty("line.separator"));
				}
				result = sbf.toString();
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			// 关闭资源
			if (null != bufferedReader) {
				try {
					bufferedReader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (null != inputStream) {
				try {
					inputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				connection.disconnect();// 关闭远程连接
			}
		}
		return result;
	}
	
	public static String doPost(String url,String parameter){
        HttpURLConnection conn =  null;
        BufferedReader reader = null;       //字符输入流
        PrintWriter writer = null;          //字符输出流
        String content = null;
        StringBuffer sbf = new StringBuffer();
        try {
            URL u = new URL(url);   //创建URL对象
            conn = (HttpURLConnection)u.openConnection();
            //设置Post请求方式，默认为Get
            conn.setRequestMethod("POST");
            //设置服务器连接超时时间（milliseconds毫秒），超过该时间则报异常
            conn.setConnectTimeout(6000);
            //设置读取服务器资源时间，已经建立连接，并开始读取服务端资源。如果到了指定的时间，没有可能的数据被客户端读取，则报异常
            conn.setReadTimeout(5000);
            /**
             * Post请求由于需要往服务区传输大量的数据，请求参数是在http的body里，因此需要设置setDoOutput(true)
             * 而无论Get、Post总是要通过getInputStream()从服务端获得响应，所以setDoInput()默认是true
             * setDoOutput()默认是false，需要手动设置为true，完了就可以调用getOutputStream()方法从服务器端获得字节输出流
             * 总结，get请求的话这两个设置setDoInput和setDoOutput默认就行了，post请求需要setDoOutput(true)，这个默认是false的
             */
            conn.setDoInput(true);
            conn.setDoOutput(true);
            //Post请求禁止使用缓存  默认为true
            conn.setUseCaches(false);
            //自动执行HTTP重定向  默认为true
            conn.setInstanceFollowRedirects(true);
            /**
             * Accept:text/xml；代表客户端希望接受的数据类型
             * Content-Type:text/html 代表发送端（客户端|服务器）发送的实体数据的数据类型
             * 设置请求头信息，可接收文档类型 *|* 所有
             * 发送数据类型为编码格式UTF-8的Json字符串
             */
            conn.setRequestProperty("accept", "*/*");
            conn.setRequestProperty("Content-type","application/json;charset=UTF-8");
            //设定传送的内容类型是可序列化的java对象
            //conn.setRequestProperty("Content-type", "application/x-java-serialized-object");
            //设置请求头编码格式
            conn.setRequestProperty("Accept-Charset", "UTF-8");
            //Connection:Keep-Alive或 Connection:close，http请求的是否保持长连接，即链接是否复用，
            //每次请求是复用已建立好的请求，还是重新建立一个新的请求
            conn.setRequestProperty("Connection","Keep-Alive");
            //设置发送请求的平台  Mozilla/5.0 (平台) 引擎版本 浏览器版本号
            conn.setRequestProperty("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3100.0 Safari/537.36");
            //带cookie的请求
            //conn.setRequestProperty("Cookie", "JSESSIONID="+SessionId);
            conn.connect();
            //conn.getResponseCode(); 获取响应码
            //获取输入流  getOutputStream会隐含的进行connect()
            writer = new PrintWriter(new OutputStreamWriter(conn.getOutputStream(),"UTF-8"));
            writer.print(parameter);      //参数写入
            writer.flush();
 
            //获取输出流
            reader = new BufferedReader(new InputStreamReader(conn.getInputStream(),"UTF-8"));
            while (null != (content=reader.readLine())){
                sbf.append(content);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            //关闭流和连接
            if (null != writer){
                writer.close();
            }
            if (null != reader){
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if(null != conn){
                conn.disconnect();
            }
        }
        return sbf.toString();
    }
	
	/**
	 * 将map型转为请求参数型
	 *
	 * @param data
	 * @return
	 */
	public static String urlencode(Map<String, ?> data) {
		StringBuilder sb = new StringBuilder();
		for (Map.Entry<String, ?> i : data.entrySet()) {
			try {
				sb.append(i.getKey()).append("=").append(URLEncoder.encode(i.getValue() + "", "UTF-8")).append("&");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		}
		String result = sb.toString();
		result = result.substring(0, result.lastIndexOf("&"));
		return result;
	}
}
