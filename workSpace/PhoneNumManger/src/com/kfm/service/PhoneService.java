package com.kfm.service;

import com.kfm.dao.PhoneDao;
import com.kfm.model.Phone;
import com.kfm.util.MobileLocationUtil;

public class PhoneService {
	private PhoneDao phoneDao=new PhoneDao();
	
	public boolean query(String mobile) {
		Phone queryPhone = MobileLocationUtil.queryPhone(mobile);
		if(queryPhone==null) return false;
		phoneDao.add(queryPhone);
		return true;
	}
	public Phone[] getLogs() {
		return phoneDao.getAll();
	}
}
