package com.kfm.model;

public class Phone {
	private String number; 
	private String province; 
	private String city; 
	private String areacode; 
	private String zip; 
	private String company; 
	private String card;
	
	public Phone() {
	}
	public Phone(String number, String province, String city, String areacode, String zip, String company,
			String card) {
		this.number = number;
		this.province = province;
		this.city = city;
		this.areacode = areacode;
		this.zip = zip;
		this.company = company;
		this.card = card;
	}
	
	@Override
	public String toString() {
		return "Phone [number=" + number + ", province=" + province + ", city=" + city + ", areacode=" + areacode
				+ ", zip=" + zip + ", company=" + company + ", card=" + card + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((number == null) ? 0 : number.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Phone other = (Phone) obj;
		if (number == null) {
			if (other.number != null)
				return false;
		} else if (!number.equals(other.number))
			return false;
		return true;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getAreacode() {
		return areacode;
	}
	public void setAreacode(String areacode) {
		this.areacode = areacode;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getCard() {
		return card;
	}
	public void setCard(String card) {
		this.card = card;
	} 
	
	
}
