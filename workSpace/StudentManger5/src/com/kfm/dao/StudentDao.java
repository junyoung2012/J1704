package com.kfm.dao;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import com.kfm.model.Student;

public class StudentDao implements Serializable {
	private List<Student> students=new LinkedList<>();
	private final File file=new File("resouce/students.dat");
	
	public StudentDao() {
		if(!file.exists()) return;
		try(ObjectInputStream objectInputStream=new ObjectInputStream(new FileInputStream(file))){
			students=(List<Student>) objectInputStream.readObject();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public boolean saveData() {
		
		try(ObjectOutputStream objectOutputStream=new ObjectOutputStream(new FileOutputStream(file))){
//			for (Student student : students) {
//				objectOutputStream.writeObject(student);
//			}
			objectOutputStream.writeObject(students);
			return true;
		}catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	public boolean add(Student student) {
		if(students.contains(student)) return false;
		return students.add(student);
	}
	
	public boolean add(int code) {
//		for (int i = 0; i < students.size(); i++) {
//			
//		}
//		students.remove(0) 一般用于出队
//		students.remove(new Student(code));
//		return students.remove(new Student(code));
		if(students.remove(new Student(code)))
			return true;
		else
			return false;
	}
	
	public boolean delete(Student student) {//更面向对象
		return students.remove(student);
	}
	
	public boolean update(Student student) {
		if(students==null || students.size()==0) return false;
		return students.remove(student);
	}
	
	public List<Student> getAll(){
		return students;
	}
//	不允许主动使用数组
//	除了考核算法，还有参数，或者返回数组
	public List<Student> getStudentsOrderByScoreDesc(){
//		1.复制数据
		List<Student> results=new LinkedList<>();
//		for (int i = 0; i < array.length; i++) {
//			
//		}
//		for (Student student : results) {//不取用下标，必须foreach
//			results.add(student);
//		}
		results.addAll(students);
//		排序
//		results.toArray()
//		Collections.sort(results,new ComparetorScoreDesc());
		Collections.sort(results,new Comparator<Student>() {

			@Override
			public int compare(Student o1, Student o2) {
				return o2.getScore()-o1.getScore();
			}
		});
		return results;
	}
//	public List<Student> getStudentsOrderByName(){
//		return getStudentsOrder(new Comparator<Student>() {
//			@Override
//			public int compare(Student o1, Student o2) {
//				return o1.getName().compareTo(o2.getName());
//			}
//		});
//	}
	public List<Student> getStudentsOrderByName(){
		return getStudentsOrder((Student o1, Student o2)-> {
			return o1.getName().compareTo(o2.getName());
		});
	}
	public List<Student> getStudentsOrder(Comparator<Student> comparator){
//		1.复制数据
		List<Student> results=new LinkedList<>();
//		for (int i = 0; i < array.length; i++) {
//			
//		}
//		for (Student student : results) {//不取用下标，必须foreach
//			results.add(student);
//		}
		results.addAll(students);
//		排序
//		results.toArray()
		Collections.sort(results,comparator);
		return results;
	}
//	你要使用两次以上这个规则
	private class ComparetorScoreDesc implements Comparator<Student>{
		@Override
		public int compare(Student o1, Student o2) {
			// TODO Auto-generated method stub
			return 0;
		}
	}
	private void test1() {
		Comparator<Student> comparator=(Student o1, Student o2)-> {
			return o1.getName().compareTo(o2.getName());
		};
	}
//	函数接口，才能作为lanbda表达式的模板
	@FunctionalInterface
	interface Calable{
//		int add(int a,int b);
		int cal(int a,int b);
		static void test1() {
			
		}
		default void test2() {
			
		}
	}
	
	interface Calable1{
		int increment(int a);
	}
	
	private void test2() {
		Calable calable=(int a,int b)->{
			return a-b;
		};
		
		System.out.println(calable.cal(2, 3));

	}
	private void test3() {
		Calable calable=(a,b)->{
			return a-b;
		};
		
		System.out.println(calable.cal(2, 3));
		
	}
	private void test4() {
		Calable1 calable=a->++a;
		
		System.out.println(calable.increment(2));
		
	}
	
	public static void main(String[] args) {
		StudentDao dao=new StudentDao();
		dao.test4();
	}
}
