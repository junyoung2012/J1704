package com.kfm.test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;

public class JdbcTest {

	public static void main(String[] args) throws ClassNotFoundException, SQLException {
//		testAdd();
//		testAdd2(new Student(1004, "庞涓", Date.valueOf("2021-10-01")));
//		testDelete();
//		testQuery();
//		System.out.println(testLogin(new Student(1004, "庞涓1", Date.valueOf("2021-10-01"))));
//		SQL注入漏洞
//		System.out.println(testLogin2(new Student(1004, "' or 1=1 --",null)));
//		System.out.println(testLogin2(new Student(1004, "庞涓", Date.valueOf("2021-10-01"))));
		testResultset();
//		testResultMeta();
	}

	private static void testResultset() throws SQLException {
		String url="jdbc:oracle:thin:@localhost:1521:xe";
		String user="scott";
		String password="tiger";
		Connection connection=DriverManager.getConnection(url, user, password);
		String sql="select id, name, bitthday from student"
				+ " order by id ";
		PreparedStatement statement = connection.prepareStatement(sql,
				ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
//		5.执行
		
		ResultSet rs = statement.executeQuery();
		rs.absolute(2);//访问指定位置
		System.out.println(rs.getInt(1)+"\t"+rs.getString("name")+"\t"+rs.getDate("bitthday"));
		rs.previous();
		System.out.println(rs.getInt(1)+"\t"+rs.getString("name")+"\t"+rs.getDate("bitthday"));
		rs.last();
		System.out.println(rs.getInt(1)+"\t"+rs.getString("name")+"\t"+rs.getDate("bitthday"));
			
	}
	private static void testResultMeta() throws SQLException {
		String url="jdbc:oracle:thin:@localhost:1521:xe";
		String user="scott";
		String password="tiger";
		Connection connection=DriverManager.getConnection(url, user, password);
		String sql="select id 编号, name, bitthday from student"
				+ " order by id ";
		PreparedStatement statement = connection.prepareStatement(sql,
				ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
//		5.执行
		
		ResultSet rs = statement.executeQuery();
		ResultSetMetaData metaData = rs.getMetaData();
		int columnCount = metaData.getColumnCount();
		System.out.println(columnCount);
		for (int i = 1; i <= columnCount; i++) {
			System.out.print(metaData.getColumnName(i)+"\t");
//			System.out.print(metaData.getColumnLabel(i)+"\t");
			System.out.print(metaData.getColumnTypeName(i)+"\t");
		}
		System.out.println();
	}

	private static void testQuery() throws ClassNotFoundException, SQLException {
		// 1. 引入驱动jar包
		//2.加载数据库驱动
//		String diverClassName="oracle.jdbc.driver.OracleDriver";
//		Class.forName(diverClassName);//只是把数据库驱动的实现类装载入JVM
		String url="jdbc:oracle:thin:@localhost:1521:xe";//数据库的连接字符串
		String user="scott";//注意不允许使用sys,system,root这样的高权限用户，不安全
		String password="tiger";
//		3. 得到connection对象 
		Connection connection=DriverManager.getConnection(url, user, password);
		
//		4.得到statement(语句，陈述)对象
		Statement statement = connection.createStatement();
		
//		5.执行
		String sql="select id, name, bitthday from student";
		ResultSet rs = statement.executeQuery(sql);
//		6.处理结果
		while(rs.next()) {
			System.out.println(rs.getInt(1)+"\t"+rs.getString("name")+"\t"+rs.getDate("bitthday"));
		}
//		7.关闭资源
		statement.close();
		connection.close();
	}

	private static void testDelete() throws SQLException, ClassNotFoundException {
		// 1. 引入驱动jar包
		//2.加载数据库驱动
		String diverClassName="oracle.jdbc.driver.OracleDriver";
//				new OracleDriver(); 耦合性大
		Class.forName(diverClassName);//只是把数据库驱动的实现类装载入JVM
		
//				3. 得到connection对象  ，连接对象，就是连接数据的对象，就是通往数据库桥梁
//				         jdbc:主协议名（数据库名）:次协议名:数据服务器地址：数据库监听端口:数据库的系统标识符sid
		String url="jdbc:oracle:thin:@localhost:1521:xe";//数据库的连接字符串
		String user="scott";//注意不允许使用sys,system,root这样的高权限用户，不安全
		String password="tiger";
		Connection connection=DriverManager.getConnection(url, user, password);
		
//				4.得到statement(语句，陈述)对象
		Statement statement = connection.createStatement();
		
		String sql="delete student"
				+ " where id = 1001";
		System.out.println(statement.executeUpdate(sql));
		
//				6.处理结果
//				7.关闭资源
		
		statement.close();
		connection.close();
	}

	private static void testAdd() throws ClassNotFoundException, SQLException {
		// 1. 引入驱动jar包
		//2.加载数据库驱动
		String diverClassName="oracle.jdbc.driver.OracleDriver";
//		new OracleDriver(); 耦合性大
		Class.forName(diverClassName);//只是把数据库驱动的实现类装载入JVM
		
//		3. 得到connection对象  ，连接对象，就是连接数据的对象，就是通往数据库桥梁
//		         jdbc:主协议名（数据库名）:次协议名:数据服务器地址：数据库监听端口:数据库的系统标识符sid
		String url="jdbc:oracle:thin:@localhost:1521:xe";//数据库的连接字符串
		String user="scott";//注意不允许使用sys,system,root这样的高权限用户，不安全
		String password="tiger";
		Connection connection=DriverManager.getConnection(url, user, password);
		
//		4.得到statement(语句，陈述)对象
		Statement statement = connection.createStatement();
//		5.执行statement 
		
//		String sql="insert into student"
//				+ " (id, name, bitthday)"
//				+ " values"
//				+ " (1001, '苏秦', date'2021-10-21')";
		String sql="insert into student"
				+ " (id, name, bitthday)"
				+ " values"
				+ " (1003, '张仪', date'2021-10-21')";
		System.out.println(statement.executeUpdate(sql));
		
//		6.处理结果
//		7.关闭资源
		statement.close();
		connection.close();
	}
	private static void testAdd1(Student student) throws ClassNotFoundException, SQLException {
		String diverClassName="oracle.jdbc.driver.OracleDriver";
		Class.forName(diverClassName);
		String url="jdbc:oracle:thin:@localhost:1521:xe";
		String user="scott";
		String password="tiger";
		Connection connection=DriverManager.getConnection(url, user, password);
		Statement statement = connection.createStatement();
	
		String sql="insert into student"
				+ " (id, name, bitthday)"
				+ " values"
				+ " ("+student.getId()+", '"+student.getName()+"', date'"
				+new SimpleDateFormat("yyyy-MM-dd").format(student.getBitthday())+"')";
		System.out.println(statement.executeUpdate(sql));
		statement.close();
		connection.close();
	}
	
	private static void testAdd2(Student student) throws ClassNotFoundException, SQLException {
		String diverClassName="oracle.jdbc.driver.OracleDriver";
		Class.forName(diverClassName);
		String url="jdbc:oracle:thin:@localhost:1521:xe";
		String user="scott";
		String password="tiger";
		Connection connection=DriverManager.getConnection(url, user, password);
		String sql="insert into student"
				+ " (id, name, bitthday)"
				+ " values"
				+ " (?,?,?)";
		PreparedStatement statement=connection.prepareStatement(sql);
		statement.setInt(1, student.getId());
		statement.setString(2, student.getName());
		statement.setDate(3, student.getBitthday());
//		!!!切记，不要在executeUpdate传入sql
		System.out.println(statement.executeUpdate());
		statement.close();
		connection.close();
	}
	
	private static boolean testLogin(Student student) throws SQLException {
		String url="jdbc:oracle:thin:@localhost:1521:xe";
		String user="scott";
		String password="tiger";
		Connection connection=DriverManager.getConnection(url, user, password);
		
//		4.得到statement(语句，陈述)对象
		Statement statement = connection.createStatement();
		
//		5.执行
		String sql="select id, name, bitthday from student "
				+ " where name ='"+student.getName()+"' and id='"+student.getId()+"'";
		System.out.println(sql);
		ResultSet rs = statement.executeQuery(sql);
		return rs.next();
	}
	private static boolean testLogin2(Student student) throws SQLException {
		String url="jdbc:oracle:thin:@localhost:1521:xe";
		String user="scott";
		String password="tiger";
		Connection connection=DriverManager.getConnection(url, user, password);
		String sql="select id, name, bitthday from student "
				+ " where name =? and id=?";
		PreparedStatement statement = connection.prepareStatement(sql);
		statement.setString(1, student.getName());
		statement.setInt(2, student.getId());
//		5.执行
		
		System.out.println(sql);
		ResultSet rs = statement.executeQuery();
		return rs.next();
	}
	 	

}
