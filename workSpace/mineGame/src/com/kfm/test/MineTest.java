package com.kfm.test;
public class MineTest {
//	private static 
//	private static int[] mines;//A
//	private String[][] mines;//B
	private static final int ROW_COUNT=9;
	private static final int MINE_COUNT=10;
	
	public static void main(String[] args) {
//		1.创建游戏棋盘
		int[] mines=createGame(ROW_COUNT);
//		2.布雷
		int[] minePos=setMines(MINE_COUNT,mines);
//		for (int i = 0; i < minePos.length; i++) {
//			System.out.print(minePos[i]+ "   " );
//		}
//		System.out.println();
//		3.计算数字
//		mines[1]=9;
//		mines[79]=8;
//		setNumbers();
//		setNumber(mines,9);
//		setNumber(mines,10);
////		setNumber(mines,11);
		setNumbers(mines,minePos);
//		4.显式游戏
		showGame(mines);
	}
	private static void setNumber(int[] mines,int pos) {
		// 设置一颗雷旁边的数字
//		mines[pos]=9;
		if(pos >= ROW_COUNT && mines[pos-ROW_COUNT]!=9)
			mines[pos-ROW_COUNT]++;//上
		if(pos < ROW_COUNT*(ROW_COUNT-1) && mines[pos+ROW_COUNT]!=9 )
			mines[pos+ROW_COUNT]++;//下
		if(pos % ROW_COUNT !=0 &&  mines[pos-1]!=9 )
			mines[pos-1]++;//左
		if(pos % ROW_COUNT !=ROW_COUNT-1 &&  mines[pos+1]!=9 )
			mines[pos+1]++;//右
		if(pos % ROW_COUNT !=0 && pos>=ROW_COUNT  && mines[pos-ROW_COUNT-1]!=9  )
			mines[pos-ROW_COUNT-1]++;//左上
		if(pos % ROW_COUNT !=ROW_COUNT-1 && pos>=ROW_COUNT && mines[pos-ROW_COUNT+1]!=9 )
			mines[pos-ROW_COUNT+1]++;//右上
		if(pos % ROW_COUNT !=0 && pos <= ROW_COUNT*(ROW_COUNT-1) && mines[pos+ROW_COUNT-1]!=9 )
			mines[pos+ROW_COUNT-1]++;//左下
		if(pos % ROW_COUNT !=ROW_COUNT-1 && pos < ROW_COUNT*(ROW_COUNT-1) 
				&& mines[pos+ROW_COUNT+1]!=9 )
			mines[pos+ROW_COUNT+1]++;//右下
	}
	private static void setNumbers(int[] mines,int[] minePos) {
		// 设置所有雷旁边的数字
		for (int i = 0; i < minePos.length; i++) {
			setNumber(mines,minePos[i]);
		}
	}
	private static int[] setMines(int mineCount,int[] mines) {
		// 1.根据需求生成多个不重复的雷的位置
//			随机产生一个数字 0-80之间的数字
//		System.out.println(Math.random());[0,1) 
		int[] minePos=new int[mineCount];//记录雷的位置的数组
		for (int i = 0; i < mineCount; i++) {//总的要随机10个雷
			int pos;//[0,80]
//			pos=(int) (Math.random()*ROW_COUNT*ROW_COUNT);
			while(mines[pos=(int) (Math.random()*ROW_COUNT*ROW_COUNT)]==9) {
//				pos=(int) (Math.random()*ROW_COUNT*ROW_COUNT);//重新在来
			}
//			随机出来不重复的数字
			mines[pos]=9;//打个标记
			minePos[i]=pos;
		}
//		System.out.println(pos);
//			2.把类的位置的数组,返回出去
		return minePos;
	}
//	创建游戏棋盘
//	把数据和显式分离,保存的是一种情况,显式成了另一种情况,处理变得更简单
	private static void showGame(int[] array) {
		for (int i = 0; i < ROW_COUNT; i++) {
			for (int j = 0; j < ROW_COUNT; j++) {
				if(array[i*ROW_COUNT+j]==9) {//雷
					System.out.print("@ ");
				}else if(array[i*ROW_COUNT+j]==0) {//空格
					System.out.print(". ");
				}else {
					System.out.print(array[i*ROW_COUNT+j]+" ");//数字
				}
			}
			System.out.println();
		}
	}
//	显式游戏
	private static int[] createGame(int rowCount) {
		int[] mines=new int[rowCount*rowCount];
		return mines;
	}
}
