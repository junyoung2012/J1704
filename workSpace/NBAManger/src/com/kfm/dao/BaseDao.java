package com.kfm.dao;

import java.util.LinkedList;
import java.util.List;
import com.kfm.model.IDable;

public class BaseDao<T extends IDable> {
	private List<T> list=new LinkedList<T>();
	private static int id;
	
	public boolean add(T obj) {
		obj.setId(++id);
		return list.add(obj);
	}
	public boolean delete(T obj) {
		return list.remove(obj);
	}
	public boolean update(T obj) {
		int index=getIndex(obj);
		if(index==-1) return false;
		list.set(index, obj);
		return true;
	}
	private int getIndex(T obj) {
		for (int i = 0; i < list.size(); i++) {
			if(list.get(i).equals(obj)) {
				return i;
			}
		}
		return -1;
	}
	public T get(int id) {
		for (T t : list) {
			if(t.getId()==id) return t;
		}
		return null;
	}
	public List<T> getAll() {
		return list;
	}
}
