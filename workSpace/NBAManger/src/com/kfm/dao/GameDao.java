package com.kfm.dao;

import java.util.LinkedList;
import java.util.List;

import com.kfm.model.Game;

public class GameDao {
	private List<Game> games=new LinkedList<Game>();
	private static int id;
	
	public boolean add(Game game) {
		game.setId(++id);
		return games.add(game);
	}
	public boolean delete(int id) {
		return games.remove(new Game(id));
	}
	public boolean update(Game game) {
		int index=getIndex(game);
		if(index==-1) return false;
		games.set(index, game);
		return true;
	}
	private int getIndex(Game game) {
		for (int i = 0; i < games.size(); i++) {
			if(games.get(i).equals(game)) return i;
		}
		return -1;
	}
	public Game get(int id) {
		return null;
	}
	public List<Game> getAll() {
		return games;
	}
}
