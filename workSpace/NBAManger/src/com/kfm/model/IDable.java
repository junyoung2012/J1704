package com.kfm.model;

public interface IDable {
	void setId(int id);
	int getId();
}
