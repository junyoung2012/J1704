package com.kfm.model;

import java.util.Date;

public class Game extends BaseModel {
//	private int id;
	private Date gameTime;
	private String homeTeam;
	private String vistingTeam;
	public Game() {
	}
	public Game(int id) {
		super(id);
	}
	public Game(Date gameTime, String homeTeam, String vistingTeam) {
		this.gameTime = gameTime;
		this.homeTeam = homeTeam;
		this.vistingTeam = vistingTeam;
	}
	
	public Date getGameTime() {
		return gameTime;
	}
	public void setGameTime(Date gameTime) {
		this.gameTime = gameTime;
	}
	public String getHomeTeam() {
		return homeTeam;
	}
	public void setHomeTeam(String homeTeam) {
		this.homeTeam = homeTeam;
	}
	public String getVistingTeam() {
		return vistingTeam;
	}
	public void setVistingTeam(String vistingTeam) {
		this.vistingTeam = vistingTeam;
	}
}
