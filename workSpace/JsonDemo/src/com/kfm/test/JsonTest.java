package com.kfm.test;

import java.util.LinkedList;
import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class JsonTest {

	public static void main(String[] args) {
		Student student=new Student(101, "颜回");
//		{"code":101,"name":"颜回"}
//		System.out.println(student);
//		testBean2Json();
//		testList2Json();
//		testJson2List();
		testJson2List1();
	}

	private static void testJson2List1() {
		List<String> banners=new LinkedList<>();
		banners.add("banner_01.png");
		banners.add("banner_02.png");
		banners.add("banner_03.png");
		banners.add("banner_04.png");
		
		System.out.println(JSONArray.fromObject(banners));
		
	}

	private static void testList2Json() {
		Student student=new Student(101, "颜回");
		student.setGrade(new Grade(11, "J1704"));
		List<Student> students=new LinkedList<>();
		students.add(student);
		students.add(student);
		students.add(student);
		
		System.out.println(JSONArray.fromObject(students));
	}
	
	private static void testJson2List() {
		String str="[{\"code\":101,\"grade\":{\"code\":11,\"name\":\"J1704\"},\"name\":\"颜回\"},"
				+ "{\"code\":101,\"grade\":{\"code\":11,\"name\":\"J1704\"},\"name\":\"颜回\"},"
				+ "{\"code\":101,\"grade\":{\"code\":11,\"name\":\"J1704\"},\"name\":\"颜回\"}]";
		JSONArray object = JSONArray.fromObject(str);
		List<Student> list = JSONArray.toList(object,Student.class);
		System.out.println(list);
	}

	private static void testBean2Json() {
		Student student=new Student(101, "颜回");
		student.setGrade(new Grade(11, "J1704"));
		System.out.println(JSONObject.fromObject(student));
	}
}
