package com.kfm.test;
public class Student{
	private int code;
	private String name;
	private Grade grade;
	
	public Student() {
	}
	public Student(int code, String name) {
		this.code = code;
		this.name = name;
	}
	@Override
	public String toString() {
		return "{"
				+"\"code\":"+code+","
				+ "\"name\":\""+name+"\""
				+ "}";
	}
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Grade getGrade() {
		return grade;
	}
	public void setGrade(Grade grade) {
		this.grade = grade;
	}
}