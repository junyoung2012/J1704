package com.kfm.test;

public class Grade {
	private int code;
	private String name;
	
	public Grade() {
	}
	public Grade(int code, String name) {
		this.code = code;
		this.name = name;
	}
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

}
