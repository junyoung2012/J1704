package com.kfm.test1;

import java.util.LinkedList;
import java.util.List;

import net.sf.json.JSONArray;

public class ServiceType {
	private int id;
	private String icon;
	private String title;
	
	public ServiceType() {
	}
	public ServiceType(int id, String icon, String title) {
		this.id = id;
		this.icon = icon;
		this.title = title;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public static void main(String[] args) {
		List<ServiceType> serviceTypes=new LinkedList<ServiceType>();
		serviceTypes.add(new ServiceType(1, "nav_icon_01.png", "推荐"));
		serviceTypes.add(new ServiceType(2, "nav_icon_02.png", "美甲"));
		serviceTypes.add(new ServiceType(3, "nav_icon_03.png", "美容"));
		serviceTypes.add(new ServiceType(4, "nav_icon_04.png", "美发"));
		serviceTypes.add(new ServiceType(5, "nav_icon_05.png", "美睫"));
		
		System.out.println(JSONArray.fromObject(serviceTypes));
	}
}
