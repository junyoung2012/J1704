package com.kfnm.model;

import java.util.List;

public class Dept {
	private int id;
	private String name;
	private List<Emp> emps;
	
	public Dept() {
	}
	public Dept(int id, String name, List<Emp> emps) {
		this.id = id;
		this.name = name;
		this.emps = emps;
	}
	
	@Override
	public String toString() {
		return  id + "\t" + name + "\t" + emps.size();
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Dept other = (Dept) obj;
		if (id != other.id)
			return false;
		return true;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<Emp> getEmps() {
		return emps;
	}
	public void setEmps(List<Emp> emps) {
		this.emps = emps;
	}
}
