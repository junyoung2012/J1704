package com.kfnm.dao;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.LinkedList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.kfnm.model.Dept;
import com.kfnm.model.Emp;

public class DeptDaoTest {
	private DeptDao deptDao=new DeptDao();
	private EmpDao empDao=new EmpDao();
	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testAdd() {
		assertTrue(deptDao.add(new Dept(101, "研发部", new LinkedList<Emp>())));
		assertTrue(deptDao.add(new Dept(102, "实施部", new LinkedList<Emp>())));
	}

	@Test
	public void testDelete() {
		fail("Not yet implemented");
	}

	@Test
	public void testUpdate() {
		fail("Not yet implemented");
	}

	@Test
	public void testGet() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetAll() {
		testAdd();
		Dept dept=deptDao.get(101);
		empDao.add(new Emp(10101, "员工1", dept));
		List<Dept> depts = deptDao.getAll();
		System.out.println("编号\t名称\t人数");
		for (Dept dept1 : depts) {
			System.out.println(dept1);
		}
	}

}
