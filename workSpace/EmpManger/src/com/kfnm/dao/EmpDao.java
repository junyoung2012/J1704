package com.kfnm.dao;

import java.util.LinkedList;
import java.util.List;

import com.kfnm.model.Emp;

public class EmpDao {
	private List<Emp> emps=new LinkedList<Emp>();
	
	public boolean add(Emp emp) {
		if(emps.contains(emp)) return false;
		emp.getDept().getEmps().add(emp);
		return emps.add(emp);
	}
	public boolean delete(Emp emp) {
		emp.getDept().getEmps().remove(emp);
		return emps.remove(emp);
	}
	public boolean update(Emp emp) {
		return false;
	}
	public Emp get(int id) {
		for (Emp emp : emps) {
			if(emp.getId()==id) return emp;
		}
		return null;
	}
	public List<Emp> getAll() {
		return null;
	}
}
