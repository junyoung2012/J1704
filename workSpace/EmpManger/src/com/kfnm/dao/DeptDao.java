package com.kfnm.dao;
import java.util.LinkedList;
import java.util.List;
import com.kfnm.model.Dept;

public class DeptDao {
	private List<Dept> depts=new LinkedList<Dept>();
	
	public boolean add(Dept dept) {
		if(depts.contains(dept)) return false;
		return depts.add(dept);
	}
	public boolean delete(Dept dept) {
		return false;
	}
	public boolean update(Dept dept) {
		return false;
	}
	public Dept get(int id) {
		for (Dept dept : depts) {
			if(dept.getId()==id) return dept;
		}
		return null;
	}
	public List<Dept> getAll() {
		return depts;
	}
}
