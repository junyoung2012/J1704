package com.yangj.web;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.annotation.WebServlet;
import com.yangj.model.JsonItem;
import net.sf.json.JSONObject;
//@WebServlet(urlPatterns="/TestServlet")
public class TestServlet extends JsonServlet {

	@Override
	public String dealInput(String input) {
		List<JsonItem> items=new LinkedList<JsonItem>();
		items.add(new JsonItem("视频广告1", 235));
		items.add(new JsonItem("视频广告2", 12));
		items.add(new JsonItem("视频广告3", 678));
		items.add(new JsonItem("视频广告4", 235));
		items.add(new JsonItem("视频广告5", 235));
		items.add(new JsonItem("视频广告6", 235));
		Map<String, List<JsonItem>> map=new HashMap<>();
		map.put("data_pie", items);
		return JSONObject.fromObject(map).toString();
	}
	
	public static void main(String[] args) {
		List<JsonItem> items=new LinkedList<JsonItem>();
		items.add(new JsonItem("视频广告", 235));
		items.add(new JsonItem("视频广告", 235));
		items.add(new JsonItem("视频广告", 235));
		items.add(new JsonItem("视频广告", 235));
		items.add(new JsonItem("视频广告", 235));
		Map<String, List<JsonItem>> map=new HashMap<>();
		map.put("data_pie", items);
		System.out.println(JSONObject.fromObject(map).toString());
	}
}
