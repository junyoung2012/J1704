package com.yangj.web;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public abstract class JsonServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String CHARSET = "UTF-8";
    public JsonServlet() {
        super();
    }
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding(CHARSET);
		response.setContentType("application/json;charset="+CHARSET);
		String input =request.getParameter("content");

		PrintWriter out = response.getWriter();
		out.write(dealInput(input));
		out.flush();
		out.close();
	}

	public abstract String dealInput(String input);

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request,response);
	}
}
