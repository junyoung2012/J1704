package com.yangj.model;
import com.yangj.server.Rebot;

public class MyRebot extends Rebot {
	
	@Override
	public void dealInput(String input) {
		if(input==null || input.isEmpty() ) {
			textMsg("我是铁蛋，你有什么问题");
			return;
		}
		inputMsg(input);
		
		textMsg("收到"+input);
	}

	@Override
	public String getName() {
		return "聊天机器人";
	}

}
