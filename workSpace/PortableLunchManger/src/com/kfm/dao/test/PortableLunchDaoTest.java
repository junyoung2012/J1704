package com.kfm.dao.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.kfm.dao.PortableLunchDao;
import com.kfm.model.PortableLunch;

public class PortableLunchDaoTest {
	private PortableLunchDao portableLunchDao=new PortableLunchDao();
	
	@Before
	public void before() {
		assertTrue(portableLunchDao.add(new PortableLunch("����",2.99)));
		assertTrue(portableLunchDao.add(new PortableLunch("����",2.99)));
		assertTrue(portableLunchDao.add(new PortableLunch("����",2.99)));
	}
	@Test
	public void testAdd() {
		assertTrue(portableLunchDao.add(new PortableLunch("����",2.99)));
		assertTrue(portableLunchDao.add(new PortableLunch("����",2.99)));
		assertTrue(portableLunchDao.add(new PortableLunch("����",2.99)));
		assertFalse(portableLunchDao.add(new PortableLunch("����",2.99)));
	}

	@Test
	public void testDelete() {
		assertTrue(portableLunchDao.delete(1));
		assertTrue(portableLunchDao.delete(1));
		assertTrue(portableLunchDao.delete(1));
		assertFalse(portableLunchDao.delete(1));
	}

	@Test
	public void testUpdate() {
		fail("Not yet implemented");
	}

	@Test
	public void testGet() {
		assertTrue(portableLunchDao.get(1).getId()==1);
	}

	@Test
	public void testGetAll() {
//		testAdd();
		assertTrue(portableLunchDao.getAll().length==3);
	}
}
