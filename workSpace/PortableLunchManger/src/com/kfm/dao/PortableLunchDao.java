package com.kfm.dao;

import com.kfm.model.PortableLunch;

public class PortableLunchDao {
	private static final int MAX_COUNT = 3;
	private PortableLunch[] portableLunchs=new PortableLunch[MAX_COUNT];
	private int pos=0;
	
	public boolean add(PortableLunch portableLunch) {
		if(pos==MAX_COUNT) return false;
		//�õ����
		if(pos==0) {
			portableLunch.setId(1);
		}else {
			int id=portableLunchs[pos-1].getId()+1;
			portableLunch.setId(id);
		}
		portableLunchs[pos++]=portableLunch;
		return true;
	}
	public boolean delete(int index) {//���ݱ��ɾ��
		index--;
		if(index<0 || index>=pos) return false;
		for (int i = index+1; i < pos; i++) {
			portableLunchs[i-1]=portableLunchs[i];
		}
		portableLunchs[pos-1]=null;
		pos--;
		return true;
	}
	public boolean update(int index,PortableLunch portableLunch) {
		index--;
		if(index<0 || index>=pos) return false;
		portableLunchs[index]=portableLunch;
		return true;
	}
	public PortableLunch get(int index) {
		index--;
		if(index<0 || index>=pos) return null;
		return portableLunchs[index];
	}
	public PortableLunch[] getAll() {
		PortableLunch[] results=new PortableLunch[pos];
		for (int i = 0; i < pos; i++) {
			results[i]=portableLunchs[i];
		}
		return results;
	}
}
