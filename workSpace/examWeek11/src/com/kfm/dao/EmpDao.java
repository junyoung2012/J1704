package com.kfm.dao;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import com.kfm.model.Dept;
import com.kfm.model.Emp;
import com.kfm.util.JdbcUtil;

public class EmpDao {
	private static final String SQL = "select E.empno, E.ename, E.job, E.mgr, E.hiredate,"
					+ " E.sal, E.comm, "
					+ " E1.ENAME mgrname,"
					+ " D.Deptno,D.DNAME,D.Loc"
					+ " from emp E"
					+ " left join emp E1 on E.Mgr=E1.Empno"
					+ " left join Dept D on E.Deptno=D.Deptno";
	
	private JdbcUtil JdbcUtil=new JdbcUtil();
	
	public boolean add(Emp emp) {
		String sql="insert into emp"
				+ " (empno, ename, job, mgr, hiredate,"
				+ "  sal, comm, deptno)"
				+ " values"
				+ " (?, ?, ?, ?, ?,"
				+ " ?, ?, ?)";
		return JdbcUtil.executeUpdate(sql,
				emp.getEmpno(),
				emp.getEname(),
				emp.getJob(),
				emp.getMgr().getEmpno(),//此处需要经理编号
				emp.getHiredate(),
				emp.getSal(),
				emp.getComm(),
				emp.getDept().getDeptno()
		)==1;
	}
	public boolean delete(int empno) {
		String sql="delete emp"
				+ " where empno = ?";
		return JdbcUtil.executeUpdate(sql,empno)==1;
	}
	public boolean update(Emp emp) {
		String sql="update emp"
				+ " set ename = ?,"
				+ " job = ?,"
				+ " mgr = ?,"
				+ " hiredate = ?,"
				+ " sal = ?,"
				+ " comm = ?,"
				+ " deptno = ?"
				+ " where empno = ?";
		return JdbcUtil.executeUpdate(sql,
				emp.getEname(),
				emp.getJob(),
				emp.getMgr().getEmpno(),//此处需要经理编号
				emp.getHiredate(),
				emp.getSal(),
				emp.getComm(),
				emp.getDept().getDeptno(),
				emp.getEmpno()
		)==1;
	}
	public Emp get(int deptno) {
		String sql=SQL+ " where E.empno =?";
		ResultSet rs=JdbcUtil.executeQuery(sql, deptno);
		try {
			if(rs.next()) {
				return new Emp(
					rs.getInt("empno"),
					rs.getString("ename"),
					rs.getString("job"),
					new Emp(
						rs.getInt("mgr"),
						rs.getString("mgrname")
					),
					rs.getDate("hiredate"),
					rs.getDouble("sal"),
					rs.getDouble("comm"),
					new Dept(
							rs.getInt("deptno"), 
							rs.getString("dname"),
							rs.getString("loc")
					)
				);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public List<Emp> getAll() {
		String sql=SQL+ " order by E.empno";
		ResultSet rs=JdbcUtil.executeQuery(sql);
		try {
			List<Emp> emps=new LinkedList<>();
			while(rs.next()) {
				emps.add(new Emp(
					rs.getInt("empno"),
					rs.getString("ename"),
					rs.getString("job"),
					new Emp(
							rs.getInt("mgr"),
							rs.getString("mgrname")
							),
					rs.getDate("hiredate"),
					rs.getDouble("sal"),
					rs.getDouble("comm"),
					new Dept(
							rs.getInt("deptno"), 
							rs.getString("dname"),
							rs.getString("loc")
							)
					)
				);
			}
			return emps;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	public List<Emp> getEmpsAfterDate(Date date) {
		String sql=SQL+ " where E.hiredate > ?";
		ResultSet rs=JdbcUtil.executeQuery(sql,date);
		try {
			List<Emp> emps=new LinkedList<>();
			while(rs.next()) {
				emps.add(new Emp(
						rs.getInt("empno"),
						rs.getString("ename"),
						rs.getString("job"),
						new Emp(
								rs.getInt("mgr"),
								rs.getString("mgrname")
								),
						rs.getDate("hiredate"),
						rs.getDouble("sal"),
						rs.getDouble("comm"),
						new Dept(
								rs.getInt("deptno"), 
								rs.getString("dname"),
								rs.getString("loc")
								)
						)
						);
			}
			return emps;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
}
