package com.kfm.dao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import com.kfm.model.Dept;
import com.kfm.util.JdbcUtil;

public class DeptDao {
	private JdbcUtil JdbcUtil=new JdbcUtil();
	
	public boolean add(Dept dept) {
		String sql="insert into dept"
				+ " (deptno, dname, loc)"
				+ " values"
				+ " (?, ?, ?)";
		return JdbcUtil.executeUpdate(sql,
			dept.getDeptno(),
			dept.getDname(),
			dept.getLoc()
		)==1;
	}
	public boolean delete(int deptno) {
		String sql="delete dept"
				+ " where deptno = ?";
		return JdbcUtil.executeUpdate(sql,deptno)==1;
	}
	public boolean update(Dept dept) {
		String sql="update dept"
				+ " set dname = ?,"
				+ " loc = ?"
				+ " where deptno = ?";
		return JdbcUtil.executeUpdate(sql,
				dept.getDname(),
				dept.getLoc(),
				dept.getDeptno()
		)==1;
	}
	public Dept get(int deptno) {
		String sql="select deptno, dname, loc from dept"
				+ "  where deptno =?";
		ResultSet rs=JdbcUtil.executeQuery(sql, deptno);
		try {
			if(rs.next()) {
				return new Dept(
					rs.getInt("deptno"),
					rs.getString("dname"),
					rs.getString("loc")
				);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	public List<Dept> getAll() {
		String sql="select deptno, dname, loc from dept"
				+ " order by deptno";
		ResultSet rs=JdbcUtil.executeQuery(sql);
		try {
			List<Dept> depts=new LinkedList<Dept>();
			while(rs.next()) {
				depts.add(new Dept(
					rs.getInt("deptno"),
					rs.getString("dname"),
					rs.getString("loc")
				));
			}
			return depts;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
}
