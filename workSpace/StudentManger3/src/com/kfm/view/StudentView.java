package com.kfm.view;

import java.util.Scanner;

import com.kfm.dao.StudentDao;
import com.kfm.model.Student;

public class StudentView {
	private static StudentDao studentDao=new StudentDao();
	private Scanner scanner=new Scanner(System.in);
	
	public static void main(String[] args) {
		StudentView studentView=new StudentView();
		studentView.showWelcomPage();
		studentView.showHomePage();
	}

	private void showHomePage() {
		String tip="1.注册学生\r\n" + 
				"2.删除学生\r\n" + 
				"3.修改学生\r\n" + 
				"4.显示学生\r\n" + 
				"0.退出系统\r\n" ;
		int input = getInputInt(tip,"[0-4]","请选择0-4");
//		青蛙测试
//		吃柿子减软的捏
		switch (input) {
			case 1://注册学生
				showAddPage();
				break;
			case 2://删除学生
				showDeletePage();
				break;
			case 3://修改学生
				showUpdatePage();
				break;
			case 4://显示学生
				showListPage();
				break;
			case 0://退出系统
				showExitPage();
				break;
			default:
				showErrorPage();
				break;
		}
	}

	private void showErrorPage() {
		print("您的输入有误");
		showHomePage();
	}

	private void showExitPage() {
		print("感谢使用，欢迎再来");
	}

	private void showListPage() {
		printStudents();
		showHomePage();
	}

	private void printStudents() {
		print("序号\t学号\t姓名");
		Student[] students=studentDao.getAll();
		for (int i = 0; i < students.length; i++) {
			print((i+1)+students[i].toString());
		}
	}

	private void showUpdatePage() {
		printStudents();
		String tip="请您输入新需要修改的学生的序号\n"
				+ "0.返回上级";
		int input=getInputInt(tip);
		if(input==0) {
			showHomePage();
			return;
		}
//		序号变学生对象
		Student student = studentDao.get(input);
		if(student==null) {
			print("您输入的编号有误。");
			showUpdatePage();
			return;
		}
		showUpdatePage2(input,student);
	}
	/**
	 * 
	 * @param index 上个函数得到的序号
	 * @param student （没修改前的学生信息）
	 */
	private void showUpdatePage2(int index,Student student) {
		String tip="您要修改的学生"+student.toString();
		print(tip);
		tip="请输入您需要修改的学生的学号，姓名，用逗号分开\r\n" + 
				"0.返回上级";
		String input=getInputString(tip,"");
		if("0".equals(input)) {
			showUpdatePage();
			return;
		}
//		1001,端木赐
		String[] array=input.split(",");
		Student student2=new Student(array[0],array[1]);//修改后的学生信息
		if(studentDao.update(index, student2)) {
			print("修改成功");
			showHomePage();
		}else {
			print("修改失败");
			showUpdatePage2(index, student);
		}
	}

	private void showDeletePage() {
		//1.打印所有学生
		printStudents();
//		让用户选择
		String tip="请您输入新需要删除的学生的序号\n"
				+ "0.返回上级";
		int input=getInputInt(tip);
		if(input==0) {
			showHomePage();
			return;
		}
		if(studentDao.delete(input)) {
			print("删除成功");
			showHomePage();
		}else {
			print("删除失败");
			showDeletePage();
		}
	}

	private void showAddPage() {
		String tip="请输入您需要注册的学生的学号，姓名，用逗号分开\r\n" + 
				"0.返回上级";
		String input=getInputString(tip,"0|\\d{5}[,，]\\S{1,10}");
		if("0".equals(input)) {
			showHomePage();
			return;
		}
//		1001,端木赐
		String[] array=input.split("[,，]",2);
		Student student=new Student(array[0],array[1]);
		if(studentDao.add(student)) {
			print("保存成功");
			showHomePage();
		}else {
			print("保存失败");
			showAddPage();
		}
	}

	private void showWelcomPage() {
		print("欢迎进入学生管理系统");
	}

	private void print(String tip) {
		System.out.println(tip);
	}
	
	private int getInputInt(String tip) {
		return getInputInt(tip,"\\d{1,9}","您输入的格式有误。");
	}
	
	private int getInputInt(String tip,String regex) {
		return getInputInt(tip,regex,"您输入的格式有误。");
	}
	
	private int getInputInt(String tip,String regex, String errorMessage) {
		String input=getInputString(tip,regex,errorMessage);
		return Integer.parseInt(input);
	}
	
	private String getInputString(String tip,String regex) {
		return getInputString(tip,regex,"您输入的格式有误。");
	}
	
	private String getInputString(String tip,String regex,String errorMessage) {
//		1.发出提示
//		2.获取输入
		print(tip);
		String input=scanner.next();
		if(!input.matches(regex)) {
			print(errorMessage);
			return getInputString(tip,regex,errorMessage);
		}
		return input;
	}
}
