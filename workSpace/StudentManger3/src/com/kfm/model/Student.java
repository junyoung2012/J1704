package com.kfm.model;
/**
 * 容器
 * @author Admin
 *
 */
public class Student {
//	1.私有的成员变量
//	2.构造方法
//	3.geter/seter
//	4.toString

	private int code;
	private String name;
	public Student() {
		super();
	}
	public Student(int code, String name) {
		super();
		this.code = code;
		this.name = name;
	}
	public Student(String code, String name) {
		this.code = Integer.parseInt(code);
		this.name = name;
	}
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public String toString() {
		return "\t" +code + "\t" + name ;
	}
}
