package com.kfm.dao.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.kfm.dao.StudentDao;
import com.kfm.model.Student;

public class StudentDaoTest {
	private StudentDao studentDao=new StudentDao();
//	为了在每个测试方法前都调用本函数
	@Before
	public void beforeTest() {
		assertTrue(studentDao.add(new Student(1001, "冉有1")));
		assertTrue(studentDao.add(new Student(1001, "冉有2")));
		assertTrue(studentDao.add(new Student(1001, "冉有3")));
		assertTrue(studentDao.add(new Student(1001, "冉有4")));
	}
	
	@Test
	public void testAdd() {
//		assert//断言
//		assertTrue(studentDao.add(new Student(1001, "冉有1")));
//		assertTrue(studentDao.add(new Student(1001, "冉有2")));
//		assertTrue(studentDao.add(new Student(1001, "冉有3")));
//		assertTrue(studentDao.add(new Student(1001, "冉有4")));
		assertTrue(studentDao.add(new Student(1001, "冉有5")));
		assertFalse(studentDao.add(new Student(1001, "冉有")));
	}

	@Test
	public void testDelete() {
//		testAdd();
		assertTrue(studentDao.delete(2));
		assertTrue("冉有3".equals(studentDao.getAll()[1].getName()));
	}

	@Test
	public void testUpdate() {
//		testAdd();
		Student student=new Student(1002, "颜回");
		assertTrue(studentDao.update(2, student));
		assertTrue("颜回".equals(studentDao.get(2).getName()));
	}

	@Test
	public void testGet() {
//		testAdd();
		assertTrue("冉有3".equals(studentDao.get(3).getName()));
	}

	@Test
	public void testGetAll() {
//		testAdd();
		assertTrue(studentDao.getAll().length==5);
		assertEquals(5, studentDao.getAll().length); 
	}

}
