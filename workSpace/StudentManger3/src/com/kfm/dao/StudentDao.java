package com.kfm.dao;

import static org.junit.Assert.assertTrue;

import com.kfm.model.Student;

public class StudentDao {
	private static final int MAX_COUNT = 5;
	private Student[] students=new Student[MAX_COUNT];
	
	private int pos;//当前存了几个学生
//	增删改查5方法
	public boolean add(Student student) {
//		1.判断数组是不是满了
		if(pos==MAX_COUNT) return false;
//		2.在当前位置，放置形参传递来的数组
		students[pos++]=student;
//		3.当前位置+1
//		pos++
//		4.返回真出口
		return true;
	}
	public boolean delete(int index) {
		index--;//统一base 0
//		1.判断index是否合法
		if(index<0 || index>=pos) return false;
//		2.循环从index+到pos-1 ，向左以为
		for (int i = index+1; i < pos; i++) {
			students[i-1]=students[i];
		}
//		3.pos-1位置变成null
		students[pos-1]=null;
		pos--;
		return true;
	}
//	修改那条信息，修改的新的信息都有谁
	/**
	 * 
	 * @param index   需要修改的学生序号
	 * @param student 需要修改新的学生信息de箱子（学号，姓名）
	 * @return
	 */
	public boolean update(int index,Student student) {
//		1、base1 转 base 0
		index--;
//		2.检查下标是否合法
		if(index<0 || index>=pos) return false;
//		3.students的学生，可以替换为 形参传递的Student
		students[index]=student;
//		4.真出口
		return true;
	}
	public Student get(int index) {
		index--;//统一base 0
//		1.判断index是否合法
		if(index<0 || index>=pos) return null;
		return students[index];
	}
	public Student[] getAll() {
//		1.返回存储数组的前pos项
		if(pos==0) return null;
		Student[] results=new Student[pos];
//		for (int i = 0; i < results.length; i++) {
//			results[i]=students[i];
//		}
//		使用系统复制数组的方法
		System.arraycopy(students, 0, results, 0, pos);//不常用
//		Arrays
		return results;
	}
}
