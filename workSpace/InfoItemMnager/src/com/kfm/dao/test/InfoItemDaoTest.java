package com.kfm.dao.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.kfm.dao.InfoItemDao;
import com.kfm.model.InfoItem;

public class InfoItemDaoTest {
	private InfoItemDao infoItemDao=new InfoItemDao();
	
	@Test
	public void testAdd() {
//		String[] powers= {"","","","",""};
		String[] powers= {"分布式技术","微服务架构","搜索引擎技术","Spring","Dubbo"};
		String[] wells= {"零食下午茶","节日福利","带薪年假","年终奖","股票期权"};
		InfoItem infoItem=new InfoItem("Soul App", "15-30K·16薪", powers, wells);
		assertTrue(infoItemDao.add(infoItem));
		
		String[] powers2= {"Java"};
		String[] wells2= {
			"餐补","带薪年假","公司团建","五险一金","零食下午茶",
			"年终奖","定期体检","股票期权","员工旅游","节日福利",
			"交通补助","补充医疗保险","免费班车","包吃","通讯补贴"
		};
		infoItem=new InfoItem("饿了么", "20-25K·16薪", powers2, wells2);
		assertTrue(infoItemDao.add(infoItem));
	}

	@Test
	public void testDelete() {
		fail("Not yet implemented");
	}

	@Test
	public void testUpdate() {
		fail("Not yet implemented");
	}

	@Test
	public void testGet() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetAll() {
		testAdd();
		InfoItem[] infoItems = infoItemDao.getAll();
		for (InfoItem infoItem : infoItems) {
			System.out.println(infoItem);
		}
	}

}
