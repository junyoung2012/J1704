package com.kfm.dao;

import java.util.Arrays;

import com.kfm.model.InfoItem;

public class InfoItemDao {
	private static final int MAX_COUNT = 5;
	private InfoItem[] infoItems=new InfoItem[MAX_COUNT];
	private int pos;
	
	public boolean add(InfoItem infoItem) {
		if(pos==MAX_COUNT) return false;
		infoItems[pos++]=infoItem;
		return true;
	}
	public boolean delete(int index) {
		return false;
	}
	public boolean update(InfoItem infoItem) {
		return false;
	}
	
	public InfoItem get(int index) {
		return null;
	}
	public InfoItem[] getAll() {
		InfoItem[] results=new InfoItem[pos];
		System.arraycopy(infoItems, 0, results, 0, pos);
		return results;
	}
}
