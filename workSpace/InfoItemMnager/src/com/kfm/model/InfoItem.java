package com.kfm.model;

import java.util.Arrays;

public class InfoItem {
//	private int id;
	private String compayName;
	private String sal;
	private String[] powers;
	private String[] wells;
	
	public InfoItem() {
	}
	public InfoItem(String compayName, String sal, String[] powers, String[] wells) {
		this.compayName = compayName;
		this.sal = sal;
		this.powers = powers;
		this.wells = wells;
	}
	public String getCompayName() {
		return compayName;
	}
	public void setCompayName(String compayName) {
		this.compayName = compayName;
	}
	public String getSal() {
		return sal;
	}
	public void setSal(String sal) {
		this.sal = sal;
	}
	public String[] getPowers() {
		return powers;
	}
	public void setPowers(String[] powers) {
		this.powers = powers;
	}
	public String[] getWells() {
		return wells;
	}
	public void setWells(String[] wells) {
		this.wells = wells;
	}
	@Override
	public String toString() {
		return compayName + "\t���ʣ�" + sal + "\n" 
				+ Arrays.toString(powers)+ "\n" 
				+ Arrays.toString(wells);
	}
}
