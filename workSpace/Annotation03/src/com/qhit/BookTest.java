package com.qhit;

import static org.junit.Assert.*;

import java.util.List;

import javax.xml.validation.Schema;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.tool.hbm2ddl.SchemaExport;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class BookTest {
	private static SessionFactory sf;
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		//强制抓下RuntimeException
		try {
			Configuration cfg=new Configuration().configure();
			new SchemaExport(cfg).create(true, true);
			sf=cfg.buildSessionFactory();
		} catch (RuntimeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		sf.close();
	}
	
	@Test
	public void testAdd(){
		
		Book bok=new Book();
		bok.setBookname("文化苦雨");
		BookType t=new BookType();
		t.setTypeName("现代文学");
		bok.setBookType(t);
		Session session=sf.getCurrentSession();
		session.beginTransaction();
		session.save(t);
		session.save(bok);
		session.getTransaction().commit();
	}
	
	@Test
	public void testGet(){
		Session session=sf.getCurrentSession();
		session.beginTransaction();
		Book bok= (Book) session.get(Book.class, 1);
		session.getTransaction().commit();
	}
	@Test
	public void testAdd1(){
		
	}

}
